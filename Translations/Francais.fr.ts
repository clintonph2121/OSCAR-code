<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <source>Dialog</source>
        <translation>Boîte de dialogue</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;Au sujet</translation>
    </message>
    <message>
        <source>Release Notes</source>
        <translation>Notes de publication</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Remerciements</translation>
    </message>
    <message>
        <source>GPL License</source>
        <translation>Licence GPL</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>Show data folder</source>
        <translation>Affiche le répertoire des données</translation>
    </message>
    <message>
        <source>Sorry, could not locate About file.</source>
        <translation>Désolé, je ne trouve pas À propos.</translation>
    </message>
    <message>
        <source>Sorry, could not locate Credits file.</source>
        <translation>Désolé, je ne trouve pas Remerciements.</translation>
    </message>
    <message>
        <source>Important:</source>
        <translation>Important :</translation>
    </message>
    <message>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>Pour voir si le texte de la licence est disponible dans votre langue, voyez %1.</translation>
    </message>
    <message>
        <source>Sorry, could not locate Release Notes.</source>
        <translation>Désolé, je ne trouve pas le journal des changements.</translation>
    </message>
    <message>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceeding, because attempting to roll back later may break things.</source>
        <translation>Comme c&apos;est une préversion il est recommandé de &lt;b&gt;sauvegarder vos données à la main&lt;b&gt; avant de continuer. Tenter un retour arrière plus tard pourrait endommager les données.</translation>
    </message>
    <message>
        <source>About OSCAR %1</source>
        <translation>À Propos de OSCAR %1</translation>
    </message>
    <message>
        <source>OSCAR %1</source>
        <translation>OSCAR %1</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <source>Could not find the oximeter file:</source>
        <translation>Fichiers d&apos;oxymétrie introuvables :</translation>
    </message>
    <message>
        <source>Could not open the oximeter file:</source>
        <translation>Impossible d&apos;ouvrir les fichiers d&apos;oxymétrie :</translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <source>Could not get data transmission from oximeter.</source>
        <translation>Impossible d&apos;obtenir des données de l&apos;oxymètre.</translation>
    </message>
    <message>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>Merci de vérifier que vous avez sélectionné &apos;upload&apos; dans les menus de votre oxymètre.</translation>
    </message>
    <message>
        <source>Could not find the oximeter file:</source>
        <translation>Fichiers d&apos;oxymétrie introuvables :</translation>
    </message>
    <message>
        <source>Could not open the oximeter file:</source>
        <translation>Impossible d&apos;ouvrir les fichiers d&apos;oxymétrie :</translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <source>B</source>
        <translation>Gras</translation>
    </message>
    <message>
        <source>u</source>
        <translation>Souligné</translation>
    </message>
    <message>
        <source> i </source>
        <translation> Italique </translation>
    </message>
    <message>
        <source>Big</source>
        <translation>Grand</translation>
    </message>
    <message>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation>Marques</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>Petit</translation>
    </message>
    <message>
        <source>Journal</source>
        <translation>Journal</translation>
    </message>
    <message>
        <source>Total time in apnea</source>
        <translation>Temps total en apnée</translation>
    </message>
    <message>
        <source>Position Sensor Sessions</source>
        <translation>Session des capteurs de position</translation>
    </message>
    <message>
        <source>Add Bookmark</source>
        <translation>Ajouter un favori</translation>
    </message>
    <message>
        <source>Remove Bookmark</source>
        <translation>Enlever le favori</translation>
    </message>
    <message>
        <source>Pick a Colour</source>
        <translation>Choisir une couleur</translation>
    </message>
    <message>
        <source>Complain to your Equipment Provider!</source>
        <translation>Plaignez-vous à votre fournisseur d&apos;équipement !</translation>
    </message>
    <message>
        <source>Session Information</source>
        <translation>Informations de session</translation>
    </message>
    <message>
        <source>Sessions all off!</source>
        <translation>Toutes les sessions sont off !</translation>
    </message>
    <message>
        <source>%1 event</source>
        <translation>Évènement %1</translation>
    </message>
    <message>
        <source>Go to the most recent day with data records</source>
        <translation>Aller au jour le plus récent avec des données</translation>
    </message>
    <message>
        <source>Machine Settings</source>
        <translation>Réglages de l&apos;appareil</translation>
    </message>
    <message>
        <source>B.M.I.</source>
        <translation>I.M.C.</translation>
    </message>
    <message>
        <source>Sleep Stage Sessions</source>
        <translation>Sessions du sommeil</translation>
    </message>
    <message>
        <source>Oximeter Information</source>
        <translation>Informations de l&apos;oxymètre</translation>
    </message>
    <message>
        <source>Events</source>
        <translation>Évènements</translation>
    </message>
    <message>
        <source>Graphs</source>
        <translation>Graphique</translation>
    </message>
    <message>
        <source>CPAP Sessions</source>
        <translation>Sessions PPC</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>Moyen</translation>
    </message>
    <message>
        <source>Starts</source>
        <translation>Début</translation>
    </message>
    <message>
        <source>Weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <source>Zombie</source>
        <translation>Zombie</translation>
    </message>
    <message>
        <source>Bookmarks</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>Session End Times</source>
        <translation>Fin de session</translation>
    </message>
    <message>
        <source>%1 events</source>
        <translation>Évènements %1</translation>
    </message>
    <message>
        <source>events</source>
        <translation>évènements</translation>
    </message>
    <message>
        <source>BRICK :(</source>
        <translation>PLANTAGE :(</translation>
    </message>
    <message>
        <source>Event Breakdown</source>
        <translation>Répartition des évènements</translation>
    </message>
    <message>
        <source>SpO2 Desaturations</source>
        <translation>Désaturation SpO2</translation>
    </message>
    <message>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;Rien ici !&quot;</translation>
    </message>
    <message>
        <source>Awesome</source>
        <translation>Bien</translation>
    </message>
    <message>
        <source>Pulse Change events</source>
        <translation>Changement du pouls</translation>
    </message>
    <message>
        <source>SpO2 Baseline Used</source>
        <translation>Ligne de base du SpO2</translation>
    </message>
    <message>
        <source>Zero hours??</source>
        <translation>Zéro heure ?!?</translation>
    </message>
    <message>
        <source>Go to the previous day</source>
        <translation>Aller au jour précédent</translation>
    </message>
    <message>
        <source>Time over leak redline</source>
        <translation>Durée au-dessus ligne rouge de fuite</translation>
    </message>
    <message>
        <source>Bookmark at %1</source>
        <translation>Favori à %1</translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation>Statistiques</translation>
    </message>
    <message>
        <source>Breakdown</source>
        <translation>Arrêt</translation>
    </message>
    <message>
        <source>Unknown Session</source>
        <translation>Session inconnue</translation>
    </message>
    <message>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>Des sessions existent pour ce jour mais sont désactivées.</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <source>View Size</source>
        <translation>Taille de la vue</translation>
    </message>
    <message>
        <source>Impossibly short session</source>
        <translation>Session trop courte</translation>
    </message>
    <message>
        <source>No %1 events are recorded this day</source>
        <translation>Aucun évènement %1 disponible pour ce jour</translation>
    </message>
    <message>
        <source>BRICK! :(</source>
        <translation>PLANTAGE ! :(</translation>
    </message>
    <message>
        <source>Show or hide the calender</source>
        <translation>Affiche ou cache le calendrier</translation>
    </message>
    <message>
        <source>Time outside of ramp</source>
        <translation>Durée hors rampe</translation>
    </message>
    <message>
        <source>Total ramp time</source>
        <translation>Durée totale de la rampe</translation>
    </message>
    <message>
        <source>Go to the next day</source>
        <translation>Aller au jour suivant</translation>
    </message>
    <message>
        <source>Session Start Times</source>
        <translation>Début de session</translation>
    </message>
    <message>
        <source>Oximetry Sessions</source>
        <translation>Sessions d&apos;oxymétrie</translation>
    </message>
    <message>
        <source>Model %1 - %2</source>
        <translation>Modèle %1 - %2</translation>
    </message>
    <message>
        <source>(Mode/Pressure settings are guessed on this day.)</source>
        <translation>(Les réglages de mode et de pression sont extrapolés pour ce jour)</translation>
    </message>
    <message>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>Jour avec informations limitées, seulement le résumé.</translation>
    </message>
    <message>
        <source>I&apos;m feeling ...</source>
        <translation>Je me sens...</translation>
    </message>
    <message>
        <source>Show/hide available graphs.</source>
        <translation>Affiche ou cache les graphiques.</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <source>Time at Pressure</source>
        <translation>Durée à cette pression</translation>
    </message>
    <message>
        <source>Click to %1 this session.</source>
        <translation>Cliquez pour %1 cette session.</translation>
    </message>
    <message>
        <source>disable</source>
        <translation>désactivé</translation>
    </message>
    <message>
        <source>enable</source>
        <translation>activé</translation>
    </message>
    <message>
        <source>%1 Session #%2</source>
        <translation>%1 Session #%2</translation>
    </message>
    <message>
        <source>%1h %2m %3s</source>
        <translation>%1h %2m %3s</translation>
    </message>
    <message>
        <source>PAP Mode: %1</source>
        <translation>Mode PAP : %1</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Début</translation>
    </message>
    <message>
        <source>End</source>
        <translation>Fin</translation>
    </message>
    <message>
        <source>Unable to display Pie Chart on this system</source>
        <translation>Impossible d&apos;afficher des graphiques sur ce système</translation>
    </message>
    <message>
        <source>Sorry, this machine only provides compliance data.</source>
        <translation>Désolé, votre appareil ne fournit que des données de conformité.</translation>
    </message>
    <message>
        <source>If height is greater than zero in Preferences Dialog, setting weight here will show Body Mass Index (BMI) value</source>
        <translation>Si la taille est supérieure à zéro dans les préférences, rentrer le poids ici affichera l&apos;indice de masse corporelle (I.M.C.)</translation>
    </message>
    <message>
        <source>No data is available for this day.</source>
        <translation>Aucune donnée disponible pour ce jour.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing has changed since previous days.</source>
        <translation>&lt;b&gt;Veuillez noter :&lt;b&gt;les réglages affichés ci-dessous sont basés sur la supposition que rien n&apos;a changé depuis les jours précédents.</translation>
    </message>
    <message>
        <source>This bookmark is in a currently disabled area..</source>
        <translation>Ce favori est actuellement en zone désactivée..</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <source>AHI</source>
        <translation>IAH</translation>
    </message>
    <message>
        <source>End</source>
        <translation>Fin</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <source>End:</source>
        <translation>Fin :</translation>
    </message>
    <message>
        <source>Quick Range:</source>
        <translation>Choix rapide :</translation>
    </message>
    <message>
        <source>Daily</source>
        <translation>Quotidien</translation>
    </message>
    <message>
        <source>Event</source>
        <translation>Évènements</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Début</translation>
    </message>
    <message>
        <source>Last Fortnight</source>
        <translation>Quatre derniers jours</translation>
    </message>
    <message>
        <source>Most Recent Day</source>
        <translation>Jour le plus récent</translation>
    </message>
    <message>
        <source> Count</source>
        <translation> Occurrence</translation>
    </message>
    <message>
        <source>Filename:</source>
        <translation>Nom de fichier :</translation>
    </message>
    <message>
        <source>Select file to export to</source>
        <translation>Choisir le fichier pour export</translation>
    </message>
    <message>
        <source>Resolution:</source>
        <translation>Résolution :</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Dates:</source>
        <translation>Dates :</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>Personnalisé</translation>
    </message>
    <message>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <source>Start:</source>
        <translation>Début :</translation>
    </message>
    <message>
        <source>Data/Duration</source>
        <translation>Date/Durée</translation>
    </message>
    <message>
        <source>CSV Files (*.csv)</source>
        <translation>Fichiers CSV (*.csv)</translation>
    </message>
    <message>
        <source>Last Month</source>
        <translation>Mois dernier</translation>
    </message>
    <message>
        <source>Last 6 Months</source>
        <translation>6 derniers mois</translation>
    </message>
    <message>
        <source>Total Time</source>
        <translation>Temps total</translation>
    </message>
    <message>
        <source>DateTime</source>
        <translation>Date et heure</translation>
    </message>
    <message>
        <source>Session Count</source>
        <translation>Nb sessions</translation>
    </message>
    <message>
        <source>Session</source>
        <translation>Session</translation>
    </message>
    <message>
        <source>Everything</source>
        <translation>Tout</translation>
    </message>
    <message>
        <source>Last Week</source>
        <translation>Semaine dernière</translation>
    </message>
    <message>
        <source>Last Year</source>
        <translation>Dernière année</translation>
    </message>
    <message>
        <source>Export as CSV</source>
        <translation>Export en CSV</translation>
    </message>
    <message>
        <source>Sessions_</source>
        <translation>Sessions_</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <source>Summary_</source>
        <translation>Résumé_</translation>
    </message>
    <message>
        <source>Details_</source>
        <translation>Détails_</translation>
    </message>
    <message>
        <source>Sessions</source>
        <translation>Sessions</translation>
    </message>
    <message>
        <source>%1% </source>
        <translation>%1% </translation>
    </message>
    <message>
        <source>OSCAR_</source>
        <translation>OSCAR_</translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation>Import impossible des données de cet appareil dans ce profil.</translation>
    </message>
    <message>
        <source>Import Error</source>
        <translation>Erreur d&apos;import</translation>
    </message>
    <message>
        <source>The Day records overlap with already existing content.</source>
        <translation>Les enregistrements du jour se chevauchent avec le contenu déjà existant.</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <source>Search Topic:</source>
        <translation>Sujet à rechercher :</translation>
    </message>
    <message>
        <source>Contents</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <source>Hide this message</source>
        <translation>Cacher ce message</translation>
    </message>
    <message>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>Aide non disponible en %1 et sera affichée en %2.</translation>
    </message>
    <message>
        <source>HelpEngine did not set up correctly</source>
        <translation>Problème à l&apos;ouverture du système d&apos;aide</translation>
    </message>
    <message>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>Problème à l&apos;enregistrement du système d&apos;aide.</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>%1 résultat(s) pour &quot;%2&quot;</translation>
    </message>
    <message>
        <source>clear</source>
        <translation>effacer</translation>
    </message>
    <message>
        <source>Help files do not appear to be present.</source>
        <translation>Problème à l&apos;ouverture du fichier d&apos;aide.</translation>
    </message>
    <message>
        <source>No documentation available</source>
        <translation>Aucune documentation disponible</translation>
    </message>
    <message>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation>Patientez, indexation en cours</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <source>Could not find the oximeter file:</source>
        <translation>Fichiers d&apos;oxymétrie introuvables :</translation>
    </message>
    <message>
        <source>Could not open the oximeter file:</source>
        <translation>Impossible d&apos;ouvrir les fichiers d&apos;oxymétrie :</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <source>Please insert your CPAP data card...</source>
        <translation>Insérez la carte de données PPC svp...</translation>
    </message>
    <message>
        <source>Daily Calendar</source>
        <translation>Calendrier quotidien</translation>
    </message>
    <message>
        <source>&amp;Data</source>
        <translation>&amp;Données</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Vues</translation>
    </message>
    <message>
        <source>There was a problem opening Somnopose Data File: </source>
        <translation>Problème à l&apos;ouverture du fichiers Somnopose suivant : </translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>Daily</source>
        <translation>Quotidien</translation>
    </message>
    <message>
        <source>Import &amp;ZEO Data</source>
        <translation>Importer des données &amp;ZEO</translation>
    </message>
    <message>
        <source>MSeries Import complete</source>
        <translation>Import du fichier terminé</translation>
    </message>
    <message>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>Erreur en enregistrant la copie d&apos;écran &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Choose a folder</source>
        <translation>Choisissez un répertoire</translation>
    </message>
    <message>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>Une structure de fichier %1 pour un %2 a été située à :</translation>
    </message>
    <message>
        <source>Importing Data</source>
        <translation>Import en cours</translation>
    </message>
    <message>
        <source>Online Users &amp;Guide</source>
        <translation>&amp;Guide de l&apos;utilisateur en ligne</translation>
    </message>
    <message>
        <source>View &amp;Welcome</source>
        <translation>Vue &amp;Bienvenue</translation>
    </message>
    <message>
        <source>Show Performance Information</source>
        <translation>Voir les informations de performance</translation>
    </message>
    <message>
        <source>There was a problem opening MSeries block File: </source>
        <translation>Problème à l&apos;ouverture du fichier MSeries : </translation>
    </message>
    <message>
        <source>Current Days</source>
        <translation>Jours courants</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;Au sujet</translation>
    </message>
    <message>
        <source>View &amp;Daily</source>
        <translation>Vue &amp;quotidienne</translation>
    </message>
    <message>
        <source>View &amp;Overview</source>
        <translation>Vue &amp;globale</translation>
    </message>
    <message>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>Les Préférences sont bloquées pendant le recalcul.</translation>
    </message>
    <message>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation>Importer des données RemStar &amp;MSeries</translation>
    </message>
    <message>
        <source>Daily Sidebar</source>
        <translation>Barre latérale quotidienne</translation>
    </message>
    <message>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>Par mesure de précaution, le dossier de sauvegarde sera laissé en place.</translation>
    </message>
    <message>
        <source>Change &amp;User</source>
        <translation>&amp;Changer de profil utilisateur</translation>
    </message>
    <message>
        <source>%1&apos;s Journal</source>
        <translation>%1&apos;s Journal</translation>
    </message>
    <message>
        <source>Import Problem</source>
        <translation>Problème d&apos;import</translation>
    </message>
    <message>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Attention ! Cette opération ne peut être annulée !&lt;/b&gt;</translation>
    </message>
    <message>
        <source>View S&amp;tatistics</source>
        <translation>Voir les s&amp;tatistiques</translation>
    </message>
    <message>
        <source>Monthly</source>
        <translation>Mensuel</translation>
    </message>
    <message>
        <source>Change &amp;Language</source>
        <translation>Changer de &amp;langue</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>Comme il n&apos;y a pas de sauvegardes internes, vous devrez restaurer à partir de votre propre sauvegarde.</translation>
    </message>
    <message>
        <source>Would you like to import from your own backups now? (you will have no data visible for this machine until you do)</source>
        <translation>Voulez-vous importer vos propres sauvegardes maintenant ? (vous n&apos;aurez pas de données visibles pour cet appareil jusqu&apos;à ce que vous le fassiez)</translation>
    </message>
    <message>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>Patientez, importation de(s) dossier(s) de sauvegarde ...</translation>
    </message>
    <message>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>Voulez-vous effacer les données de l&apos;oxymètre pour %1</translation>
    </message>
    <message>
        <source>O&amp;ximetry Wizard</source>
        <translation>Assistant d&apos;o&amp;xymétrie</translation>
    </message>
    <message>
        <source>Bookmarks</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>Right &amp;Sidebar</source>
        <translation>&amp;Barre latérale droite</translation>
    </message>
    <message>
        <source>Rebuild CPAP Data</source>
        <translation>Reconstruire les données PPC</translation>
    </message>
    <message>
        <source>XML Files (*.xml)</source>
        <translation>Fichiers XML (*.xml)</translation>
    </message>
    <message>
        <source>Date Range</source>
        <translation>Période</translation>
    </message>
    <message>
        <source>View Statistics</source>
        <translation>Voir les statistiques</translation>
    </message>
    <message>
        <source>CPAP Data Located</source>
        <translation>Données PPC trouvées</translation>
    </message>
    <message>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>Accès à l&apos;importation bloqué pendant les recalculs en cours.</translation>
    </message>
    <message>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation>&amp;Glossaire des termes des troubles du sommeil</translation>
    </message>
    <message>
        <source>Are you really sure you want to do this?</source>
        <translation>Êtes-vous vraiment sûr de vouloir faire cela ?</translation>
    </message>
    <message>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>Sélectionnez d&apos;abord un jour avec des données valides dans la vue journalière.</translation>
    </message>
    <message>
        <source>Records</source>
        <translation>Enregistrements</translation>
    </message>
    <message>
        <source>Use &amp;AntiAliasing</source>
        <translation>Utiliser l&apos;&amp;anti-aliasing</translation>
    </message>
    <message>
        <source>Would you like to import from this location?</source>
        <translation>Voulez-vous importer de cet emplacement ?</translation>
    </message>
    <message>
        <source>Somnopause Data Import complete</source>
        <translation>Import du fichier terminé</translation>
    </message>
    <message>
        <source>Report Mode</source>
        <translation>Type de rapport</translation>
    </message>
    <message>
        <source>&amp;Profiles</source>
        <translation>&amp;Profils utilisateurs</translation>
    </message>
    <message>
        <source>CSV Export Wizard</source>
        <translation>Assistant d&apos;export en CSV</translation>
    </message>
    <message>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation>Nettoyage &amp;automatique de l&apos;oxymétrie</translation>
    </message>
    <message>
        <source>Import is already running in the background.</source>
        <translation>L&apos;import est déjà lancé en tâche de fond.</translation>
    </message>
    <message>
        <source>Specify</source>
        <translation>Parcourir</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation>Statistiques</translation>
    </message>
    <message>
        <source>Up to date</source>
        <translation>À jour</translation>
    </message>
    <message>
        <source>&amp;Statistics</source>
        <translation>&amp;Statistiques</translation>
    </message>
    <message>
        <source>Backup &amp;Journal</source>
        <translation>Sauvegarde du &amp;journal</translation>
    </message>
    <message>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>%1 session(s) importée(s) de 

%2</translation>
    </message>
    <message>
        <source>Purge &amp;Current Selected Day</source>
        <translation>Purger le jour &amp;courant sélectionné</translation>
    </message>
    <message>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>Vu que vous avez fait vos &lt;i&gt; &lt;b&gt;  propres &lt;/b&gt; sauvegardes pour l&apos;ensemble de vos données PPC &lt;/i&gt;, vous pouvez toujours effectuer cette opération, mais vous aurez à restaurer manuellement à partir de vos sauvegardes.</translation>
    </message>
    <message>
        <source>&amp;Advanced</source>
        <translation>&amp;Avancé</translation>
    </message>
    <message>
        <source>Print &amp;Report</source>
        <translation>Imprimer &amp;rapport</translation>
    </message>
    <message>
        <source>Couldn&apos;t find any valid Machine Data at

%1</source>
        <translation>Impossible de trouver des données de l&apos;appareil valides à

%1</translation>
    </message>
    <message>
        <source>Export for Review</source>
        <translation>Export pour relecture</translation>
    </message>
    <message>
        <source>Take &amp;Screenshot</source>
        <translation>&amp;Faire une copie d&apos;écran</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Aperçus</translation>
    </message>
    <message>
        <source>Show Debug Pane</source>
        <translation>Voir le panneau de debug</translation>
    </message>
    <message>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Modifier le profil utilisateur</translation>
    </message>
    <message>
        <source>Import Reminder</source>
        <translation>Rappel d&apos;import</translation>
    </message>
    <message>
        <source>Exp&amp;ort Data</source>
        <translation>Exp&amp;ort des données</translation>
    </message>
    <message>
        <source>Welcome</source>
        <translation>Bienvenue</translation>
    </message>
    <message>
        <source>Import &amp;Somnopose Data</source>
        <translation>Importer des données &amp;Somnopose</translation>
    </message>
    <message>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>Copie d&apos;écran &quot;%1&quot; enregistrée</translation>
    </message>
    <message>
        <source>&amp;Preferences</source>
        <translation>&amp;Préférences</translation>
    </message>
    <message>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>Êtes-vous &lt;b&gt; absolument sûr&lt;/b&gt; de vouloir continuer ?</translation>
    </message>
    <message>
        <source>Import Success</source>
        <translation>Import réussi</translation>
    </message>
    <message>
        <source>Choose where to save journal</source>
        <translation>Choisissez où sauvegarder le journal</translation>
    </message>
    <message>
        <source>&amp;Frequently Asked Questions</source>
        <translation>Questions &amp;fréquentes</translation>
    </message>
    <message>
        <source>Oximetry</source>
        <translation>Oxymétrie</translation>
    </message>
    <message>
        <source>A %1 file structure was located at:</source>
        <translation>Une structure de fichier %1 a été trouvée à :</translation>
    </message>
    <message>
        <source>Change &amp;Data Folder</source>
        <translation>Changer de répertoire des &amp;données</translation>
    </message>
    <message>
        <source>Navigation</source>
        <translation>Navigation</translation>
    </message>
    <message>
        <source>Already up to date with CPAP data at

%1</source>
        <translation>Déjà à jour avec les données ici : 

%1</translation>
    </message>
    <message>
        <source>Profiles</source>
        <translation>Profils</translation>
    </message>
    <message>
        <source>Purge Oximetry Data</source>
        <translation>Purger les données de l&apos;oxymétrie</translation>
    </message>
    <message>
        <source>Help Browser</source>
        <translation>Aide</translation>
    </message>
    <message>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>Chargement du profil &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Please open a profile first.</source>
        <translation>Sélectionnez le profil utilisateur.</translation>
    </message>
    <message>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <source>&amp;About OSCAR</source>
        <translation>&amp;À propos de OSCAR</translation>
    </message>
    <message>
        <source>Report an Issue</source>
        <translation>Rapporter un problème</translation>
    </message>
    <message>
        <source>Updates are not yet implemented</source>
        <translation>Désolé, fonction non encore implémentée</translation>
    </message>
    <message>
        <source>The FAQ is not yet implemented</source>
        <translation>Désolé, fonction non encore implémentée</translation>
    </message>
    <message>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation>Veuillez redémarrer manuellement.</translation>
    </message>
    <message>
        <source>Are you sure you want to rebuild all CPAP data for the following machine:

</source>
        <translation>Êtes-vous sûr de vouloir reconstruire toutes les données de PPC pour l&apos;appareil suivant :

</translation>
    </message>
    <message>
        <source>For some reason, OSCAR does not have any backups for the following machine:</source>
        <translation>Pour une raison quelconque, OSCAR n&apos;a pas de sauvegardes internes pour l&apos;appareil suivante :</translation>
    </message>
    <message>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s machine database for the following machine:&lt;/p&gt;</source>
        <translation>Vous êtes sur le point de &lt;font size=+2&gt;détruire&lt;/font&gt; les données de OSCAR pour l&apos;appareil suivant :&lt;/p&gt;</translation>
    </message>
    <message>
        <source>A file permission error casued the purge process to fail; you will have to delete the following folder manually:</source>
        <translation>Une erreur d&apos;autorisation de fichier a planté le processus de purge, vous devrez supprimer manuellement le dossier suivant :</translation>
    </message>
    <message>
        <source>No help is available.</source>
        <translation>Aucune aide disponible.</translation>
    </message>
    <message>
        <source>Export review is not yet implemented</source>
        <translation>Désolé, fonction non encore implémentée</translation>
    </message>
    <message>
        <source>Reporting issues is not yet implemented</source>
        <translation>Désolé, fonction non encore implémentée</translation>
    </message>
    <message>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation>SVP, notez que cela pourrait entraîner la perte de données graphiques quand les sauvegardes internes de OSCAR ont été désactivées.</translation>
    </message>
    <message>
        <source>No profile has been selected for Import.</source>
        <translation>Aucun profil sélectionné pour l&apos;import.</translation>
    </message>
    <message>
        <source>Show Daily view</source>
        <translation>Voir la vue quotidienne</translation>
    </message>
    <message>
        <source>Show Overview view</source>
        <translation>Voir la vue globale</translation>
    </message>
    <message>
        <source>&amp;Maximize Toggle</source>
        <translation>&amp;Plein écran</translation>
    </message>
    <message>
        <source>Maximize window</source>
        <translation>Maximiser la fenêtre</translation>
    </message>
    <message>
        <source>Reset Graph &amp;Heights</source>
        <translation>Réinitialiser la &amp;hauteur des graphiques</translation>
    </message>
    <message>
        <source>Reset sizes of graphs</source>
        <translation>Réinitialiser la taille des graphiques</translation>
    </message>
    <message>
        <source>Show Right Sidebar</source>
        <translation>voir la barre latérale droite</translation>
    </message>
    <message>
        <source>Show Statistics view</source>
        <translation>Voir les statistiques</translation>
    </message>
    <message>
        <source>Show &amp;Line Cursor</source>
        <translation>Voir le curseur &amp;ligne</translation>
    </message>
    <message>
        <source>Show Daily Left Sidebar</source>
        <translation>Voir la barre latérale quotidienne</translation>
    </message>
    <message>
        <source>Show Daily Calendar</source>
        <translation>Voir le calendrier quotidien</translation>
    </message>
    <message>
        <source>System Information</source>
        <translation>Informations système</translation>
    </message>
    <message>
        <source>Show &amp;Pie Chart</source>
        <translation>Voir le gra&amp;phique sectoriel</translation>
    </message>
    <message>
        <source>Show Pie Chart on Daily page</source>
        <translation>Voir le graphique sectoriel sur la page quotidienne</translation>
    </message>
    <message>
        <source>&amp;Reset Graphs</source>
        <translation>&amp;Réinitialiser les graphiques</translation>
    </message>
    <message>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation>Le guide utilisateur sera ouvert dans le navigateur par défaut</translation>
    </message>
    <message>
        <source>The Glossary will open in your default browser</source>
        <translation>Le glossaire sera ouvert dans le navigateur par défaut</translation>
    </message>
    <message>
        <source>OSCAR Information</source>
        <translation>Informations sur OSCAR</translation>
    </message>
    <message>
        <source>Standard graph order, good for CPAP, APAP, Bi-Level</source>
        <translation>Ordre standard des graphiques, adapté pour CPAP, APAP, Bi-Level</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <source>Advanced graph order, good for ASV, AVAPS</source>
        <translation>Ordre des graph avancé, bon pour ASV, AVAPS</translation>
    </message>
    <message>
        <source>Troubleshooting</source>
        <translation>Dépannage</translation>
    </message>
    <message>
        <source>Purge ALL Machine Data</source>
        <translation>Purger TOUTES les données de l&apos;appareil</translation>
    </message>
    <message>
        <source>&amp;Import CPAP Card Data</source>
        <translation>&amp;Importer les données PPC depuis la carte SD</translation>
    </message>
    <message>
        <source>Import &amp;Dreem Data</source>
        <translation>Importer les données depuis &amp;Dreem</translation>
    </message>
    <message>
        <source>Import &amp;Viatom Data</source>
        <translation>Importer les données depuis &amp;Viatom</translation>
    </message>
    <message>
        <source>Create zip of CPAP data card</source>
        <translation>Créer un fichier zip des données de la carte SD</translation>
    </message>
    <message>
        <source>Create zip of all OSCAR data</source>
        <translation>Créer un fichier zip de toutes les données de OSCAR</translation>
    </message>
    <message>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <source>%1 (Profile: %2)</source>
        <translation>%1 (Profil : %2)</translation>
    </message>
    <message>
        <source>Please remember to select the root folder or drive letter of your data card, and not a folder inside it.</source>
        <translation>Sélectionner le dossier racine ou la lettre de lecteur de votre carte de données, et non pas un dossier à l’intérieur.</translation>
    </message>
    <message>
        <source>Choose where to save screenshot</source>
        <translation>Choisir l&apos;endroit où enregistrer la capture d’écran</translation>
    </message>
    <message>
        <source>Image files (*.png)</source>
        <translation>Fichiers image (*.png)</translation>
    </message>
    <message>
        <source>OSCAR does not have any backups for this machine!</source>
        <translation>OSCAR n’a pas de sauvegarde pour cet appareil !</translation>
    </message>
    <message>
        <source>Unless you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your data for this machine&lt;/i&gt;, &lt;font size=+2&gt;you will lose this machine&apos;s data &lt;b&gt;permanently&lt;/b&gt;!&lt;/font&gt;</source>
        <translation>Si vous n&apos;avez pas effectué &lt;i&gt;vos &lt;b&gt;propres&lt;/b&gt; sauvegardes de TOUTES les données pour cet appareil&lt;/i&gt;, &lt;font size=+2&gt;vous allez les perdre de façon &lt;b&gt;définitive&lt;/b&gt;!&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Imported %1 ZEO session(s) from

%2</source>
        <translation>%1 session (s) ZEO importée (s) de

%2</translation>
    </message>
    <message>
        <source>Already up to date with ZEO data at

%1</source>
        <translation>Les données ZEO sont déjà à jour sur

%1</translation>
    </message>
    <message>
        <source>Couldn&apos;t find any valid ZEO CSV data at

%1</source>
        <translation>Impossible de trouver des données ZEO CSV valides sur

%1</translation>
    </message>
    <message>
        <source>Imported %1 Dreem session(s) from

%2</source>
        <translation>%1 session (s) Dreem importée (s) depuis

%2</translation>
    </message>
    <message>
        <source>Already up to date with Dreem data at

%1</source>
        <translation>Les données Dreem dont déjà à jour sur

%1</translation>
    </message>
    <message>
        <source>Couldn&apos;t find any valid Dreem CSV data at

%1</source>
        <translation>Impossible de trouver des données Dreem CSV valides sur

%1</translation>
    </message>
    <message>
        <source>Imported %1 oximetry session(s) from

%2</source>
        <translation>%1 session (s) d&apos;oxymétrie importées de

%2</translation>
    </message>
    <message>
        <source>Already up to date with oximetry data at

%1</source>
        <translation>Les données d&apos;oxymétrie sont déjà à jour sur

%1</translation>
    </message>
    <message>
        <source>Couldn&apos;t find any valid data at

%1</source>
        <translation>Pas de données valides à

%1</translation>
    </message>
    <message>
        <source>Would you like to zip this card?</source>
        <translation>Souhaitez-vous compresser cette carte ?</translation>
    </message>
    <message>
        <source>Choose where to save zip</source>
        <translation>Choisissez où enregistrer le fichier compressé</translation>
    </message>
    <message>
        <source>ZIP files (*.zip)</source>
        <translation>Fichiers ZIP (*.zip)</translation>
    </message>
    <message>
        <source>Creating zip...</source>
        <translation>Création du fichier ZIP...</translation>
    </message>
    <message>
        <source>Calculating size...</source>
        <translation>Calcul de la taille...</translation>
    </message>
    <message>
        <source>Show Personal Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <source>Scaling Mode</source>
        <translation>Type d&apos;échelle</translation>
    </message>
    <message>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>Valeur Y maximum... doit être supérieure à la valeur minimum.</translation>
    </message>
    <message>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>Bouton de remise à zéro aux valeurs automatiques</translation>
    </message>
    <message>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>Échelle des Y, &apos;Automatique&apos; pour échelle automatique, &apos;Par défaut&apos; pour les réglages constructeur et &apos;Personnalisé&apos; pour choisir par vous-même.</translation>
    </message>
    <message>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>Valeur mini en Y... Les valeurs négatives sont possibles.</translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation>Par défaut</translation>
    </message>
    <message>
        <source>Auto-Fit</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <source>Override</source>
        <translation>Personnalisé</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <source>CPAP</source>
        <translation>PPC</translation>
    </message>
    <message>
        <source>Male</source>
        <translation>Homme</translation>
    </message>
    <message>
        <source>&amp;Back</source>
        <translation>&amp;Précédent</translation>
    </message>
    <message>
        <source>&amp;Next</source>
        <translation>&amp;Suivant</translation>
    </message>
    <message>
        <source>TimeZone</source>
        <translation>Fuseau horaire</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Courriel</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>Les rapports générés ne le sont QUE pour usage PERSONNEL et ne peuvent être utilisés pour aucun diagnostic médical.</translation>
    </message>
    <message>
        <source>&amp;Close this window</source>
        <translation>&amp;Fermer la fenêtre</translation>
    </message>
    <message>
        <source>Edit User Profile</source>
        <translation>Modifier le profil utilisateur</translation>
    </message>
    <message>
        <source>Please provide a username for this profile</source>
        <translation>Merci de donner un nom à ce profil</translation>
    </message>
    <message>
        <source>CPAP Treatment Information</source>
        <translation>Informations sur le traitement PPC</translation>
    </message>
    <message>
        <source>Password Protect Profile</source>
        <translation>Profil protégé par mot de passe</translation>
    </message>
    <message>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>La précision des données n&apos;est pas et ne peut pas être garantie.</translation>
    </message>
    <message>
        <source>D.O.B.</source>
        <translation>Né le.</translation>
    </message>
    <message>
        <source>Female</source>
        <translation>Femme</translation>
    </message>
    <message>
        <source>Gender</source>
        <translation>Sexe</translation>
    </message>
    <message>
        <source>Height</source>
        <translation>Taille</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Coordonnées</translation>
    </message>
    <message>
        <source>Locale Settings</source>
        <translation>Réglages locaux</translation>
    </message>
    <message>
        <source>CPAP Mode</source>
        <translation>Mode PPC</translation>
    </message>
    <message>
        <source>Select Country</source>
        <translation>Choisissez le pays</translation>
    </message>
    <message>
        <source>PLEASE READ CAREFULLY</source>
        <translation>MERCI DE LIRE ATTENTIVEMENT</translation>
    </message>
    <message>
        <source>Untreated AHI</source>
        <translation>IAH non traité</translation>
    </message>
    <message>
        <source>Keep the kids out.. Nothing more.. This isn&apos;t meant to be uber security.</source>
        <translation>Éloigner les enfant. Rien de plus. Ce n&apos;est pas non plus super sensible.</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <source>I agree to all the conditions above.</source>
        <translation>Je suis d&apos;accord avec toutes ces conditions.</translation>
    </message>
    <message>
        <source>DST Zone</source>
        <translation>Zone heure d&apos;été/hiver</translation>
    </message>
    <message>
        <source>about:blank</source>
        <translation>au sujet : blanc</translation>
    </message>
    <message>
        <source>RX Pressure</source>
        <translation>Pression RX</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>Vous utilisez ce logiciel à vos risques et périls.</translation>
    </message>
    <message>
        <source>Passwords don&apos;t match</source>
        <translation>Non correspondance des mots de passe</translation>
    </message>
    <message>
        <source>First Name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Country</source>
        <translation>Pays</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation>&amp;Fin</translation>
    </message>
    <message>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <source>Profile Changes</source>
        <translation>Modification du profil</translation>
    </message>
    <message>
        <source>Personal Information (for reports)</source>
        <translation>Informations personnelles (pour les rapports)</translation>
    </message>
    <message>
        <source>User Name</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP machines and related equipment.</source>
        <translation>Ce logiciel est conçu pour vous aider à visualiser les données de votre appareil respiratoire à Pression Positive Continue (PPC) et équipements en rapport.</translation>
    </message>
    <message>
        <source>User Information</source>
        <translation>Informations utilisateur</translation>
    </message>
    <message>
        <source>...twice...</source>
        <translation>...et de deux...</translation>
    </message>
    <message>
        <source>Doctors Name</source>
        <translation>Nom du médecin</translation>
    </message>
    <message>
        <source>Doctors / Clinic Information</source>
        <translation>Docteur / Informations sur la clinique</translation>
    </message>
    <message>
        <source>Practice Name</source>
        <translation>Spécialité</translation>
    </message>
    <message>
        <source>Date Diagnosed</source>
        <translation>Date de diagnostic</translation>
    </message>
    <message>
        <source>Accept and save this information?</source>
        <translation>Accepter et sauvegarder ?</translation>
    </message>
    <message>
        <source>Patient ID</source>
        <translation>Identifiant du patient</translation>
    </message>
    <message>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>Votre âge approximatif n&apos;est pas obligatoire mais améliorera la précision des calculs.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;L&apos;indication de votre sexe n&apos;est pas obligatoire mais améliorera la précision des calculs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation>OSCAR est sous licence &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Licence Publique V3&lt;/a&gt; Il est fourni sans aucune garantie et sans garantie de correspondre à un usage quelconque.</translation>
    </message>
    <message>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation>OSCAR est un outil de visualisation de données et n&apos;est pas un substitut à la compétence de votre médecin.</translation>
    </message>
    <message>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation>L&apos;auteur n&apos;est &lt;u&gt; en aucun cas &lt;/u&gt; responsable de l&apos; utilisation faite de ce logiciel.</translation>
    </message>
    <message>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019 Nightowl Software</source>
        <translation>OSCAR copyright &amp;copy; 2011-2014 par Mark Watkins et partiellement &amp;copy; 2019 Nightowl Software</translation>
    </message>
    <message>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Bienvenue dans O.S.C.A.R. (Open Source CPAP Analysis Reporter)</translation>
    </message>
    <message>
        <source>Metric</source>
        <translation>Métrique</translation>
    </message>
    <message>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>End:</source>
        <translation>Fin :</translation>
    </message>
    <message>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <source>Usage</source>
        <translation>Utilisation</translation>
    </message>
    <message>
        <source>Respiratory
Disturbance
Index</source>
        <translation>Index des 
troubles 
respiratoires</translation>
    </message>
    <message>
        <source>Show all graphs</source>
        <translation>Afficher les graphiques</translation>
    </message>
    <message>
        <source>Reset view to selected date range</source>
        <translation>Réinitialiser à la durée choisie</translation>
    </message>
    <message>
        <source>Total Time in Apnea</source>
        <translation>Temps total en apnée</translation>
    </message>
    <message>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>Dérouler pour voir la liste des graphiques à activer.</translation>
    </message>
    <message>
        <source>Usage
(hours)</source>
        <translation>Utilisation (heures)</translation>
    </message>
    <message>
        <source>Last Three Months</source>
        <translation>3 derniers mois</translation>
    </message>
    <message>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>Temps total en apnée 
(minutes)</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>Personnalisé</translation>
    </message>
    <message>
        <source>How you felt
(0-10)</source>
        <translation>Sensation
(0-10)</translation>
    </message>
    <message>
        <source>Graphs</source>
        <translation>Graphiques</translation>
    </message>
    <message>
        <source>Range:</source>
        <translation>Durée :</translation>
    </message>
    <message>
        <source>Start:</source>
        <translation>Début :</translation>
    </message>
    <message>
        <source>Last Month</source>
        <translation>Mois dernier</translation>
    </message>
    <message>
        <source>Apnea
Hypopnea
Index</source>
        <translation>Index des apnées et hypopnées</translation>
    </message>
    <message>
        <source>Last 6 Months</source>
        <translation>6 derniers mois</translation>
    </message>
    <message>
        <source>Body
Mass
Index</source>
        <translation>Indice
de masse
corporelle</translation>
    </message>
    <message>
        <source>Session Times</source>
        <translation>Durée session</translation>
    </message>
    <message>
        <source>Last Two Weeks</source>
        <translation>2 dernières semaines</translation>
    </message>
    <message>
        <source>Everything</source>
        <translation>Tout</translation>
    </message>
    <message>
        <source>Last Week</source>
        <translation>Semaine dernière</translation>
    </message>
    <message>
        <source>Last Year</source>
        <translation>Année dernière</translation>
    </message>
    <message>
        <source>Toggle Graph Visibility</source>
        <translation>Activer les graphiques</translation>
    </message>
    <message>
        <source>Hide all graphs</source>
        <translation>Cacher les graphiques</translation>
    </message>
    <message>
        <source>Last Two Months</source>
        <translation>2 derniers mois</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cocher pour mettre à jour l&apos;identifiant du matériel au prochain import, utile pour ceux ayant plusieurs oxymètres.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Press Start to commence recording</source>
        <translation>Appuyez sur Démarrer pour commencer à enregistrer</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>No CPAP data available on %1</source>
        <translation>Pas de données PPC disponibles sur %1</translation>
    </message>
    <message>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>OSCAR lit aussi les fichiers .dat du ChoiceMMed MD300W1.</translation>
    </message>
    <message>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>Merci d&apos;attendre la fin du transfert. Ne pas le débrancher pendant ce temps.</translation>
    </message>
    <message>
        <source>Finger not detected</source>
        <translation>Doigt non détecté</translation>
    </message>
    <message>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>Vous devez demander à l&apos;oxymètre de transmettre des données à l&apos;ordinateur.</translation>
    </message>
    <message>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>Renommer cet oxymètre de &apos;%1&apos; en &apos;%2&apos;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Make sure your correct oximeter type is selected otherwise import will fail.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Veuillez noter : &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Assurez-vous que le type d&apos;oxymètre sélectionné est correct sinon l&apos;importation échouera.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cette option vous permet d&apos;importer des données créées par le logiciel de votre oxymètre, comme SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Oximeter import completed..</source>
        <translation>Import terminé..</translation>
    </message>
    <message>
        <source>&amp;Retry</source>
        <translation>&amp;Recommencer</translation>
    </message>
    <message>
        <source>&amp;Start</source>
        <translation>&amp;Débuter</translation>
    </message>
    <message>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>Information : d&apos;autres entreprises comme Pulox rebadgent les Contec CMS50 sous les noms suivants : Pulox PO-200, PO-300, PO-400 qui devraient donc fonctionner.</translation>
    </message>
    <message>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%1 session(s) sur %2, démarrage à %3</translation>
    </message>
    <message>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>Réglage manuel du temps en l&apos;absence d&apos;horloge interne sur l&apos;oxymètre.</translation>
    </message>
    <message>
        <source>You can manually adjust the time here if required:</source>
        <translation>Vous pouvez ajuster l&apos;heure manuellement :</translation>
    </message>
    <message>
        <source>Couldn&apos;t access oximeter</source>
        <translation>Impossible d&apos;accéder à l&apos;oxymètre</translation>
    </message>
    <message>
        <source>Please connect your oximeter device</source>
        <translation>Connectez votre matériel d&apos;oxymétrie SVP</translation>
    </message>
    <message>
        <source>Important Notes:</source>
        <translation>Informations importantes :</translation>
    </message>
    <message>
        <source>Starting up...</source>
        <translation>Démarrage...</translation>
    </message>
    <message>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>Le Contec CMS50D+ n&apos;a pas d&apos;horloge interne et donc, n&apos;enregistre pas l&apos;horaire de départ. Si vous n&apos;avez pas de session de PPC à lier avec, vous allez devoir entrer l&apos;horaire de départ manuellement après import.</translation>
    </message>
    <message>
        <source>HH:mm:ssap</source>
        <translation>HH:mm:ss AP</translation>
    </message>
    <message>
        <source>Import directly from a recording on a device</source>
        <translation>Import direct d&apos;un matériel d&apos;enregistrement</translation>
    </message>
    <message>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation>Le nom de l&apos;oxymètre est différent. Si vous n&apos;en avez qu&apos;un et que vous l&apos;utilisez pour différents profils, utilisez le même nom.</translation>
    </message>
    <message>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>Même avec les appareils avec horloge interne, il est recommandé de prendre l&apos;habitude de démarrer l&apos;enregistrement de l&apos;oxymétrie et de la PPC en même temps, car les horloges internes des appareils à PPC décalent et tous ne peuvent être remis à zéro facilement.</translation>
    </message>
    <message>
        <source>Dialog</source>
        <translation>Boîte de dialogue</translation>
    </message>
    <message>
        <source>&amp;Information Page</source>
        <translation>Page d&apos;&amp;informations</translation>
    </message>
    <message>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>Utiliser l&apos;heure de l&apos;horloge interne de l&apos;oxymètre.</translation>
    </message>
    <message>
        <source>Waiting for %1 to start</source>
        <translation>Attente de %1 pour démarrer</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Mémo pour les utilisateurs de PPC : &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt; avez-vous importé votre session PPC en premier ?&lt;br/&gt;&lt;/span&gt;Si vous l&apos;oubliez, vos données ne seront pas correctement synchronisées.&lt;br/&gt;Pour assurer une bonne synchronisation des deux appareils, démarrez les toujours en même temps&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <source>Select a valid oximetry data file</source>
        <translation>Sélectionnez un fichier de données valides</translation>
    </message>
    <message>
        <source>%1 device is uploading data...</source>
        <translation>%1 transmet des données...</translation>
    </message>
    <message>
        <source>&amp;End Recording</source>
        <translation>T&amp;erminer l&apos;enregistrement</translation>
    </message>
    <message>
        <source>&amp;Choose Session</source>
        <translation>&amp;Choisir la session</translation>
    </message>
    <message>
        <source>Nothing to import</source>
        <translation>Rien à importer</translation>
    </message>
    <message>
        <source>Select upload option on %1</source>
        <translation>Sélectionner l&apos;option Transmettre sur %1</translation>
    </message>
    <message>
        <source>Select Oximeter Type:</source>
        <translation>Choisir le type d&apos;oxymètre :</translation>
    </message>
    <message>
        <source>Waiting for the device to start the upload process...</source>
        <translation>En attente du matériel pour démarrer le téléchargement...</translation>
    </message>
    <message>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>Aucun appareil détecté.</translation>
    </message>
    <message>
        <source>Oximeter Import Wizard</source>
        <translation>Assistant d&apos;import pour oxymètre</translation>
    </message>
    <message>
        <source>&amp;Save and Finish</source>
        <translation>&amp;Sauvegarder et terminer</translation>
    </message>
    <message>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>Les oxymètres sont des appareils médicaux pour mesurer la saturation en oxygène du sang. Pendant de longues apnées ou lors de respirations anormales, la saturation du sang en oxygène peut baisser significativement et indiquer un besoin de consultation médicale.</translation>
    </message>
    <message>
        <source>Start Time</source>
        <translation>Heure de début</translation>
    </message>
    <message>
        <source>Pulse Rate</source>
        <translation>Fréquence des pulsations</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note : synchroniser avec l&apos;horaire de démarrage de la session de PPC sera toujours plus précis.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Set device date/time</source>
        <translation>Régler la date et l&apos;heure du matériel</translation>
    </message>
    <message>
        <source>Import Completed. When did the recording start?</source>
        <translation>Import terminé. Quand l&apos;enregistrement a-t-il commencé ?</translation>
    </message>
    <message>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>Import d&apos;un fichier de données externes, comme ceux de SpO2Review</translation>
    </message>
    <message>
        <source>Day recording (normally would of) started</source>
        <translation>Jour de début d&apos;enregistrement</translation>
    </message>
    <message>
        <source>Multiple Sessions Detected</source>
        <translation>Plusieurs sessions détectées</translation>
    </message>
    <message>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP machine.</source>
        <translation>J&apos;ai démarré l&apos;oxymètre en même temps que la session de l&apos;appareil à Pression Positive Continue.</translation>
    </message>
    <message>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>Enregistrement rattaché à l&apos;ordinateur toute la nuit (fournit un pléthysmogramme)</translation>
    </message>
    <message>
        <source>Erase session after successful upload</source>
        <translation>Effacer la session après un import réussi</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>Set device identifier</source>
        <translation>Ajouter l&apos;identifiant de l&apos;appareil</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <source>Oximeter not detected</source>
        <translation>Oxymètre non détecté</translation>
    </message>
    <message>
        <source>Please remember:</source>
        <translation>Veuillez retenir que :</translation>
    </message>
    <message>
        <source>Where would you like to import from?</source>
        <translation>D&apos;où voulez-vous importer ?</translation>
    </message>
    <message>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>Erreur de type d&apos;oxymètre dans les préférences.</translation>
    </message>
    <message>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>Utilisation de l&apos;horaire d&apos;enregistrement de l&apos;ordinateur pour cette session d&apos;oxymétrie temps réel.</translation>
    </message>
    <message>
        <source>Scanning for compatible oximeters</source>
        <translation>Recherche d&apos;un oxymètre compatible</translation>
    </message>
    <message>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation>Connectez votre oxymètre et sélectionnez envoi de données dans les menus...</translation>
    </message>
    <message>
        <source>Choose CPAP session to sync to:</source>
        <translation>Choisir la session PPC avec laquelle synchroniser :</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>Bienvenue dans l&apos;assistant d&apos;import d&apos;oxymétrie</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Si cela ne vous gêne pas d&apos;être relié à un ordinateur toute la nuit, cette option fournit un graphique pléthysmogramme, qui indique le rythme cardiaque, en complément des informations d&apos;oxymétrie normales.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&quot;%1&quot;, session %2</source>
        <translation>&quot;%1&quot;, session %2</translation>
    </message>
    <message>
        <source>Show Live Graphs</source>
        <translation>Afficher les graphiques en temps réel</translation>
    </message>
    <message>
        <source>Live Import Stopped</source>
        <translation>Import temps réel stoppé</translation>
    </message>
    <message>
        <source>Oximeter Starting time</source>
        <translation>Heure de démarrage de l&apos;oxymètre</translation>
    </message>
    <message>
        <source>Skip this page next time.</source>
        <translation>Ne plus voir cette page.</translation>
    </message>
    <message>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>Si vous pouvez lire ceci après quelques secondes, annulez et recommencez</translation>
    </message>
    <message>
        <source>CMS50D+/E/F, Pulox PO-200/300</source>
        <translation>CMS50D+/E/F, Pulox PO-200/300</translation>
    </message>
    <message>
        <source>&amp;Sync and Save</source>
        <translation>&amp;Synchroniser et sauvegarder</translation>
    </message>
    <message>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>Votre oxymètre n&apos;a pas de session valide.</translation>
    </message>
    <message>
        <source>Something went wrong getting session data</source>
        <translation>Erreur à la récupération des données de cette session</translation>
    </message>
    <message>
        <source>Connecting to %1 Oximeter</source>
        <translation>Connexion à l&apos;oxymètre %1</translation>
    </message>
    <message>
        <source>Recording...</source>
        <translation>Enregistrement en cours...</translation>
    </message>
    <message>
        <source>ChoiceMMed MD300W1</source>
        <translation>ChoiceMMed MD300W1</translation>
    </message>
    <message>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>Fichiers d&apos;oxymétrie (*.spo *.spor *.spo2 *.SpO2 *.dat)</translation>
    </message>
    <message>
        <source>No Oximetry module could parse the given file:</source>
        <translation>Impossible de traiter le fichier :</translation>
    </message>
    <message>
        <source>Live Oximetry Mode</source>
        <translation>Mode oxymétrie temps réel</translation>
    </message>
    <message>
        <source>Live Oximetry Stopped</source>
        <translation>Oxymétrie temps réel stoppé</translation>
    </message>
    <message>
        <source>Live Oximetry import has been stopped</source>
        <translation>Import temps réel de l&apos;oxymétrie stoppé</translation>
    </message>
    <message>
        <source>Oximeter Session %1</source>
        <translation>Session d&apos;oxymétrie %1</translation>
    </message>
    <message>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>si vous voulez synchroniser les données de PPC et d&apos;oxymétrie, surtout importez celles de PPC avant de procéder !</translation>
    </message>
    <message>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation>Utilisateur de CMS50E/F, veuillez attendre que OSCAR vous le demande avant de lancer l&apos;import.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Si coché, OSCAR synchronisera automatiquement l&apos;horloge interne de votre CMS50 avec l&apos;ordinateur .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation>Merci de sélectionner celle que vous voulez importer</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR a besoin d&apos;un horaire de départ pour savoir où sauver sa session d&apos;oxymétrie.&lt;/p&gt;&lt;p&gt;Choisissez une des  options suivantes :&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation>OSCAR vous permet de suivre la saturation en oxygène pendant l&apos;utilisation d&apos;une appareil à Pression Positive Continue, ce qui donne une visibilité sur l&apos;efficacité du traitement. Cela fonctionne aussi avec votre oxymètre seul et vous permet de stocker, suivre et revoir les données.</translation>
    </message>
    <message>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation>Afin que OSCAR puisse utiliser votre oxymètre, veuillez auparavant installer les logiciels gestionnaires de votre matériel (ex USB vers série), pour plus d&apos;informations à ce sujet, %1 cliquez ici %2.</translation>
    </message>
    <message>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation>OSCAR est compatible avec les oxymètres Contec CMS50D+,CMS50E/F et CMS50I.&lt;br/&gt;(Note : l&apos;import en bluetooth n&apos;est &lt;span style=&quot; font-wright:600;&quot;&gt;probablement pas &lt;/span&gt; encore possible)</translation>
    </message>
    <message>
        <source>CMS50Fv3.7+/H/I, CMS50D+v4.6, Pulox PO-400/500</source>
        <translation>CMS50Fv3.7+/H/I, CMS50D+v4.6, Pulox PO-400/500</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Entrez le nom de 7 caractères de votre oxymètre.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  because if something goes wrong before OSCAR saves your session, you can&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cette option effacera la session importée de votre oxymètre en fin d&apos;import. &lt;/p&gt;&lt;p&gt;À utiliser avec précaution, parce que si quelque chose va mal avant que OSCAR sauvegarde votre session, vous ne pourrez pas la récupérer&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, old Contec oximeters will require you to use the device&apos;s menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cette option vous permet d&apos;importer (via câble) les enregistrements internes de votre oxymètre.&lt;/p&gt;&lt;p&gt;Après avoir choisi cette option,les anciens oxymètres Contec vous demanderons d&apos;utiliser le menu de l&apos;appareil pour lancer le téléchargement.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Please connect your oximeter device, turn it on, and enter the menu</source>
        <translation>Connectez votre oxymètre, allumez-le et entrez dans le menu</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <source>SpO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <source>Pulse</source>
        <translation>Pulsations</translation>
    </message>
    <message>
        <source>&amp;Open .spo/R File</source>
        <translation>&amp;Ouvrir Fic.spo/R</translation>
    </message>
    <message>
        <source>R&amp;eset</source>
        <translation>R&amp;emettre à zéro</translation>
    </message>
    <message>
        <source>Serial &amp;Import</source>
        <translation>&amp;Importer Série</translation>
    </message>
    <message>
        <source>Serial Port</source>
        <translation>Port série</translation>
    </message>
    <message>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation>d/MM/yy h:mm:ss AP</translation>
    </message>
    <message>
        <source>&amp;Start Live</source>
        <translation>&amp;Début mesures</translation>
    </message>
    <message>
        <source>&amp;Rescan Ports</source>
        <translation>&amp;Rescanner les ports</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <source>#1</source>
        <translation>#1</translation>
    </message>
    <message>
        <source>#2</source>
        <translation>#2</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translation>IAH</translation>
    </message>
    <message>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translation>IDR</translation>
    </message>
    <message>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <source>Graph Height</source>
        <translation>Hauteur des graphiques</translation>
    </message>
    <message>
        <source>Flag</source>
        <translation>Marque</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Polices</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>SPO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <source>Span</source>
        <translation>Envergure</translation>
    </message>
    <message>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <source>&amp;CPAP</source>
        <translation>&amp;PPC</translation>
    </message>
    <message>
        <source>General Settings</source>
        <translation>Réglages généraux</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;body&gt;&lt;p&gt;Permet de faire défiler plus facilement avec les touchpads bidirectionnels en mode zoom&lt;/p&gt;&lt;p&gt;50 ms est une valeur recommandée.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <source>Event Duration</source>
        <translation>Durée d&apos;évènement</translation>
    </message>
    <message>
        <source>Hours</source>
        <translation>Heures</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Libellé</translation>
    </message>
    <message>
        <source>Lower</source>
        <translation>Plus bas</translation>
    </message>
    <message>
        <source>Pulse</source>
        <translation>Pouls</translation>
    </message>
    <message>
        <source>Upper</source>
        <translation>Plus haut</translation>
    </message>
    <message>
        <source>days.</source>
        <translation>Jours.</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Les sessions plus courtes que cela ne seront pas affichées&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Ici vous pouvez indiquer le seuil &lt;b&gt;supérieur&lt;/B&gt; utilisé pour les calculs des courbes de %1</translation>
    </message>
    <message>
        <source>Ignore Short Sessions</source>
        <translation>Ignorer les sessions plus courtes que</translation>
    </message>
    <message>
        <source>Sleep Stage Waveforms</source>
        <translation>Courbe de période de sommeil</translation>
    </message>
    <message>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation>% de restriction de flux à partir de la valeur moyenne.
Une valeur de 20% est adéquate pour détecter les apnées. </translation>
    </message>
    <message>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>Les sessions démarrées avant cette heure iront dans le jour précédent.</translation>
    </message>
    <message>
        <source>Session Storage Options</source>
        <translation>Options de stockage des sessions</translation>
    </message>
    <message>
        <source>Show flags for machine detected events that haven&apos;t been identified yet.</source>
        <translation>Affiche les marqueurs d&apos;évènements détectés mais non identifiés.</translation>
    </message>
    <message>
        <source>Graph Titles</source>
        <translation>Titres des graphiques</translation>
    </message>
    <message>
        <source>Zero Reset</source>
        <translation>Remettre à zéro</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the machine. They are &lt;span style=&quot; text-decoration: underline;&quot;&gt;not&lt;/span&gt; included in AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Le marquage personnalisé est une méthode expérimentale de détection des évènements ratés par l&apos;appareil. Il ne sont &lt;span style=&quot; text-decoration: underline;&quot;&gt;&lt;b&gt;pas&lt;/b&gt;&lt;/span&gt; pris en compte dans l&apos;IAH (Index des apnées et hypopnées).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the machine missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation>Active/désactive le marquage amélioré expérimental.
Permet de détecter des évènements ratés par certaines appareils.
Option à activer avant import, sinon une purge est nécessaire.</translation>
    </message>
    <message>
        <source>Flow Restriction</source>
        <translation>Restriction de flux</translation>
    </message>
    <message>
        <source>Enable Unknown Events Channels</source>
        <translation>Autoriser les canaux d&apos;évènements inconnus</translation>
    </message>
    <message>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>Durée minimum de perte en saturation d&apos;oxygène</translation>
    </message>
    <message>
        <source>Overview Linecharts</source>
        <translation>Lignes Résumé</translation>
    </message>
    <message>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>Autoriser de changer l&apos;axe des y en double-cliquant sur l&apos;intitulé</translation>
    </message>
    <message>
        <source>Always Minor</source>
        <translation>Toujours inférieur</translation>
    </message>
    <message>
        <source>Unknown Events</source>
        <translation>Évènements inconnus</translation>
    </message>
    <message>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>Le cache des pixels est une technique d&apos;accélération graphique qui peut poser des soucis à l&apos;affichage des caractères sur votre plateforme.</translation>
    </message>
    <message>
        <source>Reset &amp;Defaults</source>
        <translation>Remettre aux valeurs par &amp;défaut</translation>
    </message>
    <message>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>Pas de choix d&apos;utilisateur, choisir le plus récent</translation>
    </message>
    <message>
        <source>Data Reindex Required</source>
        <translation>Réindexation des données nécessaire</translation>
    </message>
    <message>
        <source>Scroll Dampening</source>
        <translation>Défilement adouci</translation>
    </message>
    <message>
        <source> L/min</source>
        <translation> l/min</translation>
    </message>
    <message>
        <source>Flag leaks over threshold</source>
        <translation>Afficher les fuites supérieures à</translation>
    </message>
    <message>
        <source>20 cmH2O</source>
        <translation>20 cmH2O</translation>
    </message>
    <message>
        <source> hours</source>
        <translation> Heures</translation>
    </message>
    <message>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>Double-cliquez pour changer la description de ce canal.</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If your interested in helping test new features and bugfixes early, click here.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;But please be warned this will sometimes mean breaky code..&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; Pour tester les nouveautés cliquez ici&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Attention ! Sans garantie de bon fonctionnement&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Sessions older than this date will not be imported</source>
        <translation>Les sessions antérieures à cette date ne seront pas importées</translation>
    </message>
    <message>
        <source>Flag SPO2 Desaturations Below</source>
        <translation>Indiquer les désaturations SPO2 en dessous</translation>
    </message>
    <message>
        <source>Standard Bars</source>
        <translation>Barres standard</translation>
    </message>
    <message>
        <source>99% Percentile</source>
        <translation>99% pour cent</translation>
    </message>
    <message>
        <source>Memory and Startup Options</source>
        <translation>Options mémoire et démarrage</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ne montre pas les boîtes de dialogue non importantes durant l&apos;import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>Les données d&apos;oxymétrie sous cette valeur seront ignorées.</translation>
    </message>
    <message>
        <source>Oximeter Waveforms</source>
        <translation>Courbes de l&apos;oxymètre</translation>
    </message>
    <message>
        <source>User definable threshold considered large leak</source>
        <translation>Seuil de fuites importantes personnalisé</translation>
    </message>
    <message>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>RAZ du compteur à chaque début de fenêtre de temps.</translation>
    </message>
    <message>
        <source>Compliance defined as</source>
        <translation>Conformité d&apos;observance choisie</translation>
    </message>
    <message>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>Ici vous pouvez changer le type de marques affichées pour cet évènement</translation>
    </message>
    <message>
        <source>Top Markers</source>
        <translation>Marqueurs hauts</translation>
    </message>
    <message>
        <source> minutes</source>
        <translation> minutes</translation>
    </message>
    <message>
        <source>Minutes</source>
        <translation>Minutes</translation>
    </message>
    <message>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>Créer des sauvegardes de la carte SD pendant l&apos;importation (désactivez cette option à vos risques et périls !)</translation>
    </message>
    <message>
        <source>Graph Settings</source>
        <translation>Réglages du graphique</translation>
    </message>
    <message>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>Libellé court pour ce canal sur l&apos;écran.</translation>
    </message>
    <message>
        <source>CPAP Events</source>
        <translation>Évènements PPC</translation>
    </message>
    <message>
        <source>Bold  </source>
        <translation>Gras  </translation>
    </message>
    <message>
        <source>Minimum duration of pulse change event.</source>
        <translation>Durée minimum du changement de pulsations.</translation>
    </message>
    <message>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>Anti-aliasing pour les graphiques
Certains sont plus beaux ainsi 
Cela affecte aussi les impressions

À essayer pour voir.</translation>
    </message>
    <message>
        <source>Sleep Stage Events</source>
        <translation>Évènements de période de sommeil</translation>
    </message>
    <message>
        <source>Events</source>
        <translation>Évènements</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ce paramètre doit être utilisé avec précaution... &lt;/span&gt; Le décocher a des conséquences impliquant la précision des sommaires journaliers, certains calculs ne fonctionneront correctement que si les sessions des jours sont maintenues ensemble. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt; Utilisateurs ResMed :&lt;/span&gt; bien qu&apos;il semble normal que le redémarrage ait lieu la journée précédente, ça ne signifie pas que nous soyons d&apos;accord avec les données ResMed. Le format STF.edf index de synthèse présente des faiblesses graves qui font que ce n&apos;est pas une bonne idée de faire cela. &lt;/p&gt;&lt;p&gt;Cette option existe pour apaiser ceux qui veulent voir ceci &amp;quot;corrigé &amp;quot;fixed&amp;quot; peu importe les coûts, mais sachez qu&apos;il est livré avec un coût. Si vous gardez votre carte SD dedans tous les soirs, et l&apos;importez au moins une fois par semaine, vous ne verrez pas ce problème très souvent.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Median is recommended for ResMed users.</source>
        <translation>Moyenne est recommandé pour les appareils ResMed.</translation>
    </message>
    <message>
        <source>Oximeter Events</source>
        <translation>Évènements de l&apos;oxymètre</translation>
    </message>
    <message>
        <source>&amp;Check for Updates now</source>
        <translation>&amp;Vérifier les mises à jour</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>Italique</translation>
    </message>
    <message>
        <source>Enable Multithreading</source>
        <translation>Autoriser la parallélisation</translation>
    </message>
    <message>
        <source>This may not be a good idea</source>
        <translation>Cela n&apos;est peut-être pas une bonne idée</translation>
    </message>
    <message>
        <source>Weighted Average</source>
        <translation>Moyenne pondérée</translation>
    </message>
    <message>
        <source>Median</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>Indiquer les changements rapides dans les statistiques d&apos;oxymétrie</translation>
    </message>
    <message>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>Changement soudain de fréquence cardiaque d&apos;au moins ce montant</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <source>Resync Machine Detected Events (Experimental)</source>
        <translation>Resynchronisation des évènements détectés par l&apos;appareil</translation>
    </message>
    <message>
        <source>Time Weighted average of Indice</source>
        <translation>Temps pondéré par moyenne de l&apos;indice</translation>
    </message>
    <message>
        <source>Middle Calculations</source>
        <translation>Calcul de la moyenne</translation>
    </message>
    <message>
        <source>Skip over Empty Days</source>
        <translation>Ne pas prendre en compte les jours sans mesure</translation>
    </message>
    <message>
        <source>Allow duplicates near machine events.</source>
        <translation>Autoriser la duplication des évènements proches.</translation>
    </message>
    <message>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>Méthode visuelle d&apos;affichage des marques de limite des courbes.
</translation>
    </message>
    <message>
        <source>Upper Percentile</source>
        <translation>Pourcentage haut</translation>
    </message>
    <message>
        <source>Restart Required</source>
        <translation>Redémarrage nécessaire</translation>
    </message>
    <message>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>Montrer la ligne rouge des fuites dans le graphique</translation>
    </message>
    <message>
        <source>True Maximum</source>
        <translation>Maximum réel</translation>
    </message>
    <message>
        <source>Minor Flag</source>
        <translation>Marque secondaire</translation>
    </message>
    <message>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation>Par soucis de cohérence, les utilisateurs de ResMed devraient utiliser 95% ici pour une meilleure visualisation 
car c&apos;est la seule valeur disponible dans ce cas.</translation>
    </message>
    <message>
        <source>4 cmH2O</source>
        <translation>4 cmH2O</translation>
    </message>
    <message>
        <source>Pre-Load all summary data at startup</source>
        <translation>Précharger les données de synthèse au démarrage</translation>
    </message>
    <message>
        <source>Graph Text</source>
        <translation>Texte des graphiques</translation>
    </message>
    <message>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>Double-cliquez pour changer la couleur par défaut des points/marques/données de ce canal.</translation>
    </message>
    <message>
        <source>AHI/Hour Graph Time Window</source>
        <translation>Affichage IAH/Heure</translation>
    </message>
    <message>
        <source>Import without asking for confirmation</source>
        <translation>Importer sans confirmation</translation>
    </message>
    <message>
        <source>Discard segments under</source>
        <translation>Passer les mesures inférieures à</translation>
    </message>
    <message>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation>Autorise la parallélisation pour les processeurs multicœurs pour améliorer les performances.
Surtout pour l&apos;import.</translation>
    </message>
    <message>
        <source>Line Chart</source>
        <translation>Graphique en ligne</translation>
    </message>
    <message>
        <source>dd MMMM yyyy</source>
        <translation>dd MMMM yyyy</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Le véritable maximum est le maximum de l&apos;ensemble de données.&lt;/p&gt;&lt;p&gt; 99% filtre les valeurs aberrantes les plus rares. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Flag Type</source>
        <translation>Type de marqueur</translation>
    </message>
    <message>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>Calculer les fuites involontaires si non existant</translation>
    </message>
    <message>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>Durée d&apos;affichage des astuces.</translation>
    </message>
    <message>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>Double-cliquez pour changer le nom du canal &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>Les sessions plus rapprochées que cette valeur seront sur le même jour.
</translation>
    </message>
    <message>
        <source>Are you really sure you want to do this?</source>
        <translation>Êtes-vous vraiment sûr de vouloir faire cela ?</translation>
    </message>
    <message>
        <source>Duration of airflow restriction</source>
        <translation>Durée de restriction de flux d&apos;air</translation>
    </message>
    <message>
        <source>Bar Tops</source>
        <translation>Haut des barres</translation>
    </message>
    <message>
        <source>Automatically Check For Updates</source>
        <translation>Vérifier les mises à jour automatiquement</translation>
    </message>
    <message>
        <source>I want to try experimental and test builds (Advanced users only please.)</source>
        <translation>Je veux essayer les versions expérimentales (Utilisateurs confirmés seulement)</translation>
    </message>
    <message>
        <source>This calculation requires Total Leaks data to be provided by the CPAP machine. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation>Ce calcul nécessite que le total des fuites soit fourni par l&apos;appareil à PPC (i.e. PRS1 mais pas ResMed, qui les inclut déjà).
Le calcul des fuites involontaires est linéaire, il ne modélise pas la courbure du masque.
Si vous utilisez des masques différents, utilisez plutôt la valeur moyenne, ce sera assez précis.</translation>
    </message>
    <message>
        <source>Session Splitting Settings</source>
        <translation>Réglage du découpage des sessions</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note : n&apos;est pas destiné aux corrections de fuseau horaire ! Assurez-vous que l&apos;horloge PPC  et le fuseau horaire du système d&apos;exploitation sont correctement synchronisés.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Other Visual Settings</source>
        <translation>Autres réglages visuels</translation>
    </message>
    <message>
        <source>Day Split Time</source>
        <translation>Heure de séparation des jours</translation>
    </message>
    <message>
        <source>CPAP Waveforms</source>
        <translation>Courbes PPC</translation>
    </message>
    <message>
        <source>Big  Text</source>
        <translation>Grand texte</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;htlm&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fonctionnalités récemment désactivées. Elles reviendront plus tard&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>Note : une méthode de calcul linéaire est utilisée. Changer ces valeurs nécessite un recalcul.</translation>
    </message>
    <message>
        <source>ResMed S9 machines routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation>Les appareils ResMed S9 effacent régulièrement les données de plus de 7 ou 30 jours de la carte SD (selon la résolution).</translation>
    </message>
    <message>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation>Considérer les jours sous cette valeur comme non conformes. 4 heures est considéré comme conforme.</translation>
    </message>
    <message>
        <source>Do not import sessions older than:</source>
        <translation>Ne pas importer de sessions antérieures au :</translation>
    </message>
    <message>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>Le bouton Quotidien passe les jours sans données</translation>
    </message>
    <message>
        <source>Flag Pulse Rate Above</source>
        <translation>Marquer les pulsations en dessous</translation>
    </message>
    <message>
        <source>Flag Pulse Rate Below</source>
        <translation>Marquer les pulsations au-dessus</translation>
    </message>
    <message>
        <source>Seconds</source>
        <translation>secondes</translation>
    </message>
    <message>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>Ajuste le nombre de données pour chaque point du graphique IAH/Heure.
60 min. par défaut. Il est hautement recommandé de le laisser à cette valeur.</translation>
    </message>
    <message>
        <source>Show in Event Breakdown Piechart</source>
        <translation>Montrer la répartition des évènements dans le camembert</translation>
    </message>
    <message>
        <source>Other oximetry options</source>
        <translation>Autres options d&apos;oxymétrie</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>Ne pas séparer les jours résumés (Attention lire les astuces)</translation>
    </message>
    <message>
        <source>Last Checked For Updates: </source>
        <translation>Dernière vérification de disponibilité de mise à jour : </translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <source>Use Anti-Aliasing</source>
        <translation>Utiliser l&apos;anti-aliasing</translation>
    </message>
    <message>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation>Animation et effets</translation>
    </message>
    <message>
        <source>&amp;Import</source>
        <translation>&amp;Import</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ce paramètre conserve les données de forme d&apos;ondes et d&apos;évènements en mémoire après utilisation pour accélérer les jours de revisite.&lt;/p&gt;&lt;p&gt;Ce n&apos;est pas vraiment une option nécessaire, car votre système d&apos;exploitation garde en cache les fichiers déjà lus .&lt;/p&gt;&lt;p&gt;Il est recommandé de le laisser décoché, à moins que votre ordinateur ne dispose d&apos;une grande quantité de mémoire...&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>Un changement des réglages ci-dessous nécessitera un redémarrage.</translation>
    </message>
    <message>
        <source>&amp;Appearance</source>
        <translation>&amp;Apparence</translation>
    </message>
    <message>
        <source>The pixel thickness of line plots</source>
        <translation>Épaisseur de la ligne en pixel</translation>
    </message>
    <message>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>Graphique d&apos;aperçu général dédié pour cet objet.</translation>
    </message>
    <message>
        <source>This is a description of what this channel does.</source>
        <translation>Description de ce que fait le canal.</translation>
    </message>
    <message>
        <source>Combine Close Sessions </source>
        <translation>Fermeture combinée de sessions </translation>
    </message>
    <message>
        <source>Custom CPAP User Event Flagging</source>
        <translation>Comptage d&apos;évènements personnalisés</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Attention : &lt;/span&gt;Ce n&apos;est pas parce que c&apos;est possible que c&apos;est la bonne méthode&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Allow YAxis Scaling</source>
        <translation>Autoriser la mise à l&apos;échelle de l&apos;axe Y</translation>
    </message>
    <message>
        <source>Fonts (Application wide settings)</source>
        <translation>Polices (paramètres étendus)</translation>
    </message>
    <message>
        <source>Use Pixmap Caching</source>
        <translation>Utiliser le cache des pixels</translation>
    </message>
    <message>
        <source>Check for new version every</source>
        <translation>Vérifier les nouvelles versions tout les</translation>
    </message>
    <message>
        <source>Waveforms</source>
        <translation>Ondes</translation>
    </message>
    <message>
        <source>Maximum Calcs</source>
        <translation>Calculs maximum</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Aperçus</translation>
    </message>
    <message>
        <source>Tooltip Timeout</source>
        <translation>Durée des astuces</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>General CPAP and Related Settings</source>
        <translation>Réglages généraux de PPC</translation>
    </message>
    <message>
        <source>Sourceforge hosts this project for free.. Please be considerate of their resources..</source>
        <translation>Sourceforge héberge gratuitement ce projet, soyez respectueux de leur bande passante.</translation>
    </message>
    <message>
        <source>Default display height of graphs in pixels</source>
        <translation>Afficher la hauteur des graphiques en pixels</translation>
    </message>
    <message>
        <source>Overlay Flags</source>
        <translation>Marques de dépassement</translation>
    </message>
    <message>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>Rendre certains tracés plus &quot;carrés&quot;.</translation>
    </message>
    <message>
        <source>Percentage drop in oxygen saturation</source>
        <translation>% perdus lors de la saturation d&apos;oxygène</translation>
    </message>
    <message>
        <source>&amp;General</source>
        <translation>&amp;Général</translation>
    </message>
    <message>
        <source>Standard average of indice</source>
        <translation>Moyenne simple de l&apos;indice</translation>
    </message>
    <message>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>Compression de la sauvegarde de la carte SD</translation>
    </message>
    <message>
        <source>Keep Waveform/Event data in memory</source>
        <translation>Garder les ondes/évènements en mémoire</translation>
    </message>
    <message>
        <source>Culminative Indices</source>
        <translation>Indices cumulés</translation>
    </message>
    <message>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>Affiche la ventilation de cette forme d&apos;onde dans l&apos;aperçu.</translation>
    </message>
    <message>
        <source>Normal Average</source>
        <translation>Moyenne simple</translation>
    </message>
    <message>
        <source>Positional Waveforms</source>
        <translation>Courbe de position</translation>
    </message>
    <message>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Il faudra réindexer les données ce qui prendra quelques minutes.

Êtes-vous sûr de vouloir le faire ?</translation>
    </message>
    <message>
        <source>Positional Events</source>
        <translation>Évènements de position</translation>
    </message>
    <message>
        <source>Preferred Calculation Methods</source>
        <translation>Choix de la méthode de calcul</translation>
    </message>
    <message>
        <source>Combined Count divided by Total Hours</source>
        <translation>Comptes combinés / nombre d&apos;heures</translation>
    </message>
    <message>
        <source>Graph Tooltips</source>
        <translation>Bulle d&apos;aide du graphique</translation>
    </message>
    <message>
        <source>&amp;Oximetry</source>
        <translation>&amp;Oxymétrie</translation>
    </message>
    <message>
        <source>CPAP Clock Drift</source>
        <translation>Décalage d&apos;horloge de PPC</translation>
    </message>
    <message>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Ici vous pouvez indiquer le seuil &lt;b&gt;inférieur&lt;/B&gt; utilisé pour les calculs des courbes de %1</translation>
    </message>
    <message>
        <source>Square Wave Plots</source>
        <translation>Points carrés</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation>Libellé</translation>
    </message>
    <message>
        <source>Preferred major event index</source>
        <translation>Évènement majeur préféré</translation>
    </message>
    <message>
        <source>Application</source>
        <translation>Application</translation>
    </message>
    <message>
        <source>Line Thickness</source>
        <translation>Épaisseur des lignes</translation>
    </message>
    <message>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>Lancement automatique de l&apos;import après ouverture du profil utilisateur</translation>
    </message>
    <message>
        <source>Automatically load last used profile on start-up</source>
        <translation>Ouverture automatique du dernier profil utilisé au lancement</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed machines do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note : &lt;/span&gt;Les appareils ResMEd ne prennent pas en compte ces réglages du fait de leur conception.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Oximetry Settings</source>
        <translation>Réglages de l&apos;oxymétrie</translation>
    </message>
    <message>
        <source>On Opening</source>
        <translation>À l&apos;ouverture</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <source>Welcome</source>
        <translation>Bienvenue</translation>
    </message>
    <message>
        <source>Daily</source>
        <translation>Quotidien</translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation>Statistiques</translation>
    </message>
    <message>
        <source>Switch Tabs</source>
        <translation>Échanger les onglets</translation>
    </message>
    <message>
        <source>No change</source>
        <translation>Pas de changement</translation>
    </message>
    <message>
        <source>After Import</source>
        <translation>Après import</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>Jamais</translation>
    </message>
    <message>
        <source>Data Processing Required</source>
        <translation>Traitement des données nécessaire</translation>
    </message>
    <message>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Il faudra re/décompresser les données, ce qui prendra quelques minutes

Êtes-vous sûr de vouloir le faire ?</translation>
    </message>
    <message>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>Moteur graphique (nécessite un redémarrage)</translation>
    </message>
    <message>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation>Ces modifications vont nécessiter un redémarrage de l&apos;application pour être effectives.

Voulez-vous le faire maintenant ?</translation>
    </message>
    <message>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation>Compresse la sauvegarde des appareils ResMed (fichiers EDF en .gz).
Format courant sous Linux et Mac.

OSCAR peut importer de ce répertoire de sauvegarde compressé en mode natif.
Pour l&apos;utiliser avec ResScan, il faudra d&apos;abord décompresser les fichiers *.gz..</translation>
    </message>
    <message>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation>Options affectant l&apos;espace disque utilisé et la durée de l&apos;import.</translation>
    </message>
    <message>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation>Réduit de moitié l&apos;occupation des données.
Mais prendra plus de temps pour l&apos;import et les modifications.</translation>
    </message>
    <message>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation>Compresser les données de sessions</translation>
    </message>
    <message>
        <source>This maintains a backup of SD-card data for ResMed machines, 

ResMed S9 series machines delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation>Garde un copie de la carte SD des appareils ResMed.
Les appareils ResMed effacent les données précises après 7 jours, et les graphiques de plus de 30 jours...

OSCAR peut garder ces données au cas vous devriez réinstaller (Hautement recommandé, à moins que vous n&apos;ayez pas de place disque ou que les graphiques ne vous intéressent pas)</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR préchargera les informations de synthèse, ce qui améliore la vue synthétique et les autres calculs. &lt;/p&gt;&lt;p&gt;Si vous avez beaucoup de données cela ralentira le lancement, mais si vous aimez voir &lt;span style=&quot; font-style:italic;&quot;&gt;tout&lt;/span&gt; en résumé, il faudra de toutes façons tout charger. &lt;/p&gt;&lt;p&gt;N&apos;affecte pas les données d&apos;évènements qui sont chargées à la demande&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve machine detected event positioning.</source>
        <translation>Cette option expérimentale tente d&apos;utiliser le système de repérage de OSCAR pour améliorer la détection d&apos;évènements de position.</translation>
    </message>
    <message>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation>Rappel de retrait de la carte SD à l&apos;arrêt de OSCAR</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Choix de l&apos;onglet à ouvrir au chargement d&apos;un profil. (Note : l&apos;onglet Profil sera affiché automatiquement si OSCAR est réglé pour ne pas ouvrir de profil au lancement)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation>Si vous rencontrez des problèmes d&apos;affichage des graphiques, essayez de changer le réglage par défaut (Desktop OpenGL).</translation>
    </message>
    <message>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; machines due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed machines, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Note :&lt;/b&gt; Le découpage de session n&apos;est pas possible avec les appareils &lt;b&gt;ResMed&lt;/b&gt; du fait de la façon dont ils sauvegardent les données et sera de ce fait désactivé.&lt;/p&gt;&lt;p&gt;Sur les appareils ResMed, les jours &lt;b&gt; débutent à midi &lt;/b&gt; comme dans leur logiciel commercial ResScan.&lt;/p&gt;</translation>
    </message>
    <message>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation> Si vous avez besoin un jour de réimporter ces données (dans OSCAR ou ResScan) ces données auront disparu.</translation>
    </message>
    <message>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation> Si vous avez besoin de conserver de l&apos;espace disque, n&apos;oubliez pas de faire des sauvegardes manuelles.</translation>
    </message>
    <message>
        <source> Are you sure you want to disable these backups?</source>
        <translation> Êtes-vous sûr de vouloir désactiver ces sauvegardes ?</translation>
    </message>
    <message>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation>Désactiver les sauvegardes automatiques n&apos;est pas une bonne idée, car OSCAR a besoin de reconstruire sa base de données si des erreurs apparaissent.

</translation>
    </message>
    <message>
        <source>Changing SD Backup compression options doesn&apos;t automatically recompress backup data.</source>
        <translation>Changer les options de compression de la sauvegarde sur carte SD ne recompresse pas les données automatiquement.</translation>
    </message>
    <message>
        <source>Your masks vent rate at 20 cmH2O pressure</source>
        <translation>Ventilation du masque à 20 cmH2O de pression</translation>
    </message>
    <message>
        <source>Your masks vent rate at 4 cmH2O pressure</source>
        <translation>Ventilation du masque à 4 cmH2O de pression</translation>
    </message>
    <message>
        <source>Whether to include machine serial number on machine settings changes report</source>
        <translation>Inclure ou non le N° de série de l&apos;appareil sur le rapport des changements de réglage de l&apos;appareil</translation>
    </message>
    <message>
        <source>Include Serial Number</source>
        <translation>Inclure le numéro de série</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data from any machine model that has not yet been tested by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Alerter lors de l’importation de données de tout modèle d&apos;appareil qui n’est pas encore été testé par les développeurs de OSCAR.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Warn when importing data from an untested machine</source>
        <translation>Avertir lors de l’importation de données à partir d’un appareil non testé</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data that is somehow different from anything previously seen by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Alerter lors de l’importation de données différente de tout ce qui a déjà été vu par les développeurs de OSCAR.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Warn when previously unseen data is encountered</source>
        <translation>Avertir lorsque des données inédites sont rencontrées</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:7.84158pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method does &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP machine, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Synchronisation des données d&apos;oxymétrie et de PPC&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Les données de la CMS50 importées à partir de SpO2Review (depuis fichiers .spoR) ou avec importation par port série &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;n&apos;ont pas&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; le format d&apos;heure correct requis pour la synchronisation.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Le mode &quot;temps réel&quot; (à l&apos;aide d&apos;un câble série) est un moyen d&apos;obtenir une synchronisation précise sur l&apos;oxymètre CMS50, mais ne contrebalance pas la dérive de l&apos;horloge PPC. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Si vous démarrez l&apos;enregistrement de l&apos;oxymètre &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactement &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;en même temps que votre appareil PPC, la synchronisation fonctionnera... &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Le processus d&apos;importation en série prend l&apos;heure de début de la première session PPC de la nuit précédente. (N&apos;oubliez pas d&apos;importer vos données PPC en premier !)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Always save screenshots in the OSCAR Data folder</source>
        <translation>Enregistrez toujours les captures d’écran dans le dossier de données de OSCAR</translation>
    </message>
    <message>
        <source>No CPAP machines detected</source>
        <translatorcomment>Pas d&apos;appareil PPC détecté</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <source>Will you be using a ResMed brand machine?</source>
        <translation>Utiliserez-vous un appareil Resmed?</translation>
    </message>
</context>
<context>
    <name>ProfileSelect</name>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>Create a new user profile.</source>
        <translation>Créer un nouveau profil utilisateur.</translation>
    </message>
    <message>
        <source>Sorry</source>
        <translation>Désolé</translation>
    </message>
    <message>
        <source>[data directory]</source>
        <translation>[répertoire]</translation>
    </message>
    <message>
        <source>Open Profile</source>
        <translation>Ouverture du profil</translation>
    </message>
    <message>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Profil &apos;%1&apos; effacé avec succès</translation>
    </message>
    <message>
        <source>Edit Profile</source>
        <translation>Modifier le profil utilisateur</translation>
    </message>
    <message>
        <source>Enter the word DELETE below to confirm.</source>
        <translation>Entrez le mot DELETE ci-dessous pour confirmer.</translation>
    </message>
    <message>
        <source>Create new profile</source>
        <translation>Créer un nouveau profil</translation>
    </message>
    <message>
        <source>Incorrect Password</source>
        <translation>Mot de passe incorrect</translation>
    </message>
    <message>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Erreur lors de l&apos;effacement du répertoire du profil. Vous devez l&apos;effacer à la main.</translation>
    </message>
    <message>
        <source>Enter Password for %1</source>
        <translation>Mot de passe pour %1</translation>
    </message>
    <message>
        <source>New Profile</source>
        <translation>Nouveau profil utilisateur</translation>
    </message>
    <message>
        <source>&amp;Different Folder</source>
        <translation>Répertoire &amp;différent</translation>
    </message>
    <message>
        <source>&amp;Select User</source>
        <translation>&amp;Sélection du profil utilisateur</translation>
    </message>
    <message>
        <source>You entered an incorrect password</source>
        <translation>Mot de passe incorrect</translation>
    </message>
    <message>
        <source>Search:</source>
        <translation>Rechercher :</translation>
    </message>
    <message>
        <source>Start with the selected user profile.</source>
        <translation>Démarrer avec le profil sélectionné.</translation>
    </message>
    <message>
        <source>Delete Profile</source>
        <translation>Effacer le profil</translation>
    </message>
    <message>
        <source>Select Profile</source>
        <translation>Sélectionnez le profil utilisateur</translation>
    </message>
    <message>
        <source>[version]</source>
        <translation>[version]</translation>
    </message>
    <message>
        <source>You entered the password wrong too many times.</source>
        <translation>Trop d&apos;erreurs de mot de passe.</translation>
    </message>
    <message>
        <source>Enter Password</source>
        <translation>Entrez le mot de passe</translation>
    </message>
    <message>
        <source>Folder:</source>
        <translation>Répertoire :</translation>
    </message>
    <message>
        <source>You are about to destroy profile &apos;%1&apos;.</source>
        <translation>Vous allez détruire le profil &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>If you&apos;re trying to delete because you forgot the password, you need to delete it manually.</source>
        <translation>Si vous tentez d&apos;effacer car vous avez oublié le mot de passe, vous devez l&apos;effacer à la main.</translation>
    </message>
    <message>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>C&apos;est DELETE en majuscule.</translation>
    </message>
    <message>
        <source>You entered an Incorrect Password too many times. Exiting!</source>
        <translation>Trop d&apos;erreurs de mot de passe. Au revoir !</translation>
    </message>
    <message>
        <source>Choose a different OSCAR data folder.</source>
        <translation>Choisir un répertoire différent pour les données.</translation>
    </message>
    <message>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <source>Click here if you didn&apos;t want to start OSCAR.</source>
        <translation>Cliquer ici pour ne pas lancer OSCAR.</translation>
    </message>
    <message>
        <source>The current location of OSCAR data store.</source>
        <translation>Localisation actuelle des données de OSCAR.</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <source>Filter:</source>
        <translation>Filtre :</translation>
    </message>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>&amp;Open Profile</source>
        <translation>&amp;Ouverture du profil</translation>
    </message>
    <message>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Modifier le profil</translation>
    </message>
    <message>
        <source>&amp;New Profile</source>
        <translation>&amp;Nouveau profil utilisateur</translation>
    </message>
    <message>
        <source>Profile: None</source>
        <translation>Profil : aucun</translation>
    </message>
    <message>
        <source>Please select or create a profile...</source>
        <translation>Sélectionnez ou créez le profil utilisateur...</translation>
    </message>
    <message>
        <source>Destroy Profile</source>
        <translation>Effacer le profil</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <source>Ventilator Brand</source>
        <translation>Marque du ventilateur</translation>
    </message>
    <message>
        <source>Ventilator Model</source>
        <translation>Modèle du ventilateur</translation>
    </message>
    <message>
        <source>Other Data</source>
        <translation>Autres données</translation>
    </message>
    <message>
        <source>Last Imported</source>
        <translation>Dernier import</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
    <message>
        <source>Enter Password for %1</source>
        <translation>Mot de passe pour %1</translation>
    </message>
    <message>
        <source>You entered an incorrect password</source>
        <translation>Mot de passe incorrect</translation>
    </message>
    <message>
        <source>Forgot your password?</source>
        <translation>Mot de passe oublié ?</translation>
    </message>
    <message>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>Demandez sur les forums comment le réinitialiser. C&apos;est facile.</translation>
    </message>
    <message>
        <source>Select a profile first</source>
        <translation>Sélectionnez le profil utilisateur</translation>
    </message>
    <message>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>Si vous tentez d&apos;effacer car vous avez oublié le mot de passe, vous devez l&apos;effacer à la main.</translation>
    </message>
    <message>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>Vous allez détruire le profil &apos;&lt;b&gt;%1&lt;b&gt;&apos;.</translation>
    </message>
    <message>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>Attention cela va définitivement effacer le profil et toutes les &lt;b&gt;données de sauvegarde&lt;b&gt; présentes dans &lt;br/&gt;%2.</translation>
    </message>
    <message>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>Entrez le mot DELETE ci-dessous pour confirmer.</translation>
    </message>
    <message>
        <source>DELETE</source>
        <translation>DELETE</translation>
    </message>
    <message>
        <source>Sorry</source>
        <translation>Désolé</translation>
    </message>
    <message>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>C&apos;est DELETE en majuscule.</translation>
    </message>
    <message>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Erreur lors de l&apos;effacement du répertoire du profil. Vous devez l&apos;effacer à la main.</translation>
    </message>
    <message>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Profil &apos;%1&apos; effacé avec succès</translation>
    </message>
    <message>
        <source>Hide disk usage information</source>
        <translation>Cacher les informations d&apos;utilisation disque</translation>
    </message>
    <message>
        <source>Show disk usage information</source>
        <translation>Afficher les informations d&apos;utilisation disque</translation>
    </message>
    <message>
        <source>Name: %1, %2</source>
        <translation>Nom : %1, %2</translation>
    </message>
    <message>
        <source>Phone: %1</source>
        <translation>Téléphone : %1</translation>
    </message>
    <message>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>Email : &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Address:</source>
        <translation>Adresse :</translation>
    </message>
    <message>
        <source>No profile information given</source>
        <translation>Pas d&apos;information de profil</translation>
    </message>
    <message>
        <source>Profile: %1</source>
        <translation>Profil : %1</translation>
    </message>
    <message>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <source>Bytes</source>
        <translation>Octets</translation>
    </message>
    <message>
        <source>KB</source>
        <translation>Ko</translation>
    </message>
    <message>
        <source>MB</source>
        <translation>Mo</translation>
    </message>
    <message>
        <source>GB</source>
        <translation>Go</translation>
    </message>
    <message>
        <source>TB</source>
        <translation>To</translation>
    </message>
    <message>
        <source>PB</source>
        <translation>Po</translation>
    </message>
    <message>
        <source>Summaries:</source>
        <translation>Résumés :</translation>
    </message>
    <message>
        <source>Events:</source>
        <translation>Évènements :</translation>
    </message>
    <message>
        <source>Backups:</source>
        <translation>Sauvegardes :</translation>
    </message>
    <message>
        <source>You must create a profile</source>
        <translation>Vous devez créer un profil</translation>
    </message>
    <message>
        <source>Reset filter to see all profiles</source>
        <translation>Réinitialise le filtre pour voir tous les profils</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <source>Abort</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>&quot;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <source>AI</source>
        <translation>AI</translation>
    </message>
    <message>
        <source>CA</source>
        <translation>CA</translation>
    </message>
    <message>
        <source>EP</source>
        <translation>EP</translation>
    </message>
    <message>
        <source>FL</source>
        <translation>FL</translation>
    </message>
    <message>
        <source>HI</source>
        <translation>HI</translation>
    </message>
    <message>
        <source>IE</source>
        <translation>IE</translation>
    </message>
    <message>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <source>LE</source>
        <translation>LE</translation>
    </message>
    <message>
        <source>LF</source>
        <translation>LF</translation>
    </message>
    <message>
        <source>LL</source>
        <translation>LL</translation>
    </message>
    <message>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <source>O2</source>
        <translation>O2</translation>
    </message>
    <message>
        <source>OA</source>
        <translation>AO</translation>
    </message>
    <message>
        <source>NR</source>
        <translation>NR</translation>
    </message>
    <message>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <source>PC</source>
        <translation>PC</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>PP</source>
        <translation>PP</translation>
    </message>
    <message>
        <source>PS</source>
        <translation>PS</translation>
    </message>
    <message>
        <source>On</source>
        <translation>On</translation>
    </message>
    <message>
        <source>RE</source>
        <translation>RE</translation>
    </message>
    <message>
        <source>S9</source>
        <translation>S9</translation>
    </message>
    <message>
        <source>SA</source>
        <translation>SA</translation>
    </message>
    <message>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <source>UA</source>
        <translation>UA</translation>
    </message>
    <message>
        <source>VS</source>
        <translation>VS</translation>
    </message>
    <message>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <source>lb</source>
        <translation>lb</translation>
    </message>
    <message>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <source>oz</source>
        <translation>oz</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
    <message>
        <source>90%</source>
        <translation>90%</translation>
    </message>
    <message>
        <source>AHI</source>
        <translation>IAH</translation>
    </message>
    <message>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <source>BMI</source>
        <translation>IMC</translation>
    </message>
    <message>
        <source>CAI</source>
        <translation>CAI</translation>
    </message>
    <message>
        <source>Apr</source>
        <translation>Avr</translation>
    </message>
    <message>
        <source>CSR</source>
        <translation>RCS</translation>
    </message>
    <message>
        <source>Aug</source>
        <translation>Aou</translation>
    </message>
    <message>
        <source>Avg</source>
        <translation>Moyen</translation>
    </message>
    <message>
        <source>DOB</source>
        <translation>DDN</translation>
    </message>
    <message>
        <source>EPI</source>
        <translation>EPI</translation>
    </message>
    <message>
        <source>EPR</source>
        <translation>EPR</translation>
    </message>
    <message>
        <source>Dec</source>
        <translation>Déc</translation>
    </message>
    <message>
        <source>FLI</source>
        <translation>FLI</translation>
    </message>
    <message>
        <source>End</source>
        <translation>Fin</translation>
    </message>
    <message>
        <source>Feb</source>
        <translation>Fév</translation>
    </message>
    <message>
        <source>Jan</source>
        <translation>Jan</translation>
    </message>
    <message>
        <source>Jul</source>
        <translation>Jui</translation>
    </message>
    <message>
        <source>Jun</source>
        <translation>Jun</translation>
    </message>
    <message>
        <source>NRI</source>
        <translation>NRI</translation>
    </message>
    <message>
        <source>Mar</source>
        <translation>Mar</translation>
    </message>
    <message>
        <source>Max</source>
        <translation>max</translation>
    </message>
    <message>
        <source>May</source>
        <translation>Mai</translation>
    </message>
    <message>
        <source>Med</source>
        <translation>moy</translation>
    </message>
    <message>
        <source>Min</source>
        <translation>min</translation>
    </message>
    <message>
        <source>Nov</source>
        <translation>Nov</translation>
    </message>
    <message>
        <source>Oct</source>
        <translation>Oct</translation>
    </message>
    <message>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <source>RDI</source>
        <translation>IDR</translation>
    </message>
    <message>
        <source>REI</source>
        <translation>REI</translation>
    </message>
    <message>
        <source>UAI</source>
        <translation>UAI</translation>
    </message>
    <message>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <source>Sep</source>
        <translation>Sep</translation>
    </message>
    <message>
        <source>VS2</source>
        <translation>VS2</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <source>Zeo</source>
        <translation>Zeo</translation>
    </message>
    <message>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <source>%1: </source>
        <translation>%1: </translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Oui</translation>
    </message>
    <message>
        <source>15mm</source>
        <translation>15mm</translation>
    </message>
    <message>
        <source>22mm</source>
        <translation>22mm</translation>
    </message>
    <message>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <source>CPAP</source>
        <translation>PPC</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <source>Busy</source>
        <translation>Occupé</translation>
    </message>
    <message>
        <source>Min EPAP</source>
        <translation>EPAP mini</translation>
    </message>
    <message>
        <source>EPAP</source>
        <translation>EPAP</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <source>ICON</source>
        <translation>ICON</translation>
    </message>
    <message>
        <source>Min IPAP</source>
        <translation>IPAP mini</translation>
    </message>
    <message>
        <source>IPAP</source>
        <translation>IPAP</translation>
    </message>
    <message>
        <source>Last</source>
        <translation>Dernier</translation>
    </message>
    <message>
        <source>Leak</source>
        <translation>Fuite</translation>
    </message>
    <message>
        <source>Mask</source>
        <translation>Masque</translation>
    </message>
    <message>
        <source>Med.</source>
        <translation>Mil.</translation>
    </message>
    <message>
        <source>Mode</source>
        <translation>Mode</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <source>RERA</source>
        <translation>RERA (effort expiratoire éveillant)</translation>
    </message>
    <message>
        <source>Peak</source>
        <translation>Pic</translation>
    </message>
    <message>
        <source>Ramp</source>
        <translation>Rampe</translation>
    </message>
    <message>
        <source>SpO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <source>Zero</source>
        <translation>Zéro</translation>
    </message>
    <message>
        <source>Resp. Event</source>
        <translation>Évènement respiratoire</translation>
    </message>
    <message>
        <source>Inclination</source>
        <translation>Inclinaison</translation>
    </message>
    <message>
        <source>Launching Windows Explorer failed</source>
        <translation>Échec au lancement de Windows Explorer</translation>
    </message>
    <message>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <source>Hose Diameter</source>
        <translation>Diamètre du tuyau</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <source>???: </source>
        <translation>??? : </translation>
    </message>
    <message>
        <source>AVAPS</source>
        <translation>AVAPS</translation>
    </message>
    <message>
        <source>CMS50</source>
        <translation>CMS50</translation>
    </message>
    <message>
        <source>Therapy Pressure</source>
        <translation>Pression de la thérapie</translation>
    </message>
    <message>
        <source>BiPAP</source>
        <translation>BiPAP</translation>
    </message>
    <message>
        <source>Brand</source>
        <translation>Marque</translation>
    </message>
    <message>
        <source>EPR: </source>
        <translation>EPR : </translation>
    </message>
    <message>
        <source>Daily</source>
        <translation>Quotidien</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Courriel</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>First</source>
        <translation>Premier</translation>
    </message>
    <message>
        <source>Ramp Pressure</source>
        <translation>Pression de la rampe</translation>
    </message>
    <message>
        <source>L/min</source>
        <translation>l/min</translation>
    </message>
    <message>
        <source>Hours</source>
        <translation>Durée</translation>
    </message>
    <message>
        <source>MD300</source>
        <translation>MD300</translation>
    </message>
    <message>
        <source>Leaks</source>
        <translation>Fuites</translation>
    </message>
    <message>
        <source>Max: </source>
        <translation>Max : </translation>
    </message>
    <message>
        <source>Min: </source>
        <translation>Min : </translation>
    </message>
    <message>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <source>Nasal</source>
        <translation>Nasal</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>TTIA:</source>
        <translation>TTIA :</translation>
    </message>
    <message>
        <source>W-Avg</source>
        <translation>moyenne W</translation>
    </message>
    <message>
        <source>Snore</source>
        <translation>Ronflement</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Début</translation>
    </message>
    <message>
        <source>Usage</source>
        <translation>Utilisation</translation>
    </message>
    <message>
        <source>Respiratory Disturbance Index</source>
        <translation>Index des troubles respiratoires</translation>
    </message>
    <message>
        <source>cmH2O</source>
        <translation>cmH2Os</translation>
    </message>
    <message>
        <source>Pressure Support</source>
        <translation>Pression supportée</translation>
    </message>
    <message>
        <source>Bedtime: %1</source>
        <translation>Coucher : %1</translation>
    </message>
    <message>
        <source>Hypopnea</source>
        <translation>Hypopnées</translation>
    </message>
    <message>
        <source>ratio</source>
        <translation>ratio</translation>
    </message>
    <message>
        <source>Tidal Volume</source>
        <translation>Volume courant</translation>
    </message>
    <message>
        <source>Entire Day</source>
        <translation>Jour entier</translation>
    </message>
    <message>
        <source>Intellipap pressure relief mode.</source>
        <translation>Mode dépression Intellipap.</translation>
    </message>
    <message>
        <source>Personal Sleep Coach</source>
        <translation>Coach personnel de sommeil</translation>
    </message>
    <message>
        <source>Clear Airway</source>
        <translation>apnées centrales</translation>
    </message>
    <message>
        <source>Respironics</source>
        <translation>Respironics</translation>
    </message>
    <message>
        <source>Heart rate in beats per minute</source>
        <translation>Pouls en battements par minute</translation>
    </message>
    <message>
        <source>A large mask leak affecting machine performance.</source>
        <translation>Grosse fuite affectant les performances de l&apos;appareil.</translation>
    </message>
    <message>
        <source>Somnopose Software</source>
        <translation>Somnopose Software</translation>
    </message>
    <message>
        <source>Temp. Enable</source>
        <translation>Temp. activée</translation>
    </message>
    <message>
        <source>Timed Breath</source>
        <translation>Respiration chronométrée</translation>
    </message>
    <message>
        <source>Mask On Time</source>
        <translation>Durée d&apos;utilisation du masque</translation>
    </message>
    <message>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation>Il y a un fort risque de perte de données, êtes-vous sûr de vouloir continuer ?</translation>
    </message>
    <message>
        <source>Pat. Trig. Breath</source>
        <translation>Resp. par patient</translation>
    </message>
    <message>
        <source>(Summary Only)</source>
        <translation>(Résumé seulement)</translation>
    </message>
    <message>
        <source>Ramp Delay Period</source>
        <translation>Période de délai de la rampe</translation>
    </message>
    <message>
        <source>Sessions Switched Off</source>
        <translation>Sessions off</translation>
    </message>
    <message>
        <source>This folder currently resides at the following location:</source>
        <translation>Emplacement actuel de ce répertoire :</translation>
    </message>
    <message>
        <source>Pulse Change</source>
        <translation>Changement de pulsations</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Déconnecté</translation>
    </message>
    <message>
        <source>Sleep Stage</source>
        <translation>Période du sommeil</translation>
    </message>
    <message>
        <source>Minute Vent.</source>
        <translation>Vent. minute</translation>
    </message>
    <message>
        <source>SpO2 Drop</source>
        <translation>Baisse de SpO2</translation>
    </message>
    <message>
        <source>Ramp Event</source>
        <translation>Évènement de rampe</translation>
    </message>
    <message>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation>La fonctionnalitié SensAwake réduira la pression quand l&apos;éveil est détecté.</translation>
    </message>
    <message>
        <source>Show All Events</source>
        <translation>Afficher les évènements</translation>
    </message>
    <message>
        <source>CMS50E/F</source>
        <translation>CMS50E/F</translation>
    </message>
    <message>
        <source>Upright angle in degrees</source>
        <translation>Position assise à inclinée en degrés</translation>
    </message>
    <message>
        <source>%1% %2</source>
        <translation>%1% %2</translation>
    </message>
    <message>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <source>Higher Expiratory Pressure</source>
        <translation>Pression d&apos;expiration la plus haute</translation>
    </message>
    <message>
        <source>Summary Only :(</source>
        <translation>Résumé seulement :(</translation>
    </message>
    <message>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation>NRI=%1 LKI=%2 EPI=%3</translation>
    </message>
    <message>
        <source>M-Series</source>
        <translation>M-Series</translation>
    </message>
    <message>
        <source>A vibratory snore</source>
        <translation>Ronflement vibratoire</translation>
    </message>
    <message>
        <source>Vibratory Snore</source>
        <translation>Ronflement vibratoire</translation>
    </message>
    <message>
        <source>Lower Inspiratory Pressure</source>
        <translation>Pression d&apos;inspiration la plus basse</translation>
    </message>
    <message>
        <source>Humidifier Enabled Status</source>
        <translation>État de l&apos;humidificateur activé</translation>
    </message>
    <message>
        <source>Full Face</source>
        <translation>Facial</translation>
    </message>
    <message>
        <source>Full Time</source>
        <translation>Temps complet</translation>
    </message>
    <message>
        <source>SmartFlex Level</source>
        <translation>Niveau de SmartFlex</translation>
    </message>
    <message>
        <source>Journal Data</source>
        <translation>Données du journal</translation>
    </message>
    <message>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation>(%1% conforme, définie comme &gt; %2 heures)</translation>
    </message>
    <message>
        <source>Resp. Rate</source>
        <translation>Taux de respiration</translation>
    </message>
    <message>
        <source>Insp. Time</source>
        <translation>Durée inspiration</translation>
    </message>
    <message>
        <source>Exp. Time</source>
        <translation>Durée expiration</translation>
    </message>
    <message>
        <source>ClimateLine Temperature</source>
        <translation>Température ClimateLine</translation>
    </message>
    <message>
        <source>Philips Respironics</source>
        <translation>Philips Respironics</translation>
    </message>
    <message>
        <source>Machine Unsupported</source>
        <translation>Appareil non supporté</translation>
    </message>
    <message>
        <source>SOMNOsoft2</source>
        <translation>SOMNOsoft2</translation>
    </message>
    <message>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation>Une évaluation relative de la force d&apos;impulsion au niveau du site de surveillance</translation>
    </message>
    <message>
        <source>Machine</source>
        <translation>Appareil</translation>
    </message>
    <message>
        <source>Mask On</source>
        <translation>Avec masque</translation>
    </message>
    <message>
        <source>Max: %1</source>
        <translation>Max : %1</translation>
    </message>
    <message>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation>Une baisse soudaine d&apos;oxygénation du sang (définissable par l&apos;utilisateur)</translation>
    </message>
    <message>
        <source>There are no graphs visible to print</source>
        <translation>Pas de graphique à imprimer</translation>
    </message>
    <message>
        <source>Target Vent.</source>
        <translation>Vent. cible</translation>
    </message>
    <message>
        <source>Sleep position in degrees</source>
        <translation>Position du sommeil en degrés</translation>
    </message>
    <message>
        <source>Plots Disabled</source>
        <translation>Points désactivés</translation>
    </message>
    <message>
        <source>Min: %1</source>
        <translation>Min : %1</translation>
    </message>
    <message>
        <source>Minutes</source>
        <translation>Minutes</translation>
    </message>
    <message>
        <source>Periodic Breathing</source>
        <translation>Respiration périodique</translation>
    </message>
    <message>
        <source>Ramp Only</source>
        <translation>Rampe seulement</translation>
    </message>
    <message>
        <source>Ramp Time</source>
        <translation>Durée de la rampe</translation>
    </message>
    <message>
        <source>PRS1 pressure relief mode.</source>
        <translation>Mode de dépression PRS1.</translation>
    </message>
    <message>
        <source>An abnormal period of Periodic Breathing</source>
        <translation>Période anormale de respiration périodique</translation>
    </message>
    <message>
        <source>ResMed Mask Setting</source>
        <translation>Réglage du masque ResMed</translation>
    </message>
    <message>
        <source>ResMed Exhale Pressure Relief</source>
        <translation>Dépression d&apos;expiration ResMed</translation>
    </message>
    <message>
        <source>A-Flex</source>
        <translation>A-Flex</translation>
    </message>
    <message>
        <source>EPR Level</source>
        <translatorcomment>Expiration Plus Relax</translatorcomment>
        <translation>Niveau de l&apos;EPR</translation>
    </message>
    <message>
        <source>Unintentional Leaks</source>
        <translation>Fuites involontaires</translation>
    </message>
    <message>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation>Voulez-vous afficher les zones favorites dans ce rapport ?</translation>
    </message>
    <message>
        <source>Somnopose</source>
        <translation>Somnopose</translation>
    </message>
    <message>
        <source>AHI %1</source>
        <translation>IAH : %1</translation>
    </message>
    <message>
        <source>C-Flex</source>
        <translation>C-Flex</translation>
    </message>
    <message>
        <source>VPAP-S/T</source>
        <translation>VPAP-S/T</translation>
    </message>
    <message>
        <source>VPAPauto</source>
        <translation>VPAP Auto</translation>
    </message>
    <message>
        <source>Apnea Hypopnea Index</source>
        <translation>Index apnées hypopnées</translation>
    </message>
    <message>
        <source>Pt. Access</source>
        <translation>Accès pat.</translation>
    </message>
    <message>
        <source>CMS50F</source>
        <translation>CMS50F</translation>
    </message>
    <message>
        <source>ASV (Fixed EPAP)</source>
        <translation>ASV (EPAP fixe)</translation>
    </message>
    <message>
        <source>Patient Triggered Breaths</source>
        <translation>Respirations activées par le patient</translation>
    </message>
    <message>
        <source>This means you will need to import this machine data again afterwards from your own backups or data card.</source>
        <translation>Ce qui signifie que vous allez devoir réimporter les données par la suite à partir de vos propres sauvegardes.</translation>
    </message>
    <message>
        <source>Contec</source>
        <translation>Contec</translation>
    </message>
    <message>
        <source>Events</source>
        <translation>Évènements</translation>
    </message>
    <message>
        <source>Humid. Level</source>
        <translation>Niv. humidité</translation>
    </message>
    <message>
        <source>AB Filter</source>
        <translation>Filtre AB</translation>
    </message>
    <message>
        <source>%1
Line %2, column %3</source>
        <translation>%1
Ligne %2, colonne %3</translation>
    </message>
    <message>
        <source>Ramp Enable</source>
        <translation>Rampe active</translation>
    </message>
    <message>
        <source>(% %1 in events)</source>
        <translation>(% %1 en évènements)</translation>
    </message>
    <message>
        <source>Lower Threshold</source>
        <translation>Seuil le plus bas</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation>Pas de données</translation>
    </message>
    <message>
        <source>Page %1 of %2</source>
        <translation>Page %1 sur %2</translation>
    </message>
    <message>
        <source>Litres</source>
        <translation>Litres</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation>Manuel</translation>
    </message>
    <message>
        <source>Median</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <source>Fixed %1 (%2)</source>
        <translation>%1 Fixe (%2)</translation>
    </message>
    <message>
        <source>Min %1</source>
        <translation>%1 min</translation>
    </message>
    <message>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>Windows Explorer n&apos;a pas été trouvé dans le chemin indiqué.</translation>
    </message>
    <message>
        <source>Machine automatically switches off</source>
        <translation>Arrêt automatique de l&apos;appareil</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation>Connecté</translation>
    </message>
    <message>
        <source>Low Usage Days: %1</source>
        <translation>Jours de faible usage: %1</translation>
    </message>
    <message>
        <source>PS Max</source>
        <translation>PS max</translation>
    </message>
    <message>
        <source>PS Min</source>
        <translation>PS mini</translation>
    </message>
    <message>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation>Base de données périmée
Merci de reconstruire les données de PPC</translation>
    </message>
    <message>
        <source>Flow Limit.</source>
        <translation>Limitation de flux</translation>
    </message>
    <message>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation>Fuites détectées incluant les fuites naturelles du masque</translation>
    </message>
    <message>
        <source>Plethy</source>
        <translation>Pléthy</translation>
    </message>
    <message>
        <source>SensAwake</source>
        <translation>SensAwake</translation>
    </message>
    <message>
        <source>ST/ASV</source>
        <translation>ST/ASV</translation>
    </message>
    <message>
        <source>Median Leaks</source>
        <translation>Fuites moyennes</translation>
    </message>
    <message>
        <source>%1 Report</source>
        <translation>Rapport %1</translation>
    </message>
    <message>
        <source>ResMed</source>
        <translation>ResMed</translation>
    </message>
    <message>
        <source>Pr. Relief</source>
        <translation>Restants de pression</translation>
    </message>
    <message>
        <source>Graphs Switched Off</source>
        <translation>Graphiques désactivés</translation>
    </message>
    <message>
        <source>Serial</source>
        <translation>Numéro de série</translation>
    </message>
    <message>
        <source>Series</source>
        <translation>Séries</translation>
    </message>
    <message>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <source>VPAP-S</source>
        <translation>VPAP-S</translation>
    </message>
    <message>
        <source>VPAP-T</source>
        <translation>VPAP-T</translation>
    </message>
    <message>
        <source>(last night)</source>
        <translation>(la nuit dernière)</translation>
    </message>
    <message>
        <source>AHI	%1
</source>
        <translation>IAH	%1
</translation>
    </message>
    <message>
        <source>Weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <source>PRS1 pressure relief setting.</source>
        <translation>Réglage de dépression PRS1.</translation>
    </message>
    <message>
        <source>Orientation</source>
        <translation>Orientation</translation>
    </message>
    <message>
        <source>Smart Start</source>
        <translation>SmartStart</translation>
    </message>
    <message>
        <source>Event Flags</source>
        <translation>Évènements</translation>
    </message>
    <message>
        <source>A few breaths automatically starts machine</source>
        <translation>Mise en marche par respiration</translation>
    </message>
    <message>
        <source>Zombie</source>
        <translation>Zombie</translation>
    </message>
    <message>
        <source>C-Flex+</source>
        <translation>C-Flex+</translation>
    </message>
    <message>
        <source>Bookmarks</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>PAP Mode</source>
        <translation>Mode PAP</translation>
    </message>
    <message>
        <source>CPAP Mode</source>
        <translation>Mode PPC</translation>
    </message>
    <message>
        <source>SmartFlex Settings</source>
        <translation>Réglages SmartFlex</translation>
    </message>
    <message>
        <source>An apnea where the airway is open</source>
        <translation>Apnée avec passage de l&apos;air ouvert</translation>
    </message>
    <message>
        <source>Flow Limitation</source>
        <translation>Limitation du débit</translation>
    </message>
    <message>
        <source>Pin %1 Graph</source>
        <translation>Attacher le graphique %1</translation>
    </message>
    <message>
        <source>Unpin %1 Graph</source>
        <translation>Détacher le graphique %1</translation>
    </message>
    <message>
        <source>Hours: %1h, %2m, %3s</source>
        <translation>Heures : %1h, %2m, %3s</translation>
    </message>
    <message>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
Longueur : %3
Début : %2</translation>
    </message>
    <message>
        <source>CMS50F3.7</source>
        <translation>CMS50F3.7</translation>
    </message>
    <message>
        <source>RDI	%1
</source>
        <translation>IRD	%1
</translation>
    </message>
    <message>
        <source>ASVAuto</source>
        <translation>ASV Auto</translation>
    </message>
    <message>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation>PS %1 sur %2-%3 (%4)</translation>
    </message>
    <message>
        <source>Flow Rate</source>
        <translation>Débit</translation>
    </message>
    <message>
        <source>Time taken to breathe out</source>
        <translation>Temps pris pour expirer</translation>
    </message>
    <message>
        <source>Important:</source>
        <translation>Important :</translation>
    </message>
    <message>
        <source>Machine auto starts by breathing</source>
        <translation>Mise en marche par respiration</translation>
    </message>
    <message>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation>Une photo pléthysmogramme optique montrant le rythme cardiaque</translation>
    </message>
    <message>
        <source>Pillows</source>
        <translation>Coussinets</translation>
    </message>
    <message>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
Longueur : %3
Début : %2
</translation>
    </message>
    <message>
        <source>I:E Ratio</source>
        <translation>Ratio Inspiration/Expiration</translation>
    </message>
    <message>
        <source>Amount of air displaced per breath</source>
        <translation>Quantité d&apos;air déplacé par respiration</translation>
    </message>
    <message>
        <source>Pat. Trig. Breaths</source>
        <translation>Resp. act. pat.</translation>
    </message>
    <message>
        <source>% in %1</source>
        <translation>% en %1</translation>
    </message>
    <message>
        <source>Humidity Level</source>
        <translation>Niveau humidité</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <source>Leak Flag</source>
        <translation>Marqueur de fuite</translation>
    </message>
    <message>
        <source>Leak Rate</source>
        <translation>Vitesse de fuite</translation>
    </message>
    <message>
        <source>ClimateLine Temperature Enable</source>
        <translation>Température ClimateLine active</translation>
    </message>
    <message>
        <source>Severity (0-1)</source>
        <translation>Gravité (0-1)</translation>
    </message>
    <message>
        <source>Reporting from %1 to %2</source>
        <translation>Rapport du %1 au %2</translation>
    </message>
    <message>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation>Voulez-vous vraiment réinitialiser les préférences des graphiques (couleur des chaînes et réglages) aux valeurs par défaut ?</translation>
    </message>
    <message>
        <source>Inspiratory Pressure</source>
        <translation>Pression d&apos;inspiration</translation>
    </message>
    <message>
        <source>Whether or not machine allows Mask checking.</source>
        <translation>Selon que l&apos;appareil permet la vérification du masque.</translation>
    </message>
    <message>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation>Impulsion de pression envoyée pour détecter une obstruction.</translation>
    </message>
    <message>
        <source>Intellipap pressure relief level.</source>
        <translation>Niveau de dépression Intellipap.</translation>
    </message>
    <message>
        <source>CMS50D+</source>
        <translation>CMS50D+</translation>
    </message>
    <message>
        <source>Non Responding Event</source>
        <translation>Évènement ne répondant pas</translation>
    </message>
    <message>
        <source>Median Leak Rate</source>
        <translation>Volume moyen de fuite</translation>
    </message>
    <message>
        <source> (%3 sec)</source>
        <translation> (%3 sec)</translation>
    </message>
    <message>
        <source>Rate of breaths per minute</source>
        <translation>Respirations par minute</translation>
    </message>
    <message>
        <source>Usage Statistics</source>
        <translation>Statistiques d&apos;utilisation</translation>
    </message>
    <message>
        <source>Perfusion Index</source>
        <translation>Index de perfusion</translation>
    </message>
    <message>
        <source>Graph displaying snore volume</source>
        <translation>Volume du ronflement</translation>
    </message>
    <message>
        <source>Mask Off</source>
        <translation>Sans masque</translation>
    </message>
    <message>
        <source>Max EPAP</source>
        <translation>EPAP max</translation>
    </message>
    <message>
        <source>Max IPAP</source>
        <translation>IPAP max</translation>
    </message>
    <message>
        <source>Bedtime</source>
        <translation>Heure du coucher</translation>
    </message>
    <message>
        <source>Bi-Flex</source>
        <translation>Bi-Flex</translation>
    </message>
    <message>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation>EPAP %1 IPAP %2 (%3)</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation>Pression</translation>
    </message>
    <message>
        <source>Auto On</source>
        <translation>Auto On</translation>
    </message>
    <message>
        <source>Average</source>
        <translation>Moyen</translation>
    </message>
    <message>
        <source>Target Minute Ventilation</source>
        <translation>Ventilation cible par minute</translation>
    </message>
    <message>
        <source>Amount of air displaced per minute</source>
        <translation>Quantité d&apos;air déplacé par minute</translation>
    </message>
    <message>
        <source>
TTIA: %1</source>
        <translation>
TTIA : %1</translation>
    </message>
    <message>
        <source>Percentage of breaths triggered by patient</source>
        <translation>Pourcentage de respirations activées par le patient</translation>
    </message>
    <message>
        <source>Non Data Capable Machine</source>
        <translation>Appareil sans données</translation>
    </message>
    <message>
        <source>Days: %1</source>
        <translation>Jours : %1</translation>
    </message>
    <message>
        <source>Plethysomogram</source>
        <translation>Pléthysmogramme</translation>
    </message>
    <message>
        <source>Unclassified Apnea</source>
        <translation>Apnées non classifiées</translation>
    </message>
    <message>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation>Auto Bi-Level (PS fixe)</translation>
    </message>
    <message>
        <source>Please Note</source>
        <translation>Merci de noter que</translation>
    </message>
    <message>
        <source>Starting Ramp Pressure</source>
        <translation>Pression de départ de la rampe</translation>
    </message>
    <message>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation>Évènement Intellipap d&apos;expiration par la bouche.</translation>
    </message>
    <message>
        <source>ASV (Variable EPAP)</source>
        <translation>ASV (EPAP variable)</translation>
    </message>
    <message>
        <source>Exhale Pressure Relief Level</source>
        <translation>Niveau de dépression d&apos;expiration</translation>
    </message>
    <message>
        <source>Flow Limit</source>
        <translation>Limitation du flux</translation>
    </message>
    <message>
        <source>UAI=%1 </source>
        <translation>UAI = %1 </translation>
    </message>
    <message>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 faible utilisation, %2 pas d&apos;utilisation, sur %3 jours (%4% compatible) Durée : %5 / %6 / %7</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <source>Pulse Rate</source>
        <translation>Pouls</translation>
    </message>
    <message>
        <source>Rise Time</source>
        <translation>Montée temporisée</translation>
    </message>
    <message>
        <source>Cheyne Stokes Respiration</source>
        <translation>Respiration de Cheyne-Stokes</translation>
    </message>
    <message>
        <source>SmartStart</source>
        <translation>SmartStart</translation>
    </message>
    <message>
        <source>Graph showing running AHI for the past hour</source>
        <translation>IAH pour les heures précédentes</translation>
    </message>
    <message>
        <source>Graph showing running RDI for the past hour</source>
        <translation>Graphique des troubles respiratoires pour les dernières heures</translation>
    </message>
    <message>
        <source>Temperature Enable</source>
        <translation>Niv. Temp. activé</translation>
    </message>
    <message>
        <source>Could not parse Updates.xml file.</source>
        <translation>Impossible de traiter le fichier Updates.xml.</translation>
    </message>
    <message>
        <source>Sorry, your Philips Respironics CPAP machine (Model %1) is not supported yet.</source>
        <translation>Désolé votre appareil Philips Respironics (Model %1) n&apos;est pas pris en charge pour le moment.</translation>
    </message>
    <message>
        <source>Seconds</source>
        <translation>secondes</translation>
    </message>
    <message>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2 jours): </translation>
    </message>
    <message>
        <source>Snapshot %1</source>
        <translation>Instantané %1</translation>
    </message>
    <message>
        <source>Mask Time</source>
        <translation>Utilisation du masque</translation>
    </message>
    <message>
        <source>Your Philips Respironics CPAP machine (Model %1) is unfortunately not a data capable model.</source>
        <translation>Votre Philips Respironics (Model %1) n&apos;est malheureusement pas un modèle produisant des données.</translation>
    </message>
    <message>
        <source>Channel</source>
        <translation>Chaîne</translation>
    </message>
    <message>
        <source>Auto for Her</source>
        <translation>Auto pour Elle</translation>
    </message>
    <message>
        <source>Obstructive</source>
        <translation>Apnées obstructives</translation>
    </message>
    <message>
        <source>Pressure Max</source>
        <translation>Pression max</translation>
    </message>
    <message>
        <source>Pressure Min</source>
        <translation>Pression mini</translation>
    </message>
    <message>
        <source>Diameter of primary CPAP hose</source>
        <translation>Diamètre du tuyau principal de PPC</translation>
    </message>
    <message>
        <source>Max Leaks</source>
        <translation>Fuites max</translation>
    </message>
    <message>
        <source>Flex Level</source>
        <translation>Niveau de Flex</translation>
    </message>
    <message>
        <source>Respiratory Effort Related Arousal: An restriction in breathing that causes an either an awakening or sleep disturbance.</source>
        <translation>Gêne respiratoire qui cause un réveil ou un trouble du sommeil.</translation>
    </message>
    <message>
        <source>Humid. Status</source>
        <translation>État humidif.</translation>
    </message>
    <message>
        <source>(Sess: %1)</source>
        <translation>(Sess : %1)</translation>
    </message>
    <message>
        <source>Climate Control</source>
        <translation>Contrôle du climat</translation>
    </message>
    <message>
        <source>Perf. Index %</source>
        <translation>% Index de perfusion</translation>
    </message>
    <message>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation>Si vos anciennes données manquent, copier le contenu de tous les autres répertoires de journaux_XXXX manuellement dans celui-ci.</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation>Mini EPAP %1 Max IPAP %2 PS %3-%4 (%5)</translation>
    </message>
    <message>
        <source>System One</source>
        <translation>System One</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Par défaut</translation>
    </message>
    <message>
        <source>Breaths/min</source>
        <translation>Resp./min</translation>
    </message>
    <message>
        <source>Degrees</source>
        <translation>Degrés</translation>
    </message>
    <message>
        <source>&amp;Destroy</source>
        <translation>&amp;Détruire</translation>
    </message>
    <message>
        <source>%1 %2 / %3 / %4</source>
        <translation>%1 %2 / %3 / %4</translation>
    </message>
    <message>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation>Il y a un fichier de verrouillage déjà présent pour ce profil &apos;%1&apos;, demandé sur &apos;%2&apos;.</translation>
    </message>
    <message>
        <source>User Flag #1</source>
        <translation>Évènement utilisateur #1</translation>
    </message>
    <message>
        <source>User Flag #2</source>
        <translation>Évènement utilisateur #2</translation>
    </message>
    <message>
        <source>User Flag #3</source>
        <translation>Évènement utilisateur #3</translation>
    </message>
    <message>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%</translation>
    </message>
    <message>
        <source>Median rate of detected mask leakage</source>
        <translation>Volume moyen des fuites du masque</translation>
    </message>
    <message>
        <source>PAP Device Mode</source>
        <translation>PAP mode matériel</translation>
    </message>
    <message>
        <source>Mask Pressure</source>
        <translation>Pression du masque</translation>
    </message>
    <message>
        <source>A vibratory snore as detcted by a System One machine</source>
        <translation>Ronflement vibratoire détecté par un appareil SystemOne</translation>
    </message>
    <message>
        <source>Sorry, your %1 %2 machine is not currently supported.</source>
        <translation>Désolé, votre appareil %1 %2 n&apos;est pas supporté.</translation>
    </message>
    <message>
        <source>Respiratory Event</source>
        <translation>Évènement respiratoire</translation>
    </message>
    <message>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>Période anormale de respiration périodique/Cheyne-Stokes</translation>
    </message>
    <message>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation>Évènement respiratoire ne répondant pas à l&apos;augmentation de la pression.</translation>
    </message>
    <message>
        <source>Antibacterial Filter</source>
        <translation>Filtre antibactérien</translation>
    </message>
    <message>
        <source>Windows User</source>
        <translation>Utilisateur Windows</translation>
    </message>
    <message>
        <source>Question</source>
        <translation>Question</translation>
    </message>
    <message>
        <source>Waketime: %1</source>
        <translation>Réveil : %1</translation>
    </message>
    <message>
        <source>Higher Inspiratory Pressure</source>
        <translation>Pression d&apos;inspiration la plus haute</translation>
    </message>
    <message>
        <source>Weinmann</source>
        <translation>Weinmann</translation>
    </message>
    <message>
        <source>Don&apos;t forget to place your datacard back in your CPAP machine</source>
        <translation>N&apos;oubliez pas de remettre la carte SD dans votre appareil</translation>
    </message>
    <message>
        <source>The machine data folder needs to be removed manually.</source>
        <translation>Le répertroire de données de l&apos;appareil doit être effacé manuellement.</translation>
    </message>
    <message>
        <source>Summary Only</source>
        <translation>Résumé seulement</translation>
    </message>
    <message>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <source>Intellipap</source>
        <translation>Intellipap</translation>
    </message>
    <message>
        <source>&lt;i&gt;Your old machine data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Vos anciennes données seront restaurées si la sauvegarde n&apos;a pas été désactivée dans les préférences d&apos;import des données&lt;i&gt;</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>Inconnue</translation>
    </message>
    <message>
        <source>Events/hr</source>
        <translation>Évènements/Heure</translation>
    </message>
    <message>
        <source>PRS1 humidifier connected?</source>
        <translation>Humidificateur PRS1 connecté ?</translation>
    </message>
    <message>
        <source>CPAP Session contains summary data only</source>
        <translation>La session PPC ne contient qu&apos;un résumé</translation>
    </message>
    <message>
        <source>Fisher &amp; Paykel</source>
        <translation>Fisher &amp; Paykel</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <source>
Hours: %1</source>
        <translation>
Heures : %1</translation>
    </message>
    <message>
        <source>Flex Mode</source>
        <translation>Mode Flex</translation>
    </message>
    <message>
        <source>Sessions</source>
        <translation>Sessions</translation>
    </message>
    <message>
        <source>Auto Off</source>
        <translation>Auto Off</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Aperçus</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation>Température</translation>
    </message>
    <message>
        <source>Patient Access</source>
        <translation>Accès patient</translation>
    </message>
    <message>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation>Flux du jour entier</translation>
    </message>
    <message>
        <source>Exiting</source>
        <translation>Arrêt en cours</translation>
    </message>
    <message>
        <source>DeVilbiss</source>
        <translation>DeVilbiss</translation>
    </message>
    <message>
        <source>Fixed Bi-Level</source>
        <translation>Bi-Level fixe</translation>
    </message>
    <message>
        <source>Machine Information</source>
        <translation>Informations de l&apos;appareil</translation>
    </message>
    <message>
        <source>Pressure Support Maximum</source>
        <translation>Pression maximum supportée</translation>
    </message>
    <message>
        <source>Graph showing severity of flow limitations</source>
        <translation>Sévérité des limitations de flux</translation>
    </message>
    <message>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation>: %1 heure(s); %2 minute(s), %3 seconde(s)
</translation>
    </message>
    <message>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation>Bi-Level Auto (Pres. variable)</translation>
    </message>
    <message>
        <source>Mask Alert</source>
        <translation>Alerte du masque</translation>
    </message>
    <message>
        <source>A partially obstructed airway</source>
        <translation>Passage de l&apos;air partiellement obstrué</translation>
    </message>
    <message>
        <source>Pressure Support Minimum</source>
        <translation>Pression minimum supportée</translation>
    </message>
    <message>
        <source>Large Leak</source>
        <translation>Grosse fuite</translation>
    </message>
    <message>
        <source>Time started according to str.edf</source>
        <translation>Heure de départ selon str.edf</translation>
    </message>
    <message>
        <source>Wake-up</source>
        <translation>Réveil</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Alerte</translation>
    </message>
    <message>
        <source>Min Pressure</source>
        <translation>Pression min</translation>
    </message>
    <message>
        <source>Total Leak Rate</source>
        <translation>Total des fuites</translation>
    </message>
    <message>
        <source>Max Pressure</source>
        <translation>Pression maximum</translation>
    </message>
    <message>
        <source>MaskPressure</source>
        <translation>Pression du masque</translation>
    </message>
    <message>
        <source>Duration %1:%2:%3</source>
        <translation>Durée %1:%2:%3</translation>
    </message>
    <message>
        <source>Upper Threshold</source>
        <translation>Seuil le plus haut</translation>
    </message>
    <message>
        <source>Total Leaks</source>
        <translation>Total fuites</translation>
    </message>
    <message>
        <source>Minute Ventilation</source>
        <translation>Ventilation par minute</translation>
    </message>
    <message>
        <source>Rate of detected mask leakage</source>
        <translation>Vitesse de fuite du masque</translation>
    </message>
    <message>
        <source>Breathing flow rate waveform</source>
        <translation>Courbe du flux respiratoire</translation>
    </message>
    <message>
        <source>Lower Expiratory Pressure</source>
        <translation>Pression d&apos;expiration la plus basse</translation>
    </message>
    <message>
        <source>Min %1 Max %2 (%3)</source>
        <translation>Min %1 Max %2 (%3)</translation>
    </message>
    <message>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation>AI=%1 HI=%2 CAI=%3 </translation>
    </message>
    <message>
        <source>Time taken to breathe in</source>
        <translation>Temps pris pour inspirer</translation>
    </message>
    <message>
        <source>Maximum Therapy Pressure</source>
        <translation>Pression de thérapie maximum</translation>
    </message>
    <message>
        <source>Are you sure you want to use this folder?</source>
        <translation>Êtes-vous sûr de vouloir utiliser ce répertoire ?</translation>
    </message>
    <message>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 jour) : </translation>
    </message>
    <message>
        <source>Current Selection</source>
        <translation>Sélection courante</translation>
    </message>
    <message>
        <source>Blood-oxygen saturation percentage</source>
        <translation>% de saturation du sang en oxygène</translation>
    </message>
    <message>
        <source>Inspiratory Time</source>
        <translation>Temps d&apos;inspiration</translation>
    </message>
    <message>
        <source>Respiratory Rate</source>
        <translation>Vitesse respiratoire</translation>
    </message>
    <message>
        <source>Hide All Events</source>
        <translation>Cacher les évènements</translation>
    </message>
    <message>
        <source>Printing %1 Report</source>
        <translation>Impression du rapport %1</translation>
    </message>
    <message>
        <source>Expiratory Time</source>
        <translation>Temps d&apos;expiration</translation>
    </message>
    <message>
        <source>Expiratory Puff</source>
        <translation>Bouffée expiratoire</translation>
    </message>
    <message>
        <source>Maximum Leak</source>
        <translation>Fuite maximum</translation>
    </message>
    <message>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation>Ratio entre temps d&apos;inspiration et d&apos;expiration</translation>
    </message>
    <message>
        <source>APAP (Variable)</source>
        <translation>APAP (Variable)</translation>
    </message>
    <message>
        <source>Minimum Therapy Pressure</source>
        <translation>Pression de thérapie minimum</translation>
    </message>
    <message>
        <source>A sudden (user definable) change in heart rate</source>
        <translation>Changement soudain de pouls (définissable par l&apos;utilisateur)</translation>
    </message>
    <message>
        <source>Oximetry</source>
        <translation>Oxymétrie</translation>
    </message>
    <message>
        <source>Oximeter</source>
        <translation>Oxymètre</translation>
    </message>
    <message>
        <source>No Data Available</source>
        <translation>Aucune donnée disponible</translation>
    </message>
    <message>
        <source>The maximum rate of mask leakage</source>
        <translation>La vitesse maximum de fuite du masque</translation>
    </message>
    <message>
        <source>Humidifier Status</source>
        <translation>État de l&apos;humidificateur</translation>
    </message>
    <message>
        <source>Machine Initiated Breath</source>
        <translation>Respiration provoquée par l&apos;appareil</translation>
    </message>
    <message>
        <source>Machine Database Changes</source>
        <translation>La base de données de l&apos;appareil a changé</translation>
    </message>
    <message>
        <source>SmartFlex Mode</source>
        <translation>Mode SmartFlex</translation>
    </message>
    <message>
        <source> (%2 min, %3 sec)</source>
        <translation> (%2 min, %3 sec)</translation>
    </message>
    <message>
        <source>Expiratory Pressure</source>
        <translation>Pression d&apos;expiration</translation>
    </message>
    <message>
        <source>Show AHI</source>
        <translation>Affiche l&apos;IAH</translation>
    </message>
    <message>
        <source>Tgt. Min. Vent</source>
        <translation>Vent. act. min</translation>
    </message>
    <message>
        <source>Rebuilding from %1 Backup</source>
        <translation>Reconstruction de la sauvegarde de %1</translation>
    </message>
    <message>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation>Êtes-vous sûr de vouloir remettre à zéro tous vos réglages ?</translation>
    </message>
    <message>
        <source>Pressure Pulse</source>
        <translation>Pression d&apos;impulsion</translation>
    </message>
    <message>
        <source>ChoiceMMed</source>
        <translation>ChoiceMMed</translation>
    </message>
    <message>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>Sessions : %1/ %2 / %3 Longueur :%4 / %5 / %6 Plus long : %7 / %8 / %9</translation>
    </message>
    <message>
        <source>Humidifier</source>
        <translation>Humidificateur</translation>
    </message>
    <message>
        <source>Compliance Only :(</source>
        <translation>Conformité seulement :(</translation>
    </message>
    <message>
        <source>Relief: %1</source>
        <translation>Résidus : %1</translation>
    </message>
    <message>
        <source>Patient ID</source>
        <translation>Identifiant du patient</translation>
    </message>
    <message>
        <source>Patient???</source>
        <translation>Patient ???</translation>
    </message>
    <message>
        <source>An apnea caused by airway obstruction</source>
        <translation>Apnée causée par une obstruction du passage de l&apos;air</translation>
    </message>
    <message>
        <source>Vibratory Snore (VS2) </source>
        <translation>Ronflement vibratoire (VS2) </translation>
    </message>
    <message>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <source>Height</source>
        <translation>Taille</translation>
    </message>
    <message>
        <source>Physical Height</source>
        <translation>Taille physique</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>Bookmark Notes</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>Body Mass Index</source>
        <translation>Indice
de masse
corporelle</translation>
    </message>
    <message>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation>Comment vous sentez-vous (0=un vrai débri; 10=inarrêtable)</translation>
    </message>
    <message>
        <source>Bookmark Start</source>
        <translation>Début des Favoris</translation>
    </message>
    <message>
        <source>Bookmark End</source>
        <translation>Fin des Favoris</translation>
    </message>
    <message>
        <source>Last Updated</source>
        <translation>Dernière mise à jour</translation>
    </message>
    <message>
        <source>Journal Notes</source>
        <translation>Journal</translation>
    </message>
    <message>
        <source>Journal</source>
        <translation>Journal</translation>
    </message>
    <message>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation>1=Éveil 2=Endormissement 3=Sommeil léger 4=Sommeil lourd</translation>
    </message>
    <message>
        <source>Brain Wave</source>
        <translation>Ondes cérébrales</translation>
    </message>
    <message>
        <source>BrainWave</source>
        <translation>Ondes cérébrales</translation>
    </message>
    <message>
        <source>Awakenings</source>
        <translation>Réveil en cours</translation>
    </message>
    <message>
        <source>Number of Awakenings</source>
        <translation>Nombre d&apos;éveils</translation>
    </message>
    <message>
        <source>Morning Feel</source>
        <translation>Sensations du matin</translation>
    </message>
    <message>
        <source>How you felt in the morning</source>
        <translation>Comment vous sentiez-vous le matin</translation>
    </message>
    <message>
        <source>Time Awake</source>
        <translation>Durée d&apos;éveil</translation>
    </message>
    <message>
        <source>Time spent awake</source>
        <translation>Durée passée en éveil</translation>
    </message>
    <message>
        <source>Time In REM Sleep</source>
        <translation>Durée en endormissement</translation>
    </message>
    <message>
        <source>Time spent in REM Sleep</source>
        <translation>Durée passée en endormissement</translation>
    </message>
    <message>
        <source>Time in REM Sleep</source>
        <translation>Durée en endormissement</translation>
    </message>
    <message>
        <source>Time In Light Sleep</source>
        <translation>Durée en sommeil léger</translation>
    </message>
    <message>
        <source>Time spent in light sleep</source>
        <translation>Durée passée en sommeil léger</translation>
    </message>
    <message>
        <source>Time in Light Sleep</source>
        <translation>Durée en sommeil léger</translation>
    </message>
    <message>
        <source>Time In Deep Sleep</source>
        <translation>Durée en sommeil profond</translation>
    </message>
    <message>
        <source>Time spent in deep sleep</source>
        <translation>Durée passée en sommeil profond</translation>
    </message>
    <message>
        <source>Time in Deep Sleep</source>
        <translation>Durée en sommeil profond</translation>
    </message>
    <message>
        <source>Time to Sleep</source>
        <translation>Durée d&apos;endormissement</translation>
    </message>
    <message>
        <source>Time taken to get to sleep</source>
        <translation>Durée d&apos;endormissement</translation>
    </message>
    <message>
        <source>Zeo ZQ</source>
        <translation>Zeo ZQ</translation>
    </message>
    <message>
        <source>Zeo sleep quality measurement</source>
        <translation>Mesure de qualité d&apos;endormissement ZEO</translation>
    </message>
    <message>
        <source>ZEO ZQ</source>
        <translation>Zeo ZQ</translation>
    </message>
    <message>
        <source>Pop out Graph</source>
        <translation>Graphique</translation>
    </message>
    <message>
        <source>Popout %1 Graph</source>
        <translation>Graphique %1</translation>
    </message>
    <message>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <source> cm</source>
        <translation> cm</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <source>Getting Ready...</source>
        <translation>Préparation...</translation>
    </message>
    <message>
        <source>Scanning Files...</source>
        <translation>Lecture des fichiers...</translation>
    </message>
    <message>
        <source>Importing Sessions...</source>
        <translation>Import des sessions...</translation>
    </message>
    <message>
        <source>Finishing up...</source>
        <translation>Finalisation...</translation>
    </message>
    <message>
        <source>Breathing Not Detected</source>
        <translation>Pas de respiration</translation>
    </message>
    <message>
        <source>A period during a session where the machine could not detect flow.</source>
        <translation>Période pendant une session sans flux détectée.</translation>
    </message>
    <message>
        <source>BND</source>
        <translation>BND</translation>
    </message>
    <message>
        <source>Locating STR.edf File(s)...</source>
        <translation>Localisation des fichiers STR.edf...</translation>
    </message>
    <message>
        <source>Cataloguing EDF Files...</source>
        <translation>Catalogage des fichiers EDF...</translation>
    </message>
    <message>
        <source>Queueing Import Tasks...</source>
        <translation>Mise en liste de tâche d&apos;import...</translation>
    </message>
    <message>
        <source>Finishing Up...</source>
        <translation>Finalisation...</translation>
    </message>
    <message>
        <source>Loading %1 data for %2...</source>
        <translation>Chargement des données %1 pour %2...</translation>
    </message>
    <message>
        <source>Scanning Files</source>
        <translation>Lecture des fichiers</translation>
    </message>
    <message>
        <source>Migrating Summary File Location</source>
        <translation>Déplacement de fichiers récapitulatifs</translation>
    </message>
    <message>
        <source>Loading Summaries.xml.gz</source>
        <translation>Chargement de Summaries.xml.gz</translation>
    </message>
    <message>
        <source>Loading Summary Data</source>
        <translation>Chargement des données</translation>
    </message>
    <message>
        <source>Please Wait...</source>
        <translation>Veuillez patienter...</translation>
    </message>
    <message>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation>Chargement du profil &quot;%1&quot;...</translation>
    </message>
    <message>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation>Données d&apos;oxymétrie les plus récentes :&lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </translation>
    </message>
    <message>
        <source>No oximetry data has been imported yet.</source>
        <translation>Pas de données d&apos;oxymétrie importées pour le moment.</translation>
    </message>
    <message>
        <source>Software Engine</source>
        <translation>Moteur logiciel</translation>
    </message>
    <message>
        <source>ANGLE / OpenGLES</source>
        <translation>ANGLE / OpenGLES</translation>
    </message>
    <message>
        <source>Desktop OpenGL</source>
        <translation>Desktop OpenGL</translation>
    </message>
    <message>
        <source>Peak %1</source>
        <translation>Pic %1</translation>
    </message>
    <message>
        <source>I&apos;m very sorry your machine doesn&apos;t record useful data to graph in Daily View :(</source>
        <translation>Désolé votre appareil n&apos;a pas enregistré de données utilisables pour la vue quotidienne :(</translation>
    </message>
    <message>
        <source>There is no data to graph</source>
        <translation>Pas de données pour les graphiques</translation>
    </message>
    <message>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation>OSCAR a trouvé un vieux répertoire du journal, mais il semble avoir été renommé :</translation>
    </message>
    <message>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation>OSCAR ne va pas toucher à ce répertoire et va en créer un nouveau.</translation>
    </message>
    <message>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation>Merci d&apos;être prudent en jouant avec les répertoires de profils :-P</translation>
    </message>
    <message>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation>OSCAR n&apos;a pas trouvé d&apos;enregistrement de journal dans votre profil, mais plusieurs répertoires de journaux.

</translation>
    </message>
    <message>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation>OSCAR a sélectionné le premier et continuera de l&apos;utiliser:

</translation>
    </message>
    <message>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this machine.</source>
        <translation>Désolé, OSCAR ne peut suivre que les heures d&apos;utilisation et quelques informations de bases pour cet appareil.</translation>
    </message>
    <message>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation>&lt;b&gt;OSCAR maintien une copie de la carte qu&apos;il utilise dans cette optique&lt;/b&gt;</translation>
    </message>
    <message>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation>OSCAR ne fait pas de sauvegarde automatique de la carte SD pour ce matériel.</translation>
    </message>
    <message>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation>Si cela vous pose un souci, cliquez sur Non pour sortir, sauvegardez le profil manuellement avant de relancer OSCAR.</translation>
    </message>
    <message>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation>Êtes-vous prêt pour upgrader afin d&apos;utiliser la nouvelle version de OSCAR ?</translation>
    </message>
    <message>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation>Désolé la purge à échoué. Cette version de OSCAR ne peut démarrer.</translation>
    </message>
    <message>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation>Voulez-vous passer en sauvegarde automatique, ainsi la prochaine fois qu&apos;une nouvelle version de OSCAR doit le faire, elle pourra s&apos;en servir ?</translation>
    </message>
    <message>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation>OSCAR va lancer l&apos;assistant d&apos;import pour réinstaller les données de votre %1.</translation>
    </message>
    <message>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation>OSCAR va quitter, ensuite lancez votre gestionnaire de fichiers pour faire une copie de votre profil :</translation>
    </message>
    <message>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation>Utilisez votre gestionnaire de fichiers pour faire une copie de votre répertoire du profil puis relancez OSCAR pour terminer la mise à jour.</translation>
    </message>
    <message>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation>Évènement définissable par l&apos;utilisateur détecté par le moteur d&apos;analyse de flux de OSCAR.</translation>
    </message>
    <message>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation>Comme vous n&apos;avez pas sélectionné de répertoire OSCAR va quitter.</translation>
    </message>
    <message>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation>Le répertoire n&apos;est pas vide et ne contient pas de données de OSCAR valides.</translation>
    </message>
    <message>
        <source>OSCAR Reminder</source>
        <translation>Rappel de OSCAR</translation>
    </message>
    <message>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation>Vous ne pouvez travailler qu&apos;avec un seul profil OSCAR à la fois.</translation>
    </message>
    <message>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation>Si vous utilisez un stockage en cloud, assurez-vous que OSCAR est fermé et que la synchronisation est terminée sur l&apos;autre ordinateur avant de poursuivre.</translation>
    </message>
    <message>
        <source>You must run the OSCAR Migration Tool</source>
        <translation>Vous devez utiliser l&apos;outil de migration vers OSCAR</translation>
    </message>
    <message>
        <source>Using </source>
        <translation>Utilise </translation>
    </message>
    <message>
        <source>, found SleepyHead -
</source>
        <translation>, SleepyHead trouvé -
</translation>
    </message>
    <message>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation>Apnée ne pouvant être détérminée comme Centrale ou Obstructive.</translation>
    </message>
    <message>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation>Restriction respiratoire anormale causant un applatissement de l&apos;onde de flux.</translation>
    </message>
    <message>
        <source>Choose the SleepyHead data folder to migrate</source>
        <translation>Choisissez le répertoire de SleepyHead pour la migration</translation>
    </message>
    <message>
        <source>or CANCEL to skip migration.</source>
        <translation>ou choisir ANNULER pour ne pas migrer.</translation>
    </message>
    <message>
        <source>The folder you chose does not contain valid SleepyHead data.</source>
        <translation>Le répertoire ne contient pas de données SleepyHead valides.</translation>
    </message>
    <message>
        <source>You cannot use this folder:</source>
        <translation>Vous ne pouvez pas utiliser ce répertoire :</translation>
    </message>
    <message>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation>Choisissez ou créez un nouveau répertoire pour les données de OSCAR</translation>
    </message>
    <message>
        <source>App key:</source>
        <translation>Clef de l&apos;application :</translation>
    </message>
    <message>
        <source>Operating system:</source>
        <translation>Système d&apos;exploitation :</translation>
    </message>
    <message>
        <source>Graphics Engine:</source>
        <translation>Moteur graphique :</translation>
    </message>
    <message>
        <source>Graphics Engine type:</source>
        <translation>Type de moteur graphique :</translation>
    </message>
    <message>
        <source>The developers needs a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make it work with OSCAR.</source>
        <translation>Les développeurs ont besoin d&apos;une copie zippée de la carte SD de cet appareil et d&apos;un rapport de type Encore.pdf pour le faire fonctionner avec OSCAR.</translation>
    </message>
    <message>
        <source>Machine Untested</source>
        <translation>Appareil non testé</translation>
    </message>
    <message>
        <source>Your Philips Respironics CPAP machine (Model %1) has not been tested yet.</source>
        <translation>Votre appareil Philips Respironics (Model %1) n&apos;est pas été testé pour le moment.</translation>
    </message>
    <message>
        <source>It seems similar enough to other machines that it might work, but the developers would like a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make sure it works with OSCAR.</source>
        <translation>Cet appareil semble assez similaire aux autres pour pouvoir fonctionner, mais les développeurs ont besoin d&apos;une copie zipée la carte SD de cet appareil et d&apos;un rapport de type Encore.pdf pour le faire fonctionner avec OSCAR.</translation>
    </message>
    <message>
        <source>Migrating </source>
        <translation>En cours de migration </translation>
    </message>
    <message>
        <source> files</source>
        <translation> fichiers</translation>
    </message>
    <message>
        <source>from </source>
        <translation>de </translation>
    </message>
    <message>
        <source>to </source>
        <translation>à </translation>
    </message>
    <message>
        <source>OSCAR will set up a folder for your data.</source>
        <translation>OSCAR va préparer un répertoire pour vos données.</translation>
    </message>
    <message>
        <source>If you have been using SleepyHead, OSCAR can copy your old data to this folder later.</source>
        <translation>Si vous utilisiez SleepyHead, OSCAR peut copier vos anciennes données dans ce répertoire plus tard.</translation>
    </message>
    <message>
        <source>We suggest you use this folder: </source>
        <translation>Nous vous suggérons d&apos;utiliser ce répertoire : </translation>
    </message>
    <message>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation>Cliquez sur Oui pour accepter ou Non pour utiliser un autre répertoire.</translation>
    </message>
    <message>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation>La prochaine fois qu&apos;OSCAR sera lancé, cette question sera posée.</translation>
    </message>
    <message>
        <source>Data directory:</source>
        <translation>Répertoire :</translation>
    </message>
    <message>
        <source>Migrate SleepyHead Data?</source>
        <translation>Voulez vous migrer les données de SleepyHead ?</translation>
    </message>
    <message>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead data</source>
        <translation>Maintenant OSCAR va vous demander de choisir un répertoire avec des données de SleepyHead</translation>
    </message>
    <message>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead data.</source>
        <translation>Cliquez [Oui] pour poursuivre ou [Non] si vous ne voulez pas utiliser de données SpleepyHead.</translation>
    </message>
    <message>
        <source>Updating Statistics cache</source>
        <translation>Mise à jour du cache des statistiques</translation>
    </message>
    <message>
        <source>d MMM yyyy [ %1 - %2 ]</source>
        <translation>d MMM yyyy [ %1 - %2 ]</translation>
    </message>
    <message>
        <source>EPAP %1 PS %2-%3 (%4)</source>
        <translation>EPAP %1 PS %2-%3 (%4)</translation>
    </message>
    <message>
        <source>Pressure Set</source>
        <translation>Activation de la pression</translation>
    </message>
    <message>
        <source>Pressure Setting</source>
        <translation>Réglage de pression</translation>
    </message>
    <message>
        <source>IPAP Set</source>
        <translation>Sélectionner IPAP</translation>
    </message>
    <message>
        <source>IPAP Setting</source>
        <translation>Réglages IPAP</translation>
    </message>
    <message>
        <source>EPAP Set</source>
        <translation>Activer EPAP</translation>
    </message>
    <message>
        <source>EPAP Setting</source>
        <translation>Réglages EPAP</translation>
    </message>
    <message>
        <source>Loading summaries</source>
        <translation>Chargement du résumé</translation>
    </message>
    <message>
        <source>Built with Qt %1 on %2</source>
        <translation>Construit avec Qt %1 sur %2</translation>
    </message>
    <message>
        <source>Motion</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <source>Dreem</source>
        <translation>Dreem</translation>
    </message>
    <message>
        <source>Untested Data</source>
        <translation>Données non testées</translation>
    </message>
    <message>
        <source>Your Philips Respironics %1 (%2) generated data that OSCAR has never seen before.</source>
        <translation>Votre Philips Respironics %1 (%2) a généré des données que OSCAR n&apos;a jamais vues auparavant.</translation>
    </message>
    <message>
        <source>The imported data may not be entirely accurate, so the developers would like a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make sure OSCAR is handling the data correctly.</source>
        <translation>Les données importées peuvent ne pas être entièrement exactes. Les développeurs souhaitent donc une copie .zip de la carte SD de cetappareil et des rapports Encore .pdf correspondants pour s&apos;assurer que OSCAR gère correctement ces données.</translation>
    </message>
    <message>
        <source>P-Flex</source>
        <translation>P-Flex</translation>
    </message>
    <message>
        <source>Humidification Mode</source>
        <translation>Mode d&apos;humidification</translation>
    </message>
    <message>
        <source>PRS1 Humidification Mode</source>
        <translation>Mode d&apos;humidification PRS1</translation>
    </message>
    <message>
        <source>Humid. Mode</source>
        <translation>Mode humid.</translation>
    </message>
    <message>
        <source>Fixed (Classic)</source>
        <translation>Fixe (classique)</translation>
    </message>
    <message>
        <source>Adaptive (System One)</source>
        <translation>Adaptatif (System One)</translation>
    </message>
    <message>
        <source>Heated Tube</source>
        <translation>Circuit chauffé</translation>
    </message>
    <message>
        <source>Tube Temperature</source>
        <translatorcomment>Température du tube</translatorcomment>
        <translation>Temperature circuit</translation>
    </message>
    <message>
        <source>PRS1 Heated Tube Temperature</source>
        <translation>Température circuit chauffé PRS1</translation>
    </message>
    <message>
        <source>Tube Temp.</source>
        <translation>Temp. circuit</translation>
    </message>
    <message>
        <source>PRS1 Humidifier Setting</source>
        <translation>Réglage de l’humidificateur PRS1</translation>
    </message>
    <message>
        <source>Humid. Lvl</source>
        <translation>Niv. humid.</translation>
    </message>
    <message>
        <source>12mm</source>
        <translation>12 mm</translation>
    </message>
    <message>
        <source>Parsing STR.edf records...</source>
        <translation>Analyse des enregistrements STR.edf...</translation>
    </message>
    <message>
        <source>Your Viatom device generated data that OSCAR has never seen before.</source>
        <translation>Votre appareil Viatom a généré des données que OSCAR n’a jamais vues auparavant.</translation>
    </message>
    <message>
        <source>The imported data may not be entirely accurate, so the developers would like a copy of your Viatom files to make sure OSCAR is handling the data correctly.</source>
        <translation>Les données importées peuvent ne pas être totalement précises. Les développeurs souhaitent donc une copie de vos fichiers Viatom pour que OSCAR gère correctement ces données.</translation>
    </message>
    <message>
        <source>Viatom</source>
        <translation>Viatom</translation>
    </message>
    <message>
        <source>Viatom Software</source>
        <translation>Logiciel Viatom</translation>
    </message>
    <message>
        <source>OSCAR %1 needs to upgrade its database for %2 %3 %4</source>
        <translation>OSCAR %1 doit mettre à niveau sa base de données pour %2 %3 %4</translation>
    </message>
    <message>
        <source>Mask Pressure (High frequency)</source>
        <translation>Pression du masque (haute fréquence)</translation>
    </message>
    <message>
        <source>A ResMed data item: Trigger Cycle Event</source>
        <translation>Élément de données Resmed : événement du cycle de déclenchement</translation>
    </message>
    <message>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <source>Movement detector</source>
        <translation>Détecteur de mouvement</translation>
    </message>
    <message>
        <source>Version &quot;%1&quot; is invalid, cannot continue!</source>
        <translation>La version &quot;%1&quot; est invalide, impossible de continuer !</translation>
    </message>
    <message>
        <source>The version of OSCAR you are running (%1) is OLDER than the one used to create this data (%2).</source>
        <translation>La version de OSCAR que vous utilisez (%1) est PLUS ANCIENNE que celle utilisée pour créer ces données (%2).</translation>
    </message>
    <message>
        <source>Please select a location for your zip other than the data card itself!</source>
        <translation>Pour votre fichier zip, sélectionnez un emplacement différent de la carte de données !</translation>
    </message>
    <message>
        <source>Unable to create zip!</source>
        <translation>Impossible de créer un fichier zip !</translation>
    </message>
    <message>
        <source>%1 %2 %3</source>
        <translation>%1 %2 %3</translation>
    </message>
    <message>
        <source>EPAP %1 IPAP %2-%3 (%4)</source>
        <translation>EPAP %1 IPAP %2-%3 (%4)</translation>
    </message>
    <message>
        <source>Backing Up Files...</source>
        <translation>Sauvegarde des fichiers...</translation>
    </message>
    <message>
        <source>CPAP-Check</source>
        <translatorcomment>Mode CPAP-Check des appareils System One de Philips Respironics</translatorcomment>
        <translation>CPAP-Check</translation>
    </message>
    <message>
        <source>AutoCPAP</source>
        <translatorcomment>Mode AutoCPAP des appareils System One de Philips Respironics</translatorcomment>
        <translation>AutoCPAP</translation>
    </message>
    <message>
        <source>Auto-Trial</source>
        <translatorcomment>Mode Auto-Trial des appareils System One de Philips Respironics</translatorcomment>
        <translation>Auto-Trial</translation>
    </message>
    <message>
        <source>AutoBiLevel</source>
        <translatorcomment>Le système AutoBilevel modifie automatiquement la pression d’inspiration et d’expiration en fonction des besoins du patient</translatorcomment>
        <translation>AutoBiLevel</translation>
    </message>
    <message>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <source>S/T</source>
        <translation>S/T</translation>
    </message>
    <message>
        <source>S/T - AVAPS</source>
        <translatorcomment>mode ventilatoire</translatorcomment>
        <translation>S/T - AVAPS</translation>
    </message>
    <message>
        <source>PC - AVAPS</source>
        <translatorcomment>fonction Philips Respironics</translatorcomment>
        <translation>PC - AVAPS</translation>
    </message>
    <message>
        <source>Flex Lock</source>
        <translatorcomment>fonction Philips Respironics</translatorcomment>
        <translation>Verrou Flex</translation>
    </message>
    <message>
        <source>Whether Flex settings are available to you.</source>
        <translation>Si les paramètres Flex sont disponibles.</translation>
    </message>
    <message>
        <source>Amount of time it takes to transition from EPAP to IPAP, the higher the number the slower the transition</source>
        <translation>Temps nécessaire pour passer d&apos;EPAP à IPAP (plus le nombre est élevé, plus la transition est lente)</translation>
    </message>
    <message>
        <source>Rise Time Lock</source>
        <translatorcomment>Fonction Philips Respironics</translatorcomment>
        <translation>Verrouillage Pente</translation>
    </message>
    <message>
        <source>Whether Rise Time settings are available to you.</source>
        <translation>Si les paramètres Rise Time sont disponibles.</translation>
    </message>
    <message>
        <source>Rise Lock</source>
        <translatorcomment>Fonction Philips Respironics</translatorcomment>
        <translation>Rise Lock</translation>
    </message>
    <message>
        <source>Passover</source>
        <translatorcomment>Humidificateur Philips Respironics</translatorcomment>
        <translation>Passover</translation>
    </message>
    <message>
        <source>Mask Resistance Setting</source>
        <translation>Réglage résistance Type de masque</translation>
    </message>
    <message>
        <source>Mask Resist.</source>
        <translation>Résist. masque</translation>
    </message>
    <message>
        <source>Hose Diam.</source>
        <translation>Diamètre tuyau.</translation>
    </message>
    <message>
        <source>Tubing Type Lock</source>
        <translation>Verrou type de circuit</translation>
    </message>
    <message>
        <source>Whether tubing type settings are available to you.</source>
        <translation>Si les paramètres Circuit sont disponibles.</translation>
    </message>
    <message>
        <source>Tube Lock</source>
        <translation>Verrouillage circuit</translation>
    </message>
    <message>
        <source>Mask Resistance Lock</source>
        <translation>Verrouillage résistance masque</translation>
    </message>
    <message>
        <source>Whether mask resistance settings are available to you.</source>
        <translation>Si les paramètres Résistance masque sont disponibles.</translation>
    </message>
    <message>
        <source>Mask Res. Lock</source>
        <translation>Verrou type de masque</translation>
    </message>
    <message>
        <source>Whether or not machine shows AHI via built-in display.</source>
        <translation>Selon que l&apos;écran de l&apos;appareil affiche ou non l&apos;IAH.</translation>
    </message>
    <message>
        <source>Ramp Type</source>
        <translation>Durée de rampe</translation>
    </message>
    <message>
        <source>Type of ramp curve to use.</source>
        <translation>Type de courbe de rampe à utiliser.</translation>
    </message>
    <message>
        <source>Linear</source>
        <translation>Linéaire</translation>
    </message>
    <message>
        <source>SmartRamp</source>
        <translation>SmartRamp</translation>
    </message>
    <message>
        <source>Backup Breath Mode</source>
        <translation>Mode Fréquence respiratoire de secours</translation>
    </message>
    <message>
        <source>The kind of backup breath rate in use: none (off), automatic, or fixed</source>
        <translation>Type de fréquence respiratoire de secours utilisée : aucun (arrêt), automatique ou fixe</translation>
    </message>
    <message>
        <source>Breath Rate</source>
        <translation>Fréquence respiratoire</translation>
    </message>
    <message>
        <source>Fixed</source>
        <translation>Fixe</translation>
    </message>
    <message>
        <source>Fixed Backup Breath BPM</source>
        <translation>Correction de la respiration de secours BPM</translation>
    </message>
    <message>
        <source>Minimum breaths per minute (BPM) below which a timed breath will be initiated</source>
        <translation>Respiration minimale par minute (BPM) en dessous de laquelle une respiration chronométrée sera amorcée</translation>
    </message>
    <message>
        <source>Breath BPM</source>
        <translation>Respiration BPM</translation>
    </message>
    <message>
        <source>Timed Inspiration</source>
        <translation>Inspiration chronométrée</translation>
    </message>
    <message>
        <source>The time that a timed breath will provide IPAP before transitioning to EPAP</source>
        <translation>Durée d’une respiration chronométrée IPAP avant la transition vers EPAP</translation>
    </message>
    <message>
        <source>Timed Insp.</source>
        <translation>Inspiration chronométrée</translation>
    </message>
    <message>
        <source>Auto-Trial Duration</source>
        <translation>Durée de l&apos;Auto-Trial</translation>
    </message>
    <message>
        <source>The number of days in the Auto-CPAP trial period, after which the machine will revert to CPAP</source>
        <translation>Nombre de jours dans la période d&apos;essai Auto-CPAP, après quoi la machine reviendra au CPAP</translation>
    </message>
    <message>
        <source>Auto-Trial Dur.</source>
        <translation>Durée Auto-Test</translation>
    </message>
    <message>
        <source>EZ-Start</source>
        <translatorcomment>fonction EZ-Start de Philips Respironics</translatorcomment>
        <translation>EZ-Start</translation>
    </message>
    <message>
        <source>Whether or not EZ-Start is enabled</source>
        <translation>Si EZ-Start activé ou non</translation>
    </message>
    <message>
        <source>Variable Breathing</source>
        <translation>Respiration variable</translation>
    </message>
    <message>
        <source>UNCONFIRMED: Possibly variable breathing, which are periods of high deviation from the peak inspiratory flow trend</source>
        <translation>NON CONFIRMÉ : respiration peut-être variable, qui contient des écarts élevés par rapport à la tendance du débit inspiratoire de pointe</translation>
    </message>
    <message>
        <source>Peak Flow</source>
        <translation>Pic de débit</translation>
    </message>
    <message>
        <source>Peak flow during a 2-minute interval</source>
        <translation>Pic de débit pendant un intervalle de 2 minutes</translation>
    </message>
    <message>
        <source>?5?</source>
        <translation>?5?</translation>
    </message>
    <message>
        <source>?9?</source>
        <translation>?9?</translation>
    </message>
    <message>
        <source>?10?</source>
        <translation>?10?</translation>
    </message>
    <message>
        <source>Once you upgrade, you &lt;font size=+1&gt;cannot&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation>Une fois la mise à niveau effectuée, vous &lt;font size=+1&gt; ne pourrez plus&lt;/font&gt; utiliser ce profil avec la version précédente.</translation>
    </message>
    <message>
        <source>Debugging channel #1</source>
        <translation>Canal de débogage n° 1</translation>
    </message>
    <message>
        <source>Top secret internal stuff you&apos;re not supposed to see ;)</source>
        <translation>Des trucs internes top secrets que vous n&apos;êtes pas censés voir ;)</translation>
    </message>
    <message>
        <source>Test #1</source>
        <translation>Test n ° 1</translation>
    </message>
    <message>
        <source>Debugging channel #2</source>
        <translation>Canal de débogage n° 2</translation>
    </message>
    <message>
        <source>Test #2</source>
        <translation>Test n ° 2</translation>
    </message>
    <message>
        <source>Recompressing Session Files</source>
        <translation>Recompression des fichiers de session</translation>
    </message>
    <message>
        <source>OSCAR crashed due to an incompatibility with your graphics hardware.</source>
        <translation>Plantage de OSCAR en raison d&apos;incompatibilité avec votre matériel graphique.</translation>
    </message>
    <message>
        <source>To resolve this, OSCAR has reverted to a slower but more compatible method of drawing.</source>
        <translation>Pour résoudre ce problème, OSCAR est revenu à une méthode de dessin plus lente mais plus compatible.</translation>
    </message>
    <message>
        <source>Couldn&apos;t parse Channels.xml, OSCAR cannot continue and is exiting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>(1 day ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>(%2 days ago)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Report</name>
    <message>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <source>about:blank</source>
        <translation>au sujet : blanc</translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <source>No Sessions Present</source>
        <translation>Pas de sessions présentes</translation>
    </message>
    <message>
        <source>%1h %2m</source>
        <translation>%1h %2m</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <source>Days</source>
        <translation>Jours</translation>
    </message>
    <message>
        <source>Oximeter Statistics</source>
        <translation>Statistiques de l&apos;oxymètre</translation>
    </message>
    <message>
        <source>CPAP Usage</source>
        <translation>Utilisation de la PPC</translation>
    </message>
    <message>
        <source>Blood Oxygen Saturation</source>
        <translation>Saturation en oxygène du sang</translation>
    </message>
    <message>
        <source>% of time in %1</source>
        <translation>% du temps en %1</translation>
    </message>
    <message>
        <source>Last 30 Days</source>
        <translation>Mois dernier</translation>
    </message>
    <message>
        <source>%1 Index</source>
        <translation>Index %1</translation>
    </message>
    <message>
        <source>%1 day of %2 Data on %3</source>
        <translation>%1 jour sur %2 de données sur %3</translation>
    </message>
    <message>
        <source>% of time above %1 threshold</source>
        <translation>% du temps au-dessus de la limite de %1</translation>
    </message>
    <message>
        <source>Therapy Efficacy</source>
        <translation>Efficacité de la thérapie</translation>
    </message>
    <message>
        <source>% of time below %1 threshold</source>
        <translation>% du temps au-dessous de la limite de %1</translation>
    </message>
    <message>
        <source>Max %1</source>
        <translation>%1 max</translation>
    </message>
    <message>
        <source>%1 Median</source>
        <translation>%1 Moyen</translation>
    </message>
    <message>
        <source>Min %1</source>
        <translation>%1 mini</translation>
    </message>
    <message>
        <source>Most Recent</source>
        <translation>Le plus récent</translation>
    </message>
    <message>
        <source>Pressure Settings</source>
        <translation>Réglages de la pression</translation>
    </message>
    <message>
        <source>Pressure Statistics</source>
        <translation>Statistiques de pression</translation>
    </message>
    <message>
        <source>Last 6 Months</source>
        <translation>6 derniers mois</translation>
    </message>
    <message>
        <source>Average %1</source>
        <translation>Moyenne %1</translation>
    </message>
    <message>
        <source>No %1 data available.</source>
        <translation>Pas de données %1 disponibles.</translation>
    </message>
    <message>
        <source>Last Use</source>
        <translation>Dernière utilisation</translation>
    </message>
    <message>
        <source>Pressure Relief</source>
        <translation>Allègement de la pression</translation>
    </message>
    <message>
        <source>Pulse Rate</source>
        <translation>Pouls</translation>
    </message>
    <message>
        <source>First Use</source>
        <translation>Première utilisation</translation>
    </message>
    <message>
        <source>Last Week</source>
        <translation>Semaine dernière</translation>
    </message>
    <message>
        <source>Last Year</source>
        <translation>Année dernière</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation>%1 jours de %2, entre %3 et %4</translation>
    </message>
    <message>
        <source>Last Session</source>
        <translation>Dernière session</translation>
    </message>
    <message>
        <source>Machine Information</source>
        <translation>Informations de l&apos;appareil</translation>
    </message>
    <message>
        <source>CPAP Statistics</source>
        <translation>Statistiques PPC</translation>
    </message>
    <message>
        <source>Leak Statistics</source>
        <translation>Statistiques des fuites</translation>
    </message>
    <message>
        <source>Average Hours per Night</source>
        <translation>Moyenne d&apos;utilisation par nuit</translation>
    </message>
    <message>
        <source>Name: %1, %2</source>
        <translation>Nom : %1, %2</translation>
    </message>
    <message>
        <source>DOB: %1</source>
        <translation>Date de naissance : %1</translation>
    </message>
    <message>
        <source>Phone: %1</source>
        <translation>Téléphone : %1</translation>
    </message>
    <message>
        <source>Email: %1</source>
        <translation>Email : %1</translation>
    </message>
    <message>
        <source>Address:</source>
        <translation>Adresse :</translation>
    </message>
    <message>
        <source>Days Used: %1</source>
        <translation>Jours d&apos;utilisation : %1</translation>
    </message>
    <message>
        <source>Low Use Days: %1</source>
        <translation>Jours de faible usage : %1</translation>
    </message>
    <message>
        <source>Compliance: %1%</source>
        <translation>Conformité : %1%</translation>
    </message>
    <message>
        <source>Days AHI of 5 or greater: %1</source>
        <translation>Jours avec IAH à 5 ou plus : %1</translation>
    </message>
    <message>
        <source>Best AHI</source>
        <translation>Meilleur IAH</translation>
    </message>
    <message>
        <source>Date: %1 AHI: %2</source>
        <translation>Date : %1 IHA: %2</translation>
    </message>
    <message>
        <source>Worst AHI</source>
        <translation>Pire IAH</translation>
    </message>
    <message>
        <source>Best Flow Limitation</source>
        <translation>Meilleure limitation de flux</translation>
    </message>
    <message>
        <source>Date: %1 FL: %2</source>
        <translation>Date : %1 FL : %2</translation>
    </message>
    <message>
        <source>Worst Flow Limtation</source>
        <translation>Pire limitation de flux</translation>
    </message>
    <message>
        <source>No Flow Limitation on record</source>
        <translation>Pas de limitation de flux enregistrée</translation>
    </message>
    <message>
        <source>Worst Large Leaks</source>
        <translation>Pire fuites importantes</translation>
    </message>
    <message>
        <source>Date: %1 Leak: %2%</source>
        <translation>Date : %1 Fuite : %2%</translation>
    </message>
    <message>
        <source>No Large Leaks on record</source>
        <translation>Pas de fuite importante enregistrée</translation>
    </message>
    <message>
        <source>Worst CSR</source>
        <translation>Pire RCS</translation>
    </message>
    <message>
        <source>Date: %1 CSR: %2%</source>
        <translation>Date : %1 RCS : %2%</translation>
    </message>
    <message>
        <source>No CSR on record</source>
        <translation>Pas de RCS (Resp. Cheyne-Stokes) enregistrée</translation>
    </message>
    <message>
        <source>Worst PB</source>
        <translation>Pire RP (Resp. Per.)</translation>
    </message>
    <message>
        <source>Date: %1 PB: %2%</source>
        <translation>Date : %1 RP : %2%</translation>
    </message>
    <message>
        <source>No PB on record</source>
        <translation>Pas de RP enregistrée</translation>
    </message>
    <message>
        <source>Want more information?</source>
        <translation>Plus d&apos;informations ?</translation>
    </message>
    <message>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation>OSCAR a besoin de charger toutes les informations de synthèse pour calculer les meilleures/pires données journalières.</translation>
    </message>
    <message>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation>Cochez le préchargement des informations de synthèse dans les préférences pour que ces données soient disponibles.</translation>
    </message>
    <message>
        <source>Best RX Setting</source>
        <translation>Meilleurs réglages RX</translation>
    </message>
    <message>
        <source>Date: %1 - %2</source>
        <translation>Date : %1 - %2</translation>
    </message>
    <message>
        <source>Worst RX Setting</source>
        <translation>Pires réglages RX</translation>
    </message>
    <message>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation>OSCAR est un logiciel libre de création de rapports de PPC (Pression Positive Continue)</translation>
    </message>
    <message>
        <source>Oscar has no data to report :(</source>
        <translation>Pas de données pour les rapports :(</translation>
    </message>
    <message>
        <source>Compliance (%1 hrs/day)</source>
        <translation>Conformité (%1 heures/jour)</translation>
    </message>
    <message>
        <source>Changes to Machine Settings</source>
        <translation>Changements de réglages de l&apos;appareil</translation>
    </message>
    <message>
        <source>No data found?!?</source>
        <translation>Aucune donnée disponible !?</translation>
    </message>
    <message>
        <source>AHI: %1</source>
        <translation>IAH : %1</translation>
    </message>
    <message>
        <source>Total Hours: %1</source>
        <translation>Total Heures : %1</translation>
    </message>
    <message>
        <source>This report was prepared on %1 by OSCAR %2</source>
        <translation>Ce rapport a été rédigé le %1 par OSCAR %2</translation>
    </message>
</context>
<context>
    <name>UpdaterWindow</name>
    <message>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <source>&amp;Finished</source>
        <translation>&amp;Fin de mise à jour</translation>
    </message>
    <message>
        <source>Please wait while updates are downloaded and installed...</source>
        <translation>Merci d&apos;attendre. Téléchargement et installation en cours...</translation>
    </message>
    <message>
        <source>Component</source>
        <translation>Composant</translation>
    </message>
    <message>
        <source>&amp;Upgrade Now</source>
        <translation>&amp;Upgrader maintenant</translation>
    </message>
    <message>
        <source>Version Information</source>
        <translation>Informations de version</translation>
    </message>
    <message>
        <source>Build Notes</source>
        <translation>Note de compilation</translation>
    </message>
    <message>
        <source>No updates were found for your platform.</source>
        <translation>Aucune mise à jour disponible.</translation>
    </message>
    <message>
        <source>Downloading &amp; Installing Updates</source>
        <translation>Téléchargement et installation en cours</translation>
    </message>
    <message>
        <source>Maybe &amp;Later</source>
        <translation>&amp;Plus tard</translation>
    </message>
    <message>
        <source>Progress</source>
        <translation>Avancement</translation>
    </message>
    <message>
        <source>%1 bytes received</source>
        <translation>%1 octets reçus</translation>
    </message>
    <message>
        <source>A new version of $APP is available</source>
        <translation>Une nouvelle version de $APP est disponible</translation>
    </message>
    <message>
        <source>Updates</source>
        <translation>Mises à jour</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Requesting </source>
        <translation>Requête en cours </translation>
    </message>
    <message>
        <source>Release Notes</source>
        <translation>Notes de publication</translation>
    </message>
    <message>
        <source>You are already running the latest version.</source>
        <translation>C&apos;est la version la plus à jour.</translation>
    </message>
    <message>
        <source>Would you like to download and install them now?</source>
        <translation>Voulez-vous les télécharger et les installer maintenant ?</translation>
    </message>
    <message>
        <source>OSCAR Updater</source>
        <translation>Outil de mise à jour de OSCAR</translation>
    </message>
    <message>
        <source>Updates are not yet implemented</source>
        <translation>Désolé, fonction non encore implémentée</translation>
    </message>
    <message>
        <source>Checking for OSCAR Updates</source>
        <translation>Vérification de disponibilité de mise à jour</translation>
    </message>
    <message>
        <source>OSCAR Updates</source>
        <translation>Mise à jour de OSCAR</translation>
    </message>
    <message>
        <source>Version %1 of OSCAR is available, opening link to download site.</source>
        <translation>La version &lt;b&gt;v%1&lt;/b&gt; est disponible. Ouverture du lien vers le site de téléchargement.</translation>
    </message>
    <message>
        <source>New OSCAR Updates are avilable:</source>
        <translation>Une mise à jour de OSCAR est disponible :</translation>
    </message>
    <message>
        <source>OSCAR Updates are currently unavailable for this platform</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <source>What would you like to do?</source>
        <translation>Que souhaitez-vous faire ?</translation>
    </message>
    <message>
        <source>CPAP Importer</source>
        <translation>Import de PPC</translation>
    </message>
    <message>
        <source>Oximetry Wizard</source>
        <translation>Assistant d&apos;oxymétrie</translation>
    </message>
    <message>
        <source>Daily View</source>
        <translation>Vue quotidienne</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Aperçus</translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation>Statistiques</translation>
    </message>
    <message>
        <source>First import can take a few minutes.</source>
        <translation>Le premier import peut prendre quelques minutes.</translation>
    </message>
    <message>
        <source>The last time you used your %1...</source>
        <translation>Dernière utilisation de l&apos;appareil %1  :</translation>
    </message>
    <message>
        <source>last night</source>
        <translation>la nuit dernière</translation>
    </message>
    <message>
        <source>%2 days ago</source>
        <translation>il y a %2 jours</translation>
    </message>
    <message>
        <source>was %1 (on %2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1 heure(s), %2 minute(s) et %3 seconde(s)</translation>
    </message>
    <message>
        <source>Your machine was on for %1.</source>
        <translation>L&apos;appareil a fonctionné pendant %1.</translation>
    </message>
    <message>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color = red&gt; Vous n&apos;avez porté le masque que %1.&lt;/font&gt;</translation>
    </message>
    <message>
        <source>under</source>
        <translation>inférieur à</translation>
    </message>
    <message>
        <source>over</source>
        <translation>supérieur à</translation>
    </message>
    <message>
        <source>reasonably close to</source>
        <translation>assez proche de</translation>
    </message>
    <message>
        <source>equal to</source>
        <translation>égal à</translation>
    </message>
    <message>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>IAH de %1 (%2 %4, moyenne des %3 jours précédents).</translation>
    </message>
    <message>
        <source>Your machine was under %1-%2 %3 for %4% of the time.</source>
        <translation>L&apos;appreil était en dessous de %1-%2 %3 pendant %4% du temps.</translation>
    </message>
    <message>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>Fuites moyennes de %1 %2 (%3 %5, moyenne des %4 jours précédents).</translation>
    </message>
    <message>
        <source>No CPAP data has been imported yet.</source>
        <translation>Pas de données de PPC importées pour le moment.</translation>
    </message>
    <message>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation>Il est préférable de vérifier Fichier-&gt;Préférences avant toute chose,</translation>
    </message>
    <message>
        <source>as there are some options that affect import.</source>
        <translation>car il y a des options qui affectent l&apos;import.</translation>
    </message>
    <message>
        <source>Note that some preferences are forced when a ResMed machine is detected</source>
        <translation>Certaines préférences sont forcées avec les appareils ResMed</translation>
    </message>
    <message>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Bienvenue dans O.S.C.A.R. (Open Source CPAP Analysis Reporter)</translation>
    </message>
    <message>
        <source>Your CPAP machine used a constant %1 %2 of air</source>
        <translation>L&apos;appareil PPC a utilisé une pression d&apos;air constante de %1 %2</translation>
    </message>
    <message>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>La pression a été inférieure à %1 %2 pendant %3% du temps.</translation>
    </message>
    <message>
        <source>Your machine used a constant %1-%2 %3 of air.</source>
        <translation>L&apos;appareil a utilisé une pression d&apos;air constante de %1 %2 %3.</translation>
    </message>
    <message>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>La pression EPAP s&apos;est fixée %1 %2.</translation>
    </message>
    <message>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Pression IPAP en dessous de %1 %2 pendant %3% du temps.</translation>
    </message>
    <message>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Pression EPAP en dessous de %1 %2 pendant %3% du temps.</translation>
    </message>
    <message>
        <source>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards need to be locked &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;before inserting into your computer.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Some operating systems write index files to the card without asking, which can render your card unreadable by your cpap machine.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;span style=&quot; font-weight:600;&quot;&gt;Attention: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;La carte SD de l&apos;appareil ResMed S9 doit être verrouillée &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;avant d&apos;être insérée dans votre ordinateur.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Certains systèmes d&apos;exploitation écrivent des fichiers sur la carte sans le demander et peuvent rendre la carte inutilisable par votre appareil.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>1 day ago</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <source>%1 days</source>
        <translation>%1 jours</translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <source>Clone %1 Graph</source>
        <translation>Cloner le graphique %1</translation>
    </message>
    <message>
        <source>Oximeter Overlays</source>
        <translation>Dépassement de l&apos;oxymètre</translation>
    </message>
    <message>
        <source>Plots</source>
        <translation>Points</translation>
    </message>
    <message>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>RAZ de la hauteur et de l&apos;ordre de tous les graphiques.</translation>
    </message>
    <message>
        <source>Remove Clone</source>
        <translation>Enlever les clones</translation>
    </message>
    <message>
        <source>Dotted Lines</source>
        <translation>Lignes en pointillé</translation>
    </message>
    <message>
        <source>CPAP Overlays</source>
        <translation>Dépassement PPC</translation>
    </message>
    <message>
        <source>Y-Axis</source>
        <translation>Axe Y</translation>
    </message>
    <message>
        <source>Reset Graph Layout</source>
        <translation>Réinitialiser la disposition des graphiques</translation>
    </message>
    <message>
        <source>100% zoom level</source>
        <translation>Zoom à 100%</translation>
    </message>
    <message>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation>Double-clic sur le titre pour épingler/décrocher
Cliquer et glisser pour réorganiser les graphiques</translation>
    </message>
    <message>
        <source>Restore X-axis zoom to 100% to view entire selected period.</source>
        <translation>RAZ du zoom des X à 100% pour afficher la période choisie.</translation>
    </message>
    <message>
        <source>Restore X-axis zoom to 100% to view entire day&apos;s data.</source>
        <translation>RAZ du zoom des X à 100% pour afficher une journée complète.</translation>
    </message>
</context>
</TS>
