<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ar" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>الحوار</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translation>حول</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="117"/>
        <source>Release Notes</source>
        <translation>ملاحظات الإصدار</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation>قروض</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>رخصة GPL</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="239"/>
        <source>Close</source>
        <translation>قريب</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="35"/>
        <source>Show data folder</source>
        <translation>عرض مجلد البيانات</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="39"/>
        <source>About OSCAR %1</source>
        <translation>حول OSCAR %1</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="83"/>
        <source>Sorry, could not locate About file.</source>
        <translation>عذرا ، لا يمكن تحديد موقع ملف.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="96"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation>عذرًا ، تعذر تحديد موقع ملف الائتمانات.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="108"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation>عفوا، لا يمكن تحديد موقع ملاحظات الإصدار.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="118"/>
        <source>OSCAR %1</source>
        <translation>OSCAR %1</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="121"/>
        <source>Important:</source>
        <translation>مهم:</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="122"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceeding, because attempting to roll back later may break things.</source>
        <translation>نظرًا لأن هذا الإصدار ما قبل النشر ، يوصى &lt;b&gt;بعمل نسخة احتياطية من مجلد البيانات يدويًا&lt;/b&gt; قبل المتابعة ، لأن محاولة الاستعادة لاحقًا قد تؤدي إلى كسر الأمور.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="134"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>لمعرفة ما إذا كان نص الترخيص متاحًا بلغتك ، راجع %1.</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="878"/>
        <source>Could not find the oximeter file:</source>
        <translation>تعذر العثور على ملف مقياس التأكسج:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="884"/>
        <source>Could not open the oximeter file:</source>
        <translation>لا يمكن فتح ملف مقياس التأكسج:</translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>نعةزر عن عدم امكان نقل البيانات من جهاز التاكسج.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>يرجى التأكد من اختيار &quot;تحميل&quot; من قائمة الأجهزة مقياس التأكسج.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="546"/>
        <source>Could not find the oximeter file:</source>
        <translation>تعذر العثور على ملف مقياس التأكسج:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="552"/>
        <source>Could not open the oximeter file:</source>
        <translation>لا يمكن فتح ملف مقياس التأكسج:</translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="435"/>
        <source>Form</source>
        <translation>شكل</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>انتقل إلى اليوم السابق</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>إظهار أو إخفاء التقويم</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>انتقل إلى اليوم التالي</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>انتقل إلى أحدث يوم مع سجلات البيانات</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>أحداث</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translation>عرض الحجم</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1395"/>
        <source>Notes</source>
        <translation>عرض الحجم</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translation>مجلة</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1063"/>
        <source>B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1075"/>
        <source>u</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1085"/>
        <source>Color</source>
        <translation>اللون</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1107"/>
        <location filename="../oscar/daily.ui" line="1117"/>
        <source>Small</source>
        <translation>صغير</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1122"/>
        <source>Medium</source>
        <translation>متوسط</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1127"/>
        <source>Big</source>
        <translation>كبير</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1186"/>
        <source>Zombie</source>
        <translation>الاموات الاحياء</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1199"/>
        <source>I&apos;m feeling ...</source>
        <translation>أنا أشعر ...</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1215"/>
        <source>Weight</source>
        <translation>وزن</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1222"/>
        <source>If height is greater than zero in Preferences Dialog, setting weight here will show Body Mass Index (BMI) value</source>
        <translation>إذا كان الارتفاع أكبر من الصفر في مربع حوار التفضيلات ، فإن تحديد الوزن هنا سيظهر قيمة مؤشر كتلة الجسم (BMI)</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1292"/>
        <source>Awesome</source>
        <translation>ممتاز</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1330"/>
        <source>B.M.I.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1346"/>
        <source>Bookmarks</source>
        <translation>إشارات مرجعية</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1367"/>
        <source>Add Bookmark</source>
        <translation>اضافة للمفضلة</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1390"/>
        <source>Starts</source>
        <translation>يبدأ</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1403"/>
        <source>Remove Bookmark</source>
        <translation>إزالة المرجعية</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1496"/>
        <source>Flags</source>
        <translation>أعلام</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1548"/>
        <source>Graphs</source>
        <translation>الرسوم البيانية</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1573"/>
        <source>Show/hide available graphs.</source>
        <translation>إظهار / إخفاء الرسوم البيانية المتاحة.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="288"/>
        <source>Breakdown</source>
        <translation>انفصال</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="288"/>
        <source>events</source>
        <translation>أحداث</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="299"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="300"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="363"/>
        <source>Time at Pressure</source>
        <translation>الوقت في الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="609"/>
        <source>No %1 events are recorded this day</source>
        <translation>لم يتم تسجيل أحداث %1 هذا اليوم</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="729"/>
        <source>%1 event</source>
        <translation>حدث %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="730"/>
        <source>%1 events</source>
        <translation>أحداث %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="773"/>
        <source>Session Start Times</source>
        <translation>أوقات بدء الجلسة</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="774"/>
        <source>Session End Times</source>
        <translation>أوقات نهاية الجلسة</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="980"/>
        <source>Session Information</source>
        <translation>معلومات الجلسة</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1003"/>
        <source>Oximetry Sessions</source>
        <translation>جلسات قياس التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1023"/>
        <source>Duration</source>
        <translation>المدة الزمنية</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2293"/>
        <source>This bookmark is in a currently disabled area..</source>
        <translation>هذه الإشارة المرجعية موجودة في منطقة معطلة حاليًا ..</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1000"/>
        <source>CPAP Sessions</source>
        <translation>جلسات CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="183"/>
        <source>Details</source>
        <translation>تفاصيل</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1006"/>
        <source>Sleep Stage Sessions</source>
        <translation>جلسات مرحلة النوم</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1009"/>
        <source>Position Sensor Sessions</source>
        <translation>جلسات استشعار الموقف</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1014"/>
        <source>Unknown Session</source>
        <translation>جلسة غير معروفة</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1088"/>
        <source>Machine Settings</source>
        <translation>إعدادات الجهاز</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1239"/>
        <source>Model %1 - %2</source>
        <translation>النموذج %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1244"/>
        <source>PAP Mode: %1</source>
        <translation>وضع PAP: %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1248"/>
        <source>(Mode/Pressure settings are guessed on this day.)</source>
        <translation>(يتم ضبط إعدادات الوضع / الضغط في هذا اليوم.)</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1360"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>هذا اليوم يحتوي فقط على بيانات موجزة ، تتوفر معلومات محدودة فقط.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1386"/>
        <source>Total ramp time</source>
        <translation>إجمالي الوقت المنحدر</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1390"/>
        <source>Time outside of ramp</source>
        <translation>الوقت خارج المنحدر</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1431"/>
        <source>Start</source>
        <translation>بداية</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1431"/>
        <source>End</source>
        <translation>النهاية</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1468"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation>غير قادر على عرض مخطط دائري على هذا النظام</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1712"/>
        <source>Sorry, this machine only provides compliance data.</source>
        <translation>عذرًا ، لا يوفر هذا الجهاز سوى بيانات التوافق.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1731"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;لا يوجد شيء هنا!&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1734"/>
        <source>No data is available for this day.</source>
        <translation>لا توجد بيانات متاحة لهذا اليوم.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1213"/>
        <source>Oximeter Information</source>
        <translation>معلومات مقياس التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1043"/>
        <source>Click to %1 this session.</source>
        <translation>انقر فوق %1 هذه الجلسة.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1043"/>
        <source>disable</source>
        <translation>تعطيل</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1043"/>
        <source>enable</source>
        <translation>ممكن</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1059"/>
        <source>%1 Session #%2</source>
        <translation>%1 الجلسة #%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1060"/>
        <source>%1h %2m %3s</source>
        <translation>%1h %2m %3s</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1092"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing has changed since previous days.</source>
        <translation>&lt;b&gt; يرجى ملاحظة: &lt;/ b&gt; تستند جميع الإعدادات الموضحة أدناه إلى افتراضات أنه لم يتغير شيء منذ الأيام السابقة.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1217"/>
        <source>SpO2 Desaturations</source>
        <translation>التشوهات SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1218"/>
        <source>Pulse Change events</source>
        <translation>أحداث تغيير النبض</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1219"/>
        <source>SpO2 Baseline Used</source>
        <translation>خط الأساس SPO2 المستخدمة</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1290"/>
        <source>%1%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1285"/>
        <source>Statistics</source>
        <translation>الإحصاء</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1370"/>
        <source>Total time in apnea</source>
        <translation>الوقت الإجمالي في انقطاع النفس</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1380"/>
        <source>Time over leak redline</source>
        <translation>الوقت على تسرب الخط الأحمر</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1647"/>
        <source>BRICK! :(</source>
        <translation>قالب طوب! :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1455"/>
        <source>Event Breakdown</source>
        <translation>انهيار الحدث</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1702"/>
        <source>Sessions all off!</source>
        <translation>جلسات جميع قبالة!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1704"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>توجد جلسات لهذا اليوم ولكن تم إيقافها.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1707"/>
        <source>Impossibly short session</source>
        <translation>جلسة قصيرة مستحيلة</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1708"/>
        <source>Zero hours??</source>
        <translation>ساعات الصفر؟</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1711"/>
        <source>BRICK :(</source>
        <translation>قالب طوب :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1713"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>شكوى إلى مزود المعدات الخاص بك!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2029"/>
        <source>Pick a Colour</source>
        <translation>اختيار اللون</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2336"/>
        <source>Bookmark at %1</source>
        <translation>إشارة مرجعية في %1</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>تصدير بتنسيق CSV</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>تواريخ:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>الدقة:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>تفاصيل</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>جلسات</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>اليومي</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>اسم الملف:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>تصدير</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>بداية:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>النهاية:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation>المدى السريع:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="61"/>
        <location filename="../oscar/exportcsv.cpp" line="123"/>
        <source>Most Recent Day</source>
        <translation>معظم الأيام الأخيرة</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="126"/>
        <source>Last Week</source>
        <translation>الاسبوع الماضى</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="129"/>
        <source>Last Fortnight</source>
        <translation>آخر أسبوعين</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="132"/>
        <source>Last Month</source>
        <translation>الشهر الماضي</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="135"/>
        <source>Last 6 Months</source>
        <translation>آخر 6 أشهر</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="138"/>
        <source>Last Year</source>
        <translation>العام الماضي</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="120"/>
        <source>Everything</source>
        <translation>كل شىء</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="109"/>
        <source>Custom</source>
        <translation>مخصص</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="74"/>
        <source>OSCAR_</source>
        <translation>OSCAR_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="77"/>
        <source>Details_</source>
        <translation>تفاصيل_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="79"/>
        <source>Sessions_</source>
        <translation>جلسات_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="81"/>
        <source>Summary_</source>
        <translation>ملخص_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>Select file to export to</source>
        <translation>حدد ملف للتصدير إليه</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="89"/>
        <source>CSV Files (*.csv)</source>
        <translation>ملفات CSV (* .csv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="196"/>
        <source>DateTime</source>
        <translation>التاريخ والوقت</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="196"/>
        <location filename="../oscar/exportcsv.cpp" line="202"/>
        <source>Session</source>
        <translation>جلسة</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="196"/>
        <source>Event</source>
        <translation>حدث</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="196"/>
        <source>Data/Duration</source>
        <translation>البيانات / المدة</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="199"/>
        <location filename="../oscar/exportcsv.cpp" line="202"/>
        <source>Date</source>
        <translation>تاريخ</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="199"/>
        <source>Session Count</source>
        <translation>عدد الجلسة</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="199"/>
        <location filename="../oscar/exportcsv.cpp" line="202"/>
        <source>Start</source>
        <translation>بداية</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="199"/>
        <location filename="../oscar/exportcsv.cpp" line="202"/>
        <source>End</source>
        <translation>النهاية</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Total Time</source>
        <translation>الوقت الكلي</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>AHI</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <source> Count</source>
        <translation> عد</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="215"/>
        <source>%1% </source>
        <translation>%1% </translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="139"/>
        <source>Import Error</source>
        <translation>خطأ في الاستيراد</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation>لا يمكن استيراد سجل الجهاز في ملف التعريف هذا.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>تتداخل سجلات اليوم مع المحتوى الموجود بالفعل.</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.ui" line="20"/>
        <source>Form</source>
        <translation>شكل</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="92"/>
        <source>Hide this message</source>
        <translation>إخفاء هذه الرسالة</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="198"/>
        <source>Search Topic:</source>
        <translation>موضوع البحث:</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="58"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>ملفات المساعدة ليست متاحة بعد لـ %1 وسيتم عرضها في %2.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="68"/>
        <source>Help files do not appear to be present.</source>
        <translation>لا يبدو أن ملفات المساعدة موجودة.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="84"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>مساعدة المحرك لم يتم إعداد بشكل صحيح</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="99"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>لم تتمكن HelpEngine من تسجيل الوثائق بشكل صحيح.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="110"/>
        <source>Contents</source>
        <translation>محتويات</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Index</source>
        <translation>فهرس</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="115"/>
        <source>Search</source>
        <translation>بحث</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="125"/>
        <source>No documentation available</source>
        <translation>لا توجد وثائق متاحة</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="213"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation>الرجاء الانتظار قليلاً .. لا تزال الفهرسة جارية</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="239"/>
        <source>No</source>
        <translation>لا</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>نتيجة (نتائج) %1 لـ &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>clear</source>
        <translation>واضح</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="165"/>
        <source>Could not find the oximeter file:</source>
        <translation>تعذر العثور على ملف مقياس التأكسج:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="171"/>
        <source>Could not open the oximeter file:</source>
        <translation>لا يمكن فتح ملف مقياس التأكسج:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="942"/>
        <source>&amp;Statistics</source>
        <translation>الإحصاء</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="996"/>
        <source>Report Mode</source>
        <translation>وضع التقرير</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1003"/>
        <location filename="../oscar/mainwindow.ui" line="3277"/>
        <source>Standard</source>
        <translation>اساسي</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1013"/>
        <source>Monthly</source>
        <translation>شهريا</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1020"/>
        <source>Date Range</source>
        <translation>نطاق الموعد</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1391"/>
        <source>Statistics</source>
        <translation>الإحصاء</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1441"/>
        <source>Daily</source>
        <translation>اليومي</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1485"/>
        <source>Overview</source>
        <translation>نظرة عامة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1529"/>
        <source>Oximetry</source>
        <translation>التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1579"/>
        <source>Import</source>
        <translation>استيراد</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1629"/>
        <source>Help</source>
        <translation>مساعدة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2805"/>
        <source>&amp;File</source>
        <translation>ملف</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2830"/>
        <source>&amp;View</source>
        <translation>رأي</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2834"/>
        <source>&amp;Reset Graphs</source>
        <translation>إعادة تعيين الرسوم البيانية</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2859"/>
        <source>&amp;Help</source>
        <translation>مساعدة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2863"/>
        <source>Troubleshooting</source>
        <translation>استكشاف الأخطاء وإصلاحها</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2886"/>
        <source>&amp;Data</source>
        <translation>البيانات</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2890"/>
        <source>&amp;Advanced</source>
        <translation>المتقدمة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2900"/>
        <source>Purge ALL Machine Data</source>
        <translation>تطهير جميع بيانات الجهاز</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2910"/>
        <source>Rebuild CPAP Data</source>
        <translation>إعادة بناء بيانات CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2933"/>
        <source>&amp;Import CPAP Card Data</source>
        <translation>استيراد بيانات بطاقة CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2959"/>
        <source>Show Daily view</source>
        <translation>عرض عرض يومي</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2970"/>
        <source>Show Overview view</source>
        <translation>عرض عرض نظرة عامة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3010"/>
        <source>&amp;Maximize Toggle</source>
        <translation>تكبير الحد الأقصى</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3013"/>
        <source>Maximize window</source>
        <translation>تكبير النافذة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3032"/>
        <source>Reset Graph &amp;Heights</source>
        <translation>إعادة تعيين الرسم البياني مرتفعات</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3035"/>
        <source>Reset sizes of graphs</source>
        <translation>إعادة تعيين أحجام الرسوم البيانية</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3100"/>
        <source>Show Right Sidebar</source>
        <translation>إظهار الشريط الجانبي الأيمن</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3114"/>
        <source>Show Statistics view</source>
        <translation>عرض الاحصائيات الرأي</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3127"/>
        <source>Import &amp;Dreem Data</source>
        <translation>استيراد بيانات دريم</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3157"/>
        <source>Import &amp;Viatom Data</source>
        <translation>استيراد بيانات Viatom</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3170"/>
        <source>Show &amp;Line Cursor</source>
        <translation>عرض وخط المؤشرمشاهدة والخط المؤشر</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3192"/>
        <source>Show Daily Left Sidebar</source>
        <translation>عرض الشريط الجانبي الأيسر اليومي</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3209"/>
        <source>Show Daily Calendar</source>
        <translation>عرض التقويم اليومي</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3230"/>
        <source>Create zip of CPAP data card</source>
        <translation>إنشاء ملف مضغوط لبطاقة بيانات CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3235"/>
        <source>Create zip of all OSCAR data</source>
        <translation>إنشاء ملف مضغوط لجميع بيانات OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3250"/>
        <source>Report an Issue</source>
        <translation>بلغ عن خطأ</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3255"/>
        <source>System Information</source>
        <translation>معلومات النظام</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3266"/>
        <source>Show &amp;Pie Chart</source>
        <translation>عرض ومخطط دائري</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3269"/>
        <source>Show Pie Chart on Daily page</source>
        <translation>إظهار مخطط دائري على الصفحة اليومية</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3272"/>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3280"/>
        <source>Standard graph order, good for CPAP, APAP, Bi-Level</source>
        <translation>ترتيب الرسم البياني القياسي ، جيد ل CPAP ، APAP ، ثنائية المستوى</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3285"/>
        <source>Advanced</source>
        <translation>المتقدمة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3288"/>
        <source>Advanced graph order, good for ASV, AVAPS</source>
        <translation>ترتيب الرسم البياني المتقدم ، جيد ل ASV ، AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3299"/>
        <source>Show Personal Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2941"/>
        <source>&amp;Preferences</source>
        <translation>والتفضيلات</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2946"/>
        <source>&amp;Profiles</source>
        <translation>مظهر</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3002"/>
        <source>&amp;About OSCAR</source>
        <translation>حول OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3225"/>
        <source>Show Performance Information</source>
        <translation>عرض معلومات الأداء</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3240"/>
        <source>CSV Export Wizard</source>
        <translation>معالج تصدير CSV</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3245"/>
        <source>Export for Review</source>
        <translation>تصدير للمراجعة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="106"/>
        <source>E&amp;xit</source>
        <translation>خروج</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2951"/>
        <source>Exit</source>
        <translation>خروج</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2956"/>
        <source>View &amp;Daily</source>
        <translation>عرض و يوميا</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2967"/>
        <source>View &amp;Overview</source>
        <translation>عرض ونظرة عامة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2978"/>
        <source>View &amp;Welcome</source>
        <translation>عرض ومرحبا بكم</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2986"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2997"/>
        <source>Use &amp;AntiAliasing</source>
        <translation>استخدام ومكافحة التعرج</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3024"/>
        <source>Show Debug Pane</source>
        <translation>إظهار جزء التصحيح</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3040"/>
        <source>Take &amp;Screenshot</source>
        <translation>خد لقطة للشاشة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3048"/>
        <source>O&amp;ximetry Wizard</source>
        <translation>معالج التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3056"/>
        <source>Print &amp;Report</source>
        <translation>اطبع تقرير</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3061"/>
        <source>&amp;Edit Profile</source>
        <translation>تعديل الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3206"/>
        <source>Daily Calendar</source>
        <translation>التقويم اليومي</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3217"/>
        <source>Backup &amp;Journal</source>
        <translation>النسخ الاحتياطي ومجلة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3066"/>
        <source>Online Users &amp;Guide</source>
        <translation>دليل المستخدمين عبر الإنترنت</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3071"/>
        <source>&amp;Frequently Asked Questions</source>
        <translation>أسئلة مكررة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3076"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation>التلقائي تنظيف مقياس التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3081"/>
        <source>Change &amp;User</source>
        <translation>التغيير والمستخدم</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3086"/>
        <source>Purge &amp;Current Selected Day</source>
        <translation>تطهير واليوم المحدد الحالي</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3097"/>
        <source>Right &amp;Sidebar</source>
        <translation>الشريط الجانبي الأيمن</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3189"/>
        <source>Daily Sidebar</source>
        <translation>الشريط الجانبي اليومي</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3108"/>
        <source>View S&amp;tatistics</source>
        <translation>عرض الاحصائيات</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1320"/>
        <source>Navigation</source>
        <translation>التنقل</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="441"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1751"/>
        <source>Bookmarks</source>
        <translation>إشارات مرجعية</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2746"/>
        <source>Records</source>
        <translation>تسجيل</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2809"/>
        <source>Exp&amp;ort Data</source>
        <translation>تصدير البيانات</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1347"/>
        <source>Profiles</source>
        <translation>مظهر</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2894"/>
        <source>Purge Oximetry Data</source>
        <translation>تطهير بيانات قياس التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3111"/>
        <source>View Statistics</source>
        <translation>عرض الاحصائيات</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3122"/>
        <source>Import &amp;ZEO Data</source>
        <translation>استيراد وبيانات ZEO</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3132"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation>استيراد RemStar و MSeries البيانات</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3137"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation>شروط اضطراب النوم والمسرد</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3142"/>
        <source>Change &amp;Language</source>
        <translation>تغيير اللغة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3147"/>
        <source>Change &amp;Data Folder</source>
        <translation>التغيير ومجلد البيانات</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3152"/>
        <source>Import &amp;Somnopose Data</source>
        <translation>استيراد و Somnopose البيانات</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3162"/>
        <source>Current Days</source>
        <translation>الأيام الحالية</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="537"/>
        <location filename="../oscar/mainwindow.cpp" line="2343"/>
        <source>Welcome</source>
        <translation>أهلا بك</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="103"/>
        <source>&amp;About</source>
        <translation>حول</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="774"/>
        <location filename="../oscar/mainwindow.cpp" line="2010"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>الرجاء الانتظار ، الاستيراد من مجلد (مجلدات) النسخ الاحتياطي ...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="718"/>
        <location filename="../oscar/mainwindow.cpp" line="2384"/>
        <location filename="../oscar/mainwindow.cpp" line="2415"/>
        <location filename="../oscar/mainwindow.cpp" line="2546"/>
        <source>Import Problem</source>
        <translation>مشكلة الاستيراد</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="875"/>
        <source>Please insert your CPAP data card...</source>
        <translation>الرجاء إدخال بطاقة بيانات CPAP ...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="948"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>تم حظر الوصول إلى الاستيراد أثناء إعادة الحساب.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1000"/>
        <source>CPAP Data Located</source>
        <translation>بيانات CPAP تقع</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1035"/>
        <source>Import Reminder</source>
        <translation>استيراد تذكير</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1108"/>
        <source>Importing Data</source>
        <translation>استيراد البيانات</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1988"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation>يرجى ملاحظة أن هذا قد يؤدي إلى فقدان البيانات إذا تم تعطيل النسخ الاحتياطية لـ OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2752"/>
        <source>Export review is not yet implemented</source>
        <translation>لم يتم تنفيذ مراجعة التصدير بعد</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2762"/>
        <source>Would you like to zip this card?</source>
        <translation>هل تريد ضغط هذه البطاقة؟</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2784"/>
        <location filename="../oscar/mainwindow.cpp" line="2836"/>
        <source>Choose where to save zip</source>
        <translation>اختر مكان حفظ الملف المضغوط</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2784"/>
        <location filename="../oscar/mainwindow.cpp" line="2836"/>
        <source>ZIP files (*.zip)</source>
        <translation>ملفات ZIP (* .zip)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2813"/>
        <location filename="../oscar/mainwindow.cpp" line="2864"/>
        <source>Creating zip...</source>
        <translation>جارٍ إنشاء ملف مضغوط ...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2855"/>
        <source>Calculating size...</source>
        <translation>حساب الحجم ...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2900"/>
        <source>Reporting issues is not yet implemented</source>
        <translation>لم يتم تنفيذ مشكلات الإبلاغ بعد</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1833"/>
        <location filename="../oscar/mainwindow.cpp" line="1860"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation>إذا كنت تستطيع قراءة هذا ، فلن يعمل أمر إعادة التشغيل. سيكون عليك القيام بذلك بنفسك يدويًا.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1985"/>
        <source>Are you sure you want to rebuild all CPAP data for the following machine:

</source>
        <translation>هل تريد بالتأكيد إعادة إنشاء جميع بيانات CPAP للجهاز التالي:

</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1995"/>
        <source>For some reason, OSCAR does not have any backups for the following machine:</source>
        <translation>لسبب ما ، لا يحتوي OSCAR على أي نسخ احتياطية للجهاز التالي:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2072"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s machine database for the following machine:&lt;/p&gt;</source>
        <translation>أنت على وشك &lt;font size = + 2&gt; طمس &lt;/font&gt; قاعدة بيانات الجهاز الخاصة بـ OSCAR للجهاز التالي: &lt;/ p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2130"/>
        <source>A file permission error casued the purge process to fail; you will have to delete the following folder manually:</source>
        <translation>تسبب خطأ إذن الملف في فشل عملية التطهير؛ سيكون عليك حذف المجلد التالي يدويًا:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2180"/>
        <source>No help is available.</source>
        <translation>لا يوجد مساعدة متاحة.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2730"/>
        <source>%1&apos;s Journal</source>
        <translation>مجلة %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2732"/>
        <source>Choose where to save journal</source>
        <translation>اختر مكان حفظ دفتر اليومية</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2732"/>
        <source>XML Files (*.xml)</source>
        <translation>ملفات XML (* .xml)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="261"/>
        <source>Help Browser</source>
        <translation>مساعدة المتصفح</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="519"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>تحميل ملف التعريف &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="558"/>
        <source>%1 (Profile: %2)</source>
        <translation>%1 (ملف تعريف: %2)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1034"/>
        <source>Please remember to select the root folder or drive letter of your data card, and not a folder inside it.</source>
        <translation>يرجى تذكر تحديد المجلد الجذر أو حرف محرك الأقراص لبطاقة البيانات ، وليس مجلدًا بداخلها.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1390"/>
        <source>Please open a profile first.</source>
        <translation>يرجى فتح ملف تعريف أولا.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1502"/>
        <source>Choose where to save screenshot</source>
        <translation>اختر مكان حفظ لقطة الشاشة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1502"/>
        <source>Image files (*.png)</source>
        <translation>ملفات الصور (* .png)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1997"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>شريطة أن تكون قد قمت بعمل &lt;i&gt; نسخ احتياطي &lt;b&gt; خاصة بك &lt;/ b&gt; لجميع بيانات CPAP الخاصة بك &lt;/ i&gt; ، فلا يزال بإمكانك إكمال هذه العملية ، لكن سيتعين عليك الاستعادة من النسخ الاحتياطية يدويًا.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1998"/>
        <source>Are you really sure you want to do this?</source>
        <translation>هل أنت متأكد أنك تريد فعل ذلك؟</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2013"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>نظرًا لعدم وجود نسخ احتياطية داخلية لإعادة الإنشاء منها ، سيتعين عليك الاستعادة من جهازك.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2014"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this machine until you do)</source>
        <translation>هل ترغب في الاستيراد من النسخ الاحتياطية الخاصة بك الآن؟ (لن يكون لديك بيانات مرئية لهذا الجهاز حتى تقوم بذلك)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2063"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>لاحظ كإجراء وقائي ، سيتم ترك مجلد النسخ الاحتياطي في مكانه.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2065"/>
        <source>OSCAR does not have any backups for this machine!</source>
        <translation>ليس لدى OSCAR أي نسخ احتياطية لهذا الجهاز!</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2066"/>
        <source>Unless you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your data for this machine&lt;/i&gt;, &lt;font size=+2&gt;you will lose this machine&apos;s data &lt;b&gt;permanently&lt;/b&gt;!&lt;/font&gt;</source>
        <translation>ما لم تقم بعمل نسخ احتياطية خاصة بك لجميع بياناتك لهذا الجهاز ، فستفقد بيانات هذا الجهاز بشكل دائم!</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2075"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>هل أنت &lt;b&gt; متأكد تمامًا &lt;/ b&gt; أنك تريد المتابعة؟</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2376"/>
        <source>Imported %1 ZEO session(s) from

%2</source>
        <translation>تم استيرا 1% جلسة (جلسات) ZEO من

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2382"/>
        <source>Already up to date with ZEO data at

%1</source>
        <translation>بالفعل حتى الآن مع بيانات ZEO في

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2384"/>
        <source>Couldn&apos;t find any valid ZEO CSV data at

%1</source>
        <translation>تعذر العثور على أي بيانات ZEO CSV صالحة على

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2407"/>
        <source>Imported %1 Dreem session(s) from

%2</source>
        <translation>تم استيرا 1% جلسة (جلسات) DREEM من

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2413"/>
        <source>Already up to date with Dreem data at

%1</source>
        <translation>بالفعل حتى الآن مع بيانات Dreem في

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2415"/>
        <source>Couldn&apos;t find any valid Dreem CSV data at

%1</source>
        <translation>تعذر العثور على أي بيانات%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2539"/>
        <source>Imported %1 oximetry session(s) from

%2</source>
        <translation>تم استيراد%1 جلسة (جلسات) لقياس التأكسج من

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2544"/>
        <source>Already up to date with oximetry data at

%1</source>
        <translation>بالفعل حتى الآن مع بيانات قياس التأكسج في

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2546"/>
        <source>Couldn&apos;t find any valid data at

%1</source>
        <translation>تعذر العثور على أي بيانات صالحة في

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2645"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>هل أنت متأكد من أنك تريد حذف بيانات مقياس التأكسج لـ %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2647"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt; يرجى العلم بأنه لا يمكنك التراجع عن هذه العملية! &lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2676"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>حدد اليوم مع بيانات oximetry صالحة في العرض اليومي أولاً.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="714"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>تم استيراد جلسة (جلسات) CPAP %1 من

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="714"/>
        <location filename="../oscar/mainwindow.cpp" line="2376"/>
        <location filename="../oscar/mainwindow.cpp" line="2407"/>
        <location filename="../oscar/mainwindow.cpp" line="2539"/>
        <source>Import Success</source>
        <translation>استيراد النجاح</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="716"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation>بالفعل حتى الآن مع بيانات CPAP في

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="716"/>
        <location filename="../oscar/mainwindow.cpp" line="2382"/>
        <location filename="../oscar/mainwindow.cpp" line="2413"/>
        <location filename="../oscar/mainwindow.cpp" line="2544"/>
        <source>Up to date</source>
        <translation>حتى الآن</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="718"/>
        <source>Couldn&apos;t find any valid Machine Data at

%1</source>
        <translation>تعذر العثور على أي بيانات صالحة عن الجهاز على

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="882"/>
        <source>Choose a folder</source>
        <translation>اختيار مجلد</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="944"/>
        <source>No profile has been selected for Import.</source>
        <translation>لم يتم اختيار ملف تعريف للاستيراد.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="952"/>
        <source>Import is already running in the background.</source>
        <translation>الاستيراد قيد التشغيل بالفعل في الخلفية.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="993"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>توجد بنية ملف %1 لـ %2 على:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="995"/>
        <source>A %1 file structure was located at:</source>
        <translation>تم تحديد بنية مل %1 على:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="960"/>
        <source>Would you like to import from this location?</source>
        <translation>هل ترغب في الاستيراد من هذا الموقع؟</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1004"/>
        <source>Specify</source>
        <translation>تحديد</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1395"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>تم حظر الوصول إلى التفضيلات حتى تكتمل عملية إعادة الحساب.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1452"/>
        <source>Updates are not yet implemented</source>
        <translation>لم يتم تنفيذ التحديثات بعد</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1512"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>حدث خطأ أثناء حفظ لقطة الشاشة لملف &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1514"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>تم حفظ لقطة الشاشة في الملف &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1576"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation>سيتم فتح دليل المستخدم في المستعرض الافتراضي الخاص بك</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1584"/>
        <source>The FAQ is not yet implemented</source>
        <translation>لم يتم تنفيذ التعليمات</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2437"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>حدثت مشكلة أثناء فتح ملف :MSeries block</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2441"/>
        <source>MSeries Import complete</source>
        <translation>MSeries استيراد كاملة</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2452"/>
        <source>The Glossary will open in your default browser</source>
        <translation>سيتم فتح المسرد في المستعرض الافتراضي الخاص بك</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2500"/>
        <source>There was a problem opening Somnopose Data File: </source>
        <translation>حدثت مشكلة أثناء فتح ملف بيانات :Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2504"/>
        <source>Somnopause Data Import complete</source>
        <translation>اكتمال عملية استيراد البيانات</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2909"/>
        <source>OSCAR Information</source>
        <translation>معلومات OSCAR</translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2019"/>
        <source>Auto-Fit</source>
        <translation>لصناعة السيارات في صالح</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2020"/>
        <source>Defaults</source>
        <translation>التخلف</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2021"/>
        <source>Override</source>
        <translation>تجاوز</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2022"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>وضع التحجيم Y-Axis ، &quot;الاحتواء التلقائي&quot; للتحجيم التلقائي ، &quot;الإعدادات الافتراضية&quot; للإعدادات وفقًا للشركة المصنعة ، و &quot;التجاوز&quot; لاختيار إعداداتك.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2028"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>الحد الأدنى لقيمة المحور ص .. لاحظ أن هذا يمكن أن يكون رقمًا سالبًا إذا كنت ترغب في ذلك.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2029"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>القيمة القصوى لمحور Y .. يجب أن تكون أكبر من الحد الأدنى للعمل.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2064"/>
        <source>Scaling Mode</source>
        <translation>وضع التحجيم</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2086"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>يعيد هذا الزر ضبط Min و Max ليتناسب مع Auto-Fit</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>تحرير ملف تعريف المستخدم</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="70"/>
        <source>I agree to all the conditions above.</source>
        <translation>أوافق على جميع الشروط المذكورة أعلاه.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="111"/>
        <source>User Information</source>
        <translation>معلومات المستخدم</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="137"/>
        <source>User Name</source>
        <translation>اسم المستخدم</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="152"/>
        <source>Keep the kids out.. Nothing more.. This isn&apos;t meant to be uber security.</source>
        <translation>أبقِ الأطفال خارجًا .. لا شيء أكثر .. هذا ليس المقصود منه أن يكون هناك إجراءات أمنية مشددة.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Password Protect Profile</source>
        <translation>كلمة السر حماية الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="182"/>
        <source>Password</source>
        <translation>كلمه السر</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="196"/>
        <source>...twice...</source>
        <translation>...مرتين...</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="213"/>
        <source>Locale Settings</source>
        <translation>الإعدادات المحلية</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="285"/>
        <source>Country</source>
        <translation>بلد</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="253"/>
        <source>TimeZone</source>
        <translation>وحدة زمنية</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="240"/>
        <source>DST Zone</source>
        <translation>التوقيت الصيفي</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="320"/>
        <source>Personal Information (for reports)</source>
        <translation>المعلومات الشخصية (للتقارير)</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="344"/>
        <source>First Name</source>
        <translation>الاسم الاول</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="354"/>
        <source>Last Name</source>
        <translation>الكنية</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="364"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>من المقبول تمامًا تخطي ذلك أو تخطي ذلك ، ولكن سنك القاسي ضروري لتعزيز دقة بعض الحسابات.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>D.O.B.</source>
        <translation>D.O.B.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="383"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt; &lt;p&gt; يلزم في بعض الأحيان الجنس (الولادة) البيولوجي لتعزيز دقة بعض الحسابات ، ولا تتردد في ترك هذا فارغًا وتخطي أيًا منها. &lt;/ p&gt; &lt;/body&gt; &lt;/ HTML&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>Gender</source>
        <translation>جنس</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="399"/>
        <source>Male</source>
        <translation>الذكر</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="404"/>
        <source>Female</source>
        <translation>أنثى</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="420"/>
        <source>Height</source>
        <translation>ارتفاع</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="459"/>
        <source>Metric</source>
        <translation>قياس</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="464"/>
        <source>English</source>
        <translation>الإنجليزية</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="477"/>
        <source>Contact Information</source>
        <translation>معلومات الاتصال</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="504"/>
        <location filename="../oscar/newprofile.ui" line="779"/>
        <source>Address</source>
        <translation>عنوان</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="521"/>
        <location filename="../oscar/newprofile.ui" line="810"/>
        <source>Email</source>
        <translation>البريد الإلكتروني</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="531"/>
        <location filename="../oscar/newprofile.ui" line="800"/>
        <source>Phone</source>
        <translation>هاتف</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="576"/>
        <source>CPAP Treatment Information</source>
        <translation>معلومات علاج CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="603"/>
        <source>Date Diagnosed</source>
        <translation>تاريخ التشخيص</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="617"/>
        <source>Untreated AHI</source>
        <translation>غير المعالجة AHI</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="631"/>
        <source>CPAP Mode</source>
        <translation>وضع CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="639"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="644"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="649"/>
        <source>Bi-Level</source>
        <translation>مستوى ثنائي</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="654"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="662"/>
        <source>RX Pressure</source>
        <translation>ضغط آر إكس</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="708"/>
        <source>Doctors / Clinic Information</source>
        <translation>معلومات الأطباء / العيادة</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="735"/>
        <source>Doctors Name</source>
        <translation>اسم الطبيب</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="752"/>
        <source>Practice Name</source>
        <translation>اسم الممارسة</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="762"/>
        <source>Patient ID</source>
        <translation>رقم المريض</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="877"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="955"/>
        <source>&amp;Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="971"/>
        <source>&amp;Back</source>
        <translation>عودة</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="987"/>
        <location filename="../oscar/newprofile.cpp" line="276"/>
        <location filename="../oscar/newprofile.cpp" line="285"/>
        <source>&amp;Next</source>
        <translation>التالى</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="63"/>
        <source>Select Country</source>
        <translation>حدد الدولة</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>مرحبًا بكم في مراسل تحليل CPAP مفتوح المصدر (OSCAR)</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="113"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP machines and related equipment.</source>
        <translation>تم تصميم هذا البرنامج لمساعدتك في مراجعة البيانات التي تنتجها أجهزة CPAP والأجهزة ذات الصلة.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="118"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>يرجى قراءة بعناية</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>دقة أي من البيانات المعروضة ليست ولا يمكن ضمانه.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="124"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>أي تقارير يتم إنشاؤها مخصصة للاستخدام الشخصي فقط ، وليست مناسبة بأي حال من الأحوال للامتثال أو لأغراض التشخيص الطبي.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="131"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>استخدام هذا البرنامج بالكامل على مسؤوليتك الخاصة.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation>تم إصدار OSCAR بحرية بموجب &lt;a href=&apos;qrc:/COPYING&apos;&gt; GNU Public License v3 &lt;/a&gt; ، ولا يأتي مع أي ضمان ، وبدون أي مطالبات بالرشاقة لأي غرض من الأغراض.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="119"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation>الغرض من OSCAR هو مجرد عارض للبيانات ، وبالتأكيد ليس بديلاً عن التوجيه الطبي المختص من طبيبك.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="127"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation>لن يتحمل المؤلفون مسؤولية &lt;u&gt; أي شيء &lt;/ u&gt; يتعلق باستخدام أو سوء استخدام هذا البرنامج.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="134"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019 Nightowl Software</source>
        <translation>OSCAR هي حقوق الطبع والنشر 2011-2018 مارك واتكينز وأجزاء 2019 Nightowl Software</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="157"/>
        <source>Please provide a username for this profile</source>
        <translation>يرجى تقديم اسم مستخدم لملف التعريف هذا</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="167"/>
        <source>Passwords don&apos;t match</source>
        <translation>كلمات المرور غير متطابقة</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Profile Changes</source>
        <translation>التغييرات الشخصية</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Accept and save this information?</source>
        <translation>قبول وحفظ هذه المعلومات؟</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="274"/>
        <source>&amp;Finish</source>
        <translation>إنهاء</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="450"/>
        <source>&amp;Close this window</source>
        <translation>أغلق هذه النافذة</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="14"/>
        <source>Form</source>
        <translation>شكل</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>نطاق:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>الاسبوع الماضى</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>الاسبوعين الماضيين</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>الشهر الماضي</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>آخر شهرين</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>آخر ثلاثة أشهر</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>آخر 6 أشهر</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>العام الماضي</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>كل شىء</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>مخصص</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="130"/>
        <source>Start:</source>
        <translation>بداية:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="153"/>
        <source>End:</source>
        <translation>النهاية:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="176"/>
        <source>Reset view to selected date range</source>
        <translation>إعادة تعيين العرض إلى نطاق التاريخ المحدد</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="195"/>
        <location filename="../oscar/overview.ui" line="244"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="225"/>
        <source>Toggle Graph Visibility</source>
        <translation>تبديل رؤية الرسم البياني</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="260"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>المنسدلة لرؤية قائمة من الرسوم البيانية لتشغيل / إيقاف.</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="267"/>
        <source>Graphs</source>
        <translation>الرسوم البيانية</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="185"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation>تنفسي
إزعاج
فهرس</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="187"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation>توقف التنفس أثناء
ضعف التنفس
فهرس</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="193"/>
        <source>Usage</source>
        <translation>استعمال</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="193"/>
        <source>Usage
(hours)</source>
        <translation>استعمال
(ساعات)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="196"/>
        <source>Session Times</source>
        <translation>أوقات الجلسة</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="204"/>
        <source>Total Time in Apnea</source>
        <translation>الوقت الإجمالي في انقطاع النفس</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="204"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>الوقت الإجمالي في انقطاع النفس
(الدقائق)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="239"/>
        <source>Body
Mass
Index</source>
        <translation>الجسم
كتلة
فهرس</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="240"/>
        <source>How you felt
(0-10)</source>
        <translation>كيف شعرت
(0-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="579"/>
        <source>Show all graphs</source>
        <translation>عرض كل الرسوم البيانية</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="592"/>
        <source>Hide all graphs</source>
        <translation>إخفاء جميع الرسوم البيانية</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="14"/>
        <source>Dialog</source>
        <translation>الحوار</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="37"/>
        <source>Oximeter Import Wizard</source>
        <translation>مقياس التأكسج استيراد معالج</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="768"/>
        <source>Skip this page next time.</source>
        <translation>تخطي هذه الصفحة في المرة القادمة.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="866"/>
        <source>Where would you like to import from?</source>
        <translation>من أين تريد الاستيراد من؟</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="921"/>
        <source>CMS50Fv3.7+/H/I, CMS50D+v4.6, Pulox PO-400/500</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="967"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation>مستخدمو CMS50E / F ، عند الاستيراد مباشرة ، يرجى عدم تحديد تحميل على جهازك حتى يطالبك OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1004"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;إذا تم تمكينه ، فسيقوم OSCAR تلقائيًا بإعادة ضبط الساعة الداخلية لـ CMS50 باستخدام الوقت الحالي لأجهزة الكمبيوتر.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1036"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;هنا يمكنك إدخال اسم مكون من 7 أحرف لهذا التأكسج.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1077"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  because if something goes wrong before OSCAR saves your session, you can&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;هذا الخيار سوف يمحو الجلسة المستوردة من مقياس التأكسج الخاص بك بعد اكتمال الاستيراد. &lt;/p&gt; &lt;p&gt; استخدم بحذر ، لأنه إذا حدث خطأ ما قبل حفظ OSCAR لجلستك ، فلن تتمكن من استعادته.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1106"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, old Contec oximeters will require you to use the device&apos;s menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;يسمح لك هذا الخيار باستيراد (عبر الكابل) من التسجيلات الداخلية لمقاييس التأكسج. بعد تحديد هذا الخيار ، سيتطلب منك مقياس التأكسج القديم في Contec استخدام قائمة الجهاز لبدء التحميل.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;إذا كنت لا تمانع في أن تكون مرتبطًا بجهاز كمبيوتر يعمل بين عشية وضحاها ، فإن هذا الخيار يوفر رسمًا بيانيًا تخطيطيًا plethysomogram مفيدًا ، والذي يعطي إشارة إلى إيقاع القلب ، أعلى قراءات التأكسج الطبيعية.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1154"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>سجل مرفق بجهاز الكمبيوتر طوال الليل (يوفر plethysomnogram)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;يسمح لك هذا الخيار بالاستيراد من ملفات البيانات التي تم إنشاؤها بواسطة البرنامج الذي يأتي مع مقياس التأكسج النبضي ، مثل SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1193"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>استيراد من ملف بيانات تم حفظه بواسطة برنامج آخر ، مثل SpO2Review</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1272"/>
        <source>Please connect your oximeter device</source>
        <translation>يرجى توصيل جهاز قياس التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1290"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>إذا كنت تستطيع قراءة هذا ، فمن المحتمل أن يكون لديك نوع مقياس التأكسج مضبوطًا في التفضيلات.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1319"/>
        <source>Please connect your oximeter device, turn it on, and enter the menu</source>
        <translation>يرجى توصيل جهاز قياس التأكسج ، وتشغيله ، وإدخال القائمة</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1345"/>
        <source>Press Start to commence recording</source>
        <translation>اضغط على &quot;ابدأ&quot; لبدء التسجيل</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1391"/>
        <source>Show Live Graphs</source>
        <translation>عرض الرسوم البيانية الحية</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1422"/>
        <location filename="../oscar/oximeterimport.ui" line="1676"/>
        <source>Duration</source>
        <translation>المدة الزمنية</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1458"/>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1540"/>
        <source>Pulse Rate</source>
        <translation>معدل النبض</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1632"/>
        <source>Multiple Sessions Detected</source>
        <translation>تم الكشف عن جلسات متعددة</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1671"/>
        <source>Start Time</source>
        <translation>وقت البدء</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1681"/>
        <source>Details</source>
        <translation>تفاصيل</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1698"/>
        <source>Import Completed. When did the recording start?</source>
        <translation>اكتمل الاستيراد. متى بدأ التسجيل؟</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1722"/>
        <source>Day recording (normally would of) started</source>
        <translation>بدأ التسجيل اليومي (عادة سيكون من)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1768"/>
        <source>Oximeter Starting time</source>
        <translation>مقياس التأكسج وقت البدء</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1780"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>أرغب في استخدام الوقت الذي تم الإبلاغ عنه بواسطة مقياس التأكسج الخاص بي المدمج في الساعة.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1796"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP machine.</source>
        <translation>لقد بدأت في تسجيل مقياس التأكسج هذا (أو بالقرب منه) في نفس الوقت كجلسة على جهاز CPAP الخاص بي.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1857"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ملاحظة: ستكون المزامنة مع وقت بدء جلسة CPAP أكثر دقة دائمًا.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1878"/>
        <source>Choose CPAP session to sync to:</source>
        <translation>اختر جلسة CPAP للمزامنة مع:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1917"/>
        <location filename="../oscar/oximeterimport.ui" line="1956"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1998"/>
        <source>You can manually adjust the time here if required:</source>
        <translation>يمكنك ضبط الوقت هنا يدويًا إذا لزم الأمر:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2019"/>
        <source>HH:mm:ssap</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2116"/>
        <source>&amp;Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2097"/>
        <source>&amp;Information Page</source>
        <translation>صفحة المعلومات</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="823"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Make sure your correct oximeter type is selected otherwise import will fail.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;يرجى الملاحظة: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;تأكد من تحديد نوع مقياس التأكسج الصحيح وإلا فستفشل عملية الاستيراد.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="910"/>
        <source>Select Oximeter Type:</source>
        <translation>اختر نوع مقياس التأكسج:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="926"/>
        <source>CMS50D+/E/F, Pulox PO-200/300</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="931"/>
        <source>ChoiceMMed MD300W1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1007"/>
        <source>Set device date/time</source>
        <translation>ضبط تاريخ الجهاز / الوقت</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1014"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;تحقق لتمكين تحديث معرف الجهاز التالي للاستيراد ، وهو أمر مفيد لأولئك الذين لديهم أجهزة تأكسج متعددة.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1017"/>
        <source>Set device identifier</source>
        <translation>ضبط معرف الجهاز</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1080"/>
        <source>Erase session after successful upload</source>
        <translation>محو الجلسة بعد نجاح التحميل</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1112"/>
        <source>Import directly from a recording on a device</source>
        <translation>استيراد مباشرة من تسجيل على جهاز</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1237"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;تذكير لمستخدمي CPAP:&lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;هل تذكر أن تستورد جلسات CPAP أولاً؟&lt;br/&gt;&lt;/span&gt;إذا نسيت ، فلن يكون لديك وقت صالح لمزامنة جلسة التأكسج هذه مع.&lt;br/&gt;لضمان مزامنة جيدة بين الأجهزة ، حاول دائمًا تشغيل كليهما في نفس الوقت.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1648"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation>يرجى اختيار أي واحد تريد استيراده إلى OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1825"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;يحتاج OSCAR إلى وقت بدء لمعرفة مكان حفظ جلسة التأكسج هذه إلى.&lt;p&gt;&lt;/p&gt;&lt;p&gt;اختيار واحد من الخيارات التالية:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2135"/>
        <source>&amp;Retry</source>
        <translation>وإعادة المحاولة</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2154"/>
        <source>&amp;Choose Session</source>
        <translation>اختيار الجلسة</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2173"/>
        <source>&amp;End Recording</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2192"/>
        <source>&amp;Sync and Save</source>
        <translation>وتسجيل نهاية</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2211"/>
        <source>&amp;Save and Finish</source>
        <translation>حفظ وإنهاء</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2230"/>
        <source>&amp;Start</source>
        <translation>بداية</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="188"/>
        <source>Scanning for compatible oximeters</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="220"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>المسح الضوئي لمقاييس التأكسج المتوافقة.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="228"/>
        <source>Connecting to %1 Oximeter</source>
        <translation>الاتصال بـ %1 Oximeter</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="256"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>إعادة تسمية مقياس التأكسج من &apos;%1&apos; إلى &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="259"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation>يختلف اسم Oximeter .. إذا كان لديك واحدًا فقط وتقوم بمشاركته بين ملفات التعريف ، فاضبط الاسم على كل من ملفي التعريف.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="302"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation>&quot;%1&quot; ، الجلسة %2</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="327"/>
        <source>Nothing to import</source>
        <translation>لا شيء للاستيراد</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="328"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>لم يكن لديك مقياس التأكسج أي جلسات صالحة.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="329"/>
        <source>Close</source>
        <translation>قريب</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="332"/>
        <source>Waiting for %1 to start</source>
        <translation>في انتظار %1 لبدء</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="333"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation>في انتظار أن يبدأ الجهاز عملية التحميل ...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="335"/>
        <source>Select upload option on %1</source>
        <translation>حدد خيار التحميل على %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="336"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>تحتاج إلى إخبار مقياس التأكسج لديك لبدء إرسال البيانات إلى الكمبيوتر.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="337"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation>يرجى توصيل مقياس التأكسج ، وإدخاله في القائمة وتحديد التحميل لبدء نقل البيانات ...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="367"/>
        <source>%1 device is uploading data...</source>
        <translation>يقوم %1 جهاز بتحميل البيانات ...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="368"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>الرجاء الانتظار حتى تكتمل عملية تحميل مقياس التأكسج. لا افصل مقياس التأكسج.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="387"/>
        <source>Oximeter import completed..</source>
        <translation>اكتمال استيراد مقياس التأكسج ..</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="411"/>
        <source>Select a valid oximetry data file</source>
        <translation>حدد ملف بيانات قياس صالح</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="411"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>ملفات قياس التأكسج (* .spo * .spor * .spo2 * .SpO2 * .dat)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="433"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation>لا توجد وحدة لقياس التأكسج يمكنها تحليل الملف المحدد:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="478"/>
        <source>Live Oximetry Mode</source>
        <translation>وضع قياس التأكسج الحي</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="530"/>
        <source>Live Oximetry Stopped</source>
        <translation>Oximetry لايف متوقف</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="531"/>
        <source>Live Oximetry import has been stopped</source>
        <translation>تم إيقاف استيراد مقياس التأكسج المباشر</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1091"/>
        <source>Oximeter Session %1</source>
        <translation>جلسة التأكسج %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1136"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation>يمنحك OSCAR القدرة على تتبع بيانات Oximetry جنبًا إلى جنب مع بيانات جلسة CPAP ، والتي يمكن أن تعطي نظرة ثاقبة حول فعالية علاج CPAP. سيعمل أيضًا بشكل مستقل مع مقياس تأكسج نبضك ، مما يتيح لك تخزين وتتبع ومراجعة البيانات المسجلة.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1147"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>إذا كنت تحاول مزامنة قياس التأكسج وبيانات CPAP ، فيرجى التأكد من استيراد جلسات CPAP أولاً قبل المتابعة!</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1150"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation>لكي يتمكن OSCAR من تحديد موقع وقراءة مباشرة من جهاز Oximeter ، تحتاج إلى التأكد من تثبيت برامج تشغيل الجهاز الصحيحة (على سبيل المثال ، USB إلى Serial UART) على جهاز الكمبيوتر الخاص بك. لمزيد من المعلومات حول هذا ، %1 انقر هنا %2.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="460"/>
        <source>Oximeter not detected</source>
        <translation>مقياس التأكسج لم يتم الكشف عنه</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="467"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation>لا يمكن الوصول إلى مقياس التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="481"/>
        <source>Starting up...</source>
        <translation>بدء...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="482"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>إذا كان لا يزال بإمكانك قراءة هذا بعد بضع ثوانٍ ، فقم بالإلغاء والمحاولة مرة أخرى</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="529"/>
        <source>Live Import Stopped</source>
        <translation>استيراد لايف متوقف</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="582"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%1 جلسة (جلسات) على %2 ، تبدأ من %3</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="586"/>
        <source>No CPAP data available on %1</source>
        <translation>لا تتوفر بيانات CPAP على %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="594"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="721"/>
        <source>Recording...</source>
        <translation>تسجيل...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="728"/>
        <source>Finger not detected</source>
        <translation>الاصبع لم يتم اكتشافه</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="828"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>أريد استخدام الوقت الذي سجله جهاز الكمبيوتر الخاص بي لجلسة قياس التأكسج المباشرة هذه.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="831"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>أحتاج إلى ضبط الوقت يدويًا ، لأن مقياس التأكسج لا يحتوي على ساعة داخلية.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="843"/>
        <source>Something went wrong getting session data</source>
        <translation>حدث خطأ ما في الحصول على بيانات الجلسة</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1132"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>مرحبًا بك في معالج استيراد Oximeter</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1134"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>مقياس تأكسج النبض عبارة عن أجهزة طبية تستخدم لقياس تشبع الأكسجين في الدم. خلال أحداث انقطاع النفس الموسعة وأنماط التنفس غير الطبيعية ، يمكن أن تنخفض مستويات تشبع الأكسجين في الدم بشكل كبير ، ويمكن أن تشير إلى المشكلات التي تحتاج إلى عناية طبية.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1138"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation>OSCAR متوافق حاليًا مع مقياس التأكسج التسلسلي لـ Contec CMS50D + و CMS50E و CMS50F و CMS50I.&lt;br/&gt;(ملاحظة: الاستيراد المباشر من طرز البلوتوث هو&lt;span style=&quot; font-weight:600;&quot;&gt;على الاغلب لا&lt;/span&gt;ممكن بعد)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1140"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>قد ترغب في ملاحظة أن شركات أخرى ، مثل Pulox ، تقوم ببساطة بإعادة تكوين Contec CMS50 تحت أسماء جديدة ، مثل Pulox PO-200 ، PO-300 ، PO-400. هذه يجب أن تعمل أيضا.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1143"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>كما يمكن قراءتها من ChoiceMMed MD300W1 oximeter .dat الملفات.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1145"/>
        <source>Please remember:</source>
        <translation>أرجوك تذكر:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1149"/>
        <source>Important Notes:</source>
        <translation>ملاحظات هامة:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1152"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>لا تملك أجهزة + Contec CMS50D ساعة داخلية ، ولا تسجل وقت بدء. إذا لم يكن لديك جلسة CPAP لربط التسجيل بها ، فسيتعين عليك إدخال وقت البدء يدويًا بعد اكتمال عملية الاستيراد.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1154"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>حتى بالنسبة للأجهزة المزودة بساعة داخلية ، لا يزال من المستحسن الدخول في عادة بدء تشغيل سجلات مقياس التأكسج في نفس وقت جلسات CPAP ، لأن الساعات الداخلية لـ CPAP تميل إلى الانجراف بمرور الوقت ، ولا يمكن إعادة تعيينها جميعها بسهولة.</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="14"/>
        <source>Form</source>
        <translation>شكل</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>تاريخ</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation>إعادة تعيين</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="160"/>
        <source>SpO2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>نبض</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="346"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation>افتح. spo/R ملف</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation>استيراد المسلسل</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation>بدء لايف</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>منفذ تسلسلي</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation>منافذ إعادة التفتيش</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="23"/>
        <source>Preferences</source>
        <translation>تفضيلات</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="64"/>
        <source>&amp;Import</source>
        <translation>استيراد</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="154"/>
        <source>Combine Close Sessions </source>
        <translation>الجمع بين جلسات مغلقة </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="164"/>
        <location filename="../oscar/preferencesdialog.ui" line="249"/>
        <location filename="../oscar/preferencesdialog.ui" line="752"/>
        <source>Minutes</source>
        <translation>الدقائق</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="184"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>سيتم الاحتفاظ بجلسات متعددة أقرب من هذه القيمة في نفس اليوم.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="239"/>
        <source>Ignore Short Sessions</source>
        <translation>تجاهل الجلسات القصيرة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="266"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;لن يتم عرض الجلسات الأقصر من المدة&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="310"/>
        <source>Day Split Time</source>
        <translation>يوم سبليت الوقت</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="320"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>ستذهب الجلسات التي تبدأ قبل هذا الوقت إلى اليوم الميلادي السابق.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="393"/>
        <source>Session Storage Options</source>
        <translation>خيارات تخزين الجلسة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="422"/>
        <source>Changing SD Backup compression options doesn&apos;t automatically recompress backup data.</source>
        <translation>لا يؤدي تغيير خيارات ضغط نسخ احتياطي SD إلى إعادة ضغط بيانات النسخ الاحتياطي تلقائيًا.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="440"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>ضغط النسخ الاحتياطي لبطاقة SD (الاستيراد الأول أبطأ ، ولكن يجعل النسخ الاحتياطية أصغر)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="658"/>
        <source>&amp;CPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1301"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation>تعتبر الأيام مع تحت هذا الاستخدام بأنها &quot;غير مكتملة&quot;. عادة ما تعتبر 4 ساعات متوافقة.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1304"/>
        <source> hours</source>
        <translation> ساعات</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="966"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the machine missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation>تمكين / تعطيل تحسينات الإبلاغ عن الحدث التجريبي.
انها تسمح للكشف عن أحداث الشريط الحدودي ، وبعض آلة غاب.
يجب تمكين هذا الخيار قبل الاستيراد ، وإلا فالتطهير مطلوب.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1057"/>
        <source>Flow Restriction</source>
        <translation>تقييد التدفق</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1098"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation>النسبة المئوية للقيود المفروضة على تدفق الهواء من القيمة المتوسطة.
قيمة 20 ٪ تعمل بشكل جيد للكشف عن انقطاع النفس. </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1008"/>
        <location filename="../oscar/preferencesdialog.ui" line="1102"/>
        <location filename="../oscar/preferencesdialog.ui" line="1579"/>
        <location filename="../oscar/preferencesdialog.ui" line="1739"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1075"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the machine. They are &lt;span style=&quot; text-decoration: underline;&quot;&gt;not&lt;/span&gt; included in AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;وضع علامة مخصصة هي طريقة تجريبية للكشف عن الأحداث التي فاتتها الآلة. هم انهم&lt;span style=&quot; text-decoration: underline;&quot;&gt;ليس&lt;/span&gt; المدرجة في AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1119"/>
        <source>Duration of airflow restriction</source>
        <translation>مدة تقييد تدفق الهواء</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="998"/>
        <location filename="../oscar/preferencesdialog.ui" line="1122"/>
        <location filename="../oscar/preferencesdialog.ui" line="1596"/>
        <location filename="../oscar/preferencesdialog.ui" line="1684"/>
        <location filename="../oscar/preferencesdialog.ui" line="1713"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1158"/>
        <source>Event Duration</source>
        <translation>مدة الحدث</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1145"/>
        <source>Allow duplicates near machine events.</source>
        <translation>السماح بالتكرارات بالقرب من أحداث الجهاز.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1223"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>يضبط مقدار البيانات التي تم النظر فيها لكل نقطة في الرسم البياني AHI / Hour.
الافتراضات إلى 60 دقيقة .. نوصي بشدة تركها عند هذه القيمة.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1227"/>
        <source> minutes</source>
        <translation> الدقائق</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1266"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>إعادة تعيين العداد إلى صفر في بداية كل نافذة (الوقت).</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1269"/>
        <source>Zero Reset</source>
        <translation>إعادة تعيين صفر</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="705"/>
        <source>CPAP Clock Drift</source>
        <translation>الانجراف CPAP على مدار الساعة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="501"/>
        <source>Do not import sessions older than:</source>
        <translation>لا تستورد جلسات أقدم من:</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="508"/>
        <source>Sessions older than this date will not be imported</source>
        <translation>لن يتم استيراد الجلسات الأقدم من هذا التاريخ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="534"/>
        <source>dd MMMM yyyy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1282"/>
        <source>User definable threshold considered large leak</source>
        <translation>عتبة تعريف المستخدم تعتبر تسرب كبير</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1285"/>
        <source> L/min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1249"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>سواء لإظهار الخط الأحمر للتسرب في الرسم البياني للتسرب</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1826"/>
        <location filename="../oscar/preferencesdialog.ui" line="1905"/>
        <source>Search</source>
        <translation>بحث</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1522"/>
        <source>&amp;Oximetry</source>
        <translation>التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1112"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation>تظهر في مخطط انهيار مخطط الأحداث</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1135"/>
        <source>#1</source>
        <translation>#1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1041"/>
        <source>#2</source>
        <translation>#2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1021"/>
        <source>Resync Machine Detected Events (Experimental)</source>
        <translation>الأحداث التي تم اكتشافها في آلة إعادة المزامنة (تجريبية)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1674"/>
        <source>SPO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1736"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation>انخفاض النسبة المئوية في تشبع الأكسجين</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1729"/>
        <source>Pulse</source>
        <translation>نبض</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1694"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>التغيير المفاجئ في معدل النبض لا يقل عن هذا المبلغ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1586"/>
        <location filename="../oscar/preferencesdialog.ui" line="1616"/>
        <location filename="../oscar/preferencesdialog.ui" line="1697"/>
        <source> bpm</source>
        <translation> نبضة في الدقيقة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1681"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>الحد الأدنى لمدة انخفاض في تشبع الأكسجين</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1710"/>
        <source>Minimum duration of pulse change event.</source>
        <translation>الحد الأدنى لمدة الحدث تغيير النبض.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1593"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>سيتم تجاهل أجزاء صغيرة من بيانات قياس التأكسج تحت هذا المبلغ.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1961"/>
        <source>&amp;General</source>
        <translation>جنرال لواء</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1349"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>التغييرات في الإعدادات التالية تحتاج إلى إعادة تشغيل ، ولكن ليس إعادة حساب.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1352"/>
        <source>Preferred Calculation Methods</source>
        <translation>طرق الحساب المفضلة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1381"/>
        <source>Middle Calculations</source>
        <translation>الحسابات الوسطى</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1395"/>
        <source>Upper Percentile</source>
        <translation>النسبة المئوية العليا</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="91"/>
        <source>Session Splitting Settings</source>
        <translation>إعدادات تقسيم الجلسة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="356"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;يجب استخدام هذا الإعداد بحذر ...&lt;/span&gt;يأتي إيقاف تشغيله مع عواقب تتضمن دقة ملخص أيام فقط ، حيث تعمل بعض الحسابات فقط بشكل صحيح بشرط أن يتم الاحتفاظ فقط بجلسات الملخص التي جاءت من سجلات اليوم الفردية.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;المستخدمين ResMed:&lt;/span&gt;لمجرد أنه يبدو من الطبيعي لك ولأنه يجب أن تتم إعادة تشغيل جلسة 12 ظهراً في اليوم السابق ، فهذا لا يعني أن بيانات ResMed تتفق معنا. يحتوي تنسيق فهرس الملخص STF.edf على نقاط ضعف خطيرة تجعل القيام بذلك ليس فكرة جيدة.&lt;/p&gt;&lt;p&gt;يوجد هذا الخيار لتهدئة أولئك الذين لا يهتمون ويريدون رؤية هذا &quot;ثابت&quot; بغض النظر عن التكاليف ، ولكن معرفة أنه يأتي مع تكلفة. إذا احتفظت ببطاقة SD الخاصة بك في كل ليلة ، وقمت بالاستيراد مرة واحدة على الأقل في الأسبوع ، فلن تظهر لك مشاكل في كثير من الأحيان.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="359"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>لا تنقسم أيام الملخص (تحذير: اقرأ تلميح الأدوات!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="568"/>
        <source>Memory and Startup Options</source>
        <translation>خيارات الذاكرة وبدء التشغيل</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="610"/>
        <source>Pre-Load all summary data at startup</source>
        <translation>قبل تحميل جميع البيانات الموجزة عند بدء التشغيل</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="597"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;يحتفظ هذا الإعداد ببيانات الطول الموجي والحدث في الذاكرة بعد استخدامها لتسريع أيام الزيارة. هذا ليس حقًا خيارًا ضروريًا ، لأن نظام التشغيل الخاص بك يقوم بتخزين الملفات المستخدمة مسبقًا أيضًا. التوصية هي إيقاف تشغيله ، ما لم يكن الكمبيوتر يحتوي على طن من الذاكرة.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="600"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation>الحفاظ على بيانات الموجي / الحدث في الذاكرة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="624"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;يخفض من أي مربعات حوار تأكيد غير مهم أثناء الاستيراد.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="627"/>
        <source>Import without asking for confirmation</source>
        <translation>استيراد دون طلب التأكيد</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1187"/>
        <source>General CPAP and Related Settings</source>
        <translation>CPAP العامة والإعدادات ذات الصلة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1196"/>
        <source>Enable Unknown Events Channels</source>
        <translation>تمكين قنوات أحداث غير معروفة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1327"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1332"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1203"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation>AHI / ساعة الرسم البياني الوقت النافذة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1259"/>
        <source>Preferred major event index</source>
        <translation>مؤشر الحدث الرئيسي المفضل</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1210"/>
        <source>Compliance defined as</source>
        <translation>الامتثال كما هو محدد</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1252"/>
        <source>Flag leaks over threshold</source>
        <translation>تسرب العلم فوق العتبة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="786"/>
        <source>Seconds</source>
        <translation>ثواني</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="732"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ملاحظة: هذا غير مخصص للتصحيحات الزمنية! تأكد من ضبط ساعة ونظام التشغيل الخاصين بك بشكل صحيح.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="779"/>
        <source>Hours</source>
        <translation>ساعات</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="818"/>
        <source>Your masks vent rate at 20 cmH2O pressure</source>
        <translation>معدل تنفيس الأقنعة عند ضغط 20 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="874"/>
        <source>Your masks vent rate at 4 cmH2O pressure</source>
        <translation>تنفيس الأقنعة الخاصة بك عند ضغط 4 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1358"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation>للتناسق ، يجب على مستخدمي ResMed استخدام 95 ٪ هنا ،
لأن هذه هي القيمة الوحيدة المتاحة في الأيام الموجزة فقط.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1409"/>
        <source>Median is recommended for ResMed users.</source>
        <translation>ينصح الوسيط لمستخدمي ResMed.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1413"/>
        <location filename="../oscar/preferencesdialog.ui" line="1476"/>
        <source>Median</source>
        <translation>الوسيط</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1418"/>
        <source>Weighted Average</source>
        <translation>متوسط الوزن</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1423"/>
        <source>Normal Average</source>
        <translation>متوسط عادي</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1447"/>
        <source>True Maximum</source>
        <translation>الحد الأقصى الحقيقي</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1452"/>
        <source>99% Percentile</source>
        <translation>99 ٪ في المئة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1388"/>
        <source>Maximum Calcs</source>
        <translation>الحسابات القصوى</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1975"/>
        <source>General Settings</source>
        <translation>الاعدادات العامة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2731"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>ستتخطى أزرار التنقل في العرض اليومي على مدار أيام بدون سجلات بيانات</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2734"/>
        <source>Skip over Empty Days</source>
        <translation>تخطي أيام فارغة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1996"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation>السماح باستخدام نوى وحدة المعالجة المركزية المتعددة حيثما كان ذلك متاحًا لتحسين الأداء.
يؤثر بشكل رئيسي على المستورد.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2000"/>
        <source>Enable Multithreading</source>
        <translation>تمكين تعدد العمليات</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="574"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>تجاوز شاشة تسجيل الدخول وتحميل أحدث ملف تعريف المستخدم</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="483"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>إنشاء نسخ احتياطية لبطاقة SD أثناء الاستيراد (قم بإيقاف تشغيل هذا على مسؤوليتك الخاصة!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1443"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;الحد الأقصى الحقيقي هو الحد الأقصى لمجموعة البيانات. 99 في المئة مرشحات خارج أندر القيم المتطرفة.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1461"/>
        <source>Combined Count divided by Total Hours</source>
        <translation>الجمع بين عدد مقسوما على إجمالي ساعات</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1466"/>
        <source>Time Weighted average of Indice</source>
        <translation>متوسط الوقت المرجح للمؤشر</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1471"/>
        <source>Standard average of indice</source>
        <translation>متوسط قياسي من indice</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1402"/>
        <source>Culminative Indices</source>
        <translation>مؤشرات الذروة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="971"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation>عرف حدث المستخدم CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1803"/>
        <source>Events</source>
        <translation>أحداث</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1856"/>
        <location filename="../oscar/preferencesdialog.ui" line="1935"/>
        <source>Reset &amp;Defaults</source>
        <translation>إعادة تعيين الإعدادات الافتراضية</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1869"/>
        <location filename="../oscar/preferencesdialog.ui" line="1948"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;تحذير:&lt;/span&gt;فقط لأنك تستطيع ، لا يعني أنها ممارسة جيدة.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1882"/>
        <source>Waveforms</source>
        <translation>الطول الموجي</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1662"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>علم التغيرات السريعة في احصائيات قياس التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1573"/>
        <source>Other oximetry options</source>
        <translation>خيارات قياس التأكسج الأخرى</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1623"/>
        <source>Flag SPO2 Desaturations Below</source>
        <translation>علم التشبع SPO2 أدناه</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1606"/>
        <source>Discard segments under</source>
        <translation>تجاهل شرائح تحت</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1643"/>
        <source>Flag Pulse Rate Above</source>
        <translation>علم معدل النبض أعلاه</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1633"/>
        <source>Flag Pulse Rate Below</source>
        <translation>معدل نبض العلم أدناه</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="432"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation>ضغط النسخ الاحتياطية ResMed (EDF) لتوفير مساحة على القرص.
يتم تخزين ملفات EDF التي تم نسخها احتياطيًا بتنسيق .gz ،
وهو أمر شائع على منصات Mac و Linux ..

يمكن استيراد OSCAR من دليل النسخ الاحتياطي المضغوط هذا بشكل أصلي ..
لاستخدامه مع ResScan سيتطلب ملفات .gz أن تكون غير مضغوطة أولاً ..</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="452"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation>تؤثر الخيارات التالية على مقدار مساحة القرص التي يستخدمها OSCAR ، ويكون لها تأثير على المدة التي يستغرقها الاستيراد.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="462"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation>هذا يجعل بيانات OSCAR تأخذ حوالي نصف مساحة كبيرة.
لكنه يجعل الاستيراد وتغيير اليوم يستغرق وقتًا أطول ..
إذا كنت تمتلك كمبيوترًا جديدًا به قرص صلب صغير ، فهذا خيار جيد.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="467"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation>ضغط بيانات الجلسة (يجعل بيانات OSCAR أصغر ، لكن تغيير اليوم يكون أبطأ.)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="474"/>
        <source>This maintains a backup of SD-card data for ResMed machines, 

ResMed S9 series machines delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation>هذا يحتفظ بنسخة احتياطية من بيانات بطاقة SD لأجهزة ResMed ،

تحذف ماكينات سلسلة ResMed S9 بيانات عالية الدقة يزيد عمرها عن 7 أيام ،
وبيانات الرسم البياني الأقدم من 30 يومًا ..

يمكن لـ OSCAR الاحتفاظ بنسخة من هذه البيانات إذا احتجت إلى إعادة التثبيت.
(موصى به للغاية ، إلا إذا كانت لديك مساحة قصيرة على القرص أو لا تهتم ببيانات الرسم البياني)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;يجعل تشغيل OSCAR أبطأ قليلاً ، عن طريق التحميل المسبق لجميع بيانات الملخص مقدمًا ، مما يسرع من استعراض النظرة العامة وبعض الحسابات الأخرى لاحقًا. إذا كان لديك كمية كبيرة من البيانات ، فقد يكون من المفيد إيقاف تشغيل هذا الخيار ، ولكن إذا كنت ترغب عادةً في العرض&lt;span style=&quot; font-style:italic;&quot;&gt;كل شىء&lt;/span&gt;في نظرة عامة ، لا يزال يتعين تحميل جميع البيانات الموجزة على أي حال. &lt;/p&gt; &lt;p&gt; لاحظ أن هذا الإعداد لا يؤثر على شكل الموجة وبيانات الأحداث ، والتي يتم تحميلها دائمًا حسب الحاجة.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1018"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve machine detected event positioning.</source>
        <translation>يحاول هذا الخيار التجريبي استخدام نظام الإبلاغ عن الأحداث الخاص بـ OSCAR لتحسين وضع الحدث الذي تم اكتشافه بواسطة الجهاز.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1193"/>
        <source>Show flags for machine detected events that haven&apos;t been identified yet.</source>
        <translation>إظهار علامات الأحداث التي تم اكتشافها بواسطة الجهاز والتي لم يتم تحديدها بعد.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1783"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:7.84158pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method does &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP machine, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;البيانات CMS50 المستوردة من SpO2Review (من ملفات. spoR) أو طريقة الاستيراد التسلسلي&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;ليس&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; لديك الطابع الزمني الصحيح اللازم للمزامنة.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;طريقة العرض المباشر (باستخدام كبل تسلسلي) هي إحدى الطرق لتحقيق مزامنة دقيقة على مقياس تأكسج CMS50 ، ولكنها لا تتعارض مع انجراف ساعة CPAP.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;إذا بدأت تشغيل وضع تسجيل Oximeters في نفس الوقت الذي تبدأ فيه تشغيل جهاز CPAP ، فيمكنك أيضًا تحقيق المزامنة.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;تأخذ عملية الاستيراد التسلسلية وقت البدء من أول جلسة CPAP. (تذكر أن تستورد بيانات CPAP أولاً!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt; {3C?} {4.0/?} {3.?} {40/?} {1&quot;?} {2&apos;?} {7.84158p?} {400;?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {600;?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {50 ?} {2R?} {10p?} {600;?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {50 ?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {10p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?}</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2007"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation>إظهار إزالة إشعار تذكير البطاقة عند إيقاف تشغيل OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2068"/>
        <source>Automatically Check For Updates</source>
        <translation>تلقائيا بالبحث عن التحديثات</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2085"/>
        <source>Check for new version every</source>
        <translation>تحقق من وجود نسخة جديدة كل</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2092"/>
        <source>Sourceforge hosts this project for free.. Please be considerate of their resources..</source>
        <translation>تستضيف Sourceforge هذا المشروع مجانًا .. يرجى مراعاة مواردهم ..</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2108"/>
        <source>days.</source>
        <translation>أيام.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2173"/>
        <source>&amp;Check for Updates now</source>
        <translation>تحقق من وجود تحديثات الآن</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2196"/>
        <source>Last Checked For Updates: </source>
        <translation>آخر فحص للتحقق من وجود تحديثات: </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2209"/>
        <source>TextLabel</source>
        <translation>تسمية النص</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2231"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If your interested in helping test new features and bugfixes early, click here.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;But please be warned this will sometimes mean breaky code..&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;إذا كنت مهتمًا بالمساعدة في اختبار الميزات الجديدة وإصلاح الأخطاء في وقت مبكر ، انقر هنا.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;ولكن من فضلك حذر من أن هذا سوف يعني في بعض الأحيان رمز مكسورة ..&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2240"/>
        <source>I want to try experimental and test builds (Advanced users only please.)</source>
        <translation>أرغب في تجربة تصميمات تجريبية واختبارية (يرجى للمستخدمين المتقدمين فقط.)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2264"/>
        <source>&amp;Appearance</source>
        <translation>مظهر خارجي</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2293"/>
        <source>Graph Settings</source>
        <translation>إعدادات الرسم البياني</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2309"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;أي علامة تبويب لفتح على تحميل ملف التعريف. (ملاحظة: سيتم التعيين على ملف التعريف إذا تم ضبط OSCAR على عدم فتح ملف تعريف عند بدء التشغيل)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2567"/>
        <source>Bar Tops</source>
        <translation>شريط بلايز</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2572"/>
        <source>Line Chart</source>
        <translation>خط الرسم البياني</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2662"/>
        <source>Overview Linecharts</source>
        <translation>نظرة عامة على المخططات الخطية</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2046"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation>حاول تغيير هذا من الإعداد الافتراضي (Desktop OpenGL) إذا كنت تواجه مشاكل في تقديم الرسوم البيانية الخاصة بـ OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;هذا يجعل التمرير عند التكبير أسهل على لوحات اللمس الحساسة ثنائية الاتجاه. ينصح 50ms القيمة.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2501"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>إلى متى تريد أن تظل تلميحات الأدوات مرئية.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2489"/>
        <source>Scroll Dampening</source>
        <translation>التمرير الملطف</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2479"/>
        <source>Tooltip Timeout</source>
        <translation>تلميح الأدوات</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2586"/>
        <source>Default display height of graphs in pixels</source>
        <translation>الارتفاع الافتراضي لعرض الرسوم البيانية بالبكسل</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2559"/>
        <source>Graph Tooltips</source>
        <translation>الرسم البياني تلميحات</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2435"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>الطريقة البصرية لعرض أعلام تراكب الموجي.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2440"/>
        <source>Standard Bars</source>
        <translation>أشرطة القياسية</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2445"/>
        <source>Top Markers</source>
        <translation>أعلى علامات</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2393"/>
        <source>Graph Height</source>
        <translation>الرسم البياني الارتفاع</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="577"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>التشغيل التلقائي للمستورد CPAP بعد فتح ملف التعريف</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="617"/>
        <source>Automatically load last used profile on start-up</source>
        <translation>تحميل آخر ملف تعريف مستخدم تلقائيًا عند بدء التشغيل</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="634"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data from any machine model that has not yet been tested by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;قم بتوفير تنبيه عند استيراد البيانات من أي طراز آلة لم يتم اختباره من قبل مطوري OSCAR.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="637"/>
        <source>Warn when importing data from an untested machine</source>
        <translation>التحذير عند استيراد البيانات من جهاز لم يتم اختباره</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="644"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data that is somehow different from anything previously seen by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;قدم تنبيهًا عند استيراد البيانات التي تختلف بطريقة أو بأخرى عن أي شيء سبق لمطوري OSCAR رؤيته.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="647"/>
        <source>Warn when previously unseen data is encountered</source>
        <translation>التحذير عند مواجهة بيانات لم يتم رؤيتها من قبل</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="802"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP machine. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation>يتطلب هذا الحساب توفير بيانات إجمالي التسريبات بواسطة جهاز CPAP. (على سبيل المثال ، PRS1 ، ولكن ليس ResMed ، التي لديها بالفعل)

حسابات التسرب غير المقصود المستخدمة هنا خطية ، فهي لا تصمم منحنى تنفيس القناع.

إذا كنت تستخدم بعض الأقنعة المختلفة ، فاختر متوسط القيم بدلاً من ذلك. يجب أن تظل قريبة بما فيه الكفاية.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="809"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>حساب التسريبات غير المقصودة عندما لا تكون موجودة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="905"/>
        <source>4 cmH2O</source>
        <translation>4 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="915"/>
        <source>20 cmH2O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="947"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>ملاحظة: يتم استخدام طريقة الحساب الخطي. يتطلب تغيير هذه القيم إعادة حساب.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1490"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed machines do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ملحوظة:&lt;/span&gt;نظرًا لقيود التصميم الموجزة ، لا تدعم أجهزة ResMed تغيير هذه الإعدادات.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1543"/>
        <source>Oximetry Settings</source>
        <translation>إعدادات قياس التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2014"/>
        <source>Always save screenshots in the OSCAR Data folder</source>
        <translation>احفظ لقطات الشاشة دائمًا في مجلد بيانات OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2299"/>
        <source>On Opening</source>
        <translation>في الافتتاح</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2312"/>
        <location filename="../oscar/preferencesdialog.ui" line="2316"/>
        <source>Profile</source>
        <translation>الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2321"/>
        <location filename="../oscar/preferencesdialog.ui" line="2360"/>
        <source>Welcome</source>
        <translation>أهلا بك</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2326"/>
        <location filename="../oscar/preferencesdialog.ui" line="2365"/>
        <source>Daily</source>
        <translation>اليومي</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2336"/>
        <location filename="../oscar/preferencesdialog.ui" line="2375"/>
        <source>Statistics</source>
        <translation>الإحصاء</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2344"/>
        <source>Switch Tabs</source>
        <translation>تبديل علامات التبويب</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2355"/>
        <source>No change</source>
        <translation>لا تغيير</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2383"/>
        <source>After Import</source>
        <translation>بعد الاستيراد</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2409"/>
        <source>Overlay Flags</source>
        <translation>تراكب الأعلام</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2419"/>
        <source>Line Thickness</source>
        <translation>سمك الخط</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2455"/>
        <source>The pixel thickness of line plots</source>
        <translation>سماكة البيكسل للخط</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2681"/>
        <source>Other Visual Settings</source>
        <translation>الإعدادات البصرية الأخرى</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2687"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>تطبيق مكافحة التعرج ينطبق على تجانس مؤامرات الرسم البياني ..
بعض المؤامرات تبدو أكثر جاذبية مع هذا على.
هذا يؤثر أيضًا على التقارير المطبوعة.

جرب هذا وانظر إذا ما كان يعجبك.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2694"/>
        <source>Use Anti-Aliasing</source>
        <translation>استخدام مكافحة التعرج</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2701"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>يجعل بعض المؤامرات تبدو أكثر &quot;لوح مربع&quot;.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2704"/>
        <source>Square Wave Plots</source>
        <translation>مؤامرات موجة مربعة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2711"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>Pixmap التخزين المؤقت هو تقنية تسريع الرسومات. قد يسبب مشاكل في رسم الخطوط في منطقة عرض الرسم البياني على النظام الأساسي الخاص بك.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2714"/>
        <source>Use Pixmap Caching</source>
        <translation>استخدام Pixmap التخزين المؤقت</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2721"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;هذه الميزات تم تشذيبها مؤخرًا. سوف يعودون لاحقا. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2724"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation>الرسوم المتحركة والأشياء الهوى</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2741"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>ما إذا كنت تريد تغيير مقاييس yAxis بالنقر المزدوج على تسميات المحور y</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2744"/>
        <source>Allow YAxis Scaling</source>
        <translation>السماح لتوسيع نطاق المحور ص</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2751"/>
        <source>Whether to include machine serial number on machine settings changes report</source>
        <translation>سواء لتضمين الرقم التسلسلي للجهاز في تقرير تغييرات إعدادات الجهاز</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2754"/>
        <source>Include Serial Number</source>
        <translation>تشمل الرقم التسلسلي</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2040"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>محرك الرسومات (يتطلب إعادة التشغيل)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2786"/>
        <source>Fonts (Application wide settings)</source>
        <translation>الخطوط (إعدادات التطبيق الواسعة)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2821"/>
        <source>Font</source>
        <translation>الخط</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2840"/>
        <source>Size</source>
        <translation>بحجم</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2859"/>
        <source>Bold  </source>
        <translation>بالخط العريض  </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2881"/>
        <source>Italic</source>
        <translation>مائل</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2894"/>
        <source>Application</source>
        <translation>تطبيق</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2958"/>
        <source>Graph Text</source>
        <translation>نص الرسم البياني</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3019"/>
        <source>Graph Titles</source>
        <translation>عناوين الرسم البياني</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3080"/>
        <source>Big  Text</source>
        <translation>نص كبير</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3147"/>
        <location filename="../oscar/preferencesdialog.cpp" line="447"/>
        <location filename="../oscar/preferencesdialog.cpp" line="579"/>
        <source>Details</source>
        <translation>تفاصيل</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3179"/>
        <source>&amp;Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3186"/>
        <source>&amp;Ok</source>
        <translation>حسنا</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="442"/>
        <location filename="../oscar/preferencesdialog.cpp" line="573"/>
        <source>Name</source>
        <translation>اسم</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="443"/>
        <location filename="../oscar/preferencesdialog.cpp" line="574"/>
        <source>Color</source>
        <translation>اللون</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="445"/>
        <source>Flag Type</source>
        <translation>نوع العلم</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="446"/>
        <location filename="../oscar/preferencesdialog.cpp" line="578"/>
        <source>Label</source>
        <translation>ضع الكلمة المناسبة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="463"/>
        <source>CPAP Events</source>
        <translation>أحداث CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="464"/>
        <source>Oximeter Events</source>
        <translation>أحداث مقياس التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="465"/>
        <source>Positional Events</source>
        <translation>الأحداث الموقفية</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="466"/>
        <source>Sleep Stage Events</source>
        <translation>أحداث مرحلة النوم</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="467"/>
        <source>Unknown Events</source>
        <translation>أحداث غير معروفة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="639"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>انقر نقرًا مزدوجًا لتغيير الاسم الوصفي لهذه القناة.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="517"/>
        <location filename="../oscar/preferencesdialog.cpp" line="646"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>انقر مرتين لتغيير اللون الافتراضي لهذه القناة مؤامرة / العلم / البيانات.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2331"/>
        <location filename="../oscar/preferencesdialog.ui" line="2370"/>
        <location filename="../oscar/preferencesdialog.cpp" line="444"/>
        <location filename="../oscar/preferencesdialog.cpp" line="575"/>
        <source>Overview</source>
        <translation>نظرة عامة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="73"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; machines due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed machines, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;يرجى الملاحظة:&lt;/b&gt;قدرات تقسيم جلسة OSCAR المتقدمة غير ممكنة&lt;b&gt;ResMed&lt;/b&gt;آلات بسبب وجود قيود في طريقة تخزين إعداداتها وبيانات الملخص ، وبالتالي تم تعطيلها لهذا الملف الشخصي.&lt;/p&gt;&lt;p&gt;على أجهزة ResMed ، سيتم تقسيم الأيام عند الظهر كما هو الحال في برنامج ResMed التجاري.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="509"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>انقر نقرًا مزدوجًا لتغيير الاسم الوصفي للقناة &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="522"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>ما إذا كان هذا العلم لديه مخطط نظرة عامة مخصصة.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="532"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>هنا يمكنك تغيير نوع العلم الموضح لهذا الحدث</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="537"/>
        <location filename="../oscar/preferencesdialog.cpp" line="670"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>هذا هو التسمية القصيرة للإشارة إلى هذه القناة على الشاشة.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="543"/>
        <location filename="../oscar/preferencesdialog.cpp" line="676"/>
        <source>This is a description of what this channel does.</source>
        <translation>هذا وصف لما تفعله هذه القناة.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="576"/>
        <source>Lower</source>
        <translation>خفض</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="577"/>
        <source>Upper</source>
        <translation>أعلى</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="596"/>
        <source>CPAP Waveforms</source>
        <translation>CPAP الموجي</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="597"/>
        <source>Oximeter Waveforms</source>
        <translation>الموجي مقياس التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="598"/>
        <source>Positional Waveforms</source>
        <translation>الموجي الموضعي</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="599"/>
        <source>Sleep Stage Waveforms</source>
        <translation>مرحلة النوم الطول الموجي</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="655"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>ما إذا كان انهيار لهذا الشكل الموجي يعرض في نظرة عامة.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="660"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>هنا يمكنك تعيين الحد الأدنى &lt;b&gt; الأدنى &lt;/b&gt; المستخدم لحسابات معينة على شكل الموجة %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="665"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>هنا يمكنك تعيين الحد الأعلى المستخدم لحسابات معينة على شكل الموجة %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="775"/>
        <source>Data Processing Required</source>
        <translation>معالجة البيانات المطلوبة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="776"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>مطلوب إجراء إعادة / إلغاء ضغط البيانات لتطبيق هذه التغييرات. قد تستغرق هذه العملية بضع دقائق حتى تكتمل.

هل أنت متأكد أنك تريد إجراء هذه التغييرات؟</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="784"/>
        <source>Data Reindex Required</source>
        <translation>إعادة فهرسة البيانات المطلوبة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="785"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>يلزم إجراء إعادة فهرسة البيانات لتطبيق هذه التغييرات. قد تستغرق هذه العملية بضع دقائق حتى تكتمل.

هل أنت متأكد أنك تريد إجراء هذه التغييرات؟</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="791"/>
        <source>Restart Required</source>
        <translation>إعادة التشغيل المطلوبة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="792"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation>يتطلب إجراء واحد أو أكثر من التغييرات التي أجريتها إعادة تشغيل هذا التطبيق ، حتى تدخل هذه التغييرات حيز التنفيذ.

هل ترغب في القيام بذلك الآن؟</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1151"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation> إذا احتجت إلى إعادة استيراد هذه البيانات مرة أخرى (سواء في OSCAR أو ResScan) ، فلن تعود هذه البيانات.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1152"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation> إذا كنت بحاجة إلى الحفاظ على مساحة القرص ، يرجى تذكر القيام بنسخ احتياطية يدوية.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1153"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation> هل أنت متأكد من أنك تريد تعطيل هذه النسخ الاحتياطية؟</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1197"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation>لا يعد إيقاف تشغيل النسخ الاحتياطية فكرة جيدة ، لأن OSCAR يحتاج إلى إعادة إنشاء قاعدة البيانات في حالة العثور على أخطاء.

</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1198"/>
        <source>Are you really sure you want to do this?</source>
        <translation>هل أنت متأكد أنك تريد فعل ذلك؟</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="49"/>
        <source>Flag</source>
        <translation>علم</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Minor Flag</source>
        <translation>العلم الصغرى</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Span</source>
        <translation>امتداد</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="52"/>
        <source>Always Minor</source>
        <translation>دائما الصغرى</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="65"/>
        <source>No CPAP machines detected</source>
        <translation>لم يتم الكشف عن آلات CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="66"/>
        <source>Will you be using a ResMed brand machine?</source>
        <translation>هل ستستخدم آلة العلامة التجارية ResMed؟</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="275"/>
        <source>Never</source>
        <translation>أبدا</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="302"/>
        <location filename="../oscar/preferencesdialog.cpp" line="303"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1269"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1274"/>
        <source>%1 %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1149"/>
        <source>This may not be a good idea</source>
        <translation>قد لا تكون هذه فكرة جيدة</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1150"/>
        <source>ResMed S9 machines routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation>تقوم أجهزة ResMed S9 بحذف بيانات معينة بشكل روتيني من بطاقة SD الخاصة بك التي تزيد مدتها عن 7 و 30 يومًا (حسب الدقة).</translation>
    </message>
</context>
<context>
    <name>ProfileSelect</name>
    <message>
        <location filename="../oscar/profileselect.ui" line="14"/>
        <source>Select Profile</source>
        <translation>اختر صفحة شخصية</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="115"/>
        <source>Search:</source>
        <translation>بحث:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="191"/>
        <source>Start with the selected user profile.</source>
        <translation>ابدأ بملف تعريف المستخدم المحدد.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="194"/>
        <source>&amp;Select User</source>
        <translation>اختر المستخدم</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="215"/>
        <source>Create a new user profile.</source>
        <translation>إنشاء ملف تعريف مستخدم جديد.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="218"/>
        <source>New Profile</source>
        <translation>الملف الشخصي الجديد</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="235"/>
        <source>Choose a different OSCAR data folder.</source>
        <translation>اختر مجلد بيانات OSCAR مختلفًا.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="274"/>
        <source>OSCAR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="310"/>
        <source>Click here if you didn&apos;t want to start OSCAR.</source>
        <translation>انقر هنا إذا كنت لا تريد بدء تشغيل OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="383"/>
        <source>The current location of OSCAR data store.</source>
        <translation>الموقع الحالي لمتجر بيانات OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="241"/>
        <source>&amp;Different Folder</source>
        <translation>مجلد مختلف</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="287"/>
        <source>[version]</source>
        <translation>[الإصدار]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="313"/>
        <source>&amp;Quit</source>
        <translation>استقال</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="370"/>
        <source>Folder:</source>
        <translation>مجلد:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="389"/>
        <source>[data directory]</source>
        <translation>[دليل البيانات]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="92"/>
        <source>Open Profile</source>
        <translation>فتح الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="93"/>
        <source>Edit Profile</source>
        <translation>تعديل الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="95"/>
        <source>Delete Profile</source>
        <translation>حذف الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="139"/>
        <location filename="../oscar/profileselect.cpp" line="220"/>
        <source>Enter Password for %1</source>
        <translation>أدخل كلمة المرور لـ %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="158"/>
        <location filename="../oscar/profileselect.cpp" line="342"/>
        <source>Incorrect Password</source>
        <translation>كلمة سر خاطئة</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="160"/>
        <source>You entered the password wrong too many times.</source>
        <translation>لقد أدخلت كلمة المرور خطأ مرات عديدة.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>Enter the word DELETE below to confirm.</source>
        <translation>أدخل الكلمة DELETE أدناه للتأكيد.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>You are about to destroy profile &apos;%1&apos;.</source>
        <translation>أنت على وشك تدمير الملف الشخصي &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>Sorry</source>
        <translation>آسف</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>تحتاج إلى إدخال DELETE بحروف كبيرة.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="239"/>
        <source>You entered an incorrect password</source>
        <translation>لقد أدخلت كلمة مرور غير صحيحة</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="242"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to delete it manually.</source>
        <translation>إذا كنت تحاول الحذف لأنك نسيت كلمة المرور ، فعليك حذفها يدويًا.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="255"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>حدث خطأ أثناء حذف دليل ملف التعريف ، تحتاج إلى إزالته يدويًا.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="259"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>تم حذف الملف الشخصي %1&apos; بنجاح</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="289"/>
        <source>Create new profile</source>
        <translation>إنشاء ملف تعريف جديد</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="326"/>
        <source>Enter Password</source>
        <translation>أدخل كلمة المرور</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="345"/>
        <source>You entered an Incorrect Password too many times. Exiting!</source>
        <translation>لقد أدخلت كلمة مرور غير صحيحة عدة مرات. الخروج!</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.ui" line="14"/>
        <source>Form</source>
        <translation>شكل</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>منقي:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>Reset filter to see all profiles</source>
        <translation>إعادة تعيين مرشح لرؤية جميع ملامح</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="55"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="184"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="199"/>
        <source>Version</source>
        <translation>الإصدار</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="216"/>
        <source>&amp;Open Profile</source>
        <translation>فتح الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="227"/>
        <source>&amp;Edit Profile</source>
        <translation>تعديل الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="241"/>
        <source>&amp;New Profile</source>
        <translation>الملف الشخصي الجديد</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="260"/>
        <source>Profile: None</source>
        <translation>الملف الشخصي: لا شيء</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="281"/>
        <source>Please select or create a profile...</source>
        <translation>الرجاء تحديد أو إنشاء ملف تعريف ...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="332"/>
        <source>Destroy Profile</source>
        <translation>تدمير الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Profile</source>
        <translation>الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Ventilator Brand</source>
        <translation>جهاز التنفس الصناعي</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Ventilator Model</source>
        <translation>نموذج التنفس الصناعي</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Other Data</source>
        <translation>بيانات أخرى</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="94"/>
        <source>Last Imported</source>
        <translation>آخر مستورد</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="95"/>
        <source>Name</source>
        <translation>اسم</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="135"/>
        <location filename="../oscar/profileselector.cpp" line="312"/>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="170"/>
        <source>You must create a profile</source>
        <translation>يجب عليك إنشاء ملف تعريف</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="233"/>
        <location filename="../oscar/profileselector.cpp" line="363"/>
        <source>Enter Password for %1</source>
        <translation>أدخل كلمة المرور لـ %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="249"/>
        <location filename="../oscar/profileselector.cpp" line="382"/>
        <source>You entered an incorrect password</source>
        <translation>لقد أدخلت كلمة مرور غير صحيحة</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Forgot your password?</source>
        <translation>نسيت رقمك السري؟</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>اسأل في المنتديات عن كيفية إعادة تعيينه ، إنه في الواقع سهل للغاية.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="322"/>
        <source>Select a profile first</source>
        <translation>حدد ملف تعريف أولاً</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="385"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>إذا كنت تحاول الحذف لأنك نسيت كلمة المرور ، فعليك إما إعادة تعيينها أو حذف مجلد ملف التعريف يدويًا.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="395"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>أنت على وشك تدمير الملف الشخصي &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="395"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>فكر جيدًا ، لأن هذا سيؤدي إلى حذف ملف التعريف إلى جانب كل &lt;b&gt; بيانات النسخ الاحتياطي &lt;/b&gt; المخزنة تحت %2.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="395"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>أدخل الكلمة DELETE أدناه (تمامًا كما هو موضح) للتأكيد.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="413"/>
        <source>DELETE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="414"/>
        <source>Sorry</source>
        <translation>آسف</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="414"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>تحتاج إلى إدخال DELETE بحروف كبيرة.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="427"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>حدث خطأ أثناء حذف دليل ملف التعريف ، تحتاج إلى إزالته يدويًا.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="431"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>تم حذف الملف الشخصي &apos;%1&apos; بنجاح</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>Bytes</source>
        <translation>بايت</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="461"/>
        <source>Summaries:</source>
        <translation>الملخصات:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="462"/>
        <source>Events:</source>
        <translation>الأحداث:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="463"/>
        <source>Backups:</source>
        <translation>النسخ الاحتياطي:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="475"/>
        <location filename="../oscar/profileselector.cpp" line="515"/>
        <source>Hide disk usage information</source>
        <translation>إخفاء معلومات استخدام القرص</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="478"/>
        <source>Show disk usage information</source>
        <translation>إظهار معلومات استخدام القرص</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="496"/>
        <source>Name: %1, %2</source>
        <translation>الاسم: %1 ،%2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="499"/>
        <source>Phone: %1</source>
        <translation>الهاتف: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="502"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>البريد الإلكتروني: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="505"/>
        <source>Address:</source>
        <translation>عنوان:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="508"/>
        <source>No profile information given</source>
        <translation>لم يتم تقديم معلومات الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="511"/>
        <source>Profile: %1</source>
        <translation>الملف الشخصي: %1</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="56"/>
        <source>Abort</source>
        <translation>إحباط</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1246"/>
        <source>No Data</source>
        <translation>لايوجد بيانات</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="369"/>
        <source>Events</source>
        <translation>أحداث</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="367"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="370"/>
        <source>Duration</source>
        <translation>أحداث</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="384"/>
        <source>(% %1 in events)</source>
        <translation>(٪ %1 في الأحداث)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Jan</source>
        <translation>كانون الثاني</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Feb</source>
        <translation>شهر فبراير</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Mar</source>
        <translation>مارس</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Apr</source>
        <translation>أبريل</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>May</source>
        <translation>قد</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Jun</source>
        <translation>يونيو</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jul</source>
        <translation>يوليو</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Aug</source>
        <translation>أغسطس</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Sep</source>
        <translation>سبتمبر</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Oct</source>
        <translation>اكتوبر</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Nov</source>
        <translation>شهر نوفمبر</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Dec</source>
        <translation>ديسمبر</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="643"/>
        <source>&quot;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="644"/>
        <source>ft</source>
        <translation>قدم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="645"/>
        <source>lb</source>
        <translation>رطل</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="646"/>
        <source>oz</source>
        <translation>أوقية</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="647"/>
        <source>Kg</source>
        <translation>كلغ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="648"/>
        <source>cmH2O</source>
        <translation>cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="210"/>
        <source>Med.</source>
        <translation>الوسيط</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="228"/>
        <source>Min: %1</source>
        <translation>الحد الأدنى: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="259"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="269"/>
        <source>Min: </source>
        <translation>الحد الأدنى:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="264"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="274"/>
        <source>Max: </source>
        <translation>أقصى:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="278"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="282"/>
        <source>%1: </source>
        <translation>%1: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="286"/>
        <source>???: </source>
        <translation>???: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="293"/>
        <source>Max: %1</source>
        <translation>الحد الأقصى: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="299"/>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2 يوما): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="301"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 يوم): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="359"/>
        <source>% in %1</source>
        <translation>٪ في %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="365"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="692"/>
        <location filename="../oscar/SleepLib/common.cpp" line="649"/>
        <source>Hours</source>
        <translation>ساعات</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="371"/>
        <source>Min %1</source>
        <translation>الحد الأدنى %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="677"/>
        <source>
Hours: %1</source>
        <translation>
الساعات: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="743"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 انخفاض الاستخدام، %2 لا فائدة ، خارج %3 أيام (%4٪ متوافق.) الطول: %5 / %6 / %7</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="824"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>الجلسات: %1 / %2 / %3 الطول: %4 / %5 / %6 الأطول:%7 / %8 / %9</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="943"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
لمدة: %3
البداية: %2
</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="945"/>
        <source>Mask On</source>
        <translation>قناع على</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="945"/>
        <source>Mask Off</source>
        <translation>قناع قبالة</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="956"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
المدة:%3
البداية:%2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1127"/>
        <source>TTIA:</source>
        <translation>TTIA:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1140"/>
        <source>
TTIA: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1241"/>
        <source>%1 %2 / %3 / %4</source>
        <translation>%1 %2 / %3 / %4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="650"/>
        <source>Minutes</source>
        <translation>الدقائق</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="651"/>
        <source>Seconds</source>
        <translation>ثواني</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="652"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="653"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="654"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="655"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="656"/>
        <source>Events/hr</source>
        <translation>الأحداث / ساعة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="658"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="659"/>
        <source>bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="661"/>
        <source>Litres</source>
        <translation>ليتر</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="662"/>
        <source>ml</source>
        <translation>مل</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="663"/>
        <source>Breaths/min</source>
        <translation>الأنفاس / دقيقة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="664"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="666"/>
        <source>Severity (0-1)</source>
        <translation>درجة الخطورة (0-1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="667"/>
        <source>Degrees</source>
        <translation>درجات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="670"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9036"/>
        <source>Error</source>
        <translation>خطأ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="671"/>
        <source>Warning</source>
        <translation>تحذير</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="672"/>
        <source>Information</source>
        <translation>معلومات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="673"/>
        <source>Busy</source>
        <translation>مشغول</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="674"/>
        <source>Please Note</source>
        <translation>يرجى الملاحظة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="677"/>
        <source>Compliance Only :(</source>
        <translation>الامتثال فقط :(</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="678"/>
        <source>Graphs Switched Off</source>
        <translation>تم إيقاف تشغيل الرسوم البيانية</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="679"/>
        <source>Summary Only :(</source>
        <translation>ملخص فقط :(</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="680"/>
        <source>Sessions Switched Off</source>
        <translation>تم إغلاق الجلسات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="684"/>
        <source>&amp;Yes</source>
        <translation>نعم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source>&amp;No</source>
        <translation>لا</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source>&amp;Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="687"/>
        <source>&amp;Destroy</source>
        <translation>هدم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="688"/>
        <source>&amp;Save</source>
        <translation>حفظ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>BMI</source>
        <translation>BMI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="306"/>
        <source>Weight</source>
        <translation>وزن</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="692"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>Zombie</source>
        <translation>الاموات الاحياء</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="693"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Pulse Rate</source>
        <translation>معدل النبض</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="694"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>SpO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="695"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>Plethy</source>
        <translation>تخطيط التحجم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="696"/>
        <source>Pressure</source>
        <translation>الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="698"/>
        <source>Daily</source>
        <translation>اليومي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="699"/>
        <source>Profile</source>
        <translation>الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="700"/>
        <source>Overview</source>
        <translation>نظرة عامة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="701"/>
        <source>Oximetry</source>
        <translation>التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="703"/>
        <source>Oximeter</source>
        <translation>مقياس التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="704"/>
        <source>Event Flags</source>
        <translation>أعلام الحدث</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="707"/>
        <source>Default</source>
        <translation>إفتراضي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="710"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8958"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="89"/>
        <source>CPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="711"/>
        <source>BiPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="712"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8961"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="713"/>
        <source>EPAP</source>
        <translation>EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="714"/>
        <source>Min EPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="715"/>
        <source>Max EPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="716"/>
        <source>IPAP</source>
        <translation>IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="717"/>
        <source>Min IPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="718"/>
        <source>Max IPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="719"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="90"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="720"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8963"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="96"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="721"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>AVAPS</source>
        <translation>AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="722"/>
        <source>ST/ASV</source>
        <translation>ST/ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="724"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9021"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9048"/>
        <source>Humidifier</source>
        <translation>المرطب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="726"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="727"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>OA</source>
        <translation>OA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="728"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="729"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>CA</source>
        <translation>CA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="730"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>FL</source>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="731"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SA</source>
        <translation>SA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="732"/>
        <source>LE</source>
        <translation>LE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="733"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>EP</source>
        <translation>EP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="734"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>VS</source>
        <translation>VS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="736"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>VS2</source>
        <translation>VS2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="737"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>RERA</source>
        <translation>RERA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="738"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8949"/>
        <source>PP</source>
        <translation>PP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="739"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="740"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>RE</source>
        <translation>RE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="741"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>NR</source>
        <translation>NR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="742"/>
        <source>NRI</source>
        <translation>NRI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="743"/>
        <source>O2</source>
        <translation>O2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="744"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8966"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>PC</source>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="745"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="746"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="747"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="749"/>
        <source>PS</source>
        <translation>PS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="750"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="751"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>RDI</source>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="752"/>
        <source>AI</source>
        <translation>AI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="753"/>
        <source>HI</source>
        <translation>HI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="754"/>
        <source>UAI</source>
        <translation>UAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="755"/>
        <source>CAI</source>
        <translation>CAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="756"/>
        <source>FLI</source>
        <translation>FLI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="758"/>
        <source>REI</source>
        <translation>REI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="759"/>
        <source>EPI</source>
        <translation>EPI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="761"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="765"/>
        <source>IE</source>
        <translation>IE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="766"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Insp. Time</source>
        <translation>المفتش. زمن</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="767"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Exp. Time</source>
        <translation>إكسب. زمن</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="768"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Resp. Event</source>
        <translation>التركيب. حدث</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="769"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Flow Limitation</source>
        <translation>الحد من التدفق</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="770"/>
        <source>Flow Limit</source>
        <translation>الحد من التدفق</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="771"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SensAwake</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="772"/>
        <source>Pat. Trig. Breath</source>
        <translation>تربيتة. علم حساب المثلثات. نفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="773"/>
        <source>Tgt. Min. Vent</source>
        <translation>الهدف مين. منفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="774"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Target Vent.</source>
        <translation>الهدف تنفيس.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="775"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Minute Vent.</source>
        <translation>تنفيس دقيقة.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="776"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>Tidal Volume</source>
        <translation>حجم المد والجزر</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="777"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Resp. Rate</source>
        <translation>التركيب. معدل</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="778"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Snore</source>
        <translation>شخير</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="779"/>
        <source>Leak</source>
        <translation>تسرب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="780"/>
        <source>Leaks</source>
        <translation>تسرب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="781"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>Large Leak</source>
        <translation>تسرب كبير</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="782"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>LL</source>
        <translation>LL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="783"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Total Leaks</source>
        <translation>إجمالي التسريبات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="784"/>
        <source>Unintentional Leaks</source>
        <translation>تسرب غير مقصود</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="785"/>
        <source>MaskPressure</source>
        <translation>قناع الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="786"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="212"/>
        <source>Flow Rate</source>
        <translation>معدل المد و الجزر</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="787"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Sleep Stage</source>
        <translation>مرحلة النوم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="788"/>
        <source>Usage</source>
        <translation>استعمال</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="789"/>
        <source>Sessions</source>
        <translation>جلسات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="790"/>
        <source>Pr. Relief</source>
        <translation>العلاقات العامة. ارتياح</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="793"/>
        <location filename="../oscar/SleepLib/journal.cpp" line="30"/>
        <source>OSCAR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="676"/>
        <source>No Data Available</source>
        <translation>لا تتوافر بيانات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="201"/>
        <source>App key:</source>
        <translation>مفتاح التطبيق:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="193"/>
        <source>Operating system:</source>
        <translation>نظام التشغيل:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="191"/>
        <source>Built with Qt %1 on %2</source>
        <translation>تم إنشاؤه باستخدام Qt %1 على %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="194"/>
        <source>Graphics Engine:</source>
        <translation>محرك الرسومات:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="195"/>
        <source>Graphics Engine type:</source>
        <translation>نوع محرك الرسومات:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="637"/>
        <source>Software Engine</source>
        <translation>محرك البرمجيات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="638"/>
        <source>ANGLE / OpenGLES</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="639"/>
        <source>Desktop OpenGL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="641"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="642"/>
        <source> cm</source>
        <translation> cm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="792"/>
        <source>Bookmarks</source>
        <translation>إشارات مرجعية</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="796"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8953"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8955"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="85"/>
        <source>Mode</source>
        <translation>الوضع</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="797"/>
        <source>Model</source>
        <translation>نموذج</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="798"/>
        <source>Brand</source>
        <translation>علامة تجارية</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="799"/>
        <source>Serial</source>
        <translation>مسلسل</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="800"/>
        <source>Series</source>
        <translation>سلسلة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="801"/>
        <source>Machine</source>
        <translation>آلة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="802"/>
        <source>Channel</source>
        <translation>قناة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="803"/>
        <source>Settings</source>
        <translation>الإعدادات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="805"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>Inclination</source>
        <translation>ميل</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="806"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>Orientation</source>
        <translation>اتجاه</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="807"/>
        <source>Motion</source>
        <translation>اقتراح</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="809"/>
        <source>Name</source>
        <translation>اسم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="810"/>
        <source>DOB</source>
        <translation>تاريخ الميلاد</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="811"/>
        <source>Phone</source>
        <translation>هاتف</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="812"/>
        <source>Address</source>
        <translation>عنوان</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="813"/>
        <source>Email</source>
        <translation>البريد الإلكتروني</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="814"/>
        <source>Patient ID</source>
        <translation>رقم المريض</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="815"/>
        <source>Date</source>
        <translation>تاريخ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="817"/>
        <source>Bedtime</source>
        <translation>وقت النوم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="818"/>
        <source>Wake-up</source>
        <translation>استيقظ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="819"/>
        <source>Mask Time</source>
        <translation>قناع الوقت</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="820"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="124"/>
        <source>Unknown</source>
        <translation>مجهول</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="821"/>
        <source>None</source>
        <translation>لا شيء</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="822"/>
        <source>Ready</source>
        <translation>جاهز</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="824"/>
        <source>First</source>
        <translation>أول</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="825"/>
        <source>Last</source>
        <translation>الاخير</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="826"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>Start</source>
        <translation>بداية</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="827"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="313"/>
        <source>End</source>
        <translation>النهاية</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="828"/>
        <source>On</source>
        <translation>على</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="829"/>
        <source>Off</source>
        <translation>إيقاف</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="830"/>
        <source>Yes</source>
        <translation>نعم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="831"/>
        <source>No</source>
        <translation>لا</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="833"/>
        <source>Min</source>
        <translation>دقيقة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="834"/>
        <source>Max</source>
        <translation>ماكس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="835"/>
        <source>Med</source>
        <translation>ميد</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="837"/>
        <source>Average</source>
        <translation>معدل</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="838"/>
        <source>Median</source>
        <translation>الوسيط</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="214"/>
        <location filename="../oscar/SleepLib/common.cpp" line="839"/>
        <source>Avg</source>
        <translation>متوسط</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="212"/>
        <location filename="../oscar/SleepLib/common.cpp" line="840"/>
        <source>W-Avg</source>
        <translation>متوسط الوزن</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="794"/>
        <source>Non Data Capable Machine</source>
        <translation>آلة غير قادرة على البيانات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="795"/>
        <source>Your Philips Respironics CPAP machine (Model %1) is unfortunately not a data capable model.</source>
        <translation>إن جهاز Philips Respironics CPAP (الطراز %1) للأسف ليس نموذجًا قادرًا على البيانات.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="684"/>
        <source>Getting Ready...</source>
        <translation>يستعد...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="807"/>
        <source>Machine Unsupported</source>
        <translation>الجهاز غير مدعوم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="808"/>
        <source>Sorry, your Philips Respironics CPAP machine (Model %1) is not supported yet.</source>
        <translation>عذرًا ، جهاز Philips Respironics CPAP (الطراز %1) غير مدعوم حتى الآن.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="796"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this machine.</source>
        <translation>نأسف للإبلاغ بأن OSCAR يمكنه فقط تتبع ساعات الاستخدام والإعدادات الأساسية جدًا لهذا الجهاز.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="708"/>
        <source>Scanning Files...</source>
        <translation>جارٍ فحص الملفات ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="716"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="592"/>
        <source>Importing Sessions...</source>
        <translation>استيراد الجلسات ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="721"/>
        <source>Finishing up...</source>
        <translation>الانتهاء من ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="809"/>
        <source>The developers needs a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make it work with OSCAR.</source>
        <translation>يحتاج المطورون إلى نسخة بتنسيق zip من بطاقة SD لهذا الجهاز ومطابقة تقارير Encore .pdf لجعلها تعمل مع OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="831"/>
        <source>Machine Untested</source>
        <translation>آلة لم تختبر</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="832"/>
        <source>Your Philips Respironics CPAP machine (Model %1) has not been tested yet.</source>
        <translation>لم يتم اختبار جهاز Philips Respironics CPAP (الطراز %1) حتى الآن.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="833"/>
        <source>It seems similar enough to other machines that it might work, but the developers would like a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make sure it works with OSCAR.</source>
        <translation>يبدو مشابهاً بما فيه الكفاية مع الأجهزة الأخرى التي قد تعمل ، لكن المطورين يرغبون في الحصول على نسخة بتنسيق zip. من بطاقة SD لهذا الجهاز ومطابقة تقارير Encore .pdf للتأكد من أنها تعمل مع OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8994"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8996"/>
        <source>Flex Lock</source>
        <translation>فليكس لوك</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8995"/>
        <source>Whether Flex settings are available to you.</source>
        <translation>ما إذا كانت إعدادات Flex متاحة لك.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9004"/>
        <source>Amount of time it takes to transition from EPAP to IPAP, the higher the number the slower the transition</source>
        <translation>مقدار الوقت المستغرق للانتقال من EPAP إلى IPAP ، كلما زاد الرقم كلما كان الانتقال أبطأ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9010"/>
        <source>Rise Time Lock</source>
        <translation>ارتفاع قفل الوقت</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9011"/>
        <source>Whether Rise Time settings are available to you.</source>
        <translation>ما إذا كانت إعدادات &quot;وقت الارتفاع&quot; متاحة لك.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9012"/>
        <source>Rise Lock</source>
        <translation>Rise Lock</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9056"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9057"/>
        <source>Mask Resistance Setting</source>
        <translation>إعداد مقاومة القناع</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9058"/>
        <source>Mask Resist.</source>
        <translation>Mask Resist.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9066"/>
        <source>Hose Diam.</source>
        <translation>Hose Diam.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9069"/>
        <source>15mm</source>
        <translation>15 mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9068"/>
        <source>22mm</source>
        <translation>22 mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="699"/>
        <source>Backing Up Files...</source>
        <translation>النسخ الاحتياطي للملفات ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="734"/>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="84"/>
        <source>Untested Data</source>
        <translation>البيانات غير المختبرة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="735"/>
        <source>Your Philips Respironics %1 (%2) generated data that OSCAR has never seen before.</source>
        <translation>أنتج جهاز Philips Respironics %1 (%2) بيانات لم يشاهدها OSCAR من قبل.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="736"/>
        <source>The imported data may not be entirely accurate, so the developers would like a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make sure OSCAR is handling the data correctly.</source>
        <translation>قد لا تكون البيانات المستوردة دقيقة تمامًا ، لذلك يرغب المطورون في الحصول على نسخة بتنسيق .zip من بطاقة SD الخاصة بهذا الجهاز وتقارير Encore .pdf المطابقة للتأكد من أن OSCAR يتعامل مع البيانات بشكل صحيح.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8957"/>
        <source>CPAP-Check</source>
        <translation>تحقق CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8959"/>
        <source>AutoCPAP</source>
        <translation>AutoCPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8960"/>
        <source>Auto-Trial</source>
        <translation>Auto-Trial</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8962"/>
        <source>AutoBiLevel</source>
        <translation>AutoBiLevel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8964"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8965"/>
        <source>S/T</source>
        <translation>S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8967"/>
        <source>S/T - AVAPS</source>
        <translation>S/T - AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8968"/>
        <source>PC - AVAPS</source>
        <translation>PC - AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8971"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8973"/>
        <source>Flex Mode</source>
        <translation>وضع فليكس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8972"/>
        <source>PRS1 pressure relief mode.</source>
        <translation>وضع تخفيف الضغط PRS1.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8976"/>
        <source>C-Flex</source>
        <translation>C-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8977"/>
        <source>C-Flex+</source>
        <translation>C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8978"/>
        <source>A-Flex</source>
        <translation>A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8979"/>
        <source>P-Flex</source>
        <translation>P-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8980"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9003"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9005"/>
        <source>Rise Time</source>
        <translation>وقت الشروق</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8981"/>
        <source>Bi-Flex</source>
        <translation>Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8986"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8988"/>
        <source>Flex Level</source>
        <translation>فليكس المستوى</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8987"/>
        <source>PRS1 pressure relief setting.</source>
        <translation>إعداد تخفيف الضغط PRS1.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9035"/>
        <source>Passover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9074"/>
        <source>Tubing Type Lock</source>
        <translation>قفل نوع الأنابيب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9075"/>
        <source>Whether tubing type settings are available to you.</source>
        <translation>ما إذا كانت إعدادات نوع الأنابيب متاحة لك.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9076"/>
        <source>Tube Lock</source>
        <translation>قفل الأنبوب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9083"/>
        <source>Mask Resistance Lock</source>
        <translation>قفل قناع المقاومة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9084"/>
        <source>Whether mask resistance settings are available to you.</source>
        <translation>ما إذا كانت إعدادات مقاومة القناع متاحة لك.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9085"/>
        <source>Mask Res. Lock</source>
        <translation>Mask Res. Lock</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9120"/>
        <source>Whether or not machine shows AHI via built-in display.</source>
        <translation>ما إذا كان الجهاز يعرض AHI أم لا عبر شاشة مدمجة.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9128"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9130"/>
        <source>Ramp Type</source>
        <translation>نوع المنحدر</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9129"/>
        <source>Type of ramp curve to use.</source>
        <translation>نوع منحنى المنحدر للاستخدام.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9132"/>
        <source>Linear</source>
        <translation>خطي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9133"/>
        <source>SmartRamp</source>
        <translation>SmartRamp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9137"/>
        <source>Backup Breath Mode</source>
        <translation>وضع التنفس الاحتياطي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9138"/>
        <source>The kind of backup breath rate in use: none (off), automatic, or fixed</source>
        <translation>نوع معدل التنفس الاحتياطي المستخدم: بلا (إيقاف) أو تلقائي أو ثابت</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9139"/>
        <source>Breath Rate</source>
        <translation>معدل التنفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9143"/>
        <source>Fixed</source>
        <translation>ثابت</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9147"/>
        <source>Fixed Backup Breath BPM</source>
        <translation>التنفس الاحتياطي الثابت BPM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9148"/>
        <source>Minimum breaths per minute (BPM) below which a timed breath will be initiated</source>
        <translation>الحد الأدنى من الأنفاس في الدقيقة (BPM) التي سيتم بعدها بدء التنفس الموقوت</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9149"/>
        <source>Breath BPM</source>
        <translation>التنفس BPM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9154"/>
        <source>Timed Inspiration</source>
        <translation>إلهام موقوت</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9155"/>
        <source>The time that a timed breath will provide IPAP before transitioning to EPAP</source>
        <translation>الوقت الذي يوفر فيه التنفس الموقوت IPAP قبل الانتقال إلى EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9156"/>
        <source>Timed Insp.</source>
        <translation>Timed Insp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9161"/>
        <source>Auto-Trial Duration</source>
        <translation>مدة المحاكمة التلقائية</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9162"/>
        <source>The number of days in the Auto-CPAP trial period, after which the machine will revert to CPAP</source>
        <translation>عدد الأيام في الفترة التجريبية Auto-CPAP ، وبعدها يعود الجهاز إلى CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9163"/>
        <source>Auto-Trial Dur.</source>
        <translation>Auto-Trial Dur.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9168"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9170"/>
        <source>EZ-Start</source>
        <translation>EZ-Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9169"/>
        <source>Whether or not EZ-Start is enabled</source>
        <translation>سواء تم تمكين EZ-Start أم لا</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9177"/>
        <source>Variable Breathing</source>
        <translation>تنفس متغير</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9178"/>
        <source>UNCONFIRMED: Possibly variable breathing, which are periods of high deviation from the peak inspiratory flow trend</source>
        <translation>غير مؤكد: تنفس متغير محتمل ، وهي فترات انحراف كبير عن ذروة اتجاه التدفق الشهيق</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9201"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9203"/>
        <source>Peak Flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9202"/>
        <source>Peak flow during a 2-minute interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9019"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>Humidifier Status</source>
        <translation>حالة المرطب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9020"/>
        <source>PRS1 humidifier connected?</source>
        <translation>PRS1 المرطب متصلة؟</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9023"/>
        <source>Disconnected</source>
        <translation>انقطع الاتصال</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9024"/>
        <source>Connected</source>
        <translation>متصل</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9028"/>
        <source>Humidification Mode</source>
        <translation>وضع الترطيب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9029"/>
        <source>PRS1 Humidification Mode</source>
        <translation>وضع الترطيب PRS1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9030"/>
        <source>Humid. Mode</source>
        <translation>Humid. Mode</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9032"/>
        <source>Fixed (Classic)</source>
        <translation>ثابت (كلاسيكي)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9033"/>
        <source>Adaptive (System One)</source>
        <translation>التكيف (النظام الأول)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9034"/>
        <source>Heated Tube</source>
        <translation>أنبوب ساخن</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9040"/>
        <source>Tube Temperature</source>
        <translation>درجة حرارة الأنبوب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9041"/>
        <source>PRS1 Heated Tube Temperature</source>
        <translation>PRS1 درجة حرارة الأنبوب المسخن</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9042"/>
        <source>Tube Temp.</source>
        <translation>Tube Temp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9049"/>
        <source>PRS1 Humidifier Setting</source>
        <translation>إعداد مرطب PRS1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9050"/>
        <source>Humid. Lvl</source>
        <translation>Humid. Lvl</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9064"/>
        <source>Hose Diameter</source>
        <translation>قطر خرطوم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9065"/>
        <source>Diameter of primary CPAP hose</source>
        <translation>قطر أو خرطوم CPAP الأساسي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9070"/>
        <source>12mm</source>
        <translation>12mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9092"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9094"/>
        <source>Auto On</source>
        <translation>تشغيل تلقائي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9093"/>
        <source>A few breaths automatically starts machine</source>
        <translation>وهناك عدد قليل من الأنفاس يبدأ الجهاز تلقائيا</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9101"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9103"/>
        <source>Auto Off</source>
        <translation>إيقاف السيارات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9102"/>
        <source>Machine automatically switches off</source>
        <translation>الجهاز يغلق تلقائيا</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9110"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9112"/>
        <source>Mask Alert</source>
        <translation>تنبيه قناع</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9111"/>
        <source>Whether or not machine allows Mask checking.</source>
        <translation>أم لا يسمح الجهاز فحص القناع.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9119"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9121"/>
        <source>Show AHI</source>
        <translation>عرض AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9186"/>
        <source>Breathing Not Detected</source>
        <translation>التنفس لم يتم كشفه</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9187"/>
        <source>A period during a session where the machine could not detect flow.</source>
        <translation>فترة خلال الجلسة حيث لم يتمكن الجهاز من اكتشاف التدفق.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9188"/>
        <source>BND</source>
        <translation>BND</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9193"/>
        <source>Timed Breath</source>
        <translation>توقيت التنفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9194"/>
        <source>Machine Initiated Breath</source>
        <translation>آلة بدأت التنفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9195"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="36"/>
        <source>Windows User</source>
        <translation>مستخدم ويندوز</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>Using </source>
        <translation>استخدام </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>, found SleepyHead -
</source>
        <translation>، وجدت SleepyHead -
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="201"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation>يجب عليك تشغيل أداة ترحيل OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="537"/>
        <source>&lt;i&gt;Your old machine data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation>&lt;i&gt;يجب إعادة إنشاء بيانات الجهاز القديم شريطة ألا يتم تعطيل ميزة النسخ الاحتياطي هذه في التفضيلات أثناء عملية استيراد بيانات سابقة.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="470"/>
        <source>Launching Windows Explorer failed</source>
        <translation>فشل بدء تشغيل مستكشف Windows</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="471"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>تعذر العثور على explorer.exe في الطريق لبدء تشغيل مستكشف Windows.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="523"/>
        <source>OSCAR %1 needs to upgrade its database for %2 %3 %4</source>
        <translation>يحتاج OSCAR %1 إلى ترقية قاعدة بياناته لـ %2 %3 %4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="536"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation>&lt;b&gt;يحتفظ OSCAR بنسخة احتياطية من بطاقة بيانات أجهزتك التي يستخدمها لهذا الغرض.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="540"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation>لا يوجد لدى الأداة OSCAR أي نسخ احتياطية للبطاقات تلقائيًا مخزنة لهذا الجهاز.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="541"/>
        <source>This means you will need to import this machine data again afterwards from your own backups or data card.</source>
        <translation>هذا يعني أنك ستحتاج إلى استيراد بيانات الجهاز هذه مرة أخرى بعد ذلك من النسخ الاحتياطية أو بطاقة البيانات الخاصة بك.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="544"/>
        <source>Important:</source>
        <translation>هام:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="545"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation>إذا كنت مهتمًا ، فانقر فوق &quot;لا&quot; للخروج من النسخة الاحتياطية ونسخها احتياطيًا يدويًا ، قبل بدء تشغيل OSCAR مرة أخرى.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="546"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation>هل أنت مستعد للترقية ، فهل يمكنك تشغيل الإصدار الجديد أو OSCAR؟</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="560"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation>عذرًا ، فشلت عملية التطهير ، مما يعني أنه لا يمكن بدء تشغيل هذا الإصدار أو OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="579"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation>هل ترغب في التبديل إلى النسخ الاحتياطية التلقائية ، لذا في المرة القادمة التي يحتاج فيها إصدار جديد أو OSCAR إلى القيام بذلك ، يمكنه إعادة البناء من هذه؟</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="586"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation>يبدأ OSCAR الآن معالج الاستيراد حتى تتمكن من إعادة تثبيت بيانات %1 الخاصة بك.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="596"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation>سيتم الآن الخروج من الأداة OSCAR ، ثم (محاولة) بدء تشغيل مدير ملفات أجهزة الكمبيوتر حتى تتمكن من نسخ ملف التعريف الخاص بك يدويًا إلى أعلى:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="598"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation>استخدم مدير الملفات لديك لإنشاء نسخة أو دليل ملف التعريف الخاص بك ، ثم بعد ذلك ، أعد تشغيل OSCAR وإكمال عملية الترقية.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="549"/>
        <source>Machine Database Changes</source>
        <translation>تغييرات قاعدة بيانات الجهاز</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="544"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;cannot&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation>بمجرد الترقية ، لا يمكنك &lt;font size = + 1&gt; &lt;/font&gt; استخدام هذا الملف الشخصي مع الإصدار السابق بعد الآن.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="561"/>
        <source>The machine data folder needs to be removed manually.</source>
        <translation>يجب إزالة مجلد بيانات الجهاز يدويًا.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="562"/>
        <source>This folder currently resides at the following location:</source>
        <translation>يوجد هذا المجلد حاليًا في الموقع التالي:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="570"/>
        <source>Rebuilding from %1 Backup</source>
        <translation>إعادة بناء من %1 النسخ الاحتياطي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="123"/>
        <source>Therapy Pressure</source>
        <translation>ضغط العلاج</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="124"/>
        <source>Inspiratory Pressure</source>
        <translation>ضغط ملهمة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="125"/>
        <source>Lower Inspiratory Pressure</source>
        <translation>انخفاض ضغط الشهيق</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="126"/>
        <source>Higher Inspiratory Pressure</source>
        <translation>ارتفاع ضغط إلهامي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="127"/>
        <source>Expiratory Pressure</source>
        <translation>ضغط الزفير</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="128"/>
        <source>Lower Expiratory Pressure</source>
        <translation>انخفاض ضغط الزفير</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="129"/>
        <source>Higher Expiratory Pressure</source>
        <translation>ارتفاع ضغط الزفير</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="130"/>
        <source>Pressure Support</source>
        <translation>دعم الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>PS Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Pressure Support Minimum</source>
        <translation>دعم الضغط الحد الأدنى</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>PS Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Pressure Support Maximum</source>
        <translation>دعم الضغط الأقصى</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Min Pressure</source>
        <translation>دقيقة الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Minimum Therapy Pressure</source>
        <translation>الحد الأدنى من ضغط العلاج</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Max Pressure</source>
        <translation>أقصى ضغط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Maximum Therapy Pressure</source>
        <translation>أقصى ضغط العلاج</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Time</source>
        <translation>الوقت منحدر</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Delay Period</source>
        <translation>منحدر فترة التأخير</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Ramp Pressure</source>
        <translation>منحدر الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Starting Ramp Pressure</source>
        <translation>بدءا منحدر الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Ramp Event</source>
        <translation>حدث منحدر</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="180"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Ramp</source>
        <translation>المنحدر</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>Vibratory Snore (VS2) </source>
        <translation>الشخير الاهتزازي (VS2) </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>A ResMed data item: Trigger Cycle Event</source>
        <translation>عنصر بيانات ResMed: حدث دورة الزناد</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>Mask On Time</source>
        <translation>قناع في الوقت المحدد</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>Time started according to str.edf</source>
        <translation>بدأ الوقت وفقًا str.edf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>Summary Only</source>
        <translation>ملخص فقط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="657"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>An apnea where the airway is open</source>
        <translation>توقف التنفس أثناء فتح مجرى الهواء</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>An apnea caused by airway obstruction</source>
        <translation>انقطاع النفس الناجم عن انسداد مجرى الهواء</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Hypopnea</source>
        <translation>ضعف التنفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>A partially obstructed airway</source>
        <translation>مجرى الهواء عرقلة جزئيا</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>Unclassified Apnea</source>
        <translation>انقطاع النفس غير المصنف</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>UA</source>
        <translation>UA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>Vibratory Snore</source>
        <translation>الشخير الاهتزازي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>A vibratory snore</source>
        <translation>شخير اهتزازي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>A vibratory snore as detcted by a System One machine</source>
        <translation>شخير اهتزازي كما تم اكتشافه بواسطة جهاز System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8947"/>
        <source>Pressure Pulse</source>
        <translation>نبض الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8948"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation>نبضة ضغط &quot;تتعرض لضغوط&quot; لاكتشاف مجرى الهواء المغلق.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>A large mask leak affecting machine performance.</source>
        <translation>تسرب كبير للقناع يؤثر على أداء الماكينة.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>Non Responding Event</source>
        <translation>حدث غير مستجيب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation>وهناك نوع من الحدث التنفسي الذي لن يستجيب لزيادة الضغط.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Expiratory Puff</source>
        <translation>الزفير النفخة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation>حدث Intellipap حيث تتنفس فمك.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation>ميزة SensAwake ستقلل الضغط عند اكتشاف الاستيقاظ.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>User Flag #1</source>
        <translation>علم المستخدم رقم 1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>User Flag #2</source>
        <translation>علم المستخدم رقم 2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>User Flag #3</source>
        <translation>علم المستخدم رقم 3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Heart rate in beats per minute</source>
        <translation>معدل ضربات القلب في يدق في الدقيقة الواحدة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>SpO2 %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation>نسبة تشبع الدم بالأكسجين</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>Plethysomogram</source>
        <translation>تخطيط التحجم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation>رسم ضوئي بصري ضوئي يظهر إيقاع القلب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>Pulse Change</source>
        <translation>تغيير النبض</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation>تغيير مفاجئ (يمكن تعريف المستخدم) في معدل ضربات القلب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>SpO2 Drop</source>
        <translation>SpO2 إسقاط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation>انخفاض مفاجئ (يمكن تعريف المستخدم) في تشبع الأكسجين في الدم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="212"/>
        <source>Breathing flow rate waveform</source>
        <translation>معدل تدفق التنفس الموجي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="660"/>
        <source>L/min</source>
        <translation>لتر / دقيقة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="215"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>Mask Pressure</source>
        <translation>قناع الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>Amount of air displaced per breath</source>
        <translation>كمية الهواء النازحة في التنفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Graph displaying snore volume</source>
        <translation>الرسم البياني عرض حجم الشخير</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Minute Ventilation</source>
        <translation>دقيقة حداد</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Amount of air displaced per minute</source>
        <translation>كمية الهواء النازحة في الدقيقة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Respiratory Rate</source>
        <translation>معدل التنفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Rate of breaths per minute</source>
        <translation>معدل التنفس في الدقيقة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Patient Triggered Breaths</source>
        <translation>أثار المريض أنفاسه</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation>نسبة الأنفاس الناتجة عن المريض</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Pat. Trig. Breaths</source>
        <translation>تربيتة. علم حساب المثلثات. الأنفاس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Leak Rate</source>
        <translation>معدل التسرب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Rate of detected mask leakage</source>
        <translation>معدل تسرب قناع الكشف</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="240"/>
        <source>I:E Ratio</source>
        <translation>أنا: ه نسبة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="240"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation>النسبة بين الوقت الملهم والزفير</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="665"/>
        <source>ratio</source>
        <translation>نسبة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Pressure Min</source>
        <translation>الضغط دقيقة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Pressure Max</source>
        <translation>الضغط ماكس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Pressure Set</source>
        <translation>مجموعة الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Pressure Setting</source>
        <translation>وضع الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>IPAP Set</source>
        <translation>مجموعة IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>IPAP Setting</source>
        <translation>إعداد IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>EPAP Set</source>
        <translation>مجموعة EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>EPAP Setting</source>
        <translation>إعداد EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>Cheyne Stokes Respiration</source>
        <translation>شايان ستوكس التنفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>فترة غير طبيعية من شايان ستوكس تنفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="760"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>CSR</source>
        <translation>CSR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Periodic Breathing</source>
        <translation>التنفس الدوري</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation>فترة غير طبيعية من التنفس الدوري</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Clear Airway</source>
        <translation>واضح مجرى الهواء</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>Obstructive</source>
        <translation>المعوق</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation>انقطاع النفس الذي لا يمكن تحديده على أنه مركزي أو معيق.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation>وجود قيود في التنفس من المعتاد ، مما تسبب في تسطيح الموجي التدفق.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>Respiratory Effort Related Arousal: An restriction in breathing that causes an either an awakening or sleep disturbance.</source>
        <translation>الجهد التنفسي المتعلق بالإثارة: وجود قيود في التنفس تسبب إما إيقاظًا أو اضطرابًا في النوم.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>Leak Flag</source>
        <translation>تسرب العلم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>LF</source>
        <translation>LF</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation>حدث يمكن تعريفه من قِبل المستخدم تم اكتشافه بواسطة معالج شكل الموجة OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>Perfusion Index</source>
        <translation>مؤشر الارواء</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation>تقييم نسبي لقوة النبض في موقع المراقبة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>Perf. Index %</source>
        <translation>الأداء. فهرس ٪</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>Mask Pressure (High frequency)</source>
        <translation>ضغط القناع (تردد عالي)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Expiratory Time</source>
        <translation>وقت الزفير</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Time taken to breathe out</source>
        <translation>الوقت المستغرق في التنفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Inspiratory Time</source>
        <translation>وقت ملهمة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Time taken to breathe in</source>
        <translation>الوقت الذي يستغرقه التنفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Respiratory Event</source>
        <translation>الحدث التنفسي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Graph showing severity of flow limitations</source>
        <translation>رسم بياني يوضح شدة قيود التدفق</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Flow Limit.</source>
        <translation>الحد من التدفق.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Target Minute Ventilation</source>
        <translation>الهدف دقيقة التهوية</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Maximum Leak</source>
        <translation>أقصى تسرب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>The maximum rate of mask leakage</source>
        <translation>الحد الأقصى لمعدل تسرب القناع</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Max Leaks</source>
        <translation>ماكس تسرب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Apnea Hypopnea Index</source>
        <translation>تشغيل مؤشر توقف التنفس أثناء التنفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation>رسم بياني يظهر أنه يركض AHI للساعة الماضية</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Total Leak Rate</source>
        <translation>معدل التسرب الكلي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation>كشف تسرب القناع بما في ذلك تسرب القناع الطبيعي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median Leak Rate</source>
        <translation>معدل التسرب المتوسط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median rate of detected mask leakage</source>
        <translation>متوسط معدل تسرب القناع المكتشف</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median Leaks</source>
        <translation>تسرب متوسط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Respiratory Disturbance Index</source>
        <translation>تشغيل مؤشر اضطراب الجهاز التنفسي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation>رسم بياني يوضح تشغيل RDI للساعة الماضية</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>Sleep position in degrees</source>
        <translation>موقف النوم بالدرجات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>Upright angle in degrees</source>
        <translation>زاوية تستقيم بالدرجات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Movement</source>
        <translation>حركة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Movement detector</source>
        <translation>كاشف الحركة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>CPAP Session contains summary data only</source>
        <translation>تحتوي جلسة CPAP على ملخص البيانات فقط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8954"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="290"/>
        <source>PAP Mode</source>
        <translation>وضع PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="111"/>
        <source>Couldn&apos;t parse Channels.xml, OSCAR cannot continue and is exiting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="290"/>
        <source>PAP Device Mode</source>
        <translation>وضع جهاز PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="294"/>
        <source>APAP (Variable)</source>
        <translation>APAP (متغير)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="298"/>
        <source>ASV (Fixed EPAP)</source>
        <translation>ASV (ثابت EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="299"/>
        <source>ASV (Variable EPAP)</source>
        <translation>ASV (متغير EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Height</source>
        <translation>ارتفاع</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Physical Height</source>
        <translation>الارتفاع البدني</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Notes</source>
        <translation>ملاحظات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Bookmark Notes</source>
        <translation>إشارة مرجعية ملاحظات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>Body Mass Index</source>
        <translation>مؤشر كتلة الجسم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation>ما هو شعورك (0 = مثل حماقة ، 10 = لا يمكن وقفها)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>Bookmark Start</source>
        <translation>إشارة مرجعية ابدأ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="313"/>
        <source>Bookmark End</source>
        <translation>المرجعية نهاية</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="314"/>
        <source>Last Updated</source>
        <translation>آخر تحديث</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>Journal Notes</source>
        <translation>مجلة مذكرات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>Journal</source>
        <translation>مجلة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation>1 = استيقظ 2 = REM 3 = نوم خفيف 4 = نوم عميق</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Brain Wave</source>
        <translation>موجة الدماغ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>BrainWave</source>
        <translation>الفكره الراءعه</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Awakenings</source>
        <translation>الاستيقاظ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Number of Awakenings</source>
        <translation>عدد الصحوات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Morning Feel</source>
        <translation>يشعر الصباح</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>How you felt in the morning</source>
        <translation>كيف شعرت في الصباح</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Time Awake</source>
        <translation>الوقت مستيقظا</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Time spent awake</source>
        <translation>الوقت الذي يقضيه مستيقظا</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time In REM Sleep</source>
        <translation>الوقت في REM النوم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time spent in REM Sleep</source>
        <translation>الوقت الذي يقضيه في REM Sleep</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time in REM Sleep</source>
        <translation>الوقت في REM النوم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Time In Light Sleep</source>
        <translation>الوقت في النوم الخفيف</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Time spent in light sleep</source>
        <translation>الوقت الذي يقضيه في النوم الخفيف</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Time in Light Sleep</source>
        <translation>الوقت في النوم الخفيف</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="330"/>
        <source>Time In Deep Sleep</source>
        <translation>الوقت في النوم العميق</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="330"/>
        <source>Time spent in deep sleep</source>
        <translation>الوقت الذي يقضيه في النوم العميق</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="330"/>
        <source>Time in Deep Sleep</source>
        <translation>الوقت في النوم العميق</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="331"/>
        <source>Time to Sleep</source>
        <translation>وقت النوم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="331"/>
        <source>Time taken to get to sleep</source>
        <translation>الوقت المستغرق للوصول إلى النوم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="332"/>
        <source>Zeo ZQ</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="332"/>
        <source>Zeo sleep quality measurement</source>
        <translation>قياس جودة نوم النوم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="332"/>
        <source>ZEO ZQ</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Debugging channel #1</source>
        <translation>قناة التصحيح # 1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Top secret internal stuff you&apos;re not supposed to see ;)</source>
        <translation>أشياء داخلية سرية للغاية لا يفترض أن تراها ؛)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Test #1</source>
        <translation>اختبار # 1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Debugging channel #2</source>
        <translation>قناة التصحيح # 2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Test #2</source>
        <translation>اختبار # 2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="748"/>
        <source>Zero</source>
        <translation>صفر</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="751"/>
        <source>Upper Threshold</source>
        <translation>العتبة العليا</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="754"/>
        <source>Lower Threshold</source>
        <translation>العتبة السفلى</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="498"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation>نظرًا لأنك لم تحدد مجلد بيانات ، فسيتم إنهاء OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="185"/>
        <source>Choose the SleepyHead data folder to migrate</source>
        <translation>اختر مجلد بيانات SleepyHead لترحيله</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="186"/>
        <source>or CANCEL to skip migration.</source>
        <translation>أو إلغاء لتخطي الهجرة.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="200"/>
        <source>The folder you chose does not contain valid SleepyHead data.</source>
        <translation>المجلد الذي اخترته لا يحتوي على بيانات SleepyHead صالحة.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="201"/>
        <source>You cannot use this folder:</source>
        <translation>لا يمكنك استخدام هذا المجلد:</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="216"/>
        <source>Migrating </source>
        <translation>ترحيل </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="216"/>
        <source> files</source>
        <translation> ملفات</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="217"/>
        <source>from </source>
        <translation>من عند </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="217"/>
        <source>to </source>
        <translation>إلى </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="324"/>
        <source>OSCAR crashed due to an incompatibility with your graphics hardware.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="325"/>
        <source>To resolve this, OSCAR has reverted to a slower but more compatible method of drawing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="485"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation>سيقوم OSCAR بإعداد مجلد لبياناتك.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="486"/>
        <source>If you have been using SleepyHead, OSCAR can copy your old data to this folder later.</source>
        <translation>إذا كنت تستخدم SleepyHead ، فيمكن لـ OSCAR نسخ بياناتك القديمة إلى هذا المجلد لاحقًا.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="487"/>
        <source>We suggest you use this folder: </source>
        <translation>نقترح عليك استخدام هذا المجلد: </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="488"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation>انقر فوق موافق لقبول هذا ، أو لا إذا كنت تريد استخدام مجلد مختلف.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="494"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation>اختر أو أنشئ مجلدًا جديدًا لبيانات OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="499"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation>في المرة القادمة التي تقوم فيها بتشغيل OSCAR ، سيتم سؤالك مرة أخرى.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="510"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation>المجلد الذي اخترته ليس فارغًا ، كما أنه لا يحتوي بالفعل على بيانات OSCAR صالحة.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="531"/>
        <source>Data directory:</source>
        <translation>دليل البيانات:</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="539"/>
        <source>Migrate SleepyHead Data?</source>
        <translation>ترحيل بيانات SleepyHead؟</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="540"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead data</source>
        <translation>في الشاشة التالية ، سيطلب منك OSCAR تحديد مجلد يحتوي على بيانات SleepyHead</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="541"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead data.</source>
        <translation>انقر فوق [OK] للانتقال إلى الشاشة التالية أو [No] إذا كنت لا ترغب في استخدام أي من بيانات SleepyHead.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="596"/>
        <source>Version &quot;%1&quot; is invalid, cannot continue!</source>
        <translation>الإصدار &quot;%1&quot; غير صالح ، لا يمكن المتابعة!</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="605"/>
        <source>The version of OSCAR you are running (%1) is OLDER than the one used to create this data (%2).</source>
        <translation>إصدار OSCAR الذي تقوم بتشغيله (%1) هو أقدم من الإصدار المستخدم لإنشاء هذه البيانات (%2).</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="609"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation>من المحتمل أن يؤدي هذا إلى تلف البيانات ، هل أنت متأكد من رغبتك في القيام بذلك؟</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="669"/>
        <source>Question</source>
        <translation>سؤال</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="497"/>
        <source>Exiting</source>
        <translation>الخروج</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="511"/>
        <source>Are you sure you want to use this folder?</source>
        <translation>هل أنت متأكد أنك تريد استخدام هذا المجلد؟</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="277"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP machine</source>
        <translation>لا تنسَ أن تضع بطاقة بياناتك في جهاز CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="277"/>
        <source>OSCAR Reminder</source>
        <translation>أوسكار تذكير</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="460"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation>يمكنك العمل فقط مع مثيل واحد لملف تعريف OSCAR فردي في كل مرة.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="461"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation>إذا كنت تستخدم التخزين السحابي ، فتأكد من إغلاق OSCAR وإكمال المزامنة أولاً على الكمبيوتر الآخر قبل المتابعة.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="474"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation>جارٍ تحميل ملف التعريف &quot;%1&quot; ...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2248"/>
        <source>Sorry, your %1 %2 machine is not currently supported.</source>
        <translation>عذرًا ، جهاز %1 %2 غير مدعوم حاليًا.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2255"/>
        <source>Recompressing Session Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2794"/>
        <source>Please select a location for your zip other than the data card itself!</source>
        <translation>يُرجى تحديد موقع لرمزك البريدي بخلاف بطاقة البيانات نفسها!</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2821"/>
        <location filename="../oscar/mainwindow.cpp" line="2888"/>
        <source>Unable to create zip!</source>
        <translation>تعذر إنشاء ملف مضغوط!</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1186"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation>هل تريد بالتأكيد إعادة تعيين جميع ألوان قناتك وإعداداتها إلى الإعدادات الافتراضية؟</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1239"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation>هل أنت متأكد من رغبتك في إعادة تعيين كل الألوان والإعدادات لقناة الموجي إلى الإعدادات الافتراضية؟</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="40"/>
        <source>There are no graphs visible to print</source>
        <translation>لا توجد رسوم بيانية مرئية للطباعة</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="55"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation>هل ترغب في إظهار المناطق التي تمت الإشارة إليها في هذا التقرير؟</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="99"/>
        <source>Printing %1 Report</source>
        <translation>طباعة تقرير %1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="133"/>
        <source>%1 Report</source>
        <translation>%1 تقرير</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="191"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation>: %1 ساعات، %2 دقيقة، %3 ثواني
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="244"/>
        <source>RDI	%1
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="246"/>
        <source>AHI	%1
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="279"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation>AI=%1 HI=%2 CAI=%3 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="285"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="289"/>
        <source>UAI=%1 </source>
        <translation>UAI=%1 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="291"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation>NRI=%1 LKI=%2 EPI=%3</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="360"/>
        <source>Reporting from %1 to %2</source>
        <translation>الإبلاغ من %1 إلى %2</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="432"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation>كامل يوم الموجة الموجي</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="438"/>
        <source>Current Selection</source>
        <translation>الإختيار الحالي</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="448"/>
        <source>Entire Day</source>
        <translation>يوم كامل</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="564"/>
        <source>%1 %2 %3</source>
        <translation>%1 %2 %3</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="573"/>
        <source>Page %1 of %2</source>
        <translation>صفحة %1 من %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="997"/>
        <source>Days: %1</source>
        <translation>الأيام: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1001"/>
        <source>Low Usage Days: %1</source>
        <translation>أيام الاستخدام المنخفض: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1002"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation>(%1٪ متوافق ، معرّف كـ &gt; %2 ساعات)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1118"/>
        <source>(Sess: %1)</source>
        <translation>(الاختبار: %1)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1126"/>
        <source>Bedtime: %1</source>
        <translation>وقت النوم: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1128"/>
        <source>Waketime: %1</source>
        <translation>وقت التشغيل: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1168"/>
        <source>90%</source>
        <translation>90%</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1240"/>
        <source>(Summary Only)</source>
        <translation>(الملخص فقط)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="459"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation>يوجد ملف تعريف موجود بالفعل لهذا الملف الشخصي &apos;%1&apos; ، تمت المطالبة به على &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="175"/>
        <source>Peak</source>
        <translation>قمة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="179"/>
        <source>%1% %2</source>
        <translation>%1% %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="295"/>
        <source>Fixed Bi-Level</source>
        <translation>ثابت ثنائي المستوى</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="296"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation>المستوى الثنائي التلقائي (PS الثابت)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="297"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation>ثنائية المستوى التلقائي (متغير PS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1496"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1518"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1535"/>
        <source>Fixed %1 (%2)</source>
        <translation>ثابت %1 (%2)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1537"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation>الحد الأدنى %1 الحد الأقصى %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1539"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation>EPAP %1 IPAP %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1541"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation>PS %1 على %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1543"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1552"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1548"/>
        <source>EPAP %1 PS %2-%3 (%4)</source>
        <translation>EPAP %1 PS %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1559"/>
        <source>EPAP %1 IPAP %2-%3 (%4)</source>
        <translation>EPAP %1 IPAP %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="166"/>
        <location filename="../oscar/SleepLib/day.cpp" line="168"/>
        <location filename="../oscar/SleepLib/day.cpp" line="170"/>
        <location filename="../oscar/SleepLib/day.cpp" line="175"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="340"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation>أحدث بيانات قياس التأكسج: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="341"/>
        <source>(last night)</source>
        <translation>(اخر مساء)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="342"/>
        <source>(1 day ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="343"/>
        <source>(%2 days ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="348"/>
        <source>No oximetry data has been imported yet.</source>
        <translation>لم يتم استيراد بيانات قياس التأكسج حتى الآن.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>Contec</source>
        <translation>Contec</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <source>CMS50</source>
        <translation>CMS50</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>Fisher &amp; Paykel</source>
        <translation>Fisher &amp; Paykel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>ICON</source>
        <translation>ICON</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>DeVilbiss</source>
        <translation>DeVilbiss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>Intellipap</source>
        <translation>Intellipap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="85"/>
        <source>SmartFlex Settings</source>
        <translation>إعدادات SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>ChoiceMMed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>MD300</source>
        <translation>MD300</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>Respironics</source>
        <translation>Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>M-Series</source>
        <translation>M-Series</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="419"/>
        <source>Philips Respironics</source>
        <translation>Philips Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="419"/>
        <source>System One</source>
        <translation>System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="92"/>
        <source>ResMed</source>
        <translation>ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="93"/>
        <source>S9</source>
        <translation>S9</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="126"/>
        <source>EPR: </source>
        <translation>EPR: </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose</source>
        <translation>Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose Software</source>
        <translation>Somnopose البرمجيات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="39"/>
        <source>Zeo</source>
        <translation>Zeo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="39"/>
        <source>Personal Sleep Coach</source>
        <translation>مدرب النوم الشخصي</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="196"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation>قاعدة البيانات القديمة
يرجى إعادة بناء بيانات CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="373"/>
        <source> (%2 min, %3 sec)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="375"/>
        <source> (%3 sec)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="388"/>
        <source>Pop out Graph</source>
        <translation>المنبثقة الرسم البياني</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1420"/>
        <source>I&apos;m very sorry your machine doesn&apos;t record useful data to graph in Daily View :(</source>
        <translation>أنا آسف جدًا أن جهازك لا يسجل بيانات مفيدة للرسم البياني في عرض يومي :(</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1423"/>
        <source>There is no data to graph</source>
        <translation>لا توجد بيانات للرسم البياني</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1608"/>
        <source>d MMM yyyy [ %1 - %2 ]</source>
        <translation>d MMM yyyy [ %1 - %2 ]</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2166"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2209"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2280"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2297"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2261"/>
        <source>Hide All Events</source>
        <translation>إخفاء جميع الأحداث</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2262"/>
        <source>Show All Events</source>
        <translation>عرض جميع الأحداث</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2603"/>
        <source>Unpin %1 Graph</source>
        <translation>إلغاء تثبيت الرسم البياني %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2605"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2680"/>
        <source>Popout %1 Graph</source>
        <translation>المنبثقة %1 الرسم البياني</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2682"/>
        <source>Pin %1 Graph</source>
        <translation>دبوس %1 الرسم البياني</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1042"/>
        <source>Plots Disabled</source>
        <translation>مؤامرات المعوقين</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1116"/>
        <source>Duration %1:%2:%3</source>
        <translation>المدة الزمنية %1:%2:%3</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1117"/>
        <source>AHI %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="108"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="151"/>
        <source>Relief: %1</source>
        <translation>ارتياح: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="157"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation>ساعات العمل: %1 س ، %2 م ، %3 ث</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="260"/>
        <source>Machine Information</source>
        <translation>معلومات الجهاز</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="30"/>
        <source>Journal Data</source>
        <translation>مجلة البيانات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="48"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation>عثر OSCAR على مجلد &quot;دفتر يومية&quot; قديم ، لكن يبدو أنه تمت إعادة تسميته:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="50"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation>لن تلمس الأداة OSCAR هذا المجلد ، وستقوم بإنشاء مجلد جديد بدلاً منه.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="51"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation>يرجى توخي الحذر عند اللعب في مجلدات ملف تعريف OSCAR :-P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="58"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation>لسبب ما ، لم يتمكن OSCAR من العثور على سجل كائن دفتر اليومية في ملف التعريف الخاص بك ، ولكنه عثر على مجلدات بيانات دفتر يومية متعددة.

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="59"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation>اختار OSCAR أول واحد فقط من هذه ، وسوف يستخدمه في المستقبل:

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="61"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation>إذا كانت بياناتك القديمة مفقودة ، انسخ محتويات جميع مجلدات Journal_XXXXXXX الأخرى إلى هذا المجلد يدويًا.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F3.7</source>
        <translation>CMS50F3.7</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F</source>
        <translation>CMS50F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1543"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1545"/>
        <source>SmartFlex Mode</source>
        <translation>وضع SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1544"/>
        <source>Intellipap pressure relief mode.</source>
        <translation>وضع تخفيف الضغط Intellipap.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1550"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="106"/>
        <source>Ramp Only</source>
        <translation>منحدر فقط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1551"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="107"/>
        <source>Full Time</source>
        <translation>وقت كامل</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1554"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1556"/>
        <source>SmartFlex Level</source>
        <translation>مستوى SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1555"/>
        <source>Intellipap pressure relief level.</source>
        <translation>Intellipap مستوى تخفيف الضغط.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="405"/>
        <source>Locating STR.edf File(s)...</source>
        <translation>تحديد موقع ملف (ملفات) STR.edf ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="557"/>
        <source>Cataloguing EDF Files...</source>
        <translation>فهرسة ملفات EDF ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="576"/>
        <source>Queueing Import Tasks...</source>
        <translation>انتظار استيراد مهام ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="603"/>
        <source>Finishing Up...</source>
        <translation>الانتهاء من ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="85"/>
        <source>CPAP Mode</source>
        <translation>وضع CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="91"/>
        <source>VPAP-T</source>
        <translation>VPAP-T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="92"/>
        <source>VPAP-S</source>
        <translation>VPAP-S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="93"/>
        <source>VPAP-S/T</source>
        <translation>VPAP-S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="95"/>
        <source>VPAPauto</source>
        <translation>VPAPauto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="97"/>
        <source>ASVAuto</source>
        <translation>ASVAuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="100"/>
        <source>Auto for Her</source>
        <translation>السيارات لها</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="103"/>
        <source>EPR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="103"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation>ResMed الزفير تخفيف الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="108"/>
        <source>Patient???</source>
        <translation>صبور؟؟؟</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="111"/>
        <source>EPR Level</source>
        <translation>مستوى EPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="111"/>
        <source>Exhale Pressure Relief Level</source>
        <translation>الزفير تخفيف الضغط المستوى</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="94"/>
        <source>?5?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="98"/>
        <source>?9?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="99"/>
        <source>?10?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>SmartStart</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>Machine auto starts by breathing</source>
        <translation>يبدأ تشغيل الآلة عن طريق التنفس</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>Smart Start</source>
        <translation>البداية الذكية</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>Humid. Status</source>
        <translation>رطب. الحالة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>Humidifier Enabled Status</source>
        <translation>تمكين حالة المرطب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="130"/>
        <source>Humid. Level</source>
        <translation>رطب. مستوى</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="130"/>
        <source>Humidity Level</source>
        <translation>مستوى الرطوبة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="143"/>
        <source>Temperature</source>
        <translation>درجة الحرارة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="143"/>
        <source>ClimateLine Temperature</source>
        <translation>درجة حرارة خط المناخ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Temp. Enable</source>
        <translation>مؤقت. ممكن</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>ClimateLine Temperature Enable</source>
        <translation>درجة حرارة خط التكييف ممكنة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Temperature Enable</source>
        <translation>تمكين درجة الحرارة</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="155"/>
        <source>AB Filter</source>
        <translation>تصفية AB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="155"/>
        <source>Antibacterial Filter</source>
        <translation>مرشح مضاد للجراثيم</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="161"/>
        <source>Pt. Access</source>
        <translation>حزب العمال. التمكن من</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="161"/>
        <source>Patient Access</source>
        <translation>وصول المريض</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="167"/>
        <source>Climate Control</source>
        <translation>التحكم في المناخ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="170"/>
        <source>Manual</source>
        <translation>كتيب</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="989"/>
        <source>Parsing STR.edf records...</source>
        <translation>جارٍ تحليل سجلات STR.edf ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9142"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="169"/>
        <source>Auto</source>
        <translation>تلقاءي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="173"/>
        <source>Mask</source>
        <translation>قناع</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="173"/>
        <source>ResMed Mask Setting</source>
        <translation>إعداد قناع ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="175"/>
        <source>Pillows</source>
        <translation>الوسائد</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="176"/>
        <source>Full Face</source>
        <translation>كامل الوجه</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="177"/>
        <source>Nasal</source>
        <translation>أنفي</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="180"/>
        <source>Ramp Enable</source>
        <translation>منحدر تمكين</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>Weinmann</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>SOMNOsoft2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="350"/>
        <source>Snapshot %1</source>
        <translation>لقطة %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50D+</source>
        <translation>CMS50D+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50E/F</source>
        <translation>CMS50E/F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="682"/>
        <source>Loading %1 data for %2...</source>
        <translation>جارٍ تحميل بيانات %1 لـ %2 ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="692"/>
        <source>Scanning Files</source>
        <translation>مسح الملفات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="726"/>
        <source>Migrating Summary File Location</source>
        <translation>ترحيل ملف ملخص الموقع</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="889"/>
        <source>Loading Summaries.xml.gz</source>
        <translation>تحميل Summaries.xml.gz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1020"/>
        <source>Loading Summary Data</source>
        <translation>تحميل ملخص البيانات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="14"/>
        <source>Please Wait...</source>
        <translation>ارجوك انتظر...</translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="223"/>
        <source>%1
Line %2, column %3</source>
        <translation>%1
خط %2, عمود %3</translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="236"/>
        <source>Could not parse Updates.xml file.</source>
        <translation>لا يمكن تحليل ملف Updates.xml.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="312"/>
        <source>Peak %1</source>
        <translation>قمة %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="166"/>
        <source>Updating Statistics cache</source>
        <translation>تحديث الاحصائيات المخبأ</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="708"/>
        <source>Usage Statistics</source>
        <translation>إحصائيات الاستخدام</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="514"/>
        <source>Loading summaries</source>
        <translation>تحميل الملخصات</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/dreem_loader.h" line="36"/>
        <source>Dreem</source>
        <translation>Dreem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="85"/>
        <source>Your Viatom device generated data that OSCAR has never seen before.</source>
        <translation>أنتج جهاز Viatom بيانات لم يشاهدها OSCAR من قبل.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="86"/>
        <source>The imported data may not be entirely accurate, so the developers would like a copy of your Viatom files to make sure OSCAR is handling the data correctly.</source>
        <translation>قد لا تكون البيانات المستوردة دقيقة تمامًا ، لذلك يرغب المطورون في الحصول على نسخة من ملفات Viatom للتأكد من أن OSCAR يتعامل مع البيانات بشكل صحيح.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="39"/>
        <source>Viatom</source>
        <translation>Viatom</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="39"/>
        <source>Viatom Software</source>
        <translation>برنامج Viatom</translation>
    </message>
</context>
<context>
    <name>Report</name>
    <message>
        <location filename="../oscar/reports.ui" line="14"/>
        <source>Form</source>
        <translation>شكل</translation>
    </message>
    <message>
        <location filename="../oscar/reports.ui" line="27"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="246"/>
        <source>%1h %2m</source>
        <translation>%1h %2m</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="289"/>
        <source>No Sessions Present</source>
        <translation>لا توجد جلسات حالية</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="534"/>
        <source>CPAP Statistics</source>
        <translation>إحصائيات CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="537"/>
        <location filename="../oscar/statistics.cpp" line="1358"/>
        <source>CPAP Usage</source>
        <translation>استخدام CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="538"/>
        <source>Average Hours per Night</source>
        <translation>متوسط ساعات الليلة</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="541"/>
        <source>Therapy Efficacy</source>
        <translation>فعالية العلاج</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="551"/>
        <source>Leak Statistics</source>
        <translation>تسرب الاحصائيات</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="556"/>
        <source>Pressure Statistics</source>
        <translation>إحصائيات الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="581"/>
        <source>Oximeter Statistics</source>
        <translation>إحصائيات مقياس التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="585"/>
        <source>Blood Oxygen Saturation</source>
        <translation>تشبع الأكسجين في الدم</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="590"/>
        <source>Pulse Rate</source>
        <translation>معدل النبض</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="601"/>
        <source>%1 Median</source>
        <translation>%1 الوسيط</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="602"/>
        <location filename="../oscar/statistics.cpp" line="603"/>
        <source>Average %1</source>
        <translation>معدل %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="605"/>
        <source>Min %1</source>
        <translation>دقيقة %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="606"/>
        <source>Max %1</source>
        <translation>ماكس %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="607"/>
        <source>%1 Index</source>
        <translation>%1 فهرس</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="608"/>
        <source>% of time in %1</source>
        <translation>٪ من الوقت في %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="609"/>
        <source>% of time above %1 threshold</source>
        <translation>٪ من الوقت أعلاه %1 عتبة</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="610"/>
        <source>% of time below %1 threshold</source>
        <translation>٪ من الوقت أدناه %1 عتبة</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="633"/>
        <source>Name: %1, %2</source>
        <translation>اسم: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="635"/>
        <source>DOB: %1</source>
        <translation>تاريخ الميلاد: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="638"/>
        <source>Phone: %1</source>
        <translation>هاتف: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="641"/>
        <source>Email: %1</source>
        <translation>البريد الإلكتروني: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="644"/>
        <source>Address:</source>
        <translation>عنوان:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="727"/>
        <source>This report was prepared on %1 by OSCAR %2</source>
        <translation>تم تحضير هذا التقرير في %1 بواسطة OSCAR %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1359"/>
        <source>Days Used: %1</source>
        <translation>الأيام المستخدمة: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1360"/>
        <source>Low Use Days: %1</source>
        <translation>انخفاض استخدام أيام: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1361"/>
        <source>Compliance: %1%</source>
        <translation>الالتزام: %1%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1385"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation>أيام AHI من 5 أو أكبر: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1392"/>
        <source>Best AHI</source>
        <translation>أفضل AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1396"/>
        <location filename="../oscar/statistics.cpp" line="1408"/>
        <source>Date: %1 AHI: %2</source>
        <translation>تاريخ: %1 AHI: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1402"/>
        <source>Worst AHI</source>
        <translation>أسوأ AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1439"/>
        <source>Best Flow Limitation</source>
        <translation>أفضل الحد من التدفق</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1443"/>
        <location filename="../oscar/statistics.cpp" line="1456"/>
        <source>Date: %1 FL: %2</source>
        <translation>تاريخ: %1 FL: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1449"/>
        <source>Worst Flow Limtation</source>
        <translation>أسوأ قيود التدفق</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1461"/>
        <source>No Flow Limitation on record</source>
        <translation>لا توجد قيود على التدفق</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1482"/>
        <source>Worst Large Leaks</source>
        <translation>أسوأ التسريبات الكبيرة</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1490"/>
        <source>Date: %1 Leak: %2%</source>
        <translation>تاريخ: %1 تسرب: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1496"/>
        <source>No Large Leaks on record</source>
        <translation>لا تسريبات كبيرة على الاطلاق</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1519"/>
        <source>Worst CSR</source>
        <translation>أسوأ CSR</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1527"/>
        <source>Date: %1 CSR: %2%</source>
        <translation>تاريخ: %1 CSR: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1532"/>
        <source>No CSR on record</source>
        <translation>لا CSR في السجل</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1549"/>
        <source>Worst PB</source>
        <translation>أسوأ PB</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1557"/>
        <source>Date: %1 PB: %2%</source>
        <translation>تاريخ: %1 PB: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1562"/>
        <source>No PB on record</source>
        <translation>لا PB على السجل</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1570"/>
        <source>Want more information?</source>
        <translation>تريد المزيد من المعلومات؟</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1571"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation>يحتاج OSCAR إلى جميع البيانات الموجزة المحملة لحساب أفضل / أسوأ البيانات للأيام الفردية.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1572"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation>يرجى تمكين خانة الاختيار ملخص التحميل المسبق في التفضيلات للتأكد من توفر هذه البيانات.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1592"/>
        <source>Best RX Setting</source>
        <translation>أفضل إعداد RX</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1595"/>
        <location filename="../oscar/statistics.cpp" line="1607"/>
        <source>Date: %1 - %2</source>
        <translation>تاريخ: %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1598"/>
        <location filename="../oscar/statistics.cpp" line="1610"/>
        <source>AHI: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1599"/>
        <location filename="../oscar/statistics.cpp" line="1611"/>
        <source>Total Hours: %1</source>
        <translation>مجموع الساعات: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1604"/>
        <source>Worst RX Setting</source>
        <translation>إعداد RX أسوأ</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1150"/>
        <source>Most Recent</source>
        <translation>الأحدث</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="539"/>
        <source>Compliance (%1 hrs/day)</source>
        <translation>الامتثال (%1 ساعة / يوم)</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="730"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation>OSCAR هو برنامج تقرير CPAP مفتوح المصدر مجاني</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="963"/>
        <source>Changes to Machine Settings</source>
        <translation>التغييرات في إعدادات الجهاز</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1049"/>
        <source>No data found?!?</source>
        <translation>لاتوجد بيانات؟!؟</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1051"/>
        <source>Oscar has no data to report :(</source>
        <translation>أوسكار ليس لديه بيانات للإبلاغ عنها :(</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1151"/>
        <source>Last Week</source>
        <translation>الاسبوع الماضى</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1152"/>
        <source>Last 30 Days</source>
        <translation>آخر 30 يوم</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1153"/>
        <source>Last 6 Months</source>
        <translation>آخر 6 أشهر</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1154"/>
        <source>Last Year</source>
        <translation>العام الماضي</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1158"/>
        <source>Last Session</source>
        <translation>أخر موسم</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1199"/>
        <source>Details</source>
        <translation>تفاصيل</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1213"/>
        <source>No %1 data available.</source>
        <translation>لا توجد بيانات %1 متاحة.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1216"/>
        <source>%1 day of %2 Data on %3</source>
        <translation>%1 يوم من %2 بيانات على %3</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1222"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation>%1 يوم من %2 بيانات ، بين %3 و %4</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="971"/>
        <source>Days</source>
        <translation>أيام</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="975"/>
        <source>Pressure Relief</source>
        <translation>انتهاء الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="977"/>
        <source>Pressure Settings</source>
        <translation>إعدادات الضغط</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="904"/>
        <source>Machine Information</source>
        <translation>معلومات الجهاز</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="910"/>
        <source>First Use</source>
        <translation>اول استخدام</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="911"/>
        <source>Last Use</source>
        <translation>الاستخدام الأخير</translation>
    </message>
</context>
<context>
    <name>UpdaterWindow</name>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="14"/>
        <source>OSCAR Updater</source>
        <translation>OSCAR محدث</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="60"/>
        <source>A new version of $APP is available</source>
        <translation>إصدار جديد من $APP متاح</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="106"/>
        <source>Version Information</source>
        <translation>معلومات الإصدار</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="117"/>
        <source>Release Notes</source>
        <translation>ملاحظات الإصدار</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="142"/>
        <source>Build Notes</source>
        <translation>بناء ملاحظات</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="195"/>
        <source>Maybe &amp;Later</source>
        <translation>ربما في وقت لاحق</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="215"/>
        <source>&amp;Upgrade Now</source>
        <translation>تحديث الآن</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="250"/>
        <source>Please wait while updates are downloaded and installed...</source>
        <translation>الرجاء الانتظار حتى يتم تنزيل التحديثات وتثبيتها ...</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="264"/>
        <source>Updates</source>
        <translation>التحديثات</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="304"/>
        <source>Component</source>
        <translation>مكون</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="309"/>
        <source>Version</source>
        <translation>الإصدار</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="314"/>
        <source>Size</source>
        <translation>بحجم</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="319"/>
        <source>Progress</source>
        <translation>تقدم</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="328"/>
        <source>Log</source>
        <translation>سجل</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="374"/>
        <source>Downloading &amp; Installing Updates</source>
        <translation>تحميل وتثبيت التحديثات</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="394"/>
        <source>&amp;Finished</source>
        <translation>تم الانتهاء من</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="146"/>
        <source>Requesting </source>
        <translation>جاري الطلب </translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <source>OSCAR Updates are currently unavailable for this platform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="321"/>
        <source>You are already running the latest version.</source>
        <translation>أنت تقوم بالفعل بتشغيل أحدث إصدار.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="371"/>
        <source>Would you like to download and install them now?</source>
        <translation>هل ترغب في تنزيلها وتثبيتها الآن؟</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="363"/>
        <source>No updates were found for your platform.</source>
        <translation>لم يتم العثور على تحديثات للنظام الأساسي الخاص بك.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="93"/>
        <source>Updates are not yet implemented</source>
        <translation>لم يتم تنفيذ التحديثات بعد</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="127"/>
        <source>Checking for OSCAR Updates</source>
        <translation>التحقق من تحديثات OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="363"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="369"/>
        <source>OSCAR Updates</source>
        <translation>تحديثات OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="318"/>
        <source>Version %1 of OSCAR is available, opening link to download site.</source>
        <translation>الإصدار %1 من OSCAR متاح ، يفتح الرابط إلى موقع التنزيل.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="370"/>
        <source>New OSCAR Updates are avilable:</source>
        <translation>تحديثات OSCAR الجديدة متوفرة:</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="171"/>
        <source>%1 bytes received</source>
        <translation>تلقى %1 بايت</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.ui" line="14"/>
        <source>Form</source>
        <translation>شكل</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="128"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>مرحبًا بكم في مراسل تحليل CPAP مفتوح مستخرج</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="143"/>
        <source>What would you like to do?</source>
        <translation>ماذا تريد ان تفعل؟</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="186"/>
        <source>CPAP Importer</source>
        <translation>مستورد CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="224"/>
        <source>Oximetry Wizard</source>
        <translation>معالج التأكسج</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="262"/>
        <source>Daily View</source>
        <translation>عرض يومي</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="300"/>
        <source>Overview</source>
        <translation>نظرة عامة</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="338"/>
        <source>Statistics</source>
        <translation>الإحصاء</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="581"/>
        <source>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards need to be locked &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;before inserting into your computer.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Some operating systems write index files to the card without asking, which can render your card unreadable by your cpap machine.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;span style=&quot; font-weight:600;&quot;&gt;تحذير: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;بطاقات ResMed S9 SD تحتاج إلى أن تكون مؤمنة &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;قبل الإدراج في جهاز الكمبيوتر الخاص بك.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;تكتب بعض أنظمة التشغيل ملفات الفهرس إلى البطاقة دون أن تطلب ، مما قد يجعل بطاقتك غير قابلة للقراءة بواسطة جهاز cpap.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation>سيكون من الجيد التحقق من ملف&gt; تفضيلات أولاً ،</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>as there are some options that affect import.</source>
        <translation>لأن هناك بعض الخيارات التي تؤثر على الاستيراد.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="162"/>
        <source>Note that some preferences are forced when a ResMed machine is detected</source>
        <translation>لاحظ أنه يتم فرض بعض التفضيلات عند اكتشاف جهاز ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>First import can take a few minutes.</source>
        <translation>يمكن أن يستغرق الاستيراد الأول بضع دقائق.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="176"/>
        <source>The last time you used your %1...</source>
        <translation>آخر مرة استخدمت فيها %1 ...</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="180"/>
        <source>last night</source>
        <translation>اخر مساء</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="182"/>
        <source>%2 days ago</source>
        <translation>%2 منذ أيام</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="184"/>
        <source>was %1 (on %2)</source>
        <translation>كان %1 (في %2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="192"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1 ساعات ،%2 دقيقة و %3 ثواني</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="195"/>
        <source>Your machine was on for %1.</source>
        <translation>تم تشغيل الجهاز لـ %1.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="196"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&apos;&apos;&lt;font color = red&gt;كان لديك القناع فقط على%1.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="208"/>
        <source>under</source>
        <translation>تحت</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="209"/>
        <source>over</source>
        <translation>على</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="210"/>
        <source>reasonably close to</source>
        <translation>قريبة إلى حد معقول</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="211"/>
        <source>equal to</source>
        <translation>يساوي</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="225"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>كان لديك AHI من %1 ، وهو %2 الخاص بك متوسط %3 أيام %4.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="256"/>
        <source>Your CPAP machine used a constant %1 %2 of air</source>
        <translation>استخدم جهاز CPAP ثابتًا %1 %2 من الهواء</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="259"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>كان ضغطك أقل من %1 %2 لـ %3٪ من الوقت.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="263"/>
        <source>Your machine used a constant %1-%2 %3 of air.</source>
        <translation>يستخدم الجهاز ثابتًا من %1 - %2 %3 من الهواء.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="271"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>ضغط EPAP ثابت عند %1 %2.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="272"/>
        <location filename="../oscar/welcome.cpp" line="278"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>كان ضغط IPAP الخاص بك أقل من %1 %2 لـ %3٪ من الوقت.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="277"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>كان ضغط EPAP أقل من %1 %2 لـ %3٪ من الوقت.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="267"/>
        <source>Your machine was under %1-%2 %3 for %4% of the time.</source>
        <translation>كان جهازك أقل من %1 - %2 %3 لـ %4٪ من الوقت.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="181"/>
        <source>1 day ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="298"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>متوسط التسريبات الخاص بك هو %1 %2، وهو %3 الخاص بك ، متوسط %4 أيام ،%5.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="304"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>لم يتم استيراد بيانات CPAP حتى الآن.</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="793"/>
        <source>%1 days</source>
        <translation>%1 أيام</translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="393"/>
        <source>100% zoom level</source>
        <translation>100 ٪ مستوى التكبير</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="395"/>
        <source>Restore X-axis zoom to 100% to view entire selected period.</source>
        <translation>استعادة تكبير المحور X إلى 100٪ لعرض الفترة المحددة بأكملها.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="397"/>
        <source>Restore X-axis zoom to 100% to view entire day&apos;s data.</source>
        <translation>قم باستعادة تكبير المحور X إلى 100٪ لعرض بيانات اليوم بأكمله.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="399"/>
        <source>Reset Graph Layout</source>
        <translation>إعادة تعيين الرسم البياني تخطيط</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="400"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>إعادة تعيين جميع الرسوم البيانية إلى ارتفاع موحد وترتيب افتراضي.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="403"/>
        <source>Y-Axis</source>
        <translation>Y-المحور</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="404"/>
        <source>Plots</source>
        <translation>فجأة</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="409"/>
        <source>CPAP Overlays</source>
        <translation>تراكب CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="412"/>
        <source>Oximeter Overlays</source>
        <translation>مقياس التأكسج التراكبات</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="415"/>
        <source>Dotted Lines</source>
        <translation>خطوط منقطة</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1801"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1854"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation>انقر نقرًا مزدوجًا فوق عنوان الرقم السري / إلغاء التثبيت
انقر واسحب لإعادة ترتيب الرسوم البيانية</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2099"/>
        <source>Remove Clone</source>
        <translation>إزالة استنساخ</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2103"/>
        <source>Clone %1 Graph</source>
        <translation>استنساخ %1 رسم بياني</translation>
    </message>
</context>
</TS>
