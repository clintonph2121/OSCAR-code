<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translation>&amp;Om</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="117"/>
        <source>Release Notes</source>
        <translation>Utgivelsesnotater</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translatorcomment>Ingen godt norsk ord?</translatorcomment>
        <translation>Heder</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>GPL-lisens</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="239"/>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="35"/>
        <source>Show data folder</source>
        <translation>Vis datamappe</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="39"/>
        <source>About OSCAR %1</source>
        <translation>Om OSCAR %1</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="83"/>
        <source>Sorry, could not locate About file.</source>
        <translation>Beklager, kunne ikke finne Om filen.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="96"/>
        <source>Sorry, could not locate Credits file.</source>
        <translatorcomment>Inget godt ord for Credits?</translatorcomment>
        <translation>Beklager, kunne ikke finne Heder-filen.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="108"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation>Beklager, kunne ikke finne Utgivelsesnotater.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="118"/>
        <source>OSCAR %1</source>
        <translation>OSCAR %1</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="121"/>
        <source>Important:</source>
        <translation>Viktig:</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="122"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceeding, because attempting to roll back later may break things.</source>
        <translation>Siden dette er en forhåndsvisningsverjon så er det anbefalt at du &lt;b&gt;tar backup av dint data-mappe manuelt&lt;/b&gt; før du fortsetter, dette fordi forsøk på å rulle tilbake senere kan ødelegge ting.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="134"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>For å se om lisensteksten er tilgjengelig i ditt språk, se %1.</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="878"/>
        <source>Could not find the oximeter file:</source>
        <translation>Kunne ikke finne oximeterfil:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="884"/>
        <source>Could not open the oximeter file:</source>
        <translation>Kunne ikke åpne oximeterfilen:</translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>Kunne ikke få dataoverføring fra oximeter.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>Vennligst sørg for at du velger &apos;opplasting&apos; fra oximeter-enheter i menyen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="546"/>
        <source>Could not find the oximeter file:</source>
        <translation>Kunne ikke finne oximeter filen:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="552"/>
        <source>Could not open the oximeter file:</source>
        <translation>Kunne ikke åpne oximeter-filen:</translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="435"/>
        <source>Form</source>
        <translation>Fra</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>Gå til forrige dag</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>Vis eller skjul kalenderen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>Gå til neste dag</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>Gå til nyeste dag med dataregistreringer</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>Hendelser</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translation>Vis Størrelse</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1395"/>
        <source>Notes</source>
        <translation>Notater</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translation>Journal</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation> i </translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1063"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1075"/>
        <source>u</source>
        <translation>u</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1085"/>
        <source>Color</source>
        <translation>Farge</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1107"/>
        <location filename="../oscar/daily.ui" line="1117"/>
        <source>Small</source>
        <translation>Liten</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1122"/>
        <source>Medium</source>
        <translation>Middels</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1127"/>
        <source>Big</source>
        <translation>Stor</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1186"/>
        <source>Zombie</source>
        <translation>Zombie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1199"/>
        <source>I&apos;m feeling ...</source>
        <translation>Jeg føler ...</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1215"/>
        <source>Weight</source>
        <translation>Vekt</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1222"/>
        <source>If height is greater than zero in Preferences Dialog, setting weight here will show Body Mass Index (BMI) value</source>
        <translation>Hvis høyde er større enn null i Innstillinger vinduet, så vil vekt her vise Body Mass Index (BMI) verdi</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1292"/>
        <source>Awesome</source>
        <translation>Fantastisk</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1330"/>
        <source>B.M.I.</source>
        <translation>B.M.I.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1346"/>
        <source>Bookmarks</source>
        <translation>Bokmerker</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1367"/>
        <source>Add Bookmark</source>
        <translation>Legg til Bokmerke</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1390"/>
        <source>Starts</source>
        <translation>Starter</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1403"/>
        <source>Remove Bookmark</source>
        <translation>Fjern Bokmerke</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1496"/>
        <source>Flags</source>
        <translation>Flagg</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1548"/>
        <source>Graphs</source>
        <translation>Grafer</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1573"/>
        <source>Show/hide available graphs.</source>
        <translation>Vis/skjul tilgjengelige grafer.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="288"/>
        <source>Breakdown</source>
        <translation>Brutt ned</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="288"/>
        <source>events</source>
        <translation>hendelser</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="299"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="300"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="363"/>
        <source>Time at Pressure</source>
        <translation>Tid på trykk</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="609"/>
        <source>No %1 events are recorded this day</source>
        <translation>Ingen %1 hendelser er registrert denne dag</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="729"/>
        <source>%1 event</source>
        <translation>%1 hendelse</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="730"/>
        <source>%1 events</source>
        <translation>%1 hendelser</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="773"/>
        <source>Session Start Times</source>
        <translation>Økt-start-tid</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="774"/>
        <source>Session End Times</source>
        <translation>Økt slutt-tid</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="980"/>
        <source>Session Information</source>
        <translation>Sessjoninformasjon</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1003"/>
        <source>Oximetry Sessions</source>
        <translation>Oximetry-sessjoner</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1023"/>
        <source>Duration</source>
        <translation>Varighet</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2293"/>
        <source>This bookmark is in a currently disabled area..</source>
        <translation>Dette bokmerket er foreløpig i et deaktivert område..</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1000"/>
        <source>CPAP Sessions</source>
        <translation>CPAP-sessjoner</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1006"/>
        <source>Sleep Stage Sessions</source>
        <translation>SpO2 desatureringer</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1009"/>
        <source>Position Sensor Sessions</source>
        <translation>Posisjonssensor sessjoner</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1014"/>
        <source>Unknown Session</source>
        <translation>Ukjente sessjoner</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1088"/>
        <source>Machine Settings</source>
        <translation>Maskininnstillinger</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1239"/>
        <source>Model %1 - %2</source>
        <translation>Modell %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1244"/>
        <source>PAP Mode: %1</source>
        <translation>PAP-modus: %1%1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1248"/>
        <source>(Mode/Pressure settings are guessed on this day.)</source>
        <translation>(Modus/Trykk-innstillinger er gjettet på denne dagen.)</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1360"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>Denne dagen inneholder oppsummeringsdata, kun begrenset informasjon er tilgjengelig.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1386"/>
        <source>Total ramp time</source>
        <translation>Total rampetid</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1390"/>
        <source>Time outside of ramp</source>
        <translation>Tid utenfor rampe</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1431"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1431"/>
        <source>End</source>
        <translation>Slutt</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1468"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation>Kan ikke vise kakediagram på dette systemet</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1712"/>
        <source>Sorry, this machine only provides compliance data.</source>
        <translation>Beklager, denne maskinen tilbyr kun complicance data.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1731"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;Ingenting her!&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1734"/>
        <source>No data is available for this day.</source>
        <translation>Ingen data tilgjengelig for denne dagen.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1213"/>
        <source>Oximeter Information</source>
        <translation>Oximeterinformasjon</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="183"/>
        <source>Details</source>
        <translation>Detaljer</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1043"/>
        <source>Click to %1 this session.</source>
        <translation>Klikk for å %1 denne sessjonen.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1043"/>
        <source>disable</source>
        <translation>Deaktiver</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1043"/>
        <source>enable</source>
        <translation>Aktiver</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1059"/>
        <source>%1 Session #%2</source>
        <translation>%1 sessjon #%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1060"/>
        <source>%1h %2m %3s</source>
        <translation>%1h %2m %3s</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1092"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing has changed since previous days.</source>
        <translation>&lt;b&gt;Vennligst merk:&lt;/b&gt; Alle innstillinger vist nedenfor er basert på antagelser om at ingenting har endret seg siden forrige dager.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1217"/>
        <source>SpO2 Desaturations</source>
        <translation>SpO2 desatureringer</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1218"/>
        <source>Pulse Change events</source>
        <translation>Pulsendring hendelser</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1219"/>
        <source>SpO2 Baseline Used</source>
        <translation>SpO2 grunnlinje brukt</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1290"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1285"/>
        <source>Statistics</source>
        <translation>Statistikk</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1370"/>
        <source>Total time in apnea</source>
        <translation>Total tid i apne</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1380"/>
        <source>Time over leak redline</source>
        <translation>Tid over lekasjegrense</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1647"/>
        <source>BRICK! :(</source>
        <translation>ØDELAGT :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1455"/>
        <source>Event Breakdown</source>
        <translation>Hendelseroppsummering</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1702"/>
        <source>Sessions all off!</source>
        <translation>Alle økter av!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1704"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>Sessjoner eksisterer for denne dagen, men er skrudd av.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1707"/>
        <source>Impossibly short session</source>
        <translation>Umulig kort sessjon</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1708"/>
        <source>Zero hours??</source>
        <translation>Null timer??</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1711"/>
        <source>BRICK :(</source>
        <translation>ØDELAGT :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1713"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>Klag til din fabrikant av ustyret!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2029"/>
        <source>Pick a Colour</source>
        <translation>Velg en farge</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2336"/>
        <source>Bookmark at %1</source>
        <translation>Bokmerke på %1</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>Eksporter som CSV</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>Datoer:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>Oppløsning:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>Detaljer</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>Økter</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>Daglig</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>Filnavn:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>Eksport</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>Start:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>Slutt:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation>Hurtig fra til:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="61"/>
        <location filename="../oscar/exportcsv.cpp" line="123"/>
        <source>Most Recent Day</source>
        <translation>Nyligste dag</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="126"/>
        <source>Last Week</source>
        <translation>Siste uke</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="129"/>
        <source>Last Fortnight</source>
        <translation>Siste to uker</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="132"/>
        <source>Last Month</source>
        <translation>Siste måned</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="135"/>
        <source>Last 6 Months</source>
        <translation>Siste 6 måneder</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="138"/>
        <source>Last Year</source>
        <translation>Siste år</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="120"/>
        <source>Everything</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="109"/>
        <source>Custom</source>
        <translation>Tilpasset</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="74"/>
        <source>OSCAR_</source>
        <translation>OSCAR_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="77"/>
        <source>Details_</source>
        <translation>Detaljer_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="79"/>
        <source>Sessions_</source>
        <translation>Økter_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="81"/>
        <source>Summary_</source>
        <translation>Oppsummering_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>Select file to export to</source>
        <translation>Velg fil å eksportere til</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="89"/>
        <source>CSV Files (*.csv)</source>
        <translation>CSV-filer (*.csv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="196"/>
        <source>DateTime</source>
        <translation>DatoTid</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="196"/>
        <location filename="../oscar/exportcsv.cpp" line="202"/>
        <source>Session</source>
        <translation>Økt</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="196"/>
        <source>Event</source>
        <translation>Hendelse</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="196"/>
        <source>Data/Duration</source>
        <translation>Dato/Varighet</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="199"/>
        <location filename="../oscar/exportcsv.cpp" line="202"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="199"/>
        <source>Session Count</source>
        <translation>Øktantall</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="199"/>
        <location filename="../oscar/exportcsv.cpp" line="202"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="199"/>
        <location filename="../oscar/exportcsv.cpp" line="202"/>
        <source>End</source>
        <translation>Slutt</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Total Time</source>
        <translation>Total tid</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <source> Count</source>
        <translation> Antall</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="215"/>
        <source>%1% </source>
        <translation>%1% </translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="139"/>
        <source>Import Error</source>
        <translation>Viktig feilmelding</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation>Dette maskinopptaket kan ikke bli importert i denne profilen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>Denne dagens opptak overlapper med allerede eksiterende innhold.</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.ui" line="20"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="92"/>
        <source>Hide this message</source>
        <translation>Skjul denne meldingen</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="198"/>
        <source>Search Topic:</source>
        <translation>Søk emne:</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="58"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>Hjelperfiler er enda ikke tilgjengelig for %1 og vil bli vist i %2.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="68"/>
        <source>Help files do not appear to be present.</source>
        <translation>Hjelperfiler ser ikke ut til å vøre tilgjengelig.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="84"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>Hjelpemotor ble ikke satt opp korrekt</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="99"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>Hjelpemotor kunne ikke registere dokumentasjon korrekt.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="110"/>
        <source>Contents</source>
        <translation>Innhold</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Index</source>
        <translation>Register</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="115"/>
        <source>Search</source>
        <translation>Søk</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="125"/>
        <source>No documentation available</source>
        <translation>Ingen dokumentasjon tilgjengelig</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="213"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation>Vennligsty vent litt.. Indeksering pågår fortsatt</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="239"/>
        <source>No</source>
        <translation>Nei</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>%1 resultat(er) for &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>clear</source>
        <translation>tø,</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="165"/>
        <source>Could not find the oximeter file:</source>
        <translation>Kunne ikke finne oximeterfilen:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="171"/>
        <source>Could not open the oximeter file:</source>
        <translation>Kunne ikke åpne oximeterfilen:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="942"/>
        <source>&amp;Statistics</source>
        <translation>&amp;Statistikk</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="996"/>
        <source>Report Mode</source>
        <translation>Rapporteringsmodus</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1003"/>
        <location filename="../oscar/mainwindow.ui" line="3277"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1013"/>
        <source>Monthly</source>
        <translation>Månedlig</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1020"/>
        <source>Date Range</source>
        <translation>Dato fra til</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1391"/>
        <source>Statistics</source>
        <translation>Statistikk</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1441"/>
        <source>Daily</source>
        <translation>Daglig</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1485"/>
        <source>Overview</source>
        <translation>Oversikt</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1529"/>
        <source>Oximetry</source>
        <translation>Oximetry</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1579"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1629"/>
        <source>Help</source>
        <translation>Hjelp</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2805"/>
        <source>&amp;File</source>
        <translation>&amp;Fil</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2830"/>
        <source>&amp;View</source>
        <translation>&amp;Vis</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2834"/>
        <source>&amp;Reset Graphs</source>
        <translation>&amp;Resett grafer</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2859"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjelp</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2863"/>
        <source>Troubleshooting</source>
        <translation>Feilsøking</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2886"/>
        <source>&amp;Data</source>
        <translation>&amp;Data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2890"/>
        <source>&amp;Advanced</source>
        <translation>&amp;Avansert</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2900"/>
        <source>Purge ALL Machine Data</source>
        <translation>Tøm all maskindata</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2910"/>
        <source>Rebuild CPAP Data</source>
        <translation>Gjenoppbygg-CPAP-data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2933"/>
        <source>&amp;Import CPAP Card Data</source>
        <translation>&amp;Importer CPAP kortdata</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2959"/>
        <source>Show Daily view</source>
        <translation>Vis daglig visning</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2970"/>
        <source>Show Overview view</source>
        <translation>Vis oversiktvisning</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3010"/>
        <source>&amp;Maximize Toggle</source>
        <translation>&amp;Maksimer Toggle</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3013"/>
        <source>Maximize window</source>
        <translation>Maksimer vindu</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3032"/>
        <source>Reset Graph &amp;Heights</source>
        <translation>Nullstille graf &amp;Høyder</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3035"/>
        <source>Reset sizes of graphs</source>
        <translation>Nullstille størrelser på grafer</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3100"/>
        <source>Show Right Sidebar</source>
        <translation>Vise høyre sidebar</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3114"/>
        <source>Show Statistics view</source>
        <translation>Vis statistikkvisning</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3127"/>
        <source>Import &amp;Dreem Data</source>
        <translation>Import &amp;Dreem Data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3157"/>
        <source>Import &amp;Viatom Data</source>
        <translation>Import &amp;Viatom Data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3170"/>
        <source>Show &amp;Line Cursor</source>
        <translation>Vis &amp;linjemarkør</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3192"/>
        <source>Show Daily Left Sidebar</source>
        <translation>Vis daglig venstre sidenar</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3209"/>
        <source>Show Daily Calendar</source>
        <translation>Vis daglig kalender</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3230"/>
        <source>Create zip of CPAP data card</source>
        <translation>Lag zip av CPAP datakort</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3235"/>
        <source>Create zip of all OSCAR data</source>
        <translation>Lag zip av all OSCAR data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3250"/>
        <source>Report an Issue</source>
        <translation>Rapporter et problem</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3255"/>
        <source>System Information</source>
        <translation>Systeminformasjon</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3266"/>
        <source>Show &amp;Pie Chart</source>
        <translation>Vis &amp;kakediagramm</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3269"/>
        <source>Show Pie Chart on Daily page</source>
        <translation>Vis kakediagramm på daglig visning</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3272"/>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3280"/>
        <source>Standard graph order, good for CPAP, APAP, Bi-Level</source>
        <translation>Standard grafrekkefølge, bra for CPAP, APAP, Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3285"/>
        <source>Advanced</source>
        <translation>Avansert</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3288"/>
        <source>Advanced graph order, good for ASV, AVAPS</source>
        <translation>Avansert grafrekkefølge, bra for AVS, AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3299"/>
        <source>Show Personal Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2941"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Innstillinger</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2946"/>
        <source>&amp;Profiles</source>
        <translation>&amp;Profiler</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3002"/>
        <source>&amp;About OSCAR</source>
        <translation>&amp;Om OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3225"/>
        <source>Show Performance Information</source>
        <translation>Vis ytelsesinformasjon</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3240"/>
        <source>CSV Export Wizard</source>
        <translation>CSV eksportveiviser</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3245"/>
        <source>Export for Review</source>
        <translation>Eksporter for gjennomgang</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="106"/>
        <source>E&amp;xit</source>
        <translation>A&amp;vslutt</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2951"/>
        <source>Exit</source>
        <translation>Avslutt</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2956"/>
        <source>View &amp;Daily</source>
        <translation>Vis &amp;daglig</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2967"/>
        <source>View &amp;Overview</source>
        <translation>Vis &amp;oversikt</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2978"/>
        <source>View &amp;Welcome</source>
        <translation>Vis &amp;velkommen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2986"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2997"/>
        <source>Use &amp;AntiAliasing</source>
        <translation>Bruk &amp;antialiasing</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3024"/>
        <source>Show Debug Pane</source>
        <translation>Vis feilsøkingsrute</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3040"/>
        <source>Take &amp;Screenshot</source>
        <translation>Ta &amp;skjermbilde</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3048"/>
        <source>O&amp;ximetry Wizard</source>
        <translation>O&amp;ksimetriveiviser</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3056"/>
        <source>Print &amp;Report</source>
        <translation>Skriv ut &amp;rapport</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3061"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Endre profil</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3206"/>
        <source>Daily Calendar</source>
        <translation>Daglig kalender</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3217"/>
        <source>Backup &amp;Journal</source>
        <translation>Sikkerhetskopi &amp;journal</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3066"/>
        <source>Online Users &amp;Guide</source>
        <translation>&amp;Brukermanual på nett</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3071"/>
        <source>&amp;Frequently Asked Questions</source>
        <translation>&amp;Ofte stilte spørsmål</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3076"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation>&amp;Automatisk opprydding av oksimetri</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3081"/>
        <source>Change &amp;User</source>
        <translation>Endre &amp;bruker</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3086"/>
        <source>Purge &amp;Current Selected Day</source>
        <translation>Rens &amp;valgt dag</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3097"/>
        <source>Right &amp;Sidebar</source>
        <translation>Høyre &amp;sidebar</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3189"/>
        <source>Daily Sidebar</source>
        <translation>Daglig sidebar</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3108"/>
        <source>View S&amp;tatistics</source>
        <translation>Vis s&amp;tatistikk</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1320"/>
        <source>Navigation</source>
        <translation>Navigering</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="441"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1751"/>
        <source>Bookmarks</source>
        <translation>Bokmerker</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2746"/>
        <source>Records</source>
        <translation>Opptak</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2809"/>
        <source>Exp&amp;ort Data</source>
        <translation>Eks&amp;porter data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1347"/>
        <source>Profiles</source>
        <translation>Profiler</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2894"/>
        <source>Purge Oximetry Data</source>
        <translation>Tøm oksimetri data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3111"/>
        <source>View Statistics</source>
        <translation>Vis statistikk</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3122"/>
        <source>Import &amp;ZEO Data</source>
        <translation>Importer &amp;ZEO data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3132"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation>Importer RemStar &amp;MSeries data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3137"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation>Vilkår og &amp;ordliste for søvnproblemer</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3142"/>
        <source>Change &amp;Language</source>
        <translation>Endre &amp;språk</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3147"/>
        <source>Change &amp;Data Folder</source>
        <translation>Endre &amp;datamappe</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3152"/>
        <source>Import &amp;Somnopose Data</source>
        <translation>Importer &amp;Somnopose-data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3162"/>
        <source>Current Days</source>
        <translation>Aktuelle dager</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="537"/>
        <location filename="../oscar/mainwindow.cpp" line="2343"/>
        <source>Welcome</source>
        <translation>Velkommen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="103"/>
        <source>&amp;About</source>
        <translation>&amp;Om</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="774"/>
        <location filename="../oscar/mainwindow.cpp" line="2010"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>Vent, importerer fra sikkerhetskopimappe (r) ...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="718"/>
        <location filename="../oscar/mainwindow.cpp" line="2384"/>
        <location filename="../oscar/mainwindow.cpp" line="2415"/>
        <location filename="../oscar/mainwindow.cpp" line="2546"/>
        <source>Import Problem</source>
        <translation>Importproblem</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="875"/>
        <source>Please insert your CPAP data card...</source>
        <translation>Sett inn CPAP-datakortet ...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="948"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>Tilgang til import er blokkert mens omberegninger pågår.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1000"/>
        <source>CPAP Data Located</source>
        <translation>CPAP-data ligger</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1035"/>
        <source>Import Reminder</source>
        <translation>Importer påminnelse</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1108"/>
        <source>Importing Data</source>
        <translation>Importerer data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1452"/>
        <source>Updates are not yet implemented</source>
        <translation>Oppdateringer er ikke implementert ennå</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1576"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation>Brukerhåndboken åpnes i standard nettleser</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1584"/>
        <source>The FAQ is not yet implemented</source>
        <translation>Ofte stilte spørsmål er ikke implementert</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1833"/>
        <location filename="../oscar/mainwindow.cpp" line="1860"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation>Hvis du kan lese dette, fungerte ikke omstartkommandoen. Du må gjøre det selv manuelt.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1985"/>
        <source>Are you sure you want to rebuild all CPAP data for the following machine:

</source>
        <translation>Er du sikker på at du vil gjenoppbygge alle CPAP-data for følgende maskin:

</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1995"/>
        <source>For some reason, OSCAR does not have any backups for the following machine:</source>
        <translation>Av en eller annen grunn har OSCAR ingen sikkerhetskopier for følgende maskin:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2072"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s machine database for the following machine:&lt;/p&gt;</source>
        <translation>Du er i ferd med å &lt;font size =+2&gt;utslette&lt;/font&gt; OSCARs maskindatabase for følgende maskin:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2130"/>
        <source>A file permission error casued the purge process to fail; you will have to delete the following folder manually:</source>
        <translation>En filtillatelsesfeil gjorde at renseprosessen mislyktes; Du må slette følgende mappe manuelt:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2180"/>
        <source>No help is available.</source>
        <translation>Ingen hjelp er tilgjengelig.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2730"/>
        <source>%1&apos;s Journal</source>
        <translation>%1&apos;s Journal</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2732"/>
        <source>Choose where to save journal</source>
        <translation>Velg hvor du vil lagre journal</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2732"/>
        <source>XML Files (*.xml)</source>
        <translation>XML-filer (*.xml)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2752"/>
        <source>Export review is not yet implemented</source>
        <translation>Eksportgjennomgang er ikke implementert ennå</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2762"/>
        <source>Would you like to zip this card?</source>
        <translation>Ønsker du å zippe dette kortet?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2784"/>
        <location filename="../oscar/mainwindow.cpp" line="2836"/>
        <source>Choose where to save zip</source>
        <translation>Velg hvor du ønsker å lagre zip</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2784"/>
        <location filename="../oscar/mainwindow.cpp" line="2836"/>
        <source>ZIP files (*.zip)</source>
        <translation>ZIP-filer (*.zip)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2813"/>
        <location filename="../oscar/mainwindow.cpp" line="2864"/>
        <source>Creating zip...</source>
        <translation>Opprettet zip...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2855"/>
        <source>Calculating size...</source>
        <translation>Beregner størrelse...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2900"/>
        <source>Reporting issues is not yet implemented</source>
        <translation>Rapportering av problemer er ikke implementert ennå</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="261"/>
        <source>Help Browser</source>
        <translation>Hjelp nettleser</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="558"/>
        <source>%1 (Profile: %2)</source>
        <translation>%1 (Profil: %2)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1034"/>
        <source>Please remember to select the root folder or drive letter of your data card, and not a folder inside it.</source>
        <translation>Husk å velge rotmappe eller stasjonsbokstav på datakortet, og ikke en mappe inni den.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1390"/>
        <source>Please open a profile first.</source>
        <translation>Åpne en profil først.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1502"/>
        <source>Choose where to save screenshot</source>
        <translation>Velg hvor du vil lagre skjermbildet</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1502"/>
        <source>Image files (*.png)</source>
        <translation>Bildefiler (*.png)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1997"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>Forutsatt at du har laget &lt;i&gt; dine &lt;b&gt; egne &lt;/b&gt; sikkerhetskopier for ALLE CPAP-dataene dine &lt;/i&gt;, kan du fortsatt fullføre denne operasjonen, men du må gjenopprette fra sikkerhetskopiene manuelt.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1998"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Er du virkelig sikker på at du vil gjøre dette?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2013"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>Fordi det ikke er noen interne sikkerhetskopier å gjenoppbygge fra, må du gjenopprette fra din egen.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2014"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this machine until you do)</source>
        <translation>Vil du importere fra dine egne sikkerhetskopier nå? (du vil ikke ha noen data synlige for denne maskinen før du gjør det)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2063"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>Merk som en forholdsregel at sikkerhetskopimappen blir liggende igjen.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2065"/>
        <source>OSCAR does not have any backups for this machine!</source>
        <translation>OSCAR har ingen sikkerhetskopier for denne maskinen!</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2066"/>
        <source>Unless you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your data for this machine&lt;/i&gt;, &lt;font size=+2&gt;you will lose this machine&apos;s data &lt;b&gt;permanently&lt;/b&gt;!&lt;/font&gt;</source>
        <translation>Om du ikke har tatt &lt;i&gt;din &lt;b&gt;egen&lt;b&gt; sikkerhetskopi for ALLE dine data for denne maskinen&lt;/i&gt;, &lt;font size=+2&gt;så vil du miste denne maskinens data &lt;b&gt;permanent&lt;/b&gt;!&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2075"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>Er du &lt;b&gt; helt sikker &lt;/b&gt; på at du vil fortsette?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2376"/>
        <source>Imported %1 ZEO session(s) from

%2</source>
        <translation>Importert %1 ZEO økt(er) fra %2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2382"/>
        <source>Already up to date with ZEO data at

%1</source>
        <translation>Allerede oppdatert med ZEO-data på %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2384"/>
        <source>Couldn&apos;t find any valid ZEO CSV data at

%1</source>
        <translation>Kunne ikke finne nye gykdug ZEO CSV-data på %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2407"/>
        <source>Imported %1 Dreem session(s) from

%2</source>
        <translation>Importert %1 Dream-økter fra %2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2413"/>
        <source>Already up to date with Dreem data at

%1</source>
        <translation>Allerede oppdatert med Dreeam data på

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2415"/>
        <source>Couldn&apos;t find any valid Dreem CSV data at

%1</source>
        <translation>Kunne ikke finne noe gyldig Dreem CSV-data på 

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2452"/>
        <source>The Glossary will open in your default browser</source>
        <translation>Ordlisten åpnes i standard nettleser</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2539"/>
        <source>Imported %1 oximetry session(s) from

%2</source>
        <translation>Importert %1 oksimetriøkter fra %2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2544"/>
        <source>Already up to date with oximetry data at

%1</source>
        <translation>Allerede oppdatert med oksimetridata på %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2546"/>
        <source>Couldn&apos;t find any valid data at

%1</source>
        <translation>Kunne ikke finne noe gyldig data på %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2645"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>Er du sikker på at du vil slette oksimetri-data for%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2647"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Vær oppmerksom på at du ikke kan angre denne operasjonen!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2676"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>Velg dagen med gyldige oksimetridata i daglig visning først.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="519"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>Laster profil &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="714"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>Importert %1 CPAP sessjon(er) fra %2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="714"/>
        <location filename="../oscar/mainwindow.cpp" line="2376"/>
        <location filename="../oscar/mainwindow.cpp" line="2407"/>
        <location filename="../oscar/mainwindow.cpp" line="2539"/>
        <source>Import Success</source>
        <translation>Import vellykket</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="716"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation>Allerede oppdatert med CPAP data på %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="716"/>
        <location filename="../oscar/mainwindow.cpp" line="2382"/>
        <location filename="../oscar/mainwindow.cpp" line="2413"/>
        <location filename="../oscar/mainwindow.cpp" line="2544"/>
        <source>Up to date</source>
        <translation>Oppdatert</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="718"/>
        <source>Couldn&apos;t find any valid Machine Data at

%1</source>
        <translation>Kunne ikke finne noe gyldig maskindata på %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="882"/>
        <source>Choose a folder</source>
        <translation>Velg en mappe</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="944"/>
        <source>No profile has been selected for Import.</source>
        <translation>Ingen profil har blitt valgt for import.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="952"/>
        <source>Import is already running in the background.</source>
        <translation>Import kjører allerede i bakgrunnen.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="993"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>En %1 filstruktur for en %2 var lokalisert på:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="995"/>
        <source>A %1 file structure was located at:</source>
        <translation>En %1 filestruktur var lokalisert på:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="960"/>
        <source>Would you like to import from this location?</source>
        <translation>Vil du importere fra denne lokasjonen?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1004"/>
        <source>Specify</source>
        <translation>Spesifiser</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1395"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>Tilgang til innstillinger har blitt blokkert inntil rekalkulering er ferdig.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1512"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>Det var en feil med lagring av skjermbilde til fil &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1514"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>Skjermbilde lagret til fil &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1988"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation>Vennligst merk, dette kan resulterte i tap av data hvis OSCARs sikkerhetskopi har blitt deaktivert.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2437"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>Det var et problem med å åpne MSeries blokkfil: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2441"/>
        <source>MSeries Import complete</source>
        <translation>MSeries Import ferdig</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2500"/>
        <source>There was a problem opening Somnopose Data File: </source>
        <translation>Det var et problem med åpning av Somnopoise datafil: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2504"/>
        <source>Somnopause Data Import complete</source>
        <translation>Somnopause Data Import ferdig</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2909"/>
        <source>OSCAR Information</source>
        <translation>OSCAR-informasjon</translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2019"/>
        <source>Auto-Fit</source>
        <translation>Autotilpass</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2020"/>
        <source>Defaults</source>
        <translation>Standarder</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2021"/>
        <source>Override</source>
        <translation>Overstyring</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2022"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>Skalermodus Y-akse, &apos;Autotilpass&apos; for automatisk skalering, &apos;Standard&apos; for innstillinger i henhold til produsent, og &apos;Overstyring&apos; for å velge din egen.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2028"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>Minimum Y-akse-verdi .. Merk at dette kan være et negativt tall hvis du ønsker det.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2029"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>Maksimal Y-akse verdi .. Må være større enn Minimum for å fungere.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2064"/>
        <source>Scaling Mode</source>
        <translation>Skalermodus</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2086"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>Denne knappen tilbakestiller Min og Maks til å matche Autotilpass</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>Endre brukerprofil</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="70"/>
        <source>I agree to all the conditions above.</source>
        <translation>Jeg aksepterer alle vilkår ovenfor.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="111"/>
        <source>User Information</source>
        <translation>Brukerinformasjon</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="137"/>
        <source>User Name</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="152"/>
        <source>Keep the kids out.. Nothing more.. This isn&apos;t meant to be uber security.</source>
        <translation>Hold ungene ute..Ingenting mer..Dette er ikke ment å være høysikkerhet.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Password Protect Profile</source>
        <translation>Passordbeskyttet profil</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="182"/>
        <source>Password</source>
        <translation>Passord</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="196"/>
        <source>...twice...</source>
        <translation>...to ganger...</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="213"/>
        <source>Locale Settings</source>
        <translation>Lokaliseringsinnstillinger</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="285"/>
        <source>Country</source>
        <translation>Land</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="253"/>
        <source>TimeZone</source>
        <translation>Tidssone</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="240"/>
        <source>DST Zone</source>
        <translation>DST-sone</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="320"/>
        <source>Personal Information (for reports)</source>
        <translation>Personlig informasjon (for rapporter)</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="344"/>
        <source>First Name</source>
        <translation>Fornavn</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="354"/>
        <source>Last Name</source>
        <translation>Etternavn</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="364"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>Det er helt i orden å hopper over dette, men en ca alder trengs for å øke nøyaktigheten for enkelte kalkulasjoner.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>D.O.B.</source>
        <translation>F.d.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="383"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biologisk (fødsel) kjønn trengs noen ganger for å øke nøyaktigheten for enkelte kalkuleringen, ha gjerne disse blanke eller hopp over noen av de.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>Gender</source>
        <translation>Kjønn</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="399"/>
        <source>Male</source>
        <translation>Mann</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="404"/>
        <source>Female</source>
        <translation>Kvinne</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="420"/>
        <source>Height</source>
        <translation>Høyde</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="459"/>
        <source>Metric</source>
        <translation>Metrisk</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="464"/>
        <source>English</source>
        <translation>Engelsk</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="477"/>
        <source>Contact Information</source>
        <translation>Kontaktinformasjon</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="504"/>
        <location filename="../oscar/newprofile.ui" line="779"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="521"/>
        <location filename="../oscar/newprofile.ui" line="810"/>
        <source>Email</source>
        <translation>E-post</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="531"/>
        <location filename="../oscar/newprofile.ui" line="800"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="576"/>
        <source>CPAP Treatment Information</source>
        <translation>CPAP behandlingsinformasjon</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="603"/>
        <source>Date Diagnosed</source>
        <translation>Dato diagnoisert</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="617"/>
        <source>Untreated AHI</source>
        <translation>Ubehandlet AHI</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="631"/>
        <source>CPAP Mode</source>
        <translation>CPAP-modus</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="639"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="644"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="649"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="654"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="662"/>
        <source>RX Pressure</source>
        <translation>RX trykk</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="708"/>
        <source>Doctors / Clinic Information</source>
        <translation>Doktor/klinikk-informasjon</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="735"/>
        <source>Doctors Name</source>
        <translation>Doktornavn</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="752"/>
        <source>Practice Name</source>
        <translation>Legekontor</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="762"/>
        <source>Patient ID</source>
        <translation>PasientID</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="877"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="955"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Avbryt</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="971"/>
        <source>&amp;Back</source>
        <translation>&amp;Tilbake</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="987"/>
        <location filename="../oscar/newprofile.cpp" line="276"/>
        <location filename="../oscar/newprofile.cpp" line="285"/>
        <source>&amp;Next</source>
        <translation>&amp;Neste</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="63"/>
        <source>Select Country</source>
        <translation>Velg land</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Velkommen til åpen kildekode CPAP analyse og rapportering</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="113"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP machines and related equipment.</source>
        <translation>Denne programvare er utviklet for å hjelpe deg med å revidere data produsert av din CPAP-maskin og relatert utstyr.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="118"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>VENNLIGST LES NØYE</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>Nøyaktigheten av data som vises er ikke, og kan ikke garanteres.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="124"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>Rapporter som genereres er KUN FOR PERSONLIG BRUK og IKKE I NOEN SOM HELST GRAD egnet for bruk til medisinsk diagnose.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="131"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>All bruk av denne programvare er helt og holdent for egen risiko.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation>OSCAR har blitt tilgjengeliggjort fritt under &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt; og kommer uten noen garanti og uten NOEN påstander om å være egnet til noe formål.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="119"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation>OSCAR er kun ment som en datavisning, og definitivt ikke en erstatning for kompetent medisinsk veiledning fra legen din.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="127"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation>Forfatterne vil ikke bli holdt ansvarlig for &lt;u&gt; noe &lt;/u&gt; relatert til bruk eller misbruk av denne programvaren.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="134"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019 Nightowl Software</source>
        <translation>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019 Nightowl Software</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="157"/>
        <source>Please provide a username for this profile</source>
        <translation>Vennligst oppgi et brukernavn for denne profilen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="167"/>
        <source>Passwords don&apos;t match</source>
        <translation>Passord er ikke like</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Profile Changes</source>
        <translation>Profilendringer</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Accept and save this information?</source>
        <translation>Aksepterte og lagre denne informasjonen?</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="274"/>
        <source>&amp;Finish</source>
        <translation>&amp;Ferdig</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="450"/>
        <source>&amp;Close this window</source>
        <translation>&amp;Lukk dette vinduet</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="14"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>Fra til:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>Siste uke</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>Siste to uker</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>Siste måned</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>Siste to måneder</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>Siste tre måneder</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>Siste 6 måneder</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>Siste år</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>Tilpasset</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="130"/>
        <source>Start:</source>
        <translation>Sart:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="153"/>
        <source>End:</source>
        <translation>Slutt:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="176"/>
        <source>Reset view to selected date range</source>
        <translation>Resett visning for å velge fra til dato</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="195"/>
        <location filename="../oscar/overview.ui" line="244"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="225"/>
        <source>Toggle Graph Visibility</source>
        <translation>Graf synlighet</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="260"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>Velg rullemeny for å se liste over grafer å slå av/på.</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="267"/>
        <source>Graphs</source>
        <translation>Grafer</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="185"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation>Respiratory Disturbance Index</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="187"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation>Apnea Hypoapnea Index</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="193"/>
        <source>Usage</source>
        <translation>Bruk</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="193"/>
        <source>Usage
(hours)</source>
        <translation>Bruk (timer)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="196"/>
        <source>Session Times</source>
        <translation>Økt-tider</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="204"/>
        <source>Total Time in Apnea</source>
        <translation>Total tid i apné</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="204"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>Total tid i apné (Minutter)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="239"/>
        <source>Body
Mass
Index</source>
        <translation>Body Mass Index</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="240"/>
        <source>How you felt
(0-10)</source>
        <translation>Hvordan du følte deg (0-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="579"/>
        <source>Show all graphs</source>
        <translation>Vis alle grafer</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="592"/>
        <source>Hide all graphs</source>
        <translation>Skjul alle grafer</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="37"/>
        <source>Oximeter Import Wizard</source>
        <translation>Oksimeter importeringsveiviser</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="768"/>
        <source>Skip this page next time.</source>
        <translation>Hopp over denne siden neste gang.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="866"/>
        <source>Where would you like to import from?</source>
        <translation>Hvor vil du importere fra?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="921"/>
        <source>CMS50Fv3.7+/H/I, CMS50D+v4.6, Pulox PO-400/500</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="967"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1004"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1036"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1077"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  because if something goes wrong before OSCAR saves your session, you can&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1106"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, old Contec oximeters will require you to use the device&apos;s menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1154"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1193"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1272"/>
        <source>Please connect your oximeter device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1290"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1319"/>
        <source>Please connect your oximeter device, turn it on, and enter the menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1345"/>
        <source>Press Start to commence recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1391"/>
        <source>Show Live Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1422"/>
        <location filename="../oscar/oximeterimport.ui" line="1676"/>
        <source>Duration</source>
        <translation>Varighet</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1458"/>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1540"/>
        <source>Pulse Rate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1632"/>
        <source>Multiple Sessions Detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1671"/>
        <source>Start Time</source>
        <translation>Start tid</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1681"/>
        <source>Details</source>
        <translation>Detaljer</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1698"/>
        <source>Import Completed. When did the recording start?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1722"/>
        <source>Day recording (normally would of) started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1768"/>
        <source>Oximeter Starting time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1780"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1796"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP machine.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1857"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1878"/>
        <source>Choose CPAP session to sync to:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1917"/>
        <location filename="../oscar/oximeterimport.ui" line="1956"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1998"/>
        <source>You can manually adjust the time here if required:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2019"/>
        <source>HH:mm:ssap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2116"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Avbryt</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2097"/>
        <source>&amp;Information Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="823"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Make sure your correct oximeter type is selected otherwise import will fail.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="910"/>
        <source>Select Oximeter Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="926"/>
        <source>CMS50D+/E/F, Pulox PO-200/300</source>
        <translation>CMS50D+/E/F, Pulox PO-200/300</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="931"/>
        <source>ChoiceMMed MD300W1</source>
        <translation>ChoiceMMed MD300W1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1007"/>
        <source>Set device date/time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1014"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1017"/>
        <source>Set device identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1080"/>
        <source>Erase session after successful upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1112"/>
        <source>Import directly from a recording on a device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1237"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1648"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1825"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2135"/>
        <source>&amp;Retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2154"/>
        <source>&amp;Choose Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2173"/>
        <source>&amp;End Recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2192"/>
        <source>&amp;Sync and Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2211"/>
        <source>&amp;Save and Finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2230"/>
        <source>&amp;Start</source>
        <translation>&amp;Start</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="188"/>
        <source>Scanning for compatible oximeters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="220"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="228"/>
        <source>Connecting to %1 Oximeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="256"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="259"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="302"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="327"/>
        <source>Nothing to import</source>
        <translation>Ingenting å importere</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="328"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="329"/>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="332"/>
        <source>Waiting for %1 to start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="333"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="335"/>
        <source>Select upload option on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="336"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="337"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="367"/>
        <source>%1 device is uploading data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="368"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="387"/>
        <source>Oximeter import completed..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="411"/>
        <source>Select a valid oximetry data file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="411"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="433"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="478"/>
        <source>Live Oximetry Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="530"/>
        <source>Live Oximetry Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="531"/>
        <source>Live Oximetry import has been stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1091"/>
        <source>Oximeter Session %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1136"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1147"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1150"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="460"/>
        <source>Oximeter not detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="467"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="481"/>
        <source>Starting up...</source>
        <translation>Starter opp...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="482"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="529"/>
        <source>Live Import Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="582"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="586"/>
        <source>No CPAP data available on %1</source>
        <translation>Ingen CPAP data tigjengelig på %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="594"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="721"/>
        <source>Recording...</source>
        <translation>Tar opp...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="728"/>
        <source>Finger not detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="828"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="831"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="843"/>
        <source>Something went wrong getting session data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1132"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1134"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1138"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1140"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1143"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1145"/>
        <source>Please remember:</source>
        <translation>Vennligst husk:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1149"/>
        <source>Important Notes:</source>
        <translation>Viktige notater:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1152"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1154"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="14"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation>d/MM/yy-h:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation>R&amp;esett</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="160"/>
        <source>SpO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="346"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation>&amp;Åpne.spo/R-fil</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation>Seriell &amp;Import</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation>&amp;Start Direkte</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>Seriell port</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation>&amp;Søk porter igjen</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="23"/>
        <source>Preferences</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="64"/>
        <source>&amp;Import</source>
        <translation>&amp;Import</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="154"/>
        <source>Combine Close Sessions </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="164"/>
        <location filename="../oscar/preferencesdialog.ui" line="249"/>
        <location filename="../oscar/preferencesdialog.ui" line="752"/>
        <source>Minutes</source>
        <translation>Minutter</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="184"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="239"/>
        <source>Ignore Short Sessions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="266"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="310"/>
        <source>Day Split Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="320"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="393"/>
        <source>Session Storage Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="440"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="658"/>
        <source>&amp;CPAP</source>
        <translation>&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1301"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1304"/>
        <source> hours</source>
        <translation> timer</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="966"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the machine missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1057"/>
        <source>Flow Restriction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1098"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1008"/>
        <location filename="../oscar/preferencesdialog.ui" line="1102"/>
        <location filename="../oscar/preferencesdialog.ui" line="1579"/>
        <location filename="../oscar/preferencesdialog.ui" line="1739"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1075"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the machine. They are &lt;span style=&quot; text-decoration: underline;&quot;&gt;not&lt;/span&gt; included in AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1119"/>
        <source>Duration of airflow restriction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="998"/>
        <location filename="../oscar/preferencesdialog.ui" line="1122"/>
        <location filename="../oscar/preferencesdialog.ui" line="1596"/>
        <location filename="../oscar/preferencesdialog.ui" line="1684"/>
        <location filename="../oscar/preferencesdialog.ui" line="1713"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1158"/>
        <source>Event Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1145"/>
        <source>Allow duplicates near machine events.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1223"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1227"/>
        <source> minutes</source>
        <translation> minutter</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1266"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1269"/>
        <source>Zero Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="705"/>
        <source>CPAP Clock Drift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="501"/>
        <source>Do not import sessions older than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="508"/>
        <source>Sessions older than this date will not be imported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="534"/>
        <source>dd MMMM yyyy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1282"/>
        <source>User definable threshold considered large leak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1285"/>
        <source> L/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1249"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1826"/>
        <location filename="../oscar/preferencesdialog.ui" line="1905"/>
        <source>Search</source>
        <translation>Søk</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1522"/>
        <source>&amp;Oximetry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1112"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1135"/>
        <source>#1</source>
        <translation>#1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1041"/>
        <source>#2</source>
        <translation>#2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1021"/>
        <source>Resync Machine Detected Events (Experimental)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1674"/>
        <source>SPO2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1736"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1729"/>
        <source>Pulse</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1694"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1586"/>
        <location filename="../oscar/preferencesdialog.ui" line="1616"/>
        <location filename="../oscar/preferencesdialog.ui" line="1697"/>
        <source> bpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1681"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1710"/>
        <source>Minimum duration of pulse change event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1593"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1961"/>
        <source>&amp;General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1349"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1352"/>
        <source>Preferred Calculation Methods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1381"/>
        <source>Middle Calculations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1395"/>
        <source>Upper Percentile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="91"/>
        <source>Session Splitting Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="356"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="359"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="568"/>
        <source>Memory and Startup Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="610"/>
        <source>Pre-Load all summary data at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="597"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="600"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="624"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="627"/>
        <source>Import without asking for confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1187"/>
        <source>General CPAP and Related Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1196"/>
        <source>Enable Unknown Events Channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1327"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1332"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translation type="unfinished">RDI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1203"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1259"/>
        <source>Preferred major event index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1210"/>
        <source>Compliance defined as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1252"/>
        <source>Flag leaks over threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="786"/>
        <source>Seconds</source>
        <translation>Sekunder</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="732"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="779"/>
        <source>Hours</source>
        <translation>Timer</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1358"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1409"/>
        <source>Median is recommended for ResMed users.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1413"/>
        <location filename="../oscar/preferencesdialog.ui" line="1476"/>
        <source>Median</source>
        <translation>Median</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1418"/>
        <source>Weighted Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1423"/>
        <source>Normal Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1447"/>
        <source>True Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1452"/>
        <source>99% Percentile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1388"/>
        <source>Maximum Calcs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1975"/>
        <source>General Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2731"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2734"/>
        <source>Skip over Empty Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1996"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2000"/>
        <source>Enable Multithreading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="574"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="483"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1443"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1461"/>
        <source>Combined Count divided by Total Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1466"/>
        <source>Time Weighted average of Indice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1471"/>
        <source>Standard average of indice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1402"/>
        <source>Culminative Indices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="971"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1803"/>
        <source>Events</source>
        <translation>Hendelser</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1856"/>
        <location filename="../oscar/preferencesdialog.ui" line="1935"/>
        <source>Reset &amp;Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1869"/>
        <location filename="../oscar/preferencesdialog.ui" line="1948"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1882"/>
        <source>Waveforms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1662"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1573"/>
        <source>Other oximetry options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1623"/>
        <source>Flag SPO2 Desaturations Below</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1606"/>
        <source>Discard segments under</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1643"/>
        <source>Flag Pulse Rate Above</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1633"/>
        <source>Flag Pulse Rate Below</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="422"/>
        <source>Changing SD Backup compression options doesn&apos;t automatically recompress backup data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="432"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="452"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="462"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="467"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="474"/>
        <source>This maintains a backup of SD-card data for ResMed machines, 

ResMed S9 series machines delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="634"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data from any machine model that has not yet been tested by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="637"/>
        <source>Warn when importing data from an untested machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="644"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data that is somehow different from anything previously seen by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="647"/>
        <source>Warn when previously unseen data is encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="802"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP machine. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="809"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="905"/>
        <source>4 cmH2O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="915"/>
        <source>20 cmH2O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="947"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1018"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve machine detected event positioning.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1193"/>
        <source>Show flags for machine detected events that haven&apos;t been identified yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1783"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:7.84158pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method does &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP machine, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2007"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2014"/>
        <source>Always save screenshots in the OSCAR Data folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2068"/>
        <source>Automatically Check For Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2085"/>
        <source>Check for new version every</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2092"/>
        <source>Sourceforge hosts this project for free.. Please be considerate of their resources..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2108"/>
        <source>days.</source>
        <translation>dager.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2173"/>
        <source>&amp;Check for Updates now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2196"/>
        <source>Last Checked For Updates: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2209"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2231"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If your interested in helping test new features and bugfixes early, click here.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;But please be warned this will sometimes mean breaky code..&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2240"/>
        <source>I want to try experimental and test builds (Advanced users only please.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2264"/>
        <source>&amp;Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2293"/>
        <source>Graph Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2309"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2567"/>
        <source>Bar Tops</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2572"/>
        <source>Line Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2662"/>
        <source>Overview Linecharts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2751"/>
        <source>Whether to include machine serial number on machine settings changes report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2754"/>
        <source>Include Serial Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2046"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2501"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2489"/>
        <source>Scroll Dampening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2479"/>
        <source>Tooltip Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2586"/>
        <source>Default display height of graphs in pixels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2559"/>
        <source>Graph Tooltips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2435"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2440"/>
        <source>Standard Bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2445"/>
        <source>Top Markers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2393"/>
        <source>Graph Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="577"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="617"/>
        <source>Automatically load last used profile on start-up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="818"/>
        <source>Your masks vent rate at 20 cmH2O pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="874"/>
        <source>Your masks vent rate at 4 cmH2O pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1490"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed machines do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1543"/>
        <source>Oximetry Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2299"/>
        <source>On Opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2312"/>
        <location filename="../oscar/preferencesdialog.ui" line="2316"/>
        <source>Profile</source>
        <translation type="unfinished">Profil</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2321"/>
        <location filename="../oscar/preferencesdialog.ui" line="2360"/>
        <source>Welcome</source>
        <translation type="unfinished">Velkommen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2326"/>
        <location filename="../oscar/preferencesdialog.ui" line="2365"/>
        <source>Daily</source>
        <translation type="unfinished">Daglig</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2336"/>
        <location filename="../oscar/preferencesdialog.ui" line="2375"/>
        <source>Statistics</source>
        <translation>Statistikk</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2344"/>
        <source>Switch Tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2355"/>
        <source>No change</source>
        <translation>Ingen endring</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2383"/>
        <source>After Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2409"/>
        <source>Overlay Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2419"/>
        <source>Line Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2455"/>
        <source>The pixel thickness of line plots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2681"/>
        <source>Other Visual Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2687"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2694"/>
        <source>Use Anti-Aliasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2701"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2704"/>
        <source>Square Wave Plots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2711"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2714"/>
        <source>Use Pixmap Caching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2721"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2724"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2741"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2744"/>
        <source>Allow YAxis Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2040"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2786"/>
        <source>Fonts (Application wide settings)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2821"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2840"/>
        <source>Size</source>
        <translation type="unfinished">Størrelse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2859"/>
        <source>Bold  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2881"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2894"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2958"/>
        <source>Graph Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3019"/>
        <source>Graph Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3080"/>
        <source>Big  Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3147"/>
        <location filename="../oscar/preferencesdialog.cpp" line="447"/>
        <location filename="../oscar/preferencesdialog.cpp" line="579"/>
        <source>Details</source>
        <translation type="unfinished">Detaljer</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3179"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Avbryt</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3186"/>
        <source>&amp;Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="442"/>
        <location filename="../oscar/preferencesdialog.cpp" line="573"/>
        <source>Name</source>
        <translation type="unfinished">Navn</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="443"/>
        <location filename="../oscar/preferencesdialog.cpp" line="574"/>
        <source>Color</source>
        <translation>Farge</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="445"/>
        <source>Flag Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="446"/>
        <location filename="../oscar/preferencesdialog.cpp" line="578"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="463"/>
        <source>CPAP Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="464"/>
        <source>Oximeter Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="465"/>
        <source>Positional Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="466"/>
        <source>Sleep Stage Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="467"/>
        <source>Unknown Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="639"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="517"/>
        <location filename="../oscar/preferencesdialog.cpp" line="646"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="73"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; machines due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed machines, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="302"/>
        <location filename="../oscar/preferencesdialog.cpp" line="303"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1269"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1274"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2331"/>
        <location filename="../oscar/preferencesdialog.ui" line="2370"/>
        <location filename="../oscar/preferencesdialog.cpp" line="444"/>
        <location filename="../oscar/preferencesdialog.cpp" line="575"/>
        <source>Overview</source>
        <translation type="unfinished">Oversikt</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="509"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="522"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="532"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="537"/>
        <location filename="../oscar/preferencesdialog.cpp" line="670"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="543"/>
        <location filename="../oscar/preferencesdialog.cpp" line="676"/>
        <source>This is a description of what this channel does.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="576"/>
        <source>Lower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="577"/>
        <source>Upper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="596"/>
        <source>CPAP Waveforms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="597"/>
        <source>Oximeter Waveforms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="598"/>
        <source>Positional Waveforms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="599"/>
        <source>Sleep Stage Waveforms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="655"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="660"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="665"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="775"/>
        <source>Data Processing Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="776"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="784"/>
        <source>Data Reindex Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="785"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="791"/>
        <source>Restart Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="792"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1151"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1152"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1153"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1197"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1198"/>
        <source>Are you really sure you want to do this?</source>
        <translation type="unfinished">Er du virkelig sikker på at du vil gjøre dette?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="49"/>
        <source>Flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Minor Flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Span</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="52"/>
        <source>Always Minor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="65"/>
        <source>No CPAP machines detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="66"/>
        <source>Will you be using a ResMed brand machine?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="275"/>
        <source>Never</source>
        <translation>Aldri</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1149"/>
        <source>This may not be a good idea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1150"/>
        <source>ResMed S9 machines routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfileSelect</name>
    <message>
        <location filename="../oscar/profileselect.ui" line="14"/>
        <source>Select Profile</source>
        <translation>Velg profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="115"/>
        <source>Search:</source>
        <translation>Søk:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="191"/>
        <source>Start with the selected user profile.</source>
        <translation>Start med den valgte brukerprofilen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="194"/>
        <source>&amp;Select User</source>
        <translation>&amp;Velg bruker</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="215"/>
        <source>Create a new user profile.</source>
        <translation>Opprett en ny brukerprofil.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="218"/>
        <source>New Profile</source>
        <translation>Ny profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="235"/>
        <source>Choose a different OSCAR data folder.</source>
        <translation>Velg en annen OSCAR-datamappe.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="274"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="310"/>
        <source>Click here if you didn&apos;t want to start OSCAR.</source>
        <translation>Klikk her hvis du ikke vil starte OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="383"/>
        <source>The current location of OSCAR data store.</source>
        <translation>Den nåværende plasseringen til OSCAR-datalager.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="241"/>
        <source>&amp;Different Folder</source>
        <translation>&amp;Annen mappe</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="287"/>
        <source>[version]</source>
        <translation>[version]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="313"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quit</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="370"/>
        <source>Folder:</source>
        <translation>Mappe:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="389"/>
        <source>[data directory]</source>
        <translation>[datakatalog]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="92"/>
        <source>Open Profile</source>
        <translation>Åpen profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="93"/>
        <source>Edit Profile</source>
        <translation>Rediger profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="95"/>
        <source>Delete Profile</source>
        <translation>Slett profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="139"/>
        <location filename="../oscar/profileselect.cpp" line="220"/>
        <source>Enter Password for %1</source>
        <translation>Skriv inn passord for%1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="158"/>
        <location filename="../oscar/profileselect.cpp" line="342"/>
        <source>Incorrect Password</source>
        <translation>Feil passord</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="160"/>
        <source>You entered the password wrong too many times.</source>
        <translation>Du skrev inn passordet galt for mange ganger.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>Enter the word DELETE below to confirm.</source>
        <translation>Skriv inn order SLETT under for å bekrefte.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>You are about to destroy profile &apos;%1&apos;.</source>
        <translation>Du er i ferd med å ødelegge profilen &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>Sorry</source>
        <translation>Beklager</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>Du må skrive SLETT med store bokstaver.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="239"/>
        <source>You entered an incorrect password</source>
        <translation>Du skrev inn feil passord</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="242"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to delete it manually.</source>
        <translation>Hvis du prøver å slette fordi du har glemt passordet, må du slette det manuelt.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="255"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Det oppsto en feil ved sletting av profilkatalogen. Du må fjerne den manuelt.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="259"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Profilen &apos;%1&apos; ble slettet</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="289"/>
        <source>Create new profile</source>
        <translation>Lag ny profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="326"/>
        <source>Enter Password</source>
        <translation>Oppgi passord</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="345"/>
        <source>You entered an Incorrect Password too many times. Exiting!</source>
        <translation>Du skrev inn et feil passord for mange ganger. Spennende!</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.ui" line="14"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>Filter:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>Reset filter to see all profiles</source>
        <translation>Resett filter for å se alle profiler</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="55"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="184"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="199"/>
        <source>Version</source>
        <translation>Versjon</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="216"/>
        <source>&amp;Open Profile</source>
        <translation>&amp;Åpne profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="227"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Endre profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="241"/>
        <source>&amp;New Profile</source>
        <translation>&amp;Ny profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="260"/>
        <source>Profile: None</source>
        <translation>Profil: Ingen</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="281"/>
        <source>Please select or create a profile...</source>
        <translation>Vennligst velg eller opprett en profil...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="332"/>
        <source>Destroy Profile</source>
        <translation>Ødelegg Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Ventilator Brand</source>
        <translation>Ventilatormerke</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Ventilator Model</source>
        <translation>Ventilatormodell</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Other Data</source>
        <translation>Andre data</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="94"/>
        <source>Last Imported</source>
        <translation>Sist importert</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="95"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="135"/>
        <location filename="../oscar/profileselector.cpp" line="312"/>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="170"/>
        <source>You must create a profile</source>
        <translation>Du må opprette en profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="233"/>
        <location filename="../oscar/profileselector.cpp" line="363"/>
        <source>Enter Password for %1</source>
        <translation>Skriv inn passord for %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="249"/>
        <location filename="../oscar/profileselector.cpp" line="382"/>
        <source>You entered an incorrect password</source>
        <translation>Du skrev inn feil passord</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Forgot your password?</source>
        <translation>Glemt ditt passord?</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>Spør på forumet om hvordan det kan endres, det er faktisk ganske lett.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="322"/>
        <source>Select a profile first</source>
        <translation>Velg profil først</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="385"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>Hvis du forsøker å slette fordi du har glemt ditt passord, så må du enten resette det eller slette profilmappen manuelt.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="395"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>Du er i ferd med å slette profilen &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="395"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>Tenk deg godt om, dette vil slette profilen sammen med all &lt;b&gt;backup data&lt;/b lagret under&lt;br/&gt;%2.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="395"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>Skriv inn order &lt;b&gt;SLETT&lt;/B&gt; under (akkurat som vist) for å bekrefte.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="413"/>
        <source>DELETE</source>
        <translation>SLETT</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="414"/>
        <source>Sorry</source>
        <translation>Beklager</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="414"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>Du må skrive SLETT med store bokstaver.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="427"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Det skjedde en feil under sletting av profilmappen, du må fjerne den manuelt.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="431"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Profilen &apos;%1&apos; ble slettet</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="461"/>
        <source>Summaries:</source>
        <translation>Oppsummering:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="462"/>
        <source>Events:</source>
        <translation>Hendelser:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="463"/>
        <source>Backups:</source>
        <translation>Sikkerhetskopier:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="475"/>
        <location filename="../oscar/profileselector.cpp" line="515"/>
        <source>Hide disk usage information</source>
        <translation>Gjem informasjon og diskforbruk</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="478"/>
        <source>Show disk usage information</source>
        <translation>Vis informasjon om diskforbruk</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="496"/>
        <source>Name: %1, %2</source>
        <translation>Navn:%1,%2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="499"/>
        <source>Phone: %1</source>
        <translation>Telefon: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="502"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>Epost: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="505"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="508"/>
        <source>No profile information given</source>
        <translation>Ingen profilinformasjon oppgitt</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="511"/>
        <source>Profile: %1</source>
        <translation>Profil: %1</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="56"/>
        <source>Abort</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1246"/>
        <source>No Data</source>
        <translation>Ingen data</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="369"/>
        <source>Events</source>
        <translation>Hendelser</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="367"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="370"/>
        <source>Duration</source>
        <translation>Varighet</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="384"/>
        <source>(% %1 in events)</source>
        <translation>(% %1 i hendelser)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Jan</source>
        <translation>Jan</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Feb</source>
        <translation>Feb</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Mar</source>
        <translation>Mar</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Apr</source>
        <translation>Apr</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>May</source>
        <translation>Mai</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Jun</source>
        <translation>Jun</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jul</source>
        <translation>Jul</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Aug</source>
        <translation>Aug</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Sep</source>
        <translation>Sep</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Oct</source>
        <translation>Okt</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Nov</source>
        <translation>Nov</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Dec</source>
        <translation>Des</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="643"/>
        <source>&quot;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="644"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="645"/>
        <source>lb</source>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="646"/>
        <source>oz</source>
        <translation>oz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="647"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="648"/>
        <source>cmH2O</source>
        <translation>cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="210"/>
        <source>Med.</source>
        <translation>Med.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="228"/>
        <source>Min: %1</source>
        <translation>Min: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="259"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="269"/>
        <source>Min: </source>
        <translation>Min: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="264"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="274"/>
        <source>Max: </source>
        <translation>Maks: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="278"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="282"/>
        <source>%1: </source>
        <translation>%1: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="286"/>
        <source>???: </source>
        <translation>???: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="293"/>
        <source>Max: %1</source>
        <translation>Maks: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="299"/>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2 dager): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="301"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 dag): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="359"/>
        <source>% in %1</source>
        <translation>% av %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="365"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="692"/>
        <location filename="../oscar/SleepLib/common.cpp" line="649"/>
        <source>Hours</source>
        <translation>Timer</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="371"/>
        <source>Min %1</source>
        <translation>Min %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="677"/>
        <source>
Hours: %1</source>
        <translation>
Timer: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="743"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="824"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="943"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="945"/>
        <source>Mask On</source>
        <translation>Maske På</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="945"/>
        <source>Mask Off</source>
        <translation>Maske Av</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="956"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1127"/>
        <source>TTIA:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1140"/>
        <source>
TTIA: %1</source>
        <translation>
TTIA: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1241"/>
        <source>%1 %2 / %3 / %4</source>
        <translation>%1 %2 / %3 / %4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="650"/>
        <source>Minutes</source>
        <translation>Minutter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="651"/>
        <source>Seconds</source>
        <translation>Sekunder</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="652"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="653"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="654"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="655"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="656"/>
        <source>Events/hr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="658"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="659"/>
        <source>bpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="661"/>
        <source>Litres</source>
        <translation>Liter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="662"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="663"/>
        <source>Breaths/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="664"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="666"/>
        <source>Severity (0-1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="667"/>
        <source>Degrees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="670"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9036"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="671"/>
        <source>Warning</source>
        <translation>Advarsel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="672"/>
        <source>Information</source>
        <translation>Informasjon</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="673"/>
        <source>Busy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="674"/>
        <source>Please Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="677"/>
        <source>Compliance Only :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="678"/>
        <source>Graphs Switched Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="679"/>
        <source>Summary Only :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="680"/>
        <source>Sessions Switched Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="684"/>
        <source>&amp;Yes</source>
        <translation>&amp;Ja</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source>&amp;No</source>
        <translation>&amp;Nei</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Avbryt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="687"/>
        <source>&amp;Destroy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="688"/>
        <source>&amp;Save</source>
        <translation>&amp;Lagre</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>BMI</source>
        <translation>BMI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="306"/>
        <source>Weight</source>
        <translation>Vekt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="692"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>Zombie</source>
        <translation>Zombie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="693"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Pulse Rate</source>
        <translation type="unfinished">Puls</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="694"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>SpO2</source>
        <translation type="unfinished">SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="695"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>Plethy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="696"/>
        <source>Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="698"/>
        <source>Daily</source>
        <translation>Daglig</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="699"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="700"/>
        <source>Overview</source>
        <translation>Oversikt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="701"/>
        <source>Oximetry</source>
        <translation type="unfinished">Oximetry</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="703"/>
        <source>Oximeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="704"/>
        <source>Event Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="707"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="710"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8958"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="89"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="711"/>
        <source>BiPAP</source>
        <translation>BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="712"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8961"/>
        <source>Bi-Level</source>
        <translation type="unfinished">Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="713"/>
        <source>EPAP</source>
        <translation>EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="714"/>
        <source>Min EPAP</source>
        <translation>Min EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="715"/>
        <source>Max EPAP</source>
        <translation>Maks EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="716"/>
        <source>IPAP</source>
        <translation>IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="717"/>
        <source>Min IPAP</source>
        <translation>Min IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="718"/>
        <source>Max IPAP</source>
        <translation>Maks IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="719"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="90"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="720"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8963"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="96"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="721"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>AVAPS</source>
        <translation>AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="722"/>
        <source>ST/ASV</source>
        <translation>ST/ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="724"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9021"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9048"/>
        <source>Humidifier</source>
        <translation>Fukter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="726"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="727"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>OA</source>
        <translation>OA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="728"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="729"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>CA</source>
        <translation>CA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="730"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>FL</source>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="731"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SA</source>
        <translation>SA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="732"/>
        <source>LE</source>
        <translation>LE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="733"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>EP</source>
        <translation>EP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="734"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>VS</source>
        <translation>VS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="736"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>VS2</source>
        <translation>VS2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="737"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>RERA</source>
        <translation>RERA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="738"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8949"/>
        <source>PP</source>
        <translation>PP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="739"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="740"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>RE</source>
        <translation>RE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="741"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>NR</source>
        <translation>NR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="742"/>
        <source>NRI</source>
        <translation>NRI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="743"/>
        <source>O2</source>
        <translation>O2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="744"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8966"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>PC</source>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="745"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="746"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="747"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="749"/>
        <source>PS</source>
        <translation>PS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="750"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="751"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>RDI</source>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="752"/>
        <source>AI</source>
        <translation>AI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="753"/>
        <source>HI</source>
        <translation>HI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="754"/>
        <source>UAI</source>
        <translation>UAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="755"/>
        <source>CAI</source>
        <translation>CAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="756"/>
        <source>FLI</source>
        <translation>FLI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="758"/>
        <source>REI</source>
        <translation>REI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="759"/>
        <source>EPI</source>
        <translation>EPI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="761"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="765"/>
        <source>IE</source>
        <translation>IE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="766"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Insp. Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="767"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Exp. Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="768"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Resp. Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="769"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Flow Limitation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="770"/>
        <source>Flow Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="771"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SensAwake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="772"/>
        <source>Pat. Trig. Breath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="773"/>
        <source>Tgt. Min. Vent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="774"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Target Vent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="775"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Minute Vent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="776"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>Tidal Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="777"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Resp. Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="778"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Snore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="779"/>
        <source>Leak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="780"/>
        <source>Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="781"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>Large Leak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="782"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>LL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="783"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Total Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="784"/>
        <source>Unintentional Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="785"/>
        <source>MaskPressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="786"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="212"/>
        <source>Flow Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="787"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Sleep Stage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="788"/>
        <source>Usage</source>
        <translation type="unfinished">Bruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="789"/>
        <source>Sessions</source>
        <translation>Økter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="790"/>
        <source>Pr. Relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="793"/>
        <location filename="../oscar/SleepLib/journal.cpp" line="30"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="676"/>
        <source>No Data Available</source>
        <translation>Ingen data tilgjengelig</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="201"/>
        <source>App key:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="193"/>
        <source>Operating system:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="191"/>
        <source>Built with Qt %1 on %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="194"/>
        <source>Graphics Engine:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="195"/>
        <source>Graphics Engine type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="637"/>
        <source>Software Engine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="638"/>
        <source>ANGLE / OpenGLES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="639"/>
        <source>Desktop OpenGL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="641"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="642"/>
        <source> cm</source>
        <translation> cm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="792"/>
        <source>Bookmarks</source>
        <translation>Bokmerker</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="796"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8953"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8955"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="85"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="797"/>
        <source>Model</source>
        <translation>Modell</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="798"/>
        <source>Brand</source>
        <translation>Merke</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="799"/>
        <source>Serial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="800"/>
        <source>Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="801"/>
        <source>Machine</source>
        <translation>Maskin</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="802"/>
        <source>Channel</source>
        <translation>Kanal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="803"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="805"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>Inclination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="806"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>Orientation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="807"/>
        <source>Motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="809"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="810"/>
        <source>DOB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="811"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="812"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="813"/>
        <source>Email</source>
        <translation>Epost</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="814"/>
        <source>Patient ID</source>
        <translation type="unfinished">PasientID</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="815"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="817"/>
        <source>Bedtime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="818"/>
        <source>Wake-up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="819"/>
        <source>Mask Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="820"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="124"/>
        <source>Unknown</source>
        <translation>Ukjent</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="821"/>
        <source>None</source>
        <translation>Ingen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="822"/>
        <source>Ready</source>
        <translation>Klar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="824"/>
        <source>First</source>
        <translation>Første</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="825"/>
        <source>Last</source>
        <translation>Siste</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="826"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="827"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="313"/>
        <source>End</source>
        <translation>Slutt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="828"/>
        <source>On</source>
        <translation>På</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="829"/>
        <source>Off</source>
        <translation>Av</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="830"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="831"/>
        <source>No</source>
        <translation>Nei</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="833"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="834"/>
        <source>Max</source>
        <translation>Maks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="835"/>
        <source>Med</source>
        <translation>Med</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="837"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="838"/>
        <source>Median</source>
        <translation>Median</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="214"/>
        <location filename="../oscar/SleepLib/common.cpp" line="839"/>
        <source>Avg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="212"/>
        <location filename="../oscar/SleepLib/common.cpp" line="840"/>
        <source>W-Avg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="794"/>
        <source>Non Data Capable Machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="795"/>
        <source>Your Philips Respironics CPAP machine (Model %1) is unfortunately not a data capable model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="684"/>
        <source>Getting Ready...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="807"/>
        <source>Machine Unsupported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="808"/>
        <source>Sorry, your Philips Respironics CPAP machine (Model %1) is not supported yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="796"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this machine.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="708"/>
        <source>Scanning Files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="716"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="592"/>
        <source>Importing Sessions...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="721"/>
        <source>Finishing up...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="809"/>
        <source>The developers needs a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make it work with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="831"/>
        <source>Machine Untested</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="832"/>
        <source>Your Philips Respironics CPAP machine (Model %1) has not been tested yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="833"/>
        <source>It seems similar enough to other machines that it might work, but the developers would like a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make sure it works with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8994"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8996"/>
        <source>Flex Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8995"/>
        <source>Whether Flex settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9004"/>
        <source>Amount of time it takes to transition from EPAP to IPAP, the higher the number the slower the transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9010"/>
        <source>Rise Time Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9011"/>
        <source>Whether Rise Time settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9012"/>
        <source>Rise Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9056"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9057"/>
        <source>Mask Resistance Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9058"/>
        <source>Mask Resist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9066"/>
        <source>Hose Diam.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9069"/>
        <source>15mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9068"/>
        <source>22mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="699"/>
        <source>Backing Up Files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="734"/>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="84"/>
        <source>Untested Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="735"/>
        <source>Your Philips Respironics %1 (%2) generated data that OSCAR has never seen before.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="736"/>
        <source>The imported data may not be entirely accurate, so the developers would like a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make sure OSCAR is handling the data correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8957"/>
        <source>CPAP-Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8959"/>
        <source>AutoCPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8960"/>
        <source>Auto-Trial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8962"/>
        <source>AutoBiLevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8964"/>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8965"/>
        <source>S/T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8967"/>
        <source>S/T - AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8968"/>
        <source>PC - AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8971"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8973"/>
        <source>Flex Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8972"/>
        <source>PRS1 pressure relief mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8976"/>
        <source>C-Flex</source>
        <translation>C-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8977"/>
        <source>C-Flex+</source>
        <translation>C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8978"/>
        <source>A-Flex</source>
        <translation>A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8979"/>
        <source>P-Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8980"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9003"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9005"/>
        <source>Rise Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8981"/>
        <source>Bi-Flex</source>
        <translation>Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8986"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8988"/>
        <source>Flex Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8987"/>
        <source>PRS1 pressure relief setting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9035"/>
        <source>Passover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9074"/>
        <source>Tubing Type Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9075"/>
        <source>Whether tubing type settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9076"/>
        <source>Tube Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9083"/>
        <source>Mask Resistance Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9084"/>
        <source>Whether mask resistance settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9085"/>
        <source>Mask Res. Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9120"/>
        <source>Whether or not machine shows AHI via built-in display.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9128"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9130"/>
        <source>Ramp Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9129"/>
        <source>Type of ramp curve to use.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9132"/>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9133"/>
        <source>SmartRamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9137"/>
        <source>Backup Breath Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9138"/>
        <source>The kind of backup breath rate in use: none (off), automatic, or fixed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9139"/>
        <source>Breath Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9143"/>
        <source>Fixed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9147"/>
        <source>Fixed Backup Breath BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9148"/>
        <source>Minimum breaths per minute (BPM) below which a timed breath will be initiated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9149"/>
        <source>Breath BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9154"/>
        <source>Timed Inspiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9155"/>
        <source>The time that a timed breath will provide IPAP before transitioning to EPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9156"/>
        <source>Timed Insp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9161"/>
        <source>Auto-Trial Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9162"/>
        <source>The number of days in the Auto-CPAP trial period, after which the machine will revert to CPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9163"/>
        <source>Auto-Trial Dur.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9168"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9170"/>
        <source>EZ-Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9169"/>
        <source>Whether or not EZ-Start is enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9177"/>
        <source>Variable Breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9178"/>
        <source>UNCONFIRMED: Possibly variable breathing, which are periods of high deviation from the peak inspiratory flow trend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9201"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9203"/>
        <source>Peak Flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9202"/>
        <source>Peak flow during a 2-minute interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9019"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>Humidifier Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9020"/>
        <source>PRS1 humidifier connected?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9023"/>
        <source>Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9024"/>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9028"/>
        <source>Humidification Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9029"/>
        <source>PRS1 Humidification Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9030"/>
        <source>Humid. Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9032"/>
        <source>Fixed (Classic)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9033"/>
        <source>Adaptive (System One)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9034"/>
        <source>Heated Tube</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9040"/>
        <source>Tube Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9041"/>
        <source>PRS1 Heated Tube Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9042"/>
        <source>Tube Temp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9049"/>
        <source>PRS1 Humidifier Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9050"/>
        <source>Humid. Lvl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9064"/>
        <source>Hose Diameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9065"/>
        <source>Diameter of primary CPAP hose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9070"/>
        <source>12mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9092"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9094"/>
        <source>Auto On</source>
        <translation>Auto av</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9093"/>
        <source>A few breaths automatically starts machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9101"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9103"/>
        <source>Auto Off</source>
        <translation>Auto på</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9102"/>
        <source>Machine automatically switches off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9110"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9112"/>
        <source>Mask Alert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9111"/>
        <source>Whether or not machine allows Mask checking.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9119"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9121"/>
        <source>Show AHI</source>
        <translation>Vis AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9186"/>
        <source>Breathing Not Detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9187"/>
        <source>A period during a session where the machine could not detect flow.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9188"/>
        <source>BND</source>
        <translation>BND</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9193"/>
        <source>Timed Breath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9194"/>
        <source>Machine Initiated Breath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9195"/>
        <source>TB</source>
        <translation type="unfinished">TB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="36"/>
        <source>Windows User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>Using </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>, found SleepyHead -
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="201"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="537"/>
        <source>&lt;i&gt;Your old machine data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="470"/>
        <source>Launching Windows Explorer failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="471"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="523"/>
        <source>OSCAR %1 needs to upgrade its database for %2 %3 %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="536"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="540"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="541"/>
        <source>This means you will need to import this machine data again afterwards from your own backups or data card.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="544"/>
        <source>Important:</source>
        <translation>Viktig:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="545"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="546"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="560"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="579"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="586"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="596"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="598"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="549"/>
        <source>Machine Database Changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="544"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;cannot&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="561"/>
        <source>The machine data folder needs to be removed manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="562"/>
        <source>This folder currently resides at the following location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="570"/>
        <source>Rebuilding from %1 Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="123"/>
        <source>Therapy Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="124"/>
        <source>Inspiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="125"/>
        <source>Lower Inspiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="126"/>
        <source>Higher Inspiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="127"/>
        <source>Expiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="128"/>
        <source>Lower Expiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="129"/>
        <source>Higher Expiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="130"/>
        <source>Pressure Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>PS Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Pressure Support Minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>PS Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Pressure Support Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Min Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Minimum Therapy Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Max Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Maximum Therapy Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Delay Period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Ramp Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Starting Ramp Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Ramp Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="180"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Ramp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>Vibratory Snore (VS2) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>A ResMed data item: Trigger Cycle Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>Mask On Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>Time started according to str.edf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>Summary Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="657"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>An apnea where the airway is open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>An apnea caused by airway obstruction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Hypopnea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>A partially obstructed airway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>Unclassified Apnea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>UA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>Vibratory Snore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>A vibratory snore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>A vibratory snore as detcted by a System One machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8947"/>
        <source>Pressure Pulse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8948"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>A large mask leak affecting machine performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>Non Responding Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Expiratory Puff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>User Flag #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>User Flag #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>User Flag #3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Heart rate in beats per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>Plethysomogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>Pulse Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>SpO2 Drop</source>
        <translation>SpO2 dropp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="212"/>
        <source>Breathing flow rate waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="660"/>
        <source>L/min</source>
        <translation>L/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="215"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>Mask Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>Amount of air displaced per breath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Graph displaying snore volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Minute Ventilation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Amount of air displaced per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Respiratory Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Rate of breaths per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Patient Triggered Breaths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Pat. Trig. Breaths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Leak Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Rate of detected mask leakage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="240"/>
        <source>I:E Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="240"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="665"/>
        <source>ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Pressure Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Pressure Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Pressure Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Pressure Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>IPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>IPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>EPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>EPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>Cheyne Stokes Respiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="760"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>CSR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Periodic Breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Clear Airway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>Obstructive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>Respiratory Effort Related Arousal: An restriction in breathing that causes an either an awakening or sleep disturbance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>Leak Flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>LF</source>
        <translation>LF</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>Perfusion Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>Perf. Index %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>Mask Pressure (High frequency)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Expiratory Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Time taken to breathe out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Inspiratory Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Time taken to breathe in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Respiratory Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Graph showing severity of flow limitations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Flow Limit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Target Minute Ventilation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Maximum Leak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>The maximum rate of mask leakage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Max Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Apnea Hypopnea Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Total Leak Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median Leak Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median rate of detected mask leakage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Respiratory Disturbance Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>Sleep position in degrees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>Upright angle in degrees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Movement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Movement detector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>CPAP Session contains summary data only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8954"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="290"/>
        <source>PAP Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="111"/>
        <source>Couldn&apos;t parse Channels.xml, OSCAR cannot continue and is exiting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="290"/>
        <source>PAP Device Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="294"/>
        <source>APAP (Variable)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="298"/>
        <source>ASV (Fixed EPAP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="299"/>
        <source>ASV (Variable EPAP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Height</source>
        <translation>Høyde</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Physical Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Notes</source>
        <translation>Notater</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Bookmark Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>Body Mass Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>Bookmark Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="313"/>
        <source>Bookmark End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="314"/>
        <source>Last Updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>Journal Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>Journal</source>
        <translation>Journal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Brain Wave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>BrainWave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Awakenings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Number of Awakenings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Morning Feel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>How you felt in the morning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Time Awake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Time spent awake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time In REM Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time spent in REM Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time in REM Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Time In Light Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Time spent in light sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Time in Light Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="330"/>
        <source>Time In Deep Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="330"/>
        <source>Time spent in deep sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="330"/>
        <source>Time in Deep Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="331"/>
        <source>Time to Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="331"/>
        <source>Time taken to get to sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="332"/>
        <source>Zeo ZQ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="332"/>
        <source>Zeo sleep quality measurement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="332"/>
        <source>ZEO ZQ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Debugging channel #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Top secret internal stuff you&apos;re not supposed to see ;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Test #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Debugging channel #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Test #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="748"/>
        <source>Zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="751"/>
        <source>Upper Threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="754"/>
        <source>Lower Threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="498"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="185"/>
        <source>Choose the SleepyHead data folder to migrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="186"/>
        <source>or CANCEL to skip migration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="200"/>
        <source>The folder you chose does not contain valid SleepyHead data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="201"/>
        <source>You cannot use this folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="216"/>
        <source>Migrating </source>
        <translation>Migrerer </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="216"/>
        <source> files</source>
        <translation> filer</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="217"/>
        <source>from </source>
        <translation>fra </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="217"/>
        <source>to </source>
        <translation>til </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="324"/>
        <source>OSCAR crashed due to an incompatibility with your graphics hardware.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="325"/>
        <source>To resolve this, OSCAR has reverted to a slower but more compatible method of drawing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="485"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation>OSCAR vil sette opp en mappe for dine data.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="486"/>
        <source>If you have been using SleepyHead, OSCAR can copy your old data to this folder later.</source>
        <translation>Hvis du har bruk SleepyHead, så kan OSCAR kopiere dine gamle data til denne mappen senere.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="487"/>
        <source>We suggest you use this folder: </source>
        <translation>Vi foreslår at du bruker denne mappen: </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="488"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation>Klikk OK for å akseptere dette, eller Nei om du ønsker å bruke en annen mappe.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="494"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation>Velg eller lag en ny mappe for OSCAR-data</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="499"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation>Neste gang du kjører OSCAR, så vil du bli spurt igjen.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="510"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation>Mappen du valgte er ikke tom, den inneholder helle ikke gyldig OSCAR-data.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="531"/>
        <source>Data directory:</source>
        <translation>Datamappe:</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="539"/>
        <source>Migrate SleepyHead Data?</source>
        <translation>Migrere SleepyHead Data?</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="540"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead data</source>
        <translation>På neste bilde så vil OSCAR be deg om å velge en mappe med SleepyHead data</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="541"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead data.</source>
        <translation>Klikk [OK] for å gå til neste bilde, eller [Nei] om du ikke ønsker å bruke noe SleepyHead data.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="596"/>
        <source>Version &quot;%1&quot; is invalid, cannot continue!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="605"/>
        <source>The version of OSCAR you are running (%1) is OLDER than the one used to create this data (%2).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="609"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="669"/>
        <source>Question</source>
        <translation>Spørsmål</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="497"/>
        <source>Exiting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="511"/>
        <source>Are you sure you want to use this folder?</source>
        <translation>Er du sikker på at du vil bruke denne mappen?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="277"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="277"/>
        <source>OSCAR Reminder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="460"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="461"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="474"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2248"/>
        <source>Sorry, your %1 %2 machine is not currently supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2255"/>
        <source>Recompressing Session Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2794"/>
        <source>Please select a location for your zip other than the data card itself!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2821"/>
        <location filename="../oscar/mainwindow.cpp" line="2888"/>
        <source>Unable to create zip!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1186"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1239"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="40"/>
        <source>There are no graphs visible to print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="55"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="99"/>
        <source>Printing %1 Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="133"/>
        <source>%1 Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="191"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="244"/>
        <source>RDI	%1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="246"/>
        <source>AHI	%1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="279"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="285"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="289"/>
        <source>UAI=%1 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="291"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="360"/>
        <source>Reporting from %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="432"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="438"/>
        <source>Current Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="448"/>
        <source>Entire Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="564"/>
        <source>%1 %2 %3</source>
        <translation type="unfinished">%1 %2 %3</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="573"/>
        <source>Page %1 of %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="997"/>
        <source>Days: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1001"/>
        <source>Low Usage Days: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1002"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1118"/>
        <source>(Sess: %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1126"/>
        <source>Bedtime: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1128"/>
        <source>Waketime: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1168"/>
        <source>90%</source>
        <translation>90%</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1240"/>
        <source>(Summary Only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="459"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="175"/>
        <source>Peak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="179"/>
        <source>%1% %2</source>
        <translation>%1% %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="295"/>
        <source>Fixed Bi-Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="296"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="297"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1496"/>
        <source>%1%2</source>
        <translation type="unfinished">%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1518"/>
        <source>n/a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1535"/>
        <source>Fixed %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1537"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1539"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1541"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1543"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1552"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1548"/>
        <source>EPAP %1 PS %2-%3 (%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1559"/>
        <source>EPAP %1 IPAP %2-%3 (%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="166"/>
        <location filename="../oscar/SleepLib/day.cpp" line="168"/>
        <location filename="../oscar/SleepLib/day.cpp" line="170"/>
        <location filename="../oscar/SleepLib/day.cpp" line="175"/>
        <source>%1 %2</source>
        <translation type="unfinished">%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="340"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="341"/>
        <source>(last night)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="342"/>
        <source>(1 day ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="343"/>
        <source>(%2 days ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="348"/>
        <source>No oximetry data has been imported yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>Contec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <source>CMS50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>Fisher &amp; Paykel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>ICON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>DeVilbiss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>Intellipap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="85"/>
        <source>SmartFlex Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>ChoiceMMed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>MD300</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>Respironics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>M-Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="419"/>
        <source>Philips Respironics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="419"/>
        <source>System One</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="92"/>
        <source>ResMed</source>
        <translation>ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="93"/>
        <source>S9</source>
        <translation>S9</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="126"/>
        <source>EPR: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="39"/>
        <source>Zeo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="39"/>
        <source>Personal Sleep Coach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="196"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="373"/>
        <source> (%2 min, %3 sec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="375"/>
        <source> (%3 sec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="388"/>
        <source>Pop out Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1420"/>
        <source>I&apos;m very sorry your machine doesn&apos;t record useful data to graph in Daily View :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1423"/>
        <source>There is no data to graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1608"/>
        <source>d MMM yyyy [ %1 - %2 ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2166"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2209"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2280"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2297"/>
        <source>%1</source>
        <translation type="unfinished">%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2261"/>
        <source>Hide All Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2262"/>
        <source>Show All Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2603"/>
        <source>Unpin %1 Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2605"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2680"/>
        <source>Popout %1 Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2682"/>
        <source>Pin %1 Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1042"/>
        <source>Plots Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1116"/>
        <source>Duration %1:%2:%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1117"/>
        <source>AHI %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="108"/>
        <source>%1: %2</source>
        <translation type="unfinished">%1h %2m {1:?} {2?}</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="151"/>
        <source>Relief: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="157"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="260"/>
        <source>Machine Information</source>
        <translation type="unfinished">Maskininformasjon</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="30"/>
        <source>Journal Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="48"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="50"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="51"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="58"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="59"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="61"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F3.7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1543"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1545"/>
        <source>SmartFlex Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1544"/>
        <source>Intellipap pressure relief mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1550"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="106"/>
        <source>Ramp Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1551"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="107"/>
        <source>Full Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1554"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1556"/>
        <source>SmartFlex Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1555"/>
        <source>Intellipap pressure relief level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="405"/>
        <source>Locating STR.edf File(s)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="557"/>
        <source>Cataloguing EDF Files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="576"/>
        <source>Queueing Import Tasks...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="603"/>
        <source>Finishing Up...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="85"/>
        <source>CPAP Mode</source>
        <translation type="unfinished">CPAP-modus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="91"/>
        <source>VPAP-T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="92"/>
        <source>VPAP-S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="93"/>
        <source>VPAP-S/T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="95"/>
        <source>VPAPauto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="97"/>
        <source>ASVAuto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="100"/>
        <source>Auto for Her</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="103"/>
        <source>EPR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="103"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="108"/>
        <source>Patient???</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="111"/>
        <source>EPR Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="111"/>
        <source>Exhale Pressure Relief Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="94"/>
        <source>?5?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="98"/>
        <source>?9?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="99"/>
        <source>?10?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>SmartStart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>Machine auto starts by breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>Smart Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>Humid. Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>Humidifier Enabled Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="130"/>
        <source>Humid. Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="130"/>
        <source>Humidity Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="143"/>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="143"/>
        <source>ClimateLine Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Temp. Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>ClimateLine Temperature Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Temperature Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="155"/>
        <source>AB Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="155"/>
        <source>Antibacterial Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="161"/>
        <source>Pt. Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="161"/>
        <source>Patient Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="167"/>
        <source>Climate Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="170"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="989"/>
        <source>Parsing STR.edf records...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9142"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="169"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="173"/>
        <source>Mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="173"/>
        <source>ResMed Mask Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="175"/>
        <source>Pillows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="176"/>
        <source>Full Face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="177"/>
        <source>Nasal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="180"/>
        <source>Ramp Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>Weinmann</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>SOMNOsoft2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="350"/>
        <source>Snapshot %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50D+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50E/F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="223"/>
        <source>%1
Line %2, column %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="236"/>
        <source>Could not parse Updates.xml file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="682"/>
        <source>Loading %1 data for %2...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="692"/>
        <source>Scanning Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="726"/>
        <source>Migrating Summary File Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="889"/>
        <source>Loading Summaries.xml.gz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1020"/>
        <source>Loading Summary Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="14"/>
        <source>Please Wait...</source>
        <translation>Vennligst vent...</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="312"/>
        <source>Peak %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="514"/>
        <source>Loading summaries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="166"/>
        <source>Updating Statistics cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="708"/>
        <source>Usage Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/dreem_loader.h" line="36"/>
        <source>Dreem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="85"/>
        <source>Your Viatom device generated data that OSCAR has never seen before.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="86"/>
        <source>The imported data may not be entirely accurate, so the developers would like a copy of your Viatom files to make sure OSCAR is handling the data correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="39"/>
        <source>Viatom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="39"/>
        <source>Viatom Software</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Report</name>
    <message>
        <location filename="../oscar/reports.ui" line="14"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
    <message>
        <location filename="../oscar/reports.ui" line="27"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="246"/>
        <source>%1h %2m</source>
        <translation>%1h %2m</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="289"/>
        <source>No Sessions Present</source>
        <translation>Ingen-økter tilstede</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="534"/>
        <source>CPAP Statistics</source>
        <translation>CPAP statistikk</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="537"/>
        <location filename="../oscar/statistics.cpp" line="1358"/>
        <source>CPAP Usage</source>
        <translation>CPAP bruk</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="538"/>
        <source>Average Hours per Night</source>
        <translation>Gjennomsnittlig timer per natt</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="541"/>
        <source>Therapy Efficacy</source>
        <translation>Terapieffektivitet</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="551"/>
        <source>Leak Statistics</source>
        <translation>Lekkasjestatistikk</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="556"/>
        <source>Pressure Statistics</source>
        <translation>Trykkstatistikk</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="581"/>
        <source>Oximeter Statistics</source>
        <translation>Oksymeterstatistikk</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="585"/>
        <source>Blood Oxygen Saturation</source>
        <translation>Blodoksygenmetning</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="590"/>
        <source>Pulse Rate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="601"/>
        <source>%1 Median</source>
        <translation>%1 median</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="602"/>
        <location filename="../oscar/statistics.cpp" line="603"/>
        <source>Average %1</source>
        <translation>Gjennomsnutt %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="605"/>
        <source>Min %1</source>
        <translation>Min %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="606"/>
        <source>Max %1</source>
        <translation>Max %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="607"/>
        <source>%1 Index</source>
        <translation>%1 indeks</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="608"/>
        <source>% of time in %1</source>
        <translation>% av tid i %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="609"/>
        <source>% of time above %1 threshold</source>
        <translation>% av tid over %1 grense</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="610"/>
        <source>% of time below %1 threshold</source>
        <translation>% av tid under %1 grense</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="633"/>
        <source>Name: %1, %2</source>
        <translation>Navn: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="635"/>
        <source>DOB: %1</source>
        <translation>DOB: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="638"/>
        <source>Phone: %1</source>
        <translation>Telefon: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="641"/>
        <source>Email: %1</source>
        <translation>Epost: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="644"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="727"/>
        <source>This report was prepared on %1 by OSCAR %2</source>
        <translation>Denne rapporten ble forberedt på %1 av OSCAR %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1359"/>
        <source>Days Used: %1</source>
        <translation>Dager brukt: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1360"/>
        <source>Low Use Days: %1</source>
        <translation>Dager med lav bruk: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1361"/>
        <source>Compliance: %1%</source>
        <translation>Samsvar: %1%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1385"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation>Dager med AHI på 5 eller mer: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1392"/>
        <source>Best AHI</source>
        <translation>Beste AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1396"/>
        <location filename="../oscar/statistics.cpp" line="1408"/>
        <source>Date: %1 AHI: %2</source>
        <translation>Dato: %1 AHI: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1402"/>
        <source>Worst AHI</source>
        <translation>Verste AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1439"/>
        <source>Best Flow Limitation</source>
        <translation>Beste flytbegrensning</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1443"/>
        <location filename="../oscar/statistics.cpp" line="1456"/>
        <source>Date: %1 FL: %2</source>
        <translation>Dato %1 FL: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1449"/>
        <source>Worst Flow Limtation</source>
        <translation>Verste flytbegrensning</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1461"/>
        <source>No Flow Limitation on record</source>
        <translation>Ingen flytbegrensning registrert</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1482"/>
        <source>Worst Large Leaks</source>
        <translation>Verste store lekkasjer</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1490"/>
        <source>Date: %1 Leak: %2%</source>
        <translation>Dato: %1 Lekkasje: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1496"/>
        <source>No Large Leaks on record</source>
        <translation>Ingen store lekkasjer registrert</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1519"/>
        <source>Worst CSR</source>
        <translation>Verste CSR</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1527"/>
        <source>Date: %1 CSR: %2%</source>
        <translation>Dato: %1 CSR: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1532"/>
        <source>No CSR on record</source>
        <translation>Ingen CSR registrert</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1549"/>
        <source>Worst PB</source>
        <translation>Verste PB</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1557"/>
        <source>Date: %1 PB: %2%</source>
        <translation>Dato: %1 PB: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1562"/>
        <source>No PB on record</source>
        <translation>Ingen PB registrert</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1570"/>
        <source>Want more information?</source>
        <translation>Vil du ha mer informasjon?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1571"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation>OSCAR trenger all sammendragsdata lastet for å beregne beste/verste data for individuelle dager.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1572"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation>Aktiver avmerkingsboksen pre-last oppsummering i preferanser for å sikre at disse dataene er tilgjengelige.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1592"/>
        <source>Best RX Setting</source>
        <translation>Beste RX innstilling</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1595"/>
        <location filename="../oscar/statistics.cpp" line="1607"/>
        <source>Date: %1 - %2</source>
        <translation>Dato: %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1598"/>
        <location filename="../oscar/statistics.cpp" line="1610"/>
        <source>AHI: %1</source>
        <translation>AHI: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1599"/>
        <location filename="../oscar/statistics.cpp" line="1611"/>
        <source>Total Hours: %1</source>
        <translation>Totale timer: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1604"/>
        <source>Worst RX Setting</source>
        <translation>Verste RX innstilling</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1150"/>
        <source>Most Recent</source>
        <translation>Senest</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="539"/>
        <source>Compliance (%1 hrs/day)</source>
        <translation>Samsvar (%1 tmr/dag</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="730"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation>OSCAR er fri åpen kildekode CPAP-rapportprogramvare</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="963"/>
        <source>Changes to Machine Settings</source>
        <translation>Endringer i maskininnstillinger</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1049"/>
        <source>No data found?!?</source>
        <translation>Ingen data funnet?!?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1051"/>
        <source>Oscar has no data to report :(</source>
        <translation>Oscar har ingen data å rapportere :(</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1151"/>
        <source>Last Week</source>
        <translation>Siste uke</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1152"/>
        <source>Last 30 Days</source>
        <translation>Siste 30 dager</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1153"/>
        <source>Last 6 Months</source>
        <translation>Siste 6 måneder</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1154"/>
        <source>Last Year</source>
        <translation>Siste år</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1158"/>
        <source>Last Session</source>
        <translation>Siste økt</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1199"/>
        <source>Details</source>
        <translation>Detaljer</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1213"/>
        <source>No %1 data available.</source>
        <translation>Ingen %1 data tilgjengelig.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1216"/>
        <source>%1 day of %2 Data on %3</source>
        <translation>%1 dag av %2 data på %3</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1222"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation>%1 dager av %2 data, mellom %3 og %4</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="971"/>
        <source>Days</source>
        <translation>Dager</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="975"/>
        <source>Pressure Relief</source>
        <translation>Trykkavlastning</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="977"/>
        <source>Pressure Settings</source>
        <translation>Trykkinnstillinger</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="904"/>
        <source>Machine Information</source>
        <translation>Maskininformasjon</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="910"/>
        <source>First Use</source>
        <translation>Første bruk</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="911"/>
        <source>Last Use</source>
        <translation>Siste bruk</translation>
    </message>
</context>
<context>
    <name>UpdaterWindow</name>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="14"/>
        <source>OSCAR Updater</source>
        <translation>OSCAR-oppdaterer</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="60"/>
        <source>A new version of $APP is available</source>
        <translation>En ny versjon av $APP er tilgjengelig</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="106"/>
        <source>Version Information</source>
        <translation>Versjonsinformasjon</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="117"/>
        <source>Release Notes</source>
        <translation>Utgivelsesnotater</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="142"/>
        <source>Build Notes</source>
        <translation>Versjonsnotater</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="195"/>
        <source>Maybe &amp;Later</source>
        <translation>Kanskje &amp;senere</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="215"/>
        <source>&amp;Upgrade Now</source>
        <translation>&amp;Oppgrader nå</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="250"/>
        <source>Please wait while updates are downloaded and installed...</source>
        <translation>Vennligst vent mens oppdateringer er lastet ned og installert...</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="264"/>
        <source>Updates</source>
        <translation>Oppdateringer</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="304"/>
        <source>Component</source>
        <translation>Komponent</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="309"/>
        <source>Version</source>
        <translation>Versjon</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="314"/>
        <source>Size</source>
        <translation>Størrelse</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="319"/>
        <source>Progress</source>
        <translation>Framgang</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="328"/>
        <source>Log</source>
        <translation>Logg</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="374"/>
        <source>Downloading &amp; Installing Updates</source>
        <translation>Laster ned og installerer oppdateringer</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="394"/>
        <source>&amp;Finished</source>
        <translation>&amp;Ferdig</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="93"/>
        <source>Updates are not yet implemented</source>
        <translation>Oppdateringer ikke implementert enda</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="127"/>
        <source>Checking for OSCAR Updates</source>
        <translation>Sjekke etter OSCAR-oppdateringer</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="146"/>
        <source>Requesting </source>
        <translation>Forespør </translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="363"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="369"/>
        <source>OSCAR Updates</source>
        <translation>OSCAR-oppdateringer</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <source>OSCAR Updates are currently unavailable for this platform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="318"/>
        <source>Version %1 of OSCAR is available, opening link to download site.</source>
        <translation>Versjon %1 av OSCARD er tilgjengelig, åpner en lenke til nedlastingssiden.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="363"/>
        <source>No updates were found for your platform.</source>
        <translation>Ingen oppdateringen ble funnet for din plattform.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="370"/>
        <source>New OSCAR Updates are avilable:</source>
        <translation>Nye OSCAR-oppdateringer er tilgjengelig:</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="171"/>
        <source>%1 bytes received</source>
        <translation>%1 bytes mottatt</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="321"/>
        <source>You are already running the latest version.</source>
        <translation>Du kjører allerede siste versjon.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="371"/>
        <source>Would you like to download and install them now?</source>
        <translation>Vil du laste ned og installere de nå?</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.ui" line="14"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="128"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Velkommen til åpen kildekode CPAP analyse og rapportering</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="143"/>
        <source>What would you like to do?</source>
        <translation>Hva har du lyst til å gjøre?</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="186"/>
        <source>CPAP Importer</source>
        <translation>CPAP-importør</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="224"/>
        <source>Oximetry Wizard</source>
        <translation>Oksimetriveiviser</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="262"/>
        <source>Daily View</source>
        <translation>Daglig visning</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="300"/>
        <source>Overview</source>
        <translation>Oversikt</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="338"/>
        <source>Statistics</source>
        <translation>Statistikk</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="581"/>
        <source>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards need to be locked &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;before inserting into your computer.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Some operating systems write index files to the card without asking, which can render your card unreadable by your cpap machine.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SD-kort må være låst &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;før de settes inn i din datamaskin.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Noen operativsystemer skriver indeksfiler til kortet uten å spørre, noe som kan gjøre kortet ditt uleselig av cpap-maskinen din.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation>Det ville være en god idé å sjekke Fil-&gt; Innstillinger først,</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>as there are some options that affect import.</source>
        <translation>ettersom det er noen alternativer som påvirker import.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="162"/>
        <source>Note that some preferences are forced when a ResMed machine is detected</source>
        <translation>Merk at noen preferanser blir tvunget når en ResMed-maskin blir oppdaget</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>First import can take a few minutes.</source>
        <translation>Første import kan ta noen minutter.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="176"/>
        <source>The last time you used your %1...</source>
        <translation>Siste gang du brukte din %1 ...</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="180"/>
        <source>last night</source>
        <translation>i går kveld</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="182"/>
        <source>%2 days ago</source>
        <translation>%2 dager siden</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="184"/>
        <source>was %1 (on %2)</source>
        <translation>var %1 (på %2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="192"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1 timer, %2 minutter og %3 sekunder</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="195"/>
        <source>Your machine was on for %1.</source>
        <translation>Din maskin var på for %1.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="196"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color = red&gt;Du hadde kun masken på i %1.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="208"/>
        <source>under</source>
        <translation>under</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="209"/>
        <source>over</source>
        <translation>over</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="210"/>
        <source>reasonably close to</source>
        <translation>rimelig nær</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="211"/>
        <source>equal to</source>
        <translation>lik</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="225"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>Du hadde en AHI på %1, som er %2 ditt %3 dagers gjennomsnitt på %4.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="256"/>
        <source>Your CPAP machine used a constant %1 %2 of air</source>
        <translation>CPAP-maskinen din brukte konstant %1 %2 med luft</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="259"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>Trykket var under %1 %2 for %3 av tiden.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="263"/>
        <source>Your machine used a constant %1-%2 %3 of air.</source>
        <translation>Din maskin brukte konstant %1-%2 %3 med luft.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="271"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>Ditt EPAP-trykk fast på %1 %2.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="272"/>
        <location filename="../oscar/welcome.cpp" line="278"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Ditt IPAP-trykk var under %1 %2 for %3 av tiden.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="277"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Ditt EPAP-trykk var under %1 %2 for %3 av tiden.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="267"/>
        <source>Your machine was under %1-%2 %3 for %4% of the time.</source>
        <translation>Din maskin var under %1-%2 %3 for %4 av tiden.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="181"/>
        <source>1 day ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="298"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>Din gjennomsnittlige lekkasje var %1 %2. noe som er %3 av ditt %4 daglige snitt på %5.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="304"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>Ingen CPAP-data er importert ennå.</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="793"/>
        <source>%1 days</source>
        <translation>%1 dager</translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="393"/>
        <source>100% zoom level</source>
        <translation>100% zoomnivå</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="395"/>
        <source>Restore X-axis zoom to 100% to view entire selected period.</source>
        <translation>Gjenopprett zoom på X-aksen til 100% for å se hele valgt periode.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="397"/>
        <source>Restore X-axis zoom to 100% to view entire day&apos;s data.</source>
        <translation>Gjenopprett zoom på X-aksen til 100% for å se hele dagens data.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="399"/>
        <source>Reset Graph Layout</source>
        <translation>Tilbakestile grafutseende</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="400"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>Tilbakestiller alle grafer til ensartet høyde og standardrekkefølge.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="403"/>
        <source>Y-Axis</source>
        <translation>Y-akse</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="404"/>
        <source>Plots</source>
        <translation>Plotter</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="409"/>
        <source>CPAP Overlays</source>
        <translation>CPAP-overlegg</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="412"/>
        <source>Oximeter Overlays</source>
        <translation>Oksymeteroverlegg</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="415"/>
        <source>Dotted Lines</source>
        <translation>Prikkete linjer</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1801"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1854"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation>Dobbeltklikk tittele for å fest /løsne
Klikk og dra for å ordne grafer på nytt</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2099"/>
        <source>Remove Clone</source>
        <translation>Fjern klone</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2103"/>
        <source>Clone %1 Graph</source>
        <translation>Klone %1 graf</translation>
    </message>
</context>
</TS>
