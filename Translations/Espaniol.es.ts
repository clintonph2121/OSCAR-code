<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translation>&amp;Acerca de</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="117"/>
        <source>Release Notes</source>
        <translation>Notas de la Versión</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation>Créditos</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>Licencia GPL</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="239"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="35"/>
        <source>Show data folder</source>
        <translation>Mostrar carpeta datos</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="39"/>
        <source>About OSCAR %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="83"/>
        <source>Sorry, could not locate About file.</source>
        <translation>Lo siento, no he podido localizar el archivo.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="96"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation>Lo siento, no he podido localizar los créditos.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="108"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation>Lo siento, no he podido localizar notas de la versión.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="118"/>
        <source>OSCAR %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="122"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceeding, because attempting to roll back later may break things.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="121"/>
        <source>Important:</source>
        <translation>Importante:</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="134"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>Para ver sí el texto de la licencia está disponible en su idioma, vea %1.</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="878"/>
        <source>Could not find the oximeter file:</source>
        <translation>No he podido encontrar el archivo del oxímetro:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="884"/>
        <source>Could not open the oximeter file:</source>
        <translation>No se pudo abrir el archivo del oxímetro:</translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>No se recibió transmisión de datos alguna desde el oxímetro.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translatorcomment>Need to know how is &apos;upload&apos; translated</translatorcomment>
        <translation>Por favor asegúrese de seleccionar &apos;upload&apos; o &apos;descargar&apos; desde el menú de Dispositivos de Oximetría.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="546"/>
        <source>Could not find the oximeter file:</source>
        <translation>No se pudo encontrar el archivo del oxímetro:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="552"/>
        <source>Could not open the oximeter file:</source>
        <translation>No se pudo abrir el archivo del oxímetro:</translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="435"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>Ir al día anterior</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>Mostrar u ocultar el calendario</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>Ir al día siguiente</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>Ir al día más reciente con datos registrados</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translation>Ver tamaño</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1395"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translatorcomment>or agenda</translatorcomment>
        <translation>Diario</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1107"/>
        <location filename="../oscar/daily.ui" line="1117"/>
        <source>Small</source>
        <translation>Pequeño</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1122"/>
        <source>Medium</source>
        <translation>Mediano</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1127"/>
        <source>Big</source>
        <translation>Grande</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1199"/>
        <source>I&apos;m feeling ...</source>
        <translation>Me siento ...</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1222"/>
        <source>If height is greater than zero in Preferences Dialog, setting weight here will show Body Mass Index (BMI) value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1496"/>
        <source>Flags</source>
        <translation>Indicadores</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1548"/>
        <source>Graphs</source>
        <translation>Gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1573"/>
        <source>Show/hide available graphs.</source>
        <translation>Mostrar/ocultar los gráficos disponibles.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1085"/>
        <source>Color</source>
        <translation>Color</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation> i </translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1075"/>
        <source>u</source>
        <translation>u</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1063"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1186"/>
        <source>Zombie</source>
        <translation>Zombi</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1215"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1292"/>
        <source>Awesome</source>
        <translation>sorprendentes</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1330"/>
        <source>B.M.I.</source>
        <translatorcomment>Índice de masa corporal</translatorcomment>
        <translation>I.M.C.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1346"/>
        <source>Bookmarks</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1367"/>
        <source>Add Bookmark</source>
        <translation>Añadir Marcador</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1390"/>
        <source>Starts</source>
        <translatorcomment>¿inicios?</translatorcomment>
        <translation>Comienza</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1403"/>
        <source>Remove Bookmark</source>
        <translation>Eliminar Marcador</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="288"/>
        <source>Breakdown</source>
        <translatorcomment>desarrollo, descomponer, desarrollar, DESGLOSAR</translatorcomment>
        <translation>Desglose</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="288"/>
        <source>events</source>
        <translation>eventos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="609"/>
        <source>No %1 events are recorded this day</source>
        <translatorcomment>. al final</translatorcomment>
        <translation>No hay eventos %1 registrados éste día</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="729"/>
        <source>%1 event</source>
        <translation>Evento %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="730"/>
        <source>%1 events</source>
        <translation>Eventos %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="299"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="300"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="773"/>
        <source>Session Start Times</source>
        <translation>Hora de inicio de sesión</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="774"/>
        <source>Session End Times</source>
        <translation>Hora de fin de sesión</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1023"/>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1009"/>
        <source>Position Sensor Sessions</source>
        <translation>Sesiones de Posición del Sensor</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1014"/>
        <source>Unknown Session</source>
        <translation>Sesión Desconocida</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1290"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1380"/>
        <source>Time over leak redline</source>
        <translatorcomment>mmm</translatorcomment>
        <translation>Tiempo de fuga sobre la línea roja</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1647"/>
        <source>BRICK! :(</source>
        <translation>Ladrillo :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1455"/>
        <source>Event Breakdown</source>
        <translation>Desglose de eventos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1468"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation>No se puede mostrar el Gráfico Tarta en este sistema</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1702"/>
        <source>Sessions all off!</source>
        <translation>¡Todas las sesiones deshabilitadas!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1704"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>Existen sesiones para este día pero fueron deshabilitadas.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1707"/>
        <source>Impossibly short session</source>
        <translation>Una sesión demasiado corta</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1708"/>
        <source>Zero hours??</source>
        <translation>¿Cero horas?</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1711"/>
        <source>BRICK :(</source>
        <translation>Ladrillo :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1712"/>
        <source>Sorry, this machine only provides compliance data.</source>
        <translation>Lo sentimos, esta máquina sólo proporciona datos de cumplimiento.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1713"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>¡Quejese al Proveedor de su Equipo!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1285"/>
        <source>Statistics</source>
        <translation>Estadísticas</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="183"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="363"/>
        <source>Time at Pressure</source>
        <translation>Tiempo a Presión</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1043"/>
        <source>Click to %1 this session.</source>
        <translation>Haga clic en %1 en ésta sesión.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1043"/>
        <source>disable</source>
        <translation>desactivar</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1043"/>
        <source>enable</source>
        <translation>activar</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1059"/>
        <source>%1 Session #%2</source>
        <translation>%1 Sesión #%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1060"/>
        <source>%1h %2m %3s</source>
        <translation>%1h %2m %3s</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1092"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing has changed since previous days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1213"/>
        <source>Oximeter Information</source>
        <translation>Información del Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1217"/>
        <source>SpO2 Desaturations</source>
        <translation>Desaturaciones SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1218"/>
        <source>Pulse Change events</source>
        <translation>Eventos de cambio de pulso</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1219"/>
        <source>SpO2 Baseline Used</source>
        <translation>Línea basal de SpO2 utilizada</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1431"/>
        <source>Start</source>
        <translation>Inicio</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1431"/>
        <source>End</source>
        <translation>Final</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2293"/>
        <source>This bookmark is in a currently disabled area..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1088"/>
        <source>Machine Settings</source>
        <translation>Ajustes de la máquina</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="980"/>
        <source>Session Information</source>
        <translation>Información de sesión</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1000"/>
        <source>CPAP Sessions</source>
        <translation>Sesiones CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1003"/>
        <source>Oximetry Sessions</source>
        <translation>Sesiones del Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1006"/>
        <source>Sleep Stage Sessions</source>
        <translation>Sesiones de Etapas del Sueño</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1239"/>
        <source>Model %1 - %2</source>
        <translation>Modelo %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1244"/>
        <source>PAP Mode: %1</source>
        <translation>Modo PAP: %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1248"/>
        <source>(Mode/Pressure settings are guessed on this day.)</source>
        <translation>(Los ajustes de Modo/Presión se calculan en éste día.)</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1360"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>Éste día sólo contiene datos resumidos, sólo se dispone de información limitada.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1370"/>
        <source>Total time in apnea</source>
        <translation>Tiempo total en apnea</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1386"/>
        <source>Total ramp time</source>
        <translation>Tiempo total en rampa</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1390"/>
        <source>Time outside of ramp</source>
        <translation>Tiempo fuera de rampa</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1731"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;¡Aquí no hay nada!&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1734"/>
        <source>No data is available for this day.</source>
        <translation>No hay datos disponibles para éste día.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2029"/>
        <source>Pick a Colour</source>
        <translation>Escoja un color</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2336"/>
        <source>Bookmark at %1</source>
        <translation>Marcador en %1</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>Exportar como CSV</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>Fechas:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>Resolución:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>Sesiones</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>Diario</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>Nombre del archivo:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>Inicio:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>Fin:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation>Intérvalo rápido:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="61"/>
        <location filename="../oscar/exportcsv.cpp" line="123"/>
        <source>Most Recent Day</source>
        <translation>Último Día</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="126"/>
        <source>Last Week</source>
        <translation>Última Semana</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="129"/>
        <source>Last Fortnight</source>
        <translation>Última Quincena</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="132"/>
        <source>Last Month</source>
        <translation>Último Mes</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="135"/>
        <source>Last 6 Months</source>
        <translation>Último Semestre</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="138"/>
        <source>Last Year</source>
        <translation>Último Año</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="120"/>
        <source>Everything</source>
        <translation>Todo</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="109"/>
        <source>Custom</source>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="74"/>
        <source>OSCAR_</source>
        <translation>OSCAR_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="77"/>
        <source>Details_</source>
        <translation>Detalles_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="79"/>
        <source>Sessions_</source>
        <translation>Sesiones_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="81"/>
        <source>Summary_</source>
        <translation>Resumen_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>Select file to export to</source>
        <translation>Seleccione archivo a exportar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="89"/>
        <source>CSV Files (*.csv)</source>
        <translation>Archivos CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="196"/>
        <source>DateTime</source>
        <translation>Fecha /Hora</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="196"/>
        <location filename="../oscar/exportcsv.cpp" line="202"/>
        <source>Session</source>
        <translation>Sesión</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="196"/>
        <source>Event</source>
        <translation>Evento</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="196"/>
        <source>Data/Duration</source>
        <translation>Datos/Duración</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="199"/>
        <location filename="../oscar/exportcsv.cpp" line="202"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="199"/>
        <source>Session Count</source>
        <translatorcomment>¿conteo de sesiones?</translatorcomment>
        <translation>Recuento de sesiones</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="199"/>
        <location filename="../oscar/exportcsv.cpp" line="202"/>
        <source>Start</source>
        <translation>Inicio</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="199"/>
        <location filename="../oscar/exportcsv.cpp" line="202"/>
        <source>End</source>
        <translation>Fin</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Total Time</source>
        <translation>Tiempo total</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>AHI</source>
        <translation>IAH</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <source> Count</source>
        <translation> Conteo</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="215"/>
        <source>%1% </source>
        <translation>%1% </translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="139"/>
        <source>Import Error</source>
        <translation>Error de Importación</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation>Este registro de máquina no puede ser importado a este perfi.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>Estos registros diarios se traslapan con contenido previamente existente.</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.ui" line="20"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="92"/>
        <source>Hide this message</source>
        <translation>Ocultar este mensaje</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="198"/>
        <source>Search Topic:</source>
        <translation>Búsqueda Tema:</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="58"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>Los archivos de ayuda aún no están disponibles para %1 y se mostrarán en %2.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="68"/>
        <source>Help files do not appear to be present.</source>
        <translation>Los archivos de ayuda no parecen estar presentes.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="84"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>El sistema de ayuda no se ha configurado correctamente</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="99"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>El sistema de ayuda no pudo registrar la documentación correctamente.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="110"/>
        <source>Contents</source>
        <translation>Contenido</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Index</source>
        <translation>Indice</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="115"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="125"/>
        <source>No documentation available</source>
        <translation>No hay documentación disponible</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="213"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation>Por favor, espera un poco.. Indexación en curso</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="239"/>
        <source>No</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>%1 resultado(s) for &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>clear</source>
        <translatorcomment>¿despejado, lúcido, limpido, despejarse, clarificarse?</translatorcomment>
        <translation>despejada</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="165"/>
        <source>Could not find the oximeter file:</source>
        <translation>No he podido encontrar el archivo del oxímetro:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="171"/>
        <source>Could not open the oximeter file:</source>
        <translation>No se pudo abrir el archivo del oxímetro:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="942"/>
        <source>&amp;Statistics</source>
        <translatorcomment>&amp;Estadísticas</translatorcomment>
        <translation>&amp;stadísticas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="996"/>
        <source>Report Mode</source>
        <translation>Modo de informe</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1003"/>
        <location filename="../oscar/mainwindow.ui" line="3277"/>
        <source>Standard</source>
        <translation>Estándar</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1013"/>
        <source>Monthly</source>
        <translation>Mensual</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1020"/>
        <source>Date Range</source>
        <translation>Intérvalo de fechas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1391"/>
        <source>Statistics</source>
        <translation>Estadísticas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1441"/>
        <source>Daily</source>
        <translatorcomment>mmm</translatorcomment>
        <translation>Vista por día</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1485"/>
        <source>Overview</source>
        <translation>Vista general</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1529"/>
        <source>Oximetry</source>
        <translation>Oximetría</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1579"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1629"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2805"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2830"/>
        <source>&amp;View</source>
        <translation>&amp;Vista</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2834"/>
        <source>&amp;Reset Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2859"/>
        <source>&amp;Help</source>
        <translation>A&amp;yuda</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2863"/>
        <source>Troubleshooting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2886"/>
        <source>&amp;Data</source>
        <translation>&amp;Datos</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2890"/>
        <source>&amp;Advanced</source>
        <translation>&amp;Avanzado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3010"/>
        <source>&amp;Maximize Toggle</source>
        <translation>Activar &amp;Maximizado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3250"/>
        <source>Report an Issue</source>
        <translation>Informar de un problema</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2910"/>
        <source>Rebuild CPAP Data</source>
        <translation>Restablecer Datos de  CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2941"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Preferencias</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2946"/>
        <source>&amp;Profiles</source>
        <translation>&amp;Perfiles</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="106"/>
        <source>E&amp;xit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2951"/>
        <source>Exit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2956"/>
        <source>View &amp;Daily</source>
        <translation>Ver Vista por &amp;Día</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2967"/>
        <source>View &amp;Overview</source>
        <translation>Ver Vista &amp;General</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2978"/>
        <source>View &amp;Welcome</source>
        <translation>Ver &amp;Bienvenida</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2986"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2997"/>
        <source>Use &amp;AntiAliasing</source>
        <translation>Usar &amp;AntiAliasing</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3024"/>
        <source>Show Debug Pane</source>
        <translation>Mostrar panel de depuración</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3040"/>
        <source>Take &amp;Screenshot</source>
        <translation>&amp;Capturar Pantalla</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3048"/>
        <source>O&amp;ximetry Wizard</source>
        <translation>Asistente de O&amp;ximetría</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3056"/>
        <source>Print &amp;Report</source>
        <translation>Imprimir &amp;Reporte</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3061"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Editar perfil</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3170"/>
        <source>Show &amp;Line Cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3192"/>
        <source>Show Daily Left Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3209"/>
        <source>Show Daily Calendar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3225"/>
        <source>Show Performance Information</source>
        <translation>Mostrar información de rendimiento</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3230"/>
        <source>Create zip of CPAP data card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3235"/>
        <source>Create zip of all OSCAR data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3240"/>
        <source>CSV Export Wizard</source>
        <translation>Asistente de exportación CSV</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3245"/>
        <source>Export for Review</source>
        <translatorcomment>Exportar para visualización</translatorcomment>
        <translation>Exportar para revisión</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3206"/>
        <source>Daily Calendar</source>
        <translation>Calendario diario</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3217"/>
        <source>Backup &amp;Journal</source>
        <translatorcomment>espero no se contradiga</translatorcomment>
        <translation>Respaldar &amp;Diario</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3066"/>
        <source>Online Users &amp;Guide</source>
        <translation>&amp;Guía del Usuario (en línea)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="441"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3002"/>
        <source>&amp;About OSCAR</source>
        <translation>&amp;Acerca de OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3071"/>
        <source>&amp;Frequently Asked Questions</source>
        <translation>Preguntas &amp;Frecuentes</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3076"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation>&amp;Limpieza Automática de Oximetría</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3081"/>
        <source>Change &amp;User</source>
        <translation>Cambiar &amp;Usuario</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3086"/>
        <source>Purge &amp;Current Selected Day</source>
        <translation>&amp;Purgar día actualmente seleccionado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3097"/>
        <source>Right &amp;Sidebar</source>
        <translation>Acti&amp;var panel derecho</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3189"/>
        <source>Daily Sidebar</source>
        <translation>Barra lateral diaria</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3108"/>
        <source>View S&amp;tatistics</source>
        <translation>Ver Es&amp;tadísticas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1320"/>
        <source>Navigation</source>
        <translation>Navegación</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1751"/>
        <source>Bookmarks</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2746"/>
        <source>Records</source>
        <translation>Registros</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2809"/>
        <source>Exp&amp;ort Data</source>
        <translation>Exp&amp;ortar datos</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1347"/>
        <source>Profiles</source>
        <translation>Perfiles</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2894"/>
        <source>Purge Oximetry Data</source>
        <translation>Purga de datos de oximetría</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2900"/>
        <source>Purge ALL Machine Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2933"/>
        <source>&amp;Import CPAP Card Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2959"/>
        <source>Show Daily view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2970"/>
        <source>Show Overview view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3013"/>
        <source>Maximize window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3032"/>
        <source>Reset Graph &amp;Heights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3035"/>
        <source>Reset sizes of graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3100"/>
        <source>Show Right Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3111"/>
        <source>View Statistics</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3114"/>
        <source>Show Statistics view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3122"/>
        <source>Import &amp;ZEO Data</source>
        <translation>Importar Datos de &amp;ZEO</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3127"/>
        <source>Import &amp;Dreem Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3132"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation>Importar Datos de REMstar y Serie &amp;M</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3137"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation>Glosario de &amp;Términos de Transtornos del Sueño</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3142"/>
        <source>Change &amp;Language</source>
        <translation>Cambiar &amp;Idioma</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3147"/>
        <source>Change &amp;Data Folder</source>
        <translation>Cambiar &amp;Directorio de Datos</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3152"/>
        <source>Import &amp;Somnopose Data</source>
        <translation>Importar Datos de &amp;Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3157"/>
        <source>Import &amp;Viatom Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3162"/>
        <source>Current Days</source>
        <translation>Días Actuales</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3255"/>
        <source>System Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3266"/>
        <source>Show &amp;Pie Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3269"/>
        <source>Show Pie Chart on Daily page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3272"/>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3280"/>
        <source>Standard graph order, good for CPAP, APAP, Bi-Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3285"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3288"/>
        <source>Advanced graph order, good for ASV, AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3299"/>
        <source>Show Personal Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="537"/>
        <location filename="../oscar/mainwindow.cpp" line="2343"/>
        <source>Welcome</source>
        <translation>Bienvenida</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="103"/>
        <source>&amp;About</source>
        <translation>&amp;Acerca de</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="948"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>Se ha bloqueado el acceso a Importar mientras hay recalculaciones en progreso.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1108"/>
        <source>Importing Data</source>
        <translation>Importando Datos</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="774"/>
        <location filename="../oscar/mainwindow.cpp" line="2010"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>Por favor espere, importando desde el(los) directorio(s) de respaldo...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="718"/>
        <location filename="../oscar/mainwindow.cpp" line="2384"/>
        <location filename="../oscar/mainwindow.cpp" line="2415"/>
        <location filename="../oscar/mainwindow.cpp" line="2546"/>
        <source>Import Problem</source>
        <translation>Problema de Importación</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="261"/>
        <source>Help Browser</source>
        <translatorcomment>¿Navegador de ayuda?</translatorcomment>
        <translation>Búsqueda de ayuda</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="519"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>Cargar perfil &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="718"/>
        <source>Couldn&apos;t find any valid Machine Data at

%1</source>
        <translation>No se pudieron encontrar datos de máquina válidos en 

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="875"/>
        <source>Please insert your CPAP data card...</source>
        <translation>Por favor inserte la tarjeta de datos del CPAP...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="952"/>
        <source>Import is already running in the background.</source>
        <translation>La importación ya está corriendo en segundo plano.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1000"/>
        <source>CPAP Data Located</source>
        <translation>Datos de CPAP encontrados</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1035"/>
        <source>Import Reminder</source>
        <translation>Importar Recordatorio</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1988"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation>Tenga en cuenta que esto podría resultar la pérdida de datos sí las copias de seguridad de OSCAR se han desactivado.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2752"/>
        <source>Export review is not yet implemented</source>
        <translation>Todavía no se ha implementado la revisión de exportación</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2762"/>
        <source>Would you like to zip this card?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2784"/>
        <location filename="../oscar/mainwindow.cpp" line="2836"/>
        <source>Choose where to save zip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2784"/>
        <location filename="../oscar/mainwindow.cpp" line="2836"/>
        <source>ZIP files (*.zip)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2813"/>
        <location filename="../oscar/mainwindow.cpp" line="2864"/>
        <source>Creating zip...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2855"/>
        <source>Calculating size...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2900"/>
        <source>Reporting issues is not yet implemented</source>
        <translation>Aún no se han implementado los problemas de presentación de informes</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1390"/>
        <source>Please open a profile first.</source>
        <translation>Por favor, abra primero un perfil.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2376"/>
        <source>Imported %1 ZEO session(s) from

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2382"/>
        <source>Already up to date with ZEO data at

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2384"/>
        <source>Couldn&apos;t find any valid ZEO CSV data at

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2407"/>
        <source>Imported %1 Dreem session(s) from

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2413"/>
        <source>Already up to date with Dreem data at

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2415"/>
        <source>Couldn&apos;t find any valid Dreem CSV data at

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2539"/>
        <source>Imported %1 oximetry session(s) from

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2544"/>
        <source>Already up to date with oximetry data at

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2546"/>
        <source>Couldn&apos;t find any valid data at

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2730"/>
        <source>%1&apos;s Journal</source>
        <translation>Diario de %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2732"/>
        <source>Choose where to save journal</source>
        <translation>Elija dónde guardar el diario</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2732"/>
        <source>XML Files (*.xml)</source>
        <translation>Archivos XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1395"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>El acceso a Preferencias ha sido bloqueado hasta que se complete la recalculación.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1452"/>
        <source>Updates are not yet implemented</source>
        <translation>Las actualizaciones aún no se han implementado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1584"/>
        <source>The FAQ is not yet implemented</source>
        <translation>FAQ aún no está implementado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1833"/>
        <location filename="../oscar/mainwindow.cpp" line="1860"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation>Si puede leer esto, el comando de reinicio no funcionó. Tendrá que hacerlo usted mismo manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1985"/>
        <source>Are you sure you want to rebuild all CPAP data for the following machine:

</source>
        <translation>¿Está seguro de que desea reconstruir todos los datos de CPAP para el siguiente equipo?:

</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1995"/>
        <source>For some reason, OSCAR does not have any backups for the following machine:</source>
        <translation>Por alguna razón, OSCAR no tiene ninguna copia de seguridad para el siguiente equipo:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2072"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s machine database for the following machine:&lt;/p&gt;</source>
        <translation>Está a punto de &lt;font size=+2&gt;borrar la base de datos&lt;/font&gt; de la máquina OSCAR para la siguiente máquina:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2130"/>
        <source>A file permission error casued the purge process to fail; you will have to delete the following folder manually:</source>
        <translation>Un error de permiso de archivo hizo que el proceso de purga fallara; tendrá que borrar la siguiente carpeta manualmente:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2180"/>
        <source>No help is available.</source>
        <translation>No hay ayuda disponible.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2645"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>Está usted seguro de borrar los datos de oximetría para %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2647"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;¡Tenga en cuenta que no puede deshacer esta operación!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2676"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>Seleccione primero un día con datos de oximetría válidos en la Vista por Día.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="714"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>Se importó(aron) %1 sesión(es) de CPAP desde

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="714"/>
        <location filename="../oscar/mainwindow.cpp" line="2376"/>
        <location filename="../oscar/mainwindow.cpp" line="2407"/>
        <location filename="../oscar/mainwindow.cpp" line="2539"/>
        <source>Import Success</source>
        <translation>Importación Exitosa</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="716"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation>Datos de CPAP al corriente en

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="716"/>
        <location filename="../oscar/mainwindow.cpp" line="2382"/>
        <location filename="../oscar/mainwindow.cpp" line="2413"/>
        <location filename="../oscar/mainwindow.cpp" line="2544"/>
        <source>Up to date</source>
        <translation>Al corriente</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="882"/>
        <source>Choose a folder</source>
        <translation>Elija un directorio</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="944"/>
        <source>No profile has been selected for Import.</source>
        <translation>No se ha seleccionado ningún perfil para Importar.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="993"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>Una estructura de archivo %1 para un %2 fue encontrada en:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="995"/>
        <source>A %1 file structure was located at:</source>
        <translation>Una estructura de archivo %1 fue encontrada en:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="960"/>
        <source>Would you like to import from this location?</source>
        <translation>¿Desea usted importar desde esta ubicación?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="558"/>
        <source>%1 (Profile: %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1004"/>
        <source>Specify</source>
        <translation>Especifique</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1034"/>
        <source>Please remember to select the root folder or drive letter of your data card, and not a folder inside it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1502"/>
        <source>Choose where to save screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1502"/>
        <source>Image files (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1512"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>Hubo un error al guardar la captura de pantalla en el archivo &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1514"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>Captura de pantalla guardada en el archivo &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1576"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation>La Guía del usuario se abrirá en su navegador predeterminado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1997"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>Suponiendo que usted ha hecho &lt;i&gt;sus &lt;b&gt;propios&lt;/b&gt; respaldos para TODOS sus datos de CPAP&lt;/i&gt;, aún puede completar esta operación pero tendrá que restaurar desde sus respaldos manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1998"/>
        <source>Are you really sure you want to do this?</source>
        <translation>¿Está seguro de que quiere hacer ésto?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2013"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>Debido a que no hay respaldos internos desde donde reestablecer, tendrá que restaurar usted mismo.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2014"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this machine until you do)</source>
        <translation>¿Le gustaría importar desde sus propios respaldos ahora? (No habrá datos visibles para esta máquina hasta que así lo haga)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2063"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>Tenga en cuenta que, por precaución, la carpeta de copia de seguridad se dejará en su sitio.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2065"/>
        <source>OSCAR does not have any backups for this machine!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2066"/>
        <source>Unless you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your data for this machine&lt;/i&gt;, &lt;font size=+2&gt;you will lose this machine&apos;s data &lt;b&gt;permanently&lt;/b&gt;!&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2075"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>¿Está usted totalmente seguro que desea continuar?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2909"/>
        <source>OSCAR Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2437"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>Hubo un problema abriendo el Archivo de Bloques del SerieM: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2441"/>
        <source>MSeries Import complete</source>
        <translation>Importación de SerieM completa</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2452"/>
        <source>The Glossary will open in your default browser</source>
        <translation>El Glosario se abrirá en su navegador predeterminado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2500"/>
        <source>There was a problem opening Somnopose Data File: </source>
        <translation>Hubo un problema abriendo el Archivo de Datos Somnopose: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2504"/>
        <source>Somnopause Data Import complete</source>
        <translation>Importación de Datos de Somnopause completada</translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2019"/>
        <source>Auto-Fit</source>
        <translation>Ajuste automático</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2020"/>
        <source>Defaults</source>
        <translation>Predeterminados</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2021"/>
        <source>Override</source>
        <translation>Anular</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2022"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>El modo de escalado del eje Y, &apos;Auto-Fit&apos; para el escalado automático, &apos;Defaults&apos; para los ajustes según el fabricante, y &apos;Override&apos; para elegir el suyo propio.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2028"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>El valor mínimo del eje Y... Tenga en cuenta que este puede ser un número negativo si lo desea.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2029"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>El valor máximo del eje Y... Debe ser mayor que el Mínimo para trabajar.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2064"/>
        <source>Scaling Mode</source>
        <translation>Modo de escalado</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2086"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>Este botón reajusta el Mínimo y el Máximo para que coincidan con el Ajuste Automático</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>Editar Perfil de Usuario</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="70"/>
        <source>I agree to all the conditions above.</source>
        <translation>Acepto todas las condiciones arriba mencionadas.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="111"/>
        <source>User Information</source>
        <translation>Información de Usuario</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="137"/>
        <source>User Name</source>
        <translation>Nombre de Usuario</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="152"/>
        <source>Keep the kids out.. Nothing more.. This isn&apos;t meant to be uber security.</source>
        <translation>Mantenga a los niños fuera.. Nada más... Esto no se supone que sea seguridad total.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Password Protect Profile</source>
        <translation>Perfil Protegido por Contraseña</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="182"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="196"/>
        <source>...twice...</source>
        <translation>...otra vez...</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="213"/>
        <source>Locale Settings</source>
        <translation>Configuración Local</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="285"/>
        <source>Country</source>
        <translation>País</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="253"/>
        <source>TimeZone</source>
        <translation>Zona Horaria</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation>acerca de: blank</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="240"/>
        <source>DST Zone</source>
        <translation>Zona de Horario de Verano</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="320"/>
        <source>Personal Information (for reports)</source>
        <translation>Información Personal (para los reportes)</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="344"/>
        <source>First Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="354"/>
        <source>Last Name</source>
        <translation>Apellidos</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="364"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>Está totalmente bien mentir o saltarse esto, pero su edad aproximada es necesaria para mejorar la precisión de ciertos cálculos.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>D.O.B.</source>
        <translation>Fecha de Nacimiento.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="383"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;El género biológico (nacimiento) a veces es necesario para mejorar la precisión de unos pocos cálculos, no dude en dejar éste espacio en blanco y omitir cualquiera de ellos.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>Gender</source>
        <translation>Sexo</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="399"/>
        <source>Male</source>
        <translation>Masculino</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="404"/>
        <source>Female</source>
        <translation>Femenino</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="420"/>
        <source>Height</source>
        <translation>Altura</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="459"/>
        <source>Metric</source>
        <translation>Métrico</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="464"/>
        <source>English</source>
        <translation>Inglés</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="477"/>
        <source>Contact Information</source>
        <translation>Información de Contacto</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="504"/>
        <location filename="../oscar/newprofile.ui" line="779"/>
        <source>Address</source>
        <translation>Domicilio</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="521"/>
        <location filename="../oscar/newprofile.ui" line="810"/>
        <source>Email</source>
        <translation>Correo Electrónico</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="531"/>
        <location filename="../oscar/newprofile.ui" line="800"/>
        <source>Phone</source>
        <translation>Teléfono</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="576"/>
        <source>CPAP Treatment Information</source>
        <translation>Información del Tratamiento CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="603"/>
        <source>Date Diagnosed</source>
        <translation>Fecha de Diagnóstico</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="617"/>
        <source>Untreated AHI</source>
        <translation>IAH sin tratar</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="631"/>
        <source>CPAP Mode</source>
        <translation>Modo de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="639"/>
        <source>CPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="644"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="649"/>
        <source>Bi-Level</source>
        <translation>Bi-Nivel</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="654"/>
        <source>ASV</source>
        <translation>ASV: Ventilación Servoasistida Adaptativa</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="662"/>
        <source>RX Pressure</source>
        <translation>Presión Diagnosticada</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="708"/>
        <source>Doctors / Clinic Information</source>
        <translation>Información del Médico/Clínica</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="735"/>
        <source>Doctors Name</source>
        <translation>Nombre del Médico</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="752"/>
        <source>Practice Name</source>
        <translation>Nombre de la clínica</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="762"/>
        <source>Patient ID</source>
        <translation>ID de paciente</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="877"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="955"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="971"/>
        <source>&amp;Back</source>
        <translation>&amp;Anterior</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="987"/>
        <location filename="../oscar/newprofile.cpp" line="276"/>
        <location filename="../oscar/newprofile.cpp" line="285"/>
        <source>&amp;Next</source>
        <translation>&amp;Siguiente</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="63"/>
        <source>Select Country</source>
        <translation>Seleccione País</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="113"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP machines and related equipment.</source>
        <translation>Este software ha sido creado para asisitirlo a revisar los datos producidos por las máquinas de CPAP y otros equipos relacionados.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="118"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>POR FAVOR LEA CUIDADOSAMENTE</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>La exactitud de los datos mostrados no está y no puede ser garantizada.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="124"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>Cualquier reporte generado es EXCLUSIVAMENTE PARA USO PERSONAL y no es adecuado DE NINGÚN MODO para fines de apego al tratamiento o diagnóstico médico.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="131"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>El uso de este software es completamente bajo su propio riesgo.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Bienvenido al Reporter de Análisis de CPAP de Código Abierto</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation>OSCAR ha sido publicado libremente bajo la &lt;a href=&apos;qrc:/COPYING&apos;&gt;Licencia Pública GNU v3&lt;/a&gt;, y viene sin garantía, y sin NINGÚN reclamo de idoneidad para ningún propósito.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="119"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation>OSCAR está concebido simplemente como un visor de datos, y definitivamente no como un sustituto de la orientación médica competente de su médico.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="127"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation>Los autores no serán responsables de &lt;u&gt;cualquier cosa &lt;/u&gt; relacionada con el uso o mal uso de este software.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="134"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019 Nightowl Software</source>
        <translation>OSCAR es copyright &amp;copy;2011-2018 Mark Watkins y porciones &amp;copy;2019 Nightowl Software</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="157"/>
        <source>Please provide a username for this profile</source>
        <translation>Por favor proporcione un nombre de usuario para este perfiĺ</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="167"/>
        <source>Passwords don&apos;t match</source>
        <translation>Las contraseñas no coinciden</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Profile Changes</source>
        <translation>Cambio al perfil</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Accept and save this information?</source>
        <translation>¿Aceptar y guardar esta información?</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="274"/>
        <source>&amp;Finish</source>
        <translation>&amp;Finalizar</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="450"/>
        <source>&amp;Close this window</source>
        <translation>&amp;Cerrar esta Ventana</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>Intérvalo:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>Última Semana</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>Última Quincena</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>Último Mes</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>Último Bimestre</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>Último Trimestre</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>Último Semestre</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>Último Año</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>Todo</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="130"/>
        <source>Start:</source>
        <translation>Inicio:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="153"/>
        <source>End:</source>
        <translation>Fin:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="176"/>
        <source>Reset view to selected date range</source>
        <translation>Reinicializar vista al intérvalo seleccionado</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="195"/>
        <location filename="../oscar/overview.ui" line="244"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="225"/>
        <source>Toggle Graph Visibility</source>
        <translation>Activar Visibilidad del Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="260"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>Expanda esta lista para mostrar u ocultar los gráficos disponibles.</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="267"/>
        <source>Graphs</source>
        <translation>Gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="185"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation>Índice de
Perturbación
Respiratoria</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="187"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation>Índice de
Apnea-
Hipoapnea</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="193"/>
        <source>Usage</source>
        <translation>Uso</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="193"/>
        <source>Usage
(hours)</source>
        <translation>Uso
(horas)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="196"/>
        <source>Session Times</source>
        <translation>Horarios de la sesión</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="204"/>
        <source>Total Time in Apnea</source>
        <translation>Tiempo Total en Apnea</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="204"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>Tiempo Total en Apnea
(Minutos)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="239"/>
        <source>Body
Mass
Index</source>
        <translation>Índice de
Masa
Corporaĺ</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="240"/>
        <source>How you felt
(0-10)</source>
        <translation>¿Cómo se sintió?
(0-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="579"/>
        <source>Show all graphs</source>
        <translation>Mostrar todos los gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="592"/>
        <source>Hide all graphs</source>
        <translation>Ocultar todos los gráficos</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="14"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="37"/>
        <source>Oximeter Import Wizard</source>
        <translation>Asistente para la Importación de Datos desde el Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="768"/>
        <source>Skip this page next time.</source>
        <translation>Saltar esta pantalla la próxima vez.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="866"/>
        <source>Where would you like to import from?</source>
        <translation>¿Desde dónde le gustaría importar?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="921"/>
        <source>CMS50Fv3.7+/H/I, CMS50D+v4.6, Pulox PO-400/500</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="967"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation>Los usuarios de CMS50E/F, cuando importen directamente, no seleccionen cargar en su dispositivo hasta que OSCAR se lo pida.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1004"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sí está habilitado, OSCAR reajustará automáticamente el reloj interno de su CMS50 usando la hora actual de su computadora.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1036"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1077"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  because if something goes wrong before OSCAR saves your session, you can&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1106"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, old Contec oximeters will require you to use the device&apos;s menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sí no le molesta estar delante de una computadora funcionando toda la noche, esta opción le proporcionará una útil gráfica de pletismografía, la cual indica el ritmo cardiaco además de las lecturas normales de oximetría.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1154"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>Grabación adjunta a la computadora durante la noche (proporciona una pletismografía)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esta opción le permitirá importar datos desde archivos creados por el software de acompañamiento del oxímetro de pulso, tales como SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1193"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>Desde un archivo de datos guardado por otro programa, como SpO2Review</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1272"/>
        <source>Please connect your oximeter device</source>
        <translation>Por favor conecte su Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1319"/>
        <source>Please connect your oximeter device, turn it on, and enter the menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1345"/>
        <source>Press Start to commence recording</source>
        <translation>Presione Inicio para comenzar el registro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1391"/>
        <source>Show Live Graphs</source>
        <translation>Mostrar Gráficos en Vivo</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1422"/>
        <location filename="../oscar/oximeterimport.ui" line="1676"/>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1458"/>
        <source>SpO2 %</source>
        <translation>% SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1540"/>
        <source>Pulse Rate</source>
        <translation>Frecuencia de Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1632"/>
        <source>Multiple Sessions Detected</source>
        <translation>Múltiples sesiones detectadas</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1671"/>
        <source>Start Time</source>
        <translation>Hora de inicio</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1681"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1698"/>
        <source>Import Completed. When did the recording start?</source>
        <translation>Importación Completada. ¿Cuándó comenzó la grabación?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1722"/>
        <source>Day recording (normally would of) started</source>
        <translatorcomment>ding (normally would of) ??
</translatorcomment>
        <translation>Regitro diario comenzado</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1768"/>
        <source>Oximeter Starting time</source>
        <translation>Hora de inicio del Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1780"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>Quiero usar la hora registrada por el reloj interno de mi oxímetro.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1796"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP machine.</source>
        <translation>Inicié esta grabación del oxímetro al mismo (o casi) tiempo de una sesión de mi máquina CPAP.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1857"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nota: Sincronizar con el inicio de una sesión CPAP será siempre más preciso.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1878"/>
        <source>Choose CPAP session to sync to:</source>
        <translation>Elija con cuál sesión CPAP sincronizar:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1917"/>
        <location filename="../oscar/oximeterimport.ui" line="1956"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1998"/>
        <source>You can manually adjust the time here if required:</source>
        <translation>Puede ajustar manualmente la hora si es necesario:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2019"/>
        <source>HH:mm:ssap</source>
        <translatorcomment>ap?</translatorcomment>
        <translation>HH:mm:ssap</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2116"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2097"/>
        <source>&amp;Information Page</source>
        <translation>&amp;Hoja de información</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="823"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Make sure your correct oximeter type is selected otherwise import will fail.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Por favor, tenga en cuenta: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Asegúrese de que ha seleccionado el tipo de oxímetro correcto; de lo contrario, la importación fallará.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="910"/>
        <source>Select Oximeter Type:</source>
        <translation>Seleccionar Tipo de Oxímetro:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="926"/>
        <source>CMS50D+/E/F, Pulox PO-200/300</source>
        <translation>CMS50D+/E/F, Pulox PO-200/300</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="931"/>
        <source>ChoiceMMed MD300W1</source>
        <translation>Elección MMed MD300W1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1007"/>
        <source>Set device date/time</source>
        <translation>Ajustar fecha/hora del aparato</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1014"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Compruebe para habilitar la actualización del identificador del dispositivo para la próxima importación, lo que es útil para aquellos que tienen varios oxímetros por ahí.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1017"/>
        <source>Set device identifier</source>
        <translation>Ajustar el identificador del dispositivo</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1080"/>
        <source>Erase session after successful upload</source>
        <translation>Borrar sesión después de una carga exitosa</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1112"/>
        <source>Import directly from a recording on a device</source>
        <translation>Importación directa desde una grabación en un dispositivo</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1237"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;¿Recordaste importar tus sesiones de CPAP primero?&lt;br/&gt;&lt;/span&gt;Si lo olvida, no tendrá un tiempo válido para sincronizar esta sesión de oximetría.&lt;br/&gt;Para asegurar una buena sincronización entre los dispositivos, intente siempre iniciar ambos al mismo tiempo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1290"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>Si puede leer esto, es probable que su tipo de oxímetro esté mal configurado en las preferencias.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1648"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation>Por favor, elija el que desea importar a OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1825"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR necesita una hora de inicio para saber dónde guardar esta sesión de oximetría.&lt;/p&gt;&lt;p&gt;Seleccione una de las siguientes opciones::&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2135"/>
        <source>&amp;Retry</source>
        <translation>&amp;Reintentar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2154"/>
        <source>&amp;Choose Session</source>
        <translation>&amp;Escoger Sesión</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2173"/>
        <source>&amp;End Recording</source>
        <translation>&amp;Finalizar Registro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2192"/>
        <source>&amp;Sync and Save</source>
        <translation>&amp;Sincronizar y Guardar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2211"/>
        <source>&amp;Save and Finish</source>
        <translation>&amp;Guardar y Terminar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2230"/>
        <source>&amp;Start</source>
        <translation>&amp;Iniciar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="188"/>
        <source>Scanning for compatible oximeters</source>
        <translation>Buscando oxímetros compatibles</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="220"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>No se pudieron detectar oxímetros conectados.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="228"/>
        <source>Connecting to %1 Oximeter</source>
        <translation>Conectando al oxímetro %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="256"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>Renombrar este oxímetro de &apos;%1&apos; a &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="259"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation>El nombre del Oxímetro es diferente... Si sólo tienes uno y lo compartes entre perfiles, establece el mismo nombre en ambos perfiles.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="302"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation>&quot;%1&quot;, sesión %2</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="327"/>
        <source>Nothing to import</source>
        <translation>Nada que importar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="328"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>Su oxímetro no tenía ninguna sesión válida.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="329"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="332"/>
        <source>Waiting for %1 to start</source>
        <translation>Esperando que %1 comience</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="333"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation>Esperando a que el dispositivo inicie el proceso de carga....</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="335"/>
        <source>Select upload option on %1</source>
        <translation>Seleccione ĺa opción adecuada para realizar la descarga en %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="336"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>Necesita decirle a su oxímetro que empiece a enviar datos a la computadora.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="337"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translatorcomment>O bien enciendalo, vaya al menu setting</translatorcomment>
        <translation>Por favor, conecte su Oxímetro, entre en su menú y seleccione cargar para iniciar la transferencia de datos....</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="367"/>
        <source>%1 device is uploading data...</source>
        <translation>El dispositivo %1 está descargando información...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="368"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>Por favor espere hasta que la descarga desde el oxímetro finalice. No lo desconecte.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="387"/>
        <source>Oximeter import completed..</source>
        <translation>Importación desde el Oxímetro completada.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="411"/>
        <source>Select a valid oximetry data file</source>
        <translation>Seleccione un archivo válido con datos de oximetría</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="411"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>Archivos del Oxímetro (*.spo *.spor *.spo2 *.SpO2 *.dat)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="433"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation>Ningún módulo de oximetría podría analizar el archivo dado:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="478"/>
        <source>Live Oximetry Mode</source>
        <translation>Modo de oximetría en vivo</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="530"/>
        <source>Live Oximetry Stopped</source>
        <translation>La oximetría en vivo se detuvo</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="531"/>
        <source>Live Oximetry import has been stopped</source>
        <translation>Se ha detenido la importación de oximetría en vivo</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1091"/>
        <source>Oximeter Session %1</source>
        <translation>Sesión de Oxímetro %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1136"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation>OSCAR le ofrece la posibilidad de realizar un seguimiento de los datos de oximetría junto con los datos de las sesiones de CPAP, lo que puede proporcionarle una valiosa información sobre la eficacia del tratamiento con CPAP. También funcionará de forma autónoma con su oxímetro de pulso, lo que le permitirá almacenar, rastrear y revisar los datos grabados.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1147"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>Sí está intentando sincronizar la oximetría y los datos de CPAP, asegúrese de haber importado sus sesiones de CPAP antes de continuar!</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1150"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation>Para que OSCAR pueda localizar y leer directamente desde su dispositivo Oximeter, necesita asegurarse de que los controladores de dispositivo correctos (por ejemplo, USB a Serial UART) han sido instalados en su computadora. Para más información sobre esto, %1click aqui%2.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="460"/>
        <source>Oximeter not detected</source>
        <translation>Oxímetro no detectado</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="467"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation>No se pudo acceder al oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="481"/>
        <source>Starting up...</source>
        <translation>Iniciando...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="482"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>Sí aún puede leer esto después de algunos segundos, cancele e inténtelo de nuevo</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="529"/>
        <source>Live Import Stopped</source>
        <translation>Importación en vivo detenida</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="582"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%1 sesión(es) en %2, comenzando en %3</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="586"/>
        <source>No CPAP data available on %1</source>
        <translation>No hay datos de CPAP disponibles en %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="594"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="721"/>
        <source>Recording...</source>
        <translation>Registrando...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="728"/>
        <source>Finger not detected</source>
        <translation>Dedo no detectado</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="828"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>Quiero usar la hora registrada por mi computadora para esta sesión de oximetría en vivo.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="831"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>Necesito configurar la hora manualmente porque mi oxímetro no tiene reloj interno.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="843"/>
        <source>Something went wrong getting session data</source>
        <translation>Algo salió mal al obtener los datos de la sesión</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1132"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>Bienvenido al Asistente de Importación de Oxímetros</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1134"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>Los oxímetros de pulso son dispositivos médicos utilizados para medir la saturación de oxígeno en la sangre. Durante eventos de apnea prolongada y patrones de respiración anormales, los niveles de saturación de oxígeno en la sangre pueden disminuir significativamente y pueden indicar problemas que requieren atención médica.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1138"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation>OSCAR es actualmente compatible con los oxímetros seriales Contec CMS50D+, CMS50E, CMS50F y CMS50I.&lt;br/&gt;(Nota: La importación directa desde los modelos bluetooth es &lt;span style=&quot; font-weight:600;&quot;&gt;probablemente no&lt;/span&gt; es posible)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1140"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>Es posible que desee tener en cuenta, otras empresas, como Pulox, simplemente volver a clasificar a Contec CMS50 bajo nuevos nombres, como el Pulox PO-200, PO-300, PO-400. Estos también deberían funcionar.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1143"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>También puede leer de los archivos.dat del oxímetro Choice MMed MD300W1.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1145"/>
        <source>Please remember:</source>
        <translation>Por favor, recuerde:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1149"/>
        <source>Important Notes:</source>
        <translation>Notas importantes:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1152"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>Los dispositivos Contec CMS50D+ no tienen un reloj interno y no registran la hora de inicio. Si no tiene una sesión de CPAP a la que vincular una grabación, tendrá que introducir la hora de inicio manualmente una vez finalizado el proceso de importación.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1154"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>Incluso para los dispositivos con reloj interno, se recomienda acostumbrarse a iniciar los registros del oxímetro al mismo tiempo que las sesiones de CPAP, ya que los relojes internos de CPAP tienden a ir a la deriva con el tiempo, y no todos pueden ser reajustados fácilmente.</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translatorcomment>¿ap?</translatorcomment>
        <translation>d/MM/aa h:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation>R&amp;einicializar</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="160"/>
        <source>SpO2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="346"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation>&amp;Abrir archivo .spo/R</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation>&amp;Importación 
en serie</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation>Iniciar en &amp;vivo</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>Puerto Serie</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation>Volver a
&amp;buscar puertos</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="23"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="64"/>
        <source>&amp;Import</source>
        <translation>&amp;Importar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="154"/>
        <source>Combine Close Sessions </source>
        <translation>Combinar Sesiones Cerradas </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="164"/>
        <location filename="../oscar/preferencesdialog.ui" line="249"/>
        <location filename="../oscar/preferencesdialog.ui" line="752"/>
        <source>Minutes</source>
        <translation>Minutos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="184"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>Sesiones múltiples más cercanas que este valor serán mantenidas en el mimsmo día.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="239"/>
        <source>Ignore Short Sessions</source>
        <translation>Ignorar sesiones cortas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="266"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sesiones de duración menor a esta no serán mostradas&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="310"/>
        <source>Day Split Time</source>
        <translation>Hora de cambio de día</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="320"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>Sesiones comenzadas antes de esta hora van al día calendario previo.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="393"/>
        <source>Session Storage Options</source>
        <translation>Opciones de almacenamiento de sesión</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="483"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>Cree copias de seguridad de tarjetas SD durante la importación (¡apáguelo por su cuenta y riesgo!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="440"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>Comprimir Respaldos de tarjeta SD (primera importación más lenta, respaldos más pequeños)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="658"/>
        <source>&amp;CPAP</source>
        <translation>&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1301"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation>Considerar días con menos uso que este como incumplidos. Cuatro horas son generalmente consideradas como en cumplimiento.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1304"/>
        <source> hours</source>
        <translation> horas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="966"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the machine missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation>Habilitar/deshabilitar mejoras experimentales de detección de eventos.
Esto permite detectar eventos limítrofes, y algunos que la máquina pasó por alto.
Esta opción debe habilitarse antes de importar, de otro modo es requerida una purga.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1057"/>
        <source>Flow Restriction</source>
        <translation>Restricción de 
flujo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1098"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation>Porcentaje de restricción de fluje respecto al valor medio.
Un valor de 20% funciona bien para detectar apneas. </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1008"/>
        <location filename="../oscar/preferencesdialog.ui" line="1102"/>
        <location filename="../oscar/preferencesdialog.ui" line="1579"/>
        <location filename="../oscar/preferencesdialog.ui" line="1739"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1075"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the machine. They are &lt;span style=&quot; text-decoration: underline;&quot;&gt;not&lt;/span&gt; included in AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;La deteccióon personalizada es un método experimental para detectar eventos errados por la máquina. Estos &lt;span style=&quot; text-decoration: underline;&quot;&gt;no&lt;/span&gt; están incluidos en el IAH.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1119"/>
        <source>Duration of airflow restriction</source>
        <translation>Duración de la restricción de flujo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="998"/>
        <location filename="../oscar/preferencesdialog.ui" line="1122"/>
        <location filename="../oscar/preferencesdialog.ui" line="1596"/>
        <location filename="../oscar/preferencesdialog.ui" line="1684"/>
        <location filename="../oscar/preferencesdialog.ui" line="1713"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1158"/>
        <source>Event Duration</source>
        <translation>Duración del 
evento</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1145"/>
        <source>Allow duplicates near machine events.</source>
        <translation>Permitir duplicados cercanos a eventos de la máquina.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1223"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>Ajusta la cantidad de datos considerados para cada punto en el gráfico IAH/hora.
60 minutos por defecto. Se recomienda dejar este valor.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1227"/>
        <source> minutes</source>
        <translation> minutos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1266"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>Reinicializa el contador a cero al inicio de cada ventana de tiempo.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1269"/>
        <source>Zero Reset</source>
        <translation>Reinicializar a cero</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="705"/>
        <source>CPAP Clock Drift</source>
        <translation>Deriva del reloj del CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="501"/>
        <source>Do not import sessions older than:</source>
        <translation>No importar sesiones más antiguas que:</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="508"/>
        <source>Sessions older than this date will not be imported</source>
        <translation>Las sesiones más antiguas que esta fecha no serán importadas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="534"/>
        <source>dd MMMM yyyy</source>
        <translation>dd MMMM aaaa</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1112"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation>Mostra gráfico de pastel con el desglose de eventos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1135"/>
        <source>#1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1041"/>
        <source>#2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1021"/>
        <source>Resync Machine Detected Events (Experimental)</source>
        <translation>Resincronizar los eventos detectados por la máquina (EXPERIMENTAL)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1282"/>
        <source>User definable threshold considered large leak</source>
        <translation>Umbral definible por el usuario para la consideración de fugas grandes</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1285"/>
        <source> L/min</source>
        <translation> L/min</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1249"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>Mostrar/Ocultar la línea roja en el gráfico de fugas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1826"/>
        <location filename="../oscar/preferencesdialog.ui" line="1905"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1522"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;Oximetría</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1674"/>
        <source>SPO2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1736"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation>Porcentaje de caída en la saturación de oxígeno</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1729"/>
        <source>Pulse</source>
        <translation>Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1694"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>Cambio repentino en el pulso de al menos esta cantidad</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1586"/>
        <location filename="../oscar/preferencesdialog.ui" line="1616"/>
        <location filename="../oscar/preferencesdialog.ui" line="1697"/>
        <source> bpm</source>
        <translation> ppm</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1681"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>Duración mínima de la caída en la saturación de oxígeno</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1710"/>
        <source>Minimum duration of pulse change event.</source>
        <translation>Duración mínima del evento de cambio en el pulso.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1593"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>Fragmentos pequeños de datos de oximetría menores a esta cantidad serán descartados.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1961"/>
        <source>&amp;General</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1975"/>
        <source>General Settings</source>
        <translation>Configuración General</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2731"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>Los botones de navegación en la vista diaria se saltarán los días sin datos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2734"/>
        <source>Skip over Empty Days</source>
        <translation>Saltar días vacíos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1996"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation>Permite el uso de múltiples núcleos de CPU cuando estén disponibles para mejorar el rendimiento.
Afecta principalmente al importador.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2000"/>
        <source>Enable Multithreading</source>
        <translation>Habilitar Multithreading</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="574"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>Saltar la pantalla de inicio de sesión y cargar el perfil de usuario más reciente</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2721"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Éstas características han sido podadas. Volverán después.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1349"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>Cambios a los siguientes ajustes requieren un reinicio pero no una recalculación.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1352"/>
        <source>Preferred Calculation Methods</source>
        <translation>Métodos de cálculo preferidos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1381"/>
        <source>Middle Calculations</source>
        <translatorcomment>¡?</translatorcomment>
        <translation>Cálculo del centro</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1395"/>
        <source>Upper Percentile</source>
        <translation>Percentil superior</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1358"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation>Para consistencia, los usuarios de ResMed deben usar 95% aquí,
ya que es el único valor disponible en los días de sólo resumen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1409"/>
        <source>Median is recommended for ResMed users.</source>
        <translation>Se recomienda Mediana para los usuarios de ResMed.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1413"/>
        <location filename="../oscar/preferencesdialog.ui" line="1476"/>
        <source>Median</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1418"/>
        <source>Weighted Average</source>
        <translation>Promedio Ponderado</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1423"/>
        <source>Normal Average</source>
        <translation>Promedio</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1447"/>
        <source>True Maximum</source>
        <translation>Máximo Verdadero</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1452"/>
        <source>99% Percentile</source>
        <translation>Percentil 99%</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1388"/>
        <source>Maximum Calcs</source>
        <translation>Cálculo de máximo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="91"/>
        <source>Session Splitting Settings</source>
        <translation>Parámetros de partición de sesión</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="356"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Este ajuste debe utilizarse con precaución....&lt;/span&gt; Desactivarlo tiene consecuencias que implican la precisión de los días de sólo resumen, ya que ciertos cálculos sólo funcionan correctamente si se mantienen juntas las sesiones de sólo resumen que proceden de los registros de días individuales. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Usuarios de ResMed:&lt;/span&gt; El hecho de que nos parezca natural que el reinicio de la sesión de las 12 del mediodía sea el día anterior no significa que los datos de ResMed coincidan con los nuestros. El formato del índice STF.edf tiene serias debilidades que hacen que hacer esto no sea una buena idea.&lt;/p&gt;&lt;p&gt;Esta opción existe para pacificar a los que no les importa y quieren ver esto &amp;quot;fijo&amp;quot;  sin importar los costos, pero que sepas que tiene un costo. Si mantienes tu tarjeta SD todas las noches y la importas al menos una vez a la semana, no verás problemas con esto muy a menudo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="359"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>No dividir los días de resumen (Advertencia: ¡lea el consejo de la herramienta!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="568"/>
        <source>Memory and Startup Options</source>
        <translation>Opciones de memoria e inicio</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="610"/>
        <source>Pre-Load all summary data at startup</source>
        <translation>Precargar los datos de resumen al inicio</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="597"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esta configuración mantiene los datos de forma de onda y de evento en la memoria después del uso para acelerar los días de visita.&lt;/p&gt;&lt;p&gt;Esta opción no es realmente necesaria, ya que su sistema operativo también almacena en caché los archivos utilizados anteriormente.&lt;/p&gt;&lt;p&gt;Se recomienda dejarlo apagado, a menos que su ordenador tenga una tonelada de memoria.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="600"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation>Mantenga los datos de forma de onda/evento en la memoria</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="624"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reduce cualquier diálogo de confirmación sin importancia durante la importación.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="627"/>
        <source>Import without asking for confirmation</source>
        <translation>Importar sin pedir confirmación</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1187"/>
        <source>General CPAP and Related Settings</source>
        <translation>CPAP General y Ajustes Relacionados</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1196"/>
        <source>Enable Unknown Events Channels</source>
        <translation>Habilitar canales de eventos desconocidos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1327"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translation>IAH</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1332"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translation>IPR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1203"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation>Ventana de tiempo del gráfico IAH/Hora</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1259"/>
        <source>Preferred major event index</source>
        <translation>Lista de eventos importantes preferidos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1210"/>
        <source>Compliance defined as</source>
        <translation>Cumplimiento definido como</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1252"/>
        <source>Flag leaks over threshold</source>
        <translation>Fugas por encima del umbral</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="786"/>
        <source>Seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="732"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nota: ¡Ésto no es para correcciones de zona horaria! Asegúrese de que el reloj y la zona horaria de su sistema operativo estén configurados correctamente.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="779"/>
        <source>Hours</source>
        <translation>Horas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1443"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;El máximo real es el máximo del conjunto de datos.&lt;/p&gt;&lt;p&gt;El percentil 99% filtra los valores atípicos más raros.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1461"/>
        <source>Combined Count divided by Total Hours</source>
        <translation>Recuento combinado dividido por el total de horas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1466"/>
        <source>Time Weighted average of Indice</source>
        <translation>Promedio ponderado en el tiempo del Índice</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1471"/>
        <source>Standard average of indice</source>
        <translation>Promedio estándar del índice</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1402"/>
        <source>Culminative Indices</source>
        <translation>Índices Culminativos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="422"/>
        <source>Changing SD Backup compression options doesn&apos;t automatically recompress backup data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="432"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation>Comprima las copias de seguridad de ResMed (EDF) para ahorrar espacio en disco.
Los archivos EDF respaldados se almacenan en el formato.gz, 
que es común en las plataformas Mac y Linux.. 

OSCAR puede importar desde este directorio de backup comprimido de forma nativa... 
Para usarlo con ResScan se requiere que los archivos.gz se descompriman primero...</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="452"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation>Las siguientes opciones afectan a la cantidad de espacio en disco que utiliza OSCAR y tienen un efecto sobre el tiempo que tarda la importación.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="462"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation>Esto hace que los datos de OSCAR ocupen la mitad de espacio.
Pero hace que la importación y el cambio de día lleven más tiempo... 
Si tiene un equipo nuevo con un pequeño disco de estado sólido, esta es una buena opción.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="467"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation>Comprimir datos de sesión (hace que los datos de OSCAR sean más pequeños, pero el día cambia más lentamente.)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="474"/>
        <source>This maintains a backup of SD-card data for ResMed machines, 

ResMed S9 series machines delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation>Esto mantiene una copia de seguridad de los datos de la tarjeta SD en los equipos de ResMed, 

Los equipos de la serie S9 de ResMed eliminan los datos de alta resolución de más de 7 días, 
y datos gráficos de más de 30 días..

OSCAR puede guardar una copia de estos datos si alguna vez necesita reinstalarlos. 
(Altamente recomendado, a menos que te falte espacio en disco o no te preocupes por los datos del gráfico)

Traducción realizada con el traductor www.DeepL.com/Translator</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hace que el inicio de OSCAR sea un poco más lento, al pre-cargar todos los datos de resumen por adelantado, lo que acelera la navegación general y algunos otros cálculos más adelante.. &lt;/p&gt;&lt;p&gt;Sí tiene una gran cantidad de datos, puede que valga la pena mantenerla desactivada, pero sí lo que le gusta es ver &lt;span style=&quot; font-style:italic;&quot;&gt;siempre&lt;/span&gt; en el resumen, todos los datos totalizados deben cargarse de todos modos. &lt;/p&gt;&lt;p&gt;Tenga en cuenta que este ajuste no afecta a la forma de onda ni a los datos de eventos, que siempre se cargan según sea necesario.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="634"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data from any machine model that has not yet been tested by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="637"/>
        <source>Warn when importing data from an untested machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="644"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data that is somehow different from anything previously seen by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="647"/>
        <source>Warn when previously unseen data is encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="971"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation>Marcaje personalizado de eventos del usuario de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1018"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve machine detected event positioning.</source>
        <translation>Esta opción experimental intenta utilizar el sistema de señalización de eventos de OSCAR para mejorar el posicionamiento de eventos detectados por la máquina.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1783"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:7.84158pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method does &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP machine, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Sincroniza Datos Oximetría y CPAP&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Los datos CMS50 importados de SpO2Review (desde archivos.spoR) o el método de importación por puerto serie &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;NO&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;tienen la hora correcta para sincronizar.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;El modo de visualización en vivo (usando un cable serial) es una manera de lograr una sincronización precisa en los oximetros CMS50, pero no el contador de la deriva del reloj CPAP.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Sí inicia el modo de grabación de los Oxímetros en &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactamente &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;al mismo tiempo que enciende su máquina CPAP, ahora también puede lograr la sincronización. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;El proceso de importación en serie toma la hora de inicio de la primera sesión de CPAP de la noche anterior. (¡Recuerde importar primero sus datos de CPAP!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt; {3C?} {4.0/?} {3.?} {40/?} {1&quot;?} {2&apos;?} {7.84158p?} {400;?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {600;?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {50 ?} {2R?} {10p?} {600;?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {50 ?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {10p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?}</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2007"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation>Mostrar notificación de de retirada de tarjeta en el apagado de OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2014"/>
        <source>Always save screenshots in the OSCAR Data folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2068"/>
        <source>Automatically Check For Updates</source>
        <translation>Buscar actualizaciones automáticamente</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2085"/>
        <source>Check for new version every</source>
        <translation>Buscar por una nueva versión cada</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2092"/>
        <source>Sourceforge hosts this project for free.. Please be considerate of their resources..</source>
        <translation>SourceForge hospeda este proyecto de manera gratuita. Por favor sea considerado con sus recursos.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2108"/>
        <source>days.</source>
        <translation>días.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2173"/>
        <source>&amp;Check for Updates now</source>
        <translation>&amp;Buscar actualizaciones ahora</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2196"/>
        <source>Last Checked For Updates: </source>
        <translation>Ultima búsqueda de actualizaciones: </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2209"/>
        <source>TextLabel</source>
        <translation>Etiqueta de Texto</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2231"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If your interested in helping test new features and bugfixes early, click here.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;But please be warned this will sometimes mean breaky code..&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sí está interesado en ayudar a evaluar nuevas características y correcciones anticipadamente, haga clic aquí.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Pero considere que eso podría involucrar código inestable en algunas ocasiones.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2240"/>
        <source>I want to try experimental and test builds (Advanced users only please.)</source>
        <translation>Quiero probar versiones experimentales y de evaluación. (Sólo usuarios avanzados por favor.)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2264"/>
        <source>&amp;Appearance</source>
        <translation>A&amp;pariencia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2293"/>
        <source>Graph Settings</source>
        <translation>Ajustes de gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2309"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Qué pestaña abrir al cargar un perfil. (Nota: Por defecto será Perfil si OSCAR está configurado para no abrir un perfil al iniciar el programa).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2567"/>
        <source>Bar Tops</source>
        <translatorcomment>?</translatorcomment>
        <translation>Barras</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2572"/>
        <source>Line Chart</source>
        <translation>Líneas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2662"/>
        <source>Overview Linecharts</source>
        <translation>Gráficos de vista general</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ésto hace que el deplazamiento mientras se hace zoom sea más fácil en Touchpads bidireccionales sensibles.&lt;/p&gt;&lt;p&gt;El valor recomendado son 50 ms.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2489"/>
        <source>Scroll Dampening</source>
        <translation>Atenuación del desplazamiento</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2409"/>
        <source>Overlay Flags</source>
        <translation>Sobreponer indicadores</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2419"/>
        <source>Line Thickness</source>
        <translation>Grosor de línea</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2455"/>
        <source>The pixel thickness of line plots</source>
        <translation>Grosor del pixel en gráficos de línea</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2711"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>El caché Pixmap es una técnica de aceleración de gráficos. Puede causar problemas con el uso de fuentes durante el despliegue de gráficos en tu plataforma.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2046"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation>Intente cambiar esta opción desde la configuración predeterminada (Desktop OpenGL) si experimenta problemas de renderizado con los gráficos de OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2786"/>
        <source>Fonts (Application wide settings)</source>
        <translation>Fuentes (ajustes de ancho de aplicación)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2435"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>Método visual para mostrar los indicadores sobrepuestos de la forma de onda.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2440"/>
        <source>Standard Bars</source>
        <translation>Barras estándar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2393"/>
        <source>Graph Height</source>
        <translation>Altura del gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2586"/>
        <source>Default display height of graphs in pixels</source>
        <translation>Altura por defecto para el despliegue de gráficos en pixeles</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2501"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>Que tanto tiempo permanece visible la ventana de ayuda contextual.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1803"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1856"/>
        <location filename="../oscar/preferencesdialog.ui" line="1935"/>
        <source>Reset &amp;Defaults</source>
        <translation>Resetear &amp;valores predeterminados</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1869"/>
        <location filename="../oscar/preferencesdialog.ui" line="1948"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Advertencia: &lt;/span&gt;Sólo porque usted pueda, no significa que sea una buena práctica.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1882"/>
        <source>Waveforms</source>
        <translation>Formas de onda</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1662"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>Indicar cambios en estad. de oximetría</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1573"/>
        <source>Other oximetry options</source>
        <translation>Otras opciones Oximetría</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1623"/>
        <source>Flag SPO2 Desaturations Below</source>
        <translation>Desaturaciones SPO2 debajo de</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1606"/>
        <source>Discard segments under</source>
        <translation>Descartar segmentos en</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1643"/>
        <source>Flag Pulse Rate Above</source>
        <translation>Indic. Pulso Encima de</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1633"/>
        <source>Flag Pulse Rate Below</source>
        <translation>Indicar Latido Pulso Debajo de</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="802"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP machine. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation>Este cálculo requiere que los datos de fugas totales sean proporcionados por la CPAP. (Por ejemplo, PRS1, pero no ResMed, que ya los tiene)

Los cálculos de fugas involuntarias utilizados aquí son lineales, no modelan la curva de ventilación de la máscara.

Si utiliza unas cuantas máscaras diferentes, escoja en su lugar valores promedio. Todavía debería estar lo suficientemente cerca.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="809"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>Calcule las fugas involuntarias cuando no estén presentes</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="905"/>
        <source>4 cmH2O</source>
        <translation>4 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="915"/>
        <source>20 cmH2O</source>
        <translation>20 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="947"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>Nota: Se utiliza un método de cálculo lineal. La modificación de estos valores requiere un nuevo cálculo.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1193"/>
        <source>Show flags for machine detected events that haven&apos;t been identified yet.</source>
        <translation>Muestra los indicadores de eventos detectados por la máquina que aún no se han identificado.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2479"/>
        <source>Tooltip Timeout</source>
        <translation>Visibilidad del menú 
de ayuda contextual</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2559"/>
        <source>Graph Tooltips</source>
        <translation>Ayuda contextual del gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2445"/>
        <source>Top Markers</source>
        <translation>Marcadores superiores</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="577"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>Importador de CPAP de inicio automático después de abrir el perfil</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="617"/>
        <source>Automatically load last used profile on start-up</source>
        <translation>Cargar automáticamente el último perfil utilizado en la puesta en marcha</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="818"/>
        <source>Your masks vent rate at 20 cmH2O pressure</source>
        <translation>La tasa de ventilación de sus máscaras a una presión de 20 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="874"/>
        <source>Your masks vent rate at 4 cmH2O pressure</source>
        <translation>La tasa de ventilación de sus máscaras a una presión de 4 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1490"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed machines do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Debido a las limitaciones de diseño resumidas, los equipos de ResMed no admiten el cambio de estos ajustes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1543"/>
        <source>Oximetry Settings</source>
        <translation>Ajustes de Oximetría</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2299"/>
        <source>On Opening</source>
        <translation>En la Apertura</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2312"/>
        <location filename="../oscar/preferencesdialog.ui" line="2316"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2321"/>
        <location filename="../oscar/preferencesdialog.ui" line="2360"/>
        <source>Welcome</source>
        <translation>Bienvenida</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2326"/>
        <location filename="../oscar/preferencesdialog.ui" line="2365"/>
        <source>Daily</source>
        <translation>Diariamente</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2336"/>
        <location filename="../oscar/preferencesdialog.ui" line="2375"/>
        <source>Statistics</source>
        <translation>Estadísticas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2344"/>
        <source>Switch Tabs</source>
        <translation>Cambiar pestañas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2355"/>
        <source>No change</source>
        <translation>Sin cambios</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2383"/>
        <source>After Import</source>
        <translation>Después de la importación</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2681"/>
        <source>Other Visual Settings</source>
        <translation>Otras configuraciones visuales</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2687"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>Anti-Aliasing aplica suavizado a los gráficos... 
Ciertas parcelas se ven más atractivas con esto puesto. 
Ésto también afecta a los informes impresos.

Pruébelo y vea si le gusta.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2694"/>
        <source>Use Anti-Aliasing</source>
        <translation>Usar Anti-Aliasing</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2701"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>Hace que algunos gráficos luzcan más &quot;cuadrados&quot;.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2704"/>
        <source>Square Wave Plots</source>
        <translation>Gráficos de onda cuadrada</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2714"/>
        <source>Use Pixmap Caching</source>
        <translation>Usar caché Pixmap</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2724"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translatorcomment>elegantes, no coquetas</translatorcomment>
        <translation>Animaciones y cosas coquetas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2741"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>Permitir cambiar la escala del eje Y haciendo dobli clic en sus etiquetas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2744"/>
        <source>Allow YAxis Scaling</source>
        <translation>Permitir escalado del eje Y</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2040"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>Gráfico (requiere reiniciar)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2751"/>
        <source>Whether to include machine serial number on machine settings changes report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2754"/>
        <source>Include Serial Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2821"/>
        <source>Font</source>
        <translation>Fuente</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2840"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2859"/>
        <source>Bold  </source>
        <translation>Negritas  </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2881"/>
        <source>Italic</source>
        <translation>Itálica</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2894"/>
        <source>Application</source>
        <translation>Aplicación</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2958"/>
        <source>Graph Text</source>
        <translation>Texto del Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3019"/>
        <source>Graph Titles</source>
        <translation>Títulos del gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3080"/>
        <source>Big  Text</source>
        <translation>Texto grande</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3147"/>
        <location filename="../oscar/preferencesdialog.cpp" line="447"/>
        <location filename="../oscar/preferencesdialog.cpp" line="579"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3179"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3186"/>
        <source>&amp;Ok</source>
        <translation>&amp;Aceptar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="49"/>
        <source>Flag</source>
        <translation>Marcador</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Minor Flag</source>
        <translation>Indicador Menor</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Span</source>
        <translation>Extensión</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="52"/>
        <source>Always Minor</source>
        <translation>Siempre Menor</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="65"/>
        <source>No CPAP machines detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="66"/>
        <source>Will you be using a ResMed brand machine?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="275"/>
        <source>Never</source>
        <translation>Nunca</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="442"/>
        <location filename="../oscar/preferencesdialog.cpp" line="573"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="443"/>
        <location filename="../oscar/preferencesdialog.cpp" line="574"/>
        <source>Color</source>
        <translation>Color</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="445"/>
        <source>Flag Type</source>
        <translation>Tipo de indicador</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="446"/>
        <location filename="../oscar/preferencesdialog.cpp" line="578"/>
        <source>Label</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="463"/>
        <source>CPAP Events</source>
        <translation>Eventos CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="464"/>
        <source>Oximeter Events</source>
        <translation>Eventos de Oximetría</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="465"/>
        <source>Positional Events</source>
        <translation>Eventos Posicionales</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="466"/>
        <source>Sleep Stage Events</source>
        <translation>Eventos Etapa del Sueño</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="467"/>
        <source>Unknown Events</source>
        <translation>Eventos Desconocidos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="639"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>Haga doble clic para cambiar el nombre descriptivo de este canal.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="517"/>
        <location filename="../oscar/preferencesdialog.cpp" line="646"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>Haga doble clic para cambiar el color predeterminado de este gráfico de canal/indicador/datos.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="302"/>
        <location filename="../oscar/preferencesdialog.cpp" line="303"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1269"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1274"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2331"/>
        <location filename="../oscar/preferencesdialog.ui" line="2370"/>
        <location filename="../oscar/preferencesdialog.cpp" line="444"/>
        <location filename="../oscar/preferencesdialog.cpp" line="575"/>
        <source>Overview</source>
        <translation>Vista general</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="509"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>Haga doble clic para cambiar el nombre descriptivo del canal `%1&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="522"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>Si esta indicación tiene un gráfico de resumen dedicado.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="532"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>Aquí puede cambiar el tipo de indicador que se muestra para este evento</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="537"/>
        <location filename="../oscar/preferencesdialog.cpp" line="670"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>Esta es la etiqueta de forma abreviada para indicar este canal en la pantalla.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="543"/>
        <location filename="../oscar/preferencesdialog.cpp" line="676"/>
        <source>This is a description of what this channel does.</source>
        <translation>Ésta es una descripción de lo que hace este canal.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="576"/>
        <source>Lower</source>
        <translation>Inferior</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="577"/>
        <source>Upper</source>
        <translation>Superior</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="596"/>
        <source>CPAP Waveforms</source>
        <translation>Formas de onda CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="597"/>
        <source>Oximeter Waveforms</source>
        <translation>Formas de Onda Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="598"/>
        <source>Positional Waveforms</source>
        <translation>Formas de Onda Posicionales</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="599"/>
        <source>Sleep Stage Waveforms</source>
        <translation>Formas de onda de la etapa de sueño</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="655"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>Sí un desglose de esta forma de onda se muestra en resumen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="660"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Aquí puede ajustar el umbral &lt;b&gt;b&gt;bajo&lt;/b&gt; utilizado para ciertos cálculos en la forma de onda %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="665"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Aquí puede ajustar el umbral &lt;b&gt;superior&lt;/b&gt; utilizado para ciertos cálculos en la forma de onda %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="775"/>
        <source>Data Processing Required</source>
        <translation>Procesamiento de Datos Requerido</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="776"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Para aplicar estos cambios se requiere un procedimiento de re/descompresión de datos. Esta operación puede tardar un par de minutos en completarse.

¿Estás seguro de que quieres hacer estos cambios?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="784"/>
        <source>Data Reindex Required</source>
        <translation>Se requiere reindizar datos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="785"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Se requiere un procedimiento de reindexación de datos para aplicar estos cambios. Esta operación puede tardar un par de minutos en completarse.

¿Está seguro que quiere realizar estos cambios?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="791"/>
        <source>Restart Required</source>
        <translation>Reinicio Requerido</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1151"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation> Sí alguna vez necesita volver a importar estos datos (ya sea en OSCAR o en ResScan), estos datos no volverán a aparecer.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1152"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation> Sí necesita conservar espacio en disco, por favor recuerde realizar copias de seguridad manuales.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1153"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation> ¿Estás seguro de que quieres desactivar estas copias de seguridad?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1197"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation>Desactivar las copias de seguridad no es una buena idea, porque OSCAR las necesita para reconstruir la base de datos si se encuentran errores.

</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1198"/>
        <source>Are you really sure you want to do this?</source>
        <translation>¿Está verdaderamente seguro de querer realizar esto?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1149"/>
        <source>This may not be a good idea</source>
        <translation>Esto podría no ser una buena idea</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="73"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; machines due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed machines, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; Las capacidades avanzadas de división de sesiones de OSCAR no son posibles con &lt;b&gt;ResMed&lt;/b&gt; debido a una limitación en la forma en que se almacenan sus ajustes y datos de resumen, y por lo tanto han sido desactivados para este perfil.&lt;/p&gt;&lt;p&gt;En las máquinas de ResMed, los días &lt;b&gt;dividirán al mediodía.&lt;/b&gt; como en el software comercial de ResMed.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="792"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation>Uno o más de los cambios que ha hecho requerirá que esta aplicación se reinicie para que estos cambios entren en vigor.

¿Le gustaría hacer esto ahora?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1150"/>
        <source>ResMed S9 machines routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation>Las máquinas ResMed S9 rutinariamente eliminan ciertos datos de la tarjeta SD si son anteriores a 7 y 30 días (depende de la resolución).</translation>
    </message>
</context>
<context>
    <name>ProfileSelect</name>
    <message>
        <location filename="../oscar/profileselect.ui" line="14"/>
        <source>Select Profile</source>
        <translation>Seleccione perfil de usuario</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="115"/>
        <source>Search:</source>
        <translation>Buscar:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="191"/>
        <source>Start with the selected user profile.</source>
        <translation>Iniciar con el perfil de usuario seleccionado.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="215"/>
        <source>Create a new user profile.</source>
        <translation>Crear un nuevo perfil de usuario.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="235"/>
        <source>Choose a different OSCAR data folder.</source>
        <translation>Elija una carpeta de datos OSCAR diferente.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="274"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="310"/>
        <source>Click here if you didn&apos;t want to start OSCAR.</source>
        <translation>Haga clic aquí si no desea iniciar OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="383"/>
        <source>The current location of OSCAR data store.</source>
        <translation>Ubicación actual del almacén datos de OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="241"/>
        <source>&amp;Different Folder</source>
        <translation>&amp;Directorio distinto</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="287"/>
        <source>[version]</source>
        <translation>[versión]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="313"/>
        <source>&amp;Quit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="370"/>
        <source>Folder:</source>
        <translation>Directorio:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="389"/>
        <source>[data directory]</source>
        <translation>[directorio de datos]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="218"/>
        <source>New Profile</source>
        <translation>Nuevo perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="194"/>
        <source>&amp;Select User</source>
        <translation>&amp;Seleccionar Usuario</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="92"/>
        <source>Open Profile</source>
        <translation>Abrir perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="93"/>
        <source>Edit Profile</source>
        <translation>Editar perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="95"/>
        <source>Delete Profile</source>
        <translation>Eliminar perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="139"/>
        <location filename="../oscar/profileselect.cpp" line="220"/>
        <source>Enter Password for %1</source>
        <translation>Ingrese contraseña para %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="158"/>
        <location filename="../oscar/profileselect.cpp" line="342"/>
        <source>Incorrect Password</source>
        <translation>Contraseña incorrecta</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="160"/>
        <source>You entered the password wrong too many times.</source>
        <translation>Ha ingresado una contraseña equivocada demasiadas veces.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>Enter the word DELETE below to confirm.</source>
        <translatorcomment>I don know if changing here changes the string comparison also, so let DELETE as it is</translatorcomment>
        <translation>Escriba DELETE debajo para confirmar.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>You are about to destroy profile &apos;%1&apos;.</source>
        <translation>Está a punto de destruir el perfil &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>Sorry</source>
        <translation>Lo sentimos</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>Necesita escribir DELETE en mayúsculas.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="239"/>
        <source>You entered an incorrect password</source>
        <translation>Ha ingresado una contraseña incorrecta</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="242"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to delete it manually.</source>
        <translation>Si está tratando de eliminarlo porque olvidó la contraseña, tendrá que hacerlo manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="255"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Hubo un error al eliminar el directorio con el perfil, necesitará removerlo manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="259"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>El perfil &apos;%1&apos; fue eliminado con éxito</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="289"/>
        <source>Create new profile</source>
        <translation>Crear nuevo perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="326"/>
        <source>Enter Password</source>
        <translation>Ingrese contraseña</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="345"/>
        <source>You entered an Incorrect Password too many times. Exiting!</source>
        <translation>Ha ingresado una contraseña incorrecta demasiadas veces. ¡Saliendo!</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>Filtro:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>Reset filter to see all profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="55"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="184"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="199"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="216"/>
        <source>&amp;Open Profile</source>
        <translation>&amp;Abrir Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="227"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Editar perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="241"/>
        <source>&amp;New Profile</source>
        <translation>&amp;Nuevo Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="260"/>
        <source>Profile: None</source>
        <translation>Perfil: Ninguno</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="281"/>
        <source>Please select or create a profile...</source>
        <translation>Por favor, seleccione o cree un perfil....</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="332"/>
        <source>Destroy Profile</source>
        <translation>Destruir Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Ventilator Brand</source>
        <translation>Marca del Respirador</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Ventilator Model</source>
        <translation>Modelo del Respirador</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Other Data</source>
        <translation>Otros datos</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="94"/>
        <source>Last Imported</source>
        <translation>Última Import.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="95"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="135"/>
        <location filename="../oscar/profileselector.cpp" line="312"/>
        <source>%1, %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="170"/>
        <source>You must create a profile</source>
        <translation>Debe crear un perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="233"/>
        <location filename="../oscar/profileselector.cpp" line="363"/>
        <source>Enter Password for %1</source>
        <translation>Ingrese contraseña para %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="249"/>
        <location filename="../oscar/profileselector.cpp" line="382"/>
        <source>You entered an incorrect password</source>
        <translation>Ha ingresado una contraseña incorrecta</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Forgot your password?</source>
        <translation>¿Olvidó su contraseña?</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>Pregunta en los foros cómo restablecerlo, es bastante fácil.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="322"/>
        <source>Select a profile first</source>
        <translation>Seleccione primero un perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="385"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>Sí está intentando eliminar porque ha olvidado la contraseña, debe restablecerla o eliminar la carpeta del perfil manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="395"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>Está a punto de destruir el perfil &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="395"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>Piense cuidadosamente, ya que ésto borrará irremediablemente el perfil junto con todos &lt;b&gt;los datos de copia de seguridad&lt;/b&gt; almacenados bajo&lt;br/&gt;%2.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="395"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>Introduzca la palabra &lt;b&gt;ELIMINAR&lt;/b&gt; abajo (exactamente como se muestra) para confirmar.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="413"/>
        <source>DELETE</source>
        <translation>ELIMINAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="414"/>
        <source>Sorry</source>
        <translation>Lo sentimos</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="414"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>Necesita escribir ELIMINAR en mayúsculas.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="427"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Hubo un error al eliminar el directorio con el perfil, necesitará removerlo manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="431"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>El perfil &apos;%1&apos; fue eliminado con éxito</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="441"/>
        <source>PB</source>
        <translation>RP</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="461"/>
        <source>Summaries:</source>
        <translation>Sumarios:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="462"/>
        <source>Events:</source>
        <translation>Eventos:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="463"/>
        <source>Backups:</source>
        <translation>Copias:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="475"/>
        <location filename="../oscar/profileselector.cpp" line="515"/>
        <source>Hide disk usage information</source>
        <translation>Oculttar información sobre uso del disco</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="478"/>
        <source>Show disk usage information</source>
        <translation>Mostrar información sobre el uso del disco</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="496"/>
        <source>Name: %1, %2</source>
        <translation>Nombre: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="499"/>
        <source>Phone: %1</source>
        <translation>Teléfono: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="502"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="505"/>
        <source>Address:</source>
        <translation>Domicilio:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="508"/>
        <source>No profile information given</source>
        <translation>No se ha facilitado información del perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="511"/>
        <source>Profile: %1</source>
        <translation>Perfil: %1</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="56"/>
        <source>Abort</source>
        <translation>Abortar</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1246"/>
        <source>No Data</source>
        <translation>Sin datos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="828"/>
        <source>On</source>
        <translation>Activado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="829"/>
        <source>Off</source>
        <translation>Desactivado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="643"/>
        <source>&quot;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="644"/>
        <source>ft</source>
        <translation>pie(s)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="645"/>
        <source>lb</source>
        <translation>libra(s)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="646"/>
        <source>oz</source>
        <translation>onzas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="647"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="648"/>
        <source>cmH2O</source>
        <translation>cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="210"/>
        <source>Med.</source>
        <translation>Med.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="228"/>
        <source>Min: %1</source>
        <translation>Min: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="259"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="269"/>
        <source>Min: </source>
        <translation>Min: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="264"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="274"/>
        <source>Max: </source>
        <translation>Max: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="278"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="282"/>
        <source>%1: </source>
        <translation>%1: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="286"/>
        <source>???: </source>
        <translation>???: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="293"/>
        <source>Max: %1</source>
        <translation>Max: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="299"/>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2 días): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="301"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 día): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="359"/>
        <source>% in %1</source>
        <translation>% en %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="365"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="692"/>
        <location filename="../oscar/SleepLib/common.cpp" line="649"/>
        <source>Hours</source>
        <translation>Horas</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="371"/>
        <source>Min %1</source>
        <translation>Mín %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="677"/>
        <source>
Hours: %1</source>
        <translation>
Horas: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="743"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 uso bajo, %2 ningún uso, fuera de %3 días (%4% cumplimiento.) Duración: %5 / %6 / %7</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="824"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>Sesiones: %1 / %2 / %3 Duración: %4 / %5 / %6 Más largo:%7 / %8 / %9</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="943"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
Longitud: %3
Inicio: %2
</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="945"/>
        <source>Mask On</source>
        <translation>Poner Mascarilla</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="945"/>
        <source>Mask Off</source>
        <translation>Quitar la mascarilla</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="956"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
Longitud: %3
Inicio: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1127"/>
        <source>TTIA:</source>
        <translation>TTIA:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1140"/>
        <source>
TTIA: %1</source>
        <translation>
TTIA: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1241"/>
        <source>%1 %2 / %3 / %4</source>
        <translation>%1 %2 / %3 / %4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="659"/>
        <source>bpm</source>
        <translatorcomment>pulsaciones por minuto/ ¿latidos por minuto?</translatorcomment>
        <translation>ppm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="664"/>
        <source>?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="666"/>
        <source>Severity (0-1)</source>
        <translation>Severidad (0-1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="670"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9036"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="671"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="674"/>
        <source>Please Note</source>
        <translation>Por favor considere</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="677"/>
        <source>Compliance Only :(</source>
        <translation>Únicamente cumplimiento :(</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="678"/>
        <source>Graphs Switched Off</source>
        <translation>Gráficos desactivados</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="679"/>
        <source>Summary Only :(</source>
        <translation>Únicamente resumen :(</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="680"/>
        <source>Sessions Switched Off</source>
        <translation>Sesiones desactivadas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="684"/>
        <source>&amp;Yes</source>
        <translation>&amp;Sí</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source>&amp;No</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="687"/>
        <source>&amp;Destroy</source>
        <translation>&amp;Destruir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="688"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardad</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>BMI</source>
        <translatorcomment>índice de masa corporal</translatorcomment>
        <translation>IMC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="306"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="692"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>Zombie</source>
        <translation>Zombi</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="693"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Pulse Rate</source>
        <translation>Frecuencia de Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="694"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>SpO2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="695"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>Plethy</source>
        <translation>Pleti</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="696"/>
        <source>Pressure</source>
        <translation>Presión</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="698"/>
        <source>Daily</source>
        <translation>Vista por día</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="699"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="700"/>
        <source>Overview</source>
        <translation>Vista general</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="701"/>
        <source>Oximetry</source>
        <translation>Oximetría</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="703"/>
        <source>Oximeter</source>
        <translation>Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="704"/>
        <source>Event Flags</source>
        <translation>Indic. Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="707"/>
        <source>Default</source>
        <translation>Predeterminado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="710"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8958"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="89"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="711"/>
        <source>BiPAP</source>
        <translation>BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="712"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8961"/>
        <source>Bi-Level</source>
        <translation>Bi-Nivel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="713"/>
        <source>EPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="714"/>
        <source>Min EPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="715"/>
        <source>Max EPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="716"/>
        <source>IPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="719"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="90"/>
        <source>APAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="720"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8963"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="96"/>
        <source>ASV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="721"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>AVAPS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="722"/>
        <source>ST/ASV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="724"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9021"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9048"/>
        <source>Humidifier</source>
        <translation>Humidificador</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="726"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>H</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="727"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>OA</source>
        <translation>AO</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="728"/>
        <source>A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="729"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>CA</source>
        <translation>AC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="730"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>FL</source>
        <translation>LF</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="732"/>
        <source>LE</source>
        <translatorcomment>evento de fuga</translatorcomment>
        <translation>EF</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="733"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>EP</source>
        <translatorcomment>Resoplido</translatorcomment>
        <translation>RS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="734"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>VS</source>
        <translatorcomment>ronquido vibratorio</translatorcomment>
        <translation>RV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="736"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>VS2</source>
        <translation>RV2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="737"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>RERA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="738"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8949"/>
        <source>PP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="739"/>
        <source>P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="740"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>RE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="741"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>NR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="742"/>
        <source>NRI</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="743"/>
        <source>O2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="744"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8966"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>PC</source>
        <translation>CP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="745"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="746"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="747"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="749"/>
        <source>PS</source>
        <translation>SP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="750"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>AHI</source>
        <translation>IAH</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="751"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>RDI</source>
        <translation>IPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="752"/>
        <source>AI</source>
        <translation>IA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="753"/>
        <source>HI</source>
        <translation>IH</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="754"/>
        <source>UAI</source>
        <translation>IANC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="755"/>
        <source>CAI</source>
        <translatorcomment>Índice de vía aérea despejada</translatorcomment>
        <translation>IVAD</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="756"/>
        <source>FLI</source>
        <translation>ILF</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="758"/>
        <source>REI</source>
        <translation>IRE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="759"/>
        <source>EPI</source>
        <translation>IRS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="793"/>
        <location filename="../oscar/SleepLib/journal.cpp" line="30"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="717"/>
        <source>Min IPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="191"/>
        <source>Built with Qt %1 on %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="193"/>
        <source>Operating system:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="194"/>
        <source>Graphics Engine:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="195"/>
        <source>Graphics Engine type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="201"/>
        <source>App key:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="637"/>
        <source>Software Engine</source>
        <translation>Motor de software</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="638"/>
        <source>ANGLE / OpenGLES</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="639"/>
        <source>Desktop OpenGL</source>
        <translation>Escritorio OpenGL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="641"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="642"/>
        <source> cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="650"/>
        <source>Minutes</source>
        <translation>Minutos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="651"/>
        <source>Seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="652"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="653"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="654"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="655"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="656"/>
        <source>Events/hr</source>
        <translation>Eventos/hora</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="658"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="661"/>
        <source>Litres</source>
        <translation>Litros</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="662"/>
        <source>ml</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="663"/>
        <source>Breaths/min</source>
        <translation>Inspiraciones/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="667"/>
        <source>Degrees</source>
        <translation>Grados</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="672"/>
        <source>Information</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="673"/>
        <source>Busy</source>
        <translation>Ocupado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="718"/>
        <source>Max IPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="731"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="761"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>PB</source>
        <translation>RP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="765"/>
        <source>IE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="766"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Insp. Time</source>
        <translation>Tiempo Insp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="767"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Exp. Time</source>
        <translation>Tiempo Exp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="768"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Resp. Event</source>
        <translation>Evento Resp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="769"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Flow Limitation</source>
        <translation>Limitación de flujo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="770"/>
        <source>Flow Limit</source>
        <translation>Límite de flujo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="771"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SensAwake</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="772"/>
        <source>Pat. Trig. Breath</source>
        <translation>Pat. Trig. Breath</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="773"/>
        <source>Tgt. Min. Vent</source>
        <translation>Vent. Min. Objetivo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="774"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Target Vent.</source>
        <translation>Vent. Objetivo.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="775"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Minute Vent.</source>
        <translation>Vent. Minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="776"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>Tidal Volume</source>
        <translation>Volumen corriente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="777"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Resp. Rate</source>
        <translation>Frec. Resp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="778"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Snore</source>
        <translation>Ronquido</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="779"/>
        <source>Leak</source>
        <translation>Fuga</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="780"/>
        <source>Leaks</source>
        <translation>Fugas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="783"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Total Leaks</source>
        <translation>Fugas totales</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="784"/>
        <source>Unintentional Leaks</source>
        <translation>Fugas accidentales</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="785"/>
        <source>MaskPressure</source>
        <translation>PresiónMascarilla</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="786"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="212"/>
        <source>Flow Rate</source>
        <translation>Tasa de flujo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="787"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Sleep Stage</source>
        <translation>Estado del sueño</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="788"/>
        <source>Usage</source>
        <translation>Uso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="789"/>
        <source>Sessions</source>
        <translation>Sesiones</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="790"/>
        <source>Pr. Relief</source>
        <translation>Alivio de Presión</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="676"/>
        <source>No Data Available</source>
        <translation>Sin información disponible</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="792"/>
        <source>Bookmarks</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="796"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8953"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8955"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="85"/>
        <source>Mode</source>
        <translation>Modo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="797"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="798"/>
        <source>Brand</source>
        <translation>Marca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="799"/>
        <source>Serial</source>
        <translation># de Serie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="800"/>
        <source>Series</source>
        <translation>Serie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="801"/>
        <source>Machine</source>
        <translation>Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="802"/>
        <source>Channel</source>
        <translation>Canal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="803"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="807"/>
        <source>Motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="809"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="810"/>
        <source>DOB</source>
        <translation>Fecha de Nacimiento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="811"/>
        <source>Phone</source>
        <translation>Teléfono</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="812"/>
        <source>Address</source>
        <translation>Domicilio</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="813"/>
        <source>Email</source>
        <translation>Correo Electrónico</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="814"/>
        <source>Patient ID</source>
        <translation>ID de paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="815"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="817"/>
        <source>Bedtime</source>
        <translation>Hora de dormir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="818"/>
        <source>Wake-up</source>
        <translation>Hora de levantarse</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="819"/>
        <source>Mask Time</source>
        <translation>Tiempo de mascarilla</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="820"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="124"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="821"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="822"/>
        <source>Ready</source>
        <translation>Listo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="824"/>
        <source>First</source>
        <translation>Primero</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="825"/>
        <source>Last</source>
        <translation>Último</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="826"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>Start</source>
        <translation>Inicio</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="827"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="313"/>
        <source>End</source>
        <translation>Final</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="830"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="831"/>
        <source>No</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="833"/>
        <source>Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="834"/>
        <source>Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="835"/>
        <source>Med</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="837"/>
        <source>Average</source>
        <translation>Promedio</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="838"/>
        <source>Median</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="214"/>
        <location filename="../oscar/SleepLib/common.cpp" line="839"/>
        <source>Avg</source>
        <translation>Prom.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="212"/>
        <location filename="../oscar/SleepLib/common.cpp" line="840"/>
        <source>W-Avg</source>
        <translation>Prom-Pond.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="794"/>
        <source>Non Data Capable Machine</source>
        <translation>Máquina sin capacidad de registro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="795"/>
        <source>Your Philips Respironics CPAP machine (Model %1) is unfortunately not a data capable model.</source>
        <translation>Desafortunadamente el modelo de su CPAP Philips Respironics (Modelo %1) no tiene capacidad para el registro de datos.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="684"/>
        <source>Getting Ready...</source>
        <translation>Preparándose....</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="807"/>
        <source>Machine Unsupported</source>
        <translation>Máquina no soportada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="808"/>
        <source>Sorry, your Philips Respironics CPAP machine (Model %1) is not supported yet.</source>
        <translation>Lo sentimos, su máquina de CPAP de Philips Respironics (modelo %1) aún no es compatible.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="796"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this machine.</source>
        <translation>Lamento informar que OSCAR sólo puede rastrear horas de uso y ajustes muy básicos para esta máquina.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="708"/>
        <source>Scanning Files...</source>
        <translation>Escaneo de archivos....</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="716"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="592"/>
        <source>Importing Sessions...</source>
        <translation>Importacion  de Sesiones...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="721"/>
        <source>Finishing up...</source>
        <translation>Terminado...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="734"/>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="84"/>
        <source>Untested Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="735"/>
        <source>Your Philips Respironics %1 (%2) generated data that OSCAR has never seen before.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="736"/>
        <source>The imported data may not be entirely accurate, so the developers would like a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make sure OSCAR is handling the data correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="809"/>
        <source>The developers needs a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make it work with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="831"/>
        <source>Machine Untested</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="832"/>
        <source>Your Philips Respironics CPAP machine (Model %1) has not been tested yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="833"/>
        <source>It seems similar enough to other machines that it might work, but the developers would like a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make sure it works with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8957"/>
        <source>CPAP-Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8959"/>
        <source>AutoCPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8960"/>
        <source>Auto-Trial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8962"/>
        <source>AutoBiLevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8964"/>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8965"/>
        <source>S/T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8967"/>
        <source>S/T - AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8968"/>
        <source>PC - AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8994"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8996"/>
        <source>Flex Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8995"/>
        <source>Whether Flex settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9004"/>
        <source>Amount of time it takes to transition from EPAP to IPAP, the higher the number the slower the transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9010"/>
        <source>Rise Time Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9011"/>
        <source>Whether Rise Time settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9012"/>
        <source>Rise Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9028"/>
        <source>Humidification Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9029"/>
        <source>PRS1 Humidification Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9030"/>
        <source>Humid. Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9032"/>
        <source>Fixed (Classic)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9033"/>
        <source>Adaptive (System One)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9034"/>
        <source>Heated Tube</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9035"/>
        <source>Passover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9040"/>
        <source>Tube Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9041"/>
        <source>PRS1 Heated Tube Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9042"/>
        <source>Tube Temp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9074"/>
        <source>Tubing Type Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9075"/>
        <source>Whether tubing type settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9076"/>
        <source>Tube Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9083"/>
        <source>Mask Resistance Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9084"/>
        <source>Whether mask resistance settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9085"/>
        <source>Mask Res. Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9120"/>
        <source>Whether or not machine shows AHI via built-in display.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9128"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9130"/>
        <source>Ramp Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9129"/>
        <source>Type of ramp curve to use.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9132"/>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9133"/>
        <source>SmartRamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9137"/>
        <source>Backup Breath Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9138"/>
        <source>The kind of backup breath rate in use: none (off), automatic, or fixed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9139"/>
        <source>Breath Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9143"/>
        <source>Fixed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9147"/>
        <source>Fixed Backup Breath BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9148"/>
        <source>Minimum breaths per minute (BPM) below which a timed breath will be initiated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9149"/>
        <source>Breath BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9154"/>
        <source>Timed Inspiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9155"/>
        <source>The time that a timed breath will provide IPAP before transitioning to EPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9156"/>
        <source>Timed Insp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9161"/>
        <source>Auto-Trial Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9162"/>
        <source>The number of days in the Auto-CPAP trial period, after which the machine will revert to CPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9163"/>
        <source>Auto-Trial Dur.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9168"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9170"/>
        <source>EZ-Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9169"/>
        <source>Whether or not EZ-Start is enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9177"/>
        <source>Variable Breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9178"/>
        <source>UNCONFIRMED: Possibly variable breathing, which are periods of high deviation from the peak inspiratory flow trend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9201"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9203"/>
        <source>Peak Flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9202"/>
        <source>Peak flow during a 2-minute interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9049"/>
        <source>PRS1 Humidifier Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9050"/>
        <source>Humid. Lvl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9056"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9057"/>
        <source>Mask Resistance Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9058"/>
        <source>Mask Resist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9066"/>
        <source>Hose Diam.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9069"/>
        <source>15mm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9068"/>
        <source>22mm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="699"/>
        <source>Backing Up Files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8971"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8973"/>
        <source>Flex Mode</source>
        <translation>Modo Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8972"/>
        <source>PRS1 pressure relief mode.</source>
        <translation>Modo de alivio de presión PRS1.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8976"/>
        <source>C-Flex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8977"/>
        <source>C-Flex+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8978"/>
        <source>A-Flex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8979"/>
        <source>P-Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8980"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9003"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9005"/>
        <source>Rise Time</source>
        <translation>Tiempo de ascenso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8981"/>
        <source>Bi-Flex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8986"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8988"/>
        <source>Flex Level</source>
        <translation>Nivel de Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8987"/>
        <source>PRS1 pressure relief setting.</source>
        <translation>Ajuste de alivio de presión PRS1.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9019"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>Humidifier Status</source>
        <translation>Estado del humidificador</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9020"/>
        <source>PRS1 humidifier connected?</source>
        <translation>¿Está el humidificador PRS1 conectado?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9023"/>
        <source>Disconnected</source>
        <translation>Desconectado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9024"/>
        <source>Connected</source>
        <translation>Conectado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9064"/>
        <source>Hose Diameter</source>
        <translation>Diámetro de manguera</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9065"/>
        <source>Diameter of primary CPAP hose</source>
        <translation>Diámetro de la manguera primaria del CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9070"/>
        <source>12mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9092"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9094"/>
        <source>Auto On</source>
        <translation>Encendido automático</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9093"/>
        <source>A few breaths automatically starts machine</source>
        <translation>Unas cuantas inspiraciones activan la máquina automáticamente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9101"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9103"/>
        <source>Auto Off</source>
        <translation>Apagado automático</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9102"/>
        <source>Machine automatically switches off</source>
        <translation>La máquina se apaga automáticamente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9110"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9112"/>
        <source>Mask Alert</source>
        <translation>Alerta de mascarilla</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9111"/>
        <source>Whether or not machine allows Mask checking.</source>
        <translation>Define si se permite o no el chequeo de mascarilla.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9119"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9121"/>
        <source>Show AHI</source>
        <translation>Mostrar IAH</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9186"/>
        <source>Breathing Not Detected</source>
        <translation>Respiración No Detectada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9187"/>
        <source>A period during a session where the machine could not detect flow.</source>
        <translation>Un período durante una sesión en el que la máquina no puede detectar el flujo.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9188"/>
        <source>BND</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9193"/>
        <source>Timed Breath</source>
        <translation>Inspiración temporizada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9194"/>
        <source>Machine Initiated Breath</source>
        <translation>Respiración iniciada por la máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9195"/>
        <source>TB</source>
        <translatorcomment>TB: ¿inspiración temporizada?</translatorcomment>
        <translation>IT</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="36"/>
        <source>Windows User</source>
        <translation>Usuario de Windows</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>Using </source>
        <translation>Usando </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>, found SleepyHead -
</source>
        <translation>, cargando SleepyHead -
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="201"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation>Debe ejecutar la herramienta de migración OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="537"/>
        <source>&lt;i&gt;Your old machine data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Los datos antiguos de su máquina deben ser regenerados, suponiendo que esta característica no haya sido deshabilitada en Preferencias durante una importación de datos previa.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="470"/>
        <source>Launching Windows Explorer failed</source>
        <translation>Error al iniciar el Explorador de Windows</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="471"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>No se halló explorer.exe en el &apos;path&apos; para lanzar el Explorador de Windows.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="536"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation>&lt;b&gt;OSCAR mantiene una copia de seguridad de la tarjeta de datos de sus dispositivos que utiliza para éste propósito..&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="540"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation>OSCAR no tiene todavía ninguna copia de seguridad automática de la tarjeta almacenada para este dispositivo.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="541"/>
        <source>This means you will need to import this machine data again afterwards from your own backups or data card.</source>
        <translation>Esto significa que tendrá que importar los datos de esta máquina nuevamente después, desde sus propios respaldos de sa tarjeta de datos.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="544"/>
        <source>Important:</source>
        <translation>Importante:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="545"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation>Sí le preocupa, haga clic en No para salir, y haga una copia de seguridad de su perfil manualmente, antes de volver a iniciar OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="546"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation>¿Estás listo para actualizar, así que puedes ejecutar la nueva versión de OSCAR?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="560"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation>Lo sentimos, la operación de purga falló, lo que significa que esta versión de OSCAR no puede iniciarse.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="579"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation>¿Le gustaría activar las copias de seguridad automáticas, para que la próxima vez que una nueva versión de OSCAR necesite hacerlo, pueda reconstruir a partir de ellas?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="586"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation>OSCAR iniciará el asistente de importación para que pueda reinstalar sus  %1 datos.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="596"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation>OSCAR saldrá ahora, y luego (intentará) iniciar el administrador de archivos de su computadora para que pueda hacer una copia de seguridad manual de su perfil:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="598"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation>Utilice su gestor de archivos para hacer una copia del directorio de su perfil y, a continuación, reinicie OSCAR y complete el proceso de actualización.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="549"/>
        <source>Machine Database Changes</source>
        <translation>Cambios en la base de datos de la máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="523"/>
        <source>OSCAR %1 needs to upgrade its database for %2 %3 %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="544"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;cannot&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="561"/>
        <source>The machine data folder needs to be removed manually.</source>
        <translation>El directorio de datos de la máquina debe ser removido manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="562"/>
        <source>This folder currently resides at the following location:</source>
        <translation>Este directorio reside actualmente en la siguiente ubicación:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="570"/>
        <source>Rebuilding from %1 Backup</source>
        <translation>Reconstruyendo desde el respaldo de %1</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="185"/>
        <source>Choose the SleepyHead data folder to migrate</source>
        <translation>Elija la carpeta de datos de SleepyHead para migrar</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="186"/>
        <source>or CANCEL to skip migration.</source>
        <translation>o CANCELE para saltar la migración.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="200"/>
        <source>The folder you chose does not contain valid SleepyHead data.</source>
        <translation>La carpeta que ha elegido no contiene datos válidos de SleepyHead.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="201"/>
        <source>You cannot use this folder:</source>
        <translation>No puede utilizar esta carpeta:</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="216"/>
        <source>Migrating </source>
        <translation>Migrar </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="216"/>
        <source> files</source>
        <translation> archivos</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="217"/>
        <source>from </source>
        <translation>a partir de </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="217"/>
        <source>to </source>
        <translatorcomment>otras definiciones: a,hasta,hacia </translatorcomment>
        <translation>para</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="324"/>
        <source>OSCAR crashed due to an incompatibility with your graphics hardware.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="325"/>
        <source>To resolve this, OSCAR has reverted to a slower but more compatible method of drawing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="485"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation>OSCAR creará una carpeta para sus datos.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="486"/>
        <source>If you have been using SleepyHead, OSCAR can copy your old data to this folder later.</source>
        <translation>Si ha estado utilizando SleepyHead, OSCAR puede copiar sus datos antiguos a esta carpeta más tarde.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="487"/>
        <source>We suggest you use this folder: </source>
        <translation>Le sugerimos que utilice esta folder: </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="488"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation>Haga clic en Aceptar para aceptarlo, o en No si desea utilizar una carpeta diferente.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="499"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation>La próxima vez que ejecute OSCAR, se le preguntará de nuevo.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="596"/>
        <source>Version &quot;%1&quot; is invalid, cannot continue!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="605"/>
        <source>The version of OSCAR you are running (%1) is OLDER than the one used to create this data (%2).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="494"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation>Elegir o crear una nueva carpeta para los datos OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="498"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation>Como no seleccionó una carpeta de datos, OSCAR saldrá.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="510"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation>La carpeta que ha elegido no está vacía ni contiene datos válidos de OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="531"/>
        <source>Data directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="539"/>
        <source>Migrate SleepyHead Data?</source>
        <translation>¿Migración de datos de SleepyHead?</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="540"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead data</source>
        <translation>En la siguiente pantalla OSCAR le pedirá que seleccione una carpeta con los datos de SleepyHead</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="541"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead data.</source>
        <translation>Haga clic en[OK] para ir a la siguiente pantalla o en[No] si no desea utilizar ningún dato de SleepyHead.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="609"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation>Es probable que hacer esto cause corrupción de datos, ¿está seguro de que quiere hacerlo?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="669"/>
        <source>Question</source>
        <translation>Pregunta</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="497"/>
        <source>Exiting</source>
        <translation>Saliendo</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="511"/>
        <source>Are you sure you want to use this folder?</source>
        <translation>¿Está seguro que quiere usar este directorio?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="277"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP machine</source>
        <translation>No olvide volver a colocar su tarjeta de datos en su máquina CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="277"/>
        <source>OSCAR Reminder</source>
        <translation>Recordatorio OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="460"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation>Sólo se puede trabajar con una instancia de un perfil OSCAR individual a la vez.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="461"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation>Si utiliza almacenamiento en nube, asegúrese que OSCAR esté cerrado y de que la sincronización se haya completado primero en el otro equipo antes de continuar.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="474"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation>Cargando perfil &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2248"/>
        <source>Sorry, your %1 %2 machine is not currently supported.</source>
        <translation>Lo sentimos, su máquina%1 %2 no está soportada actualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2255"/>
        <source>Recompressing Session Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2794"/>
        <source>Please select a location for your zip other than the data card itself!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2821"/>
        <location filename="../oscar/mainwindow.cpp" line="2888"/>
        <source>Unable to create zip!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1186"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation>¿Está seguro de que quiere reinicializar los ajustes y colores del canal a sus valores por defecto?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1239"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation>¿Está seguro de que desea restablecer todos los colores y ajustes de su canal de forma de onda a los valores predeterminados?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="40"/>
        <source>There are no graphs visible to print</source>
        <translation>No hay graficos visibles para imprimir</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="55"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation>¿Le gustaría mostrar las áreas con marcadores en este reporte?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="99"/>
        <source>Printing %1 Report</source>
        <translation>Imprimiendo reporte %1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="133"/>
        <source>%1 Report</source>
        <translation>Reporte %1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="191"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation>: %1 horas, %2 minutos, %3 segundos
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="244"/>
        <source>RDI	%1
</source>
        <translation>IPR	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="246"/>
        <source>AHI	%1
</source>
        <translation>IAH	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="279"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation>IA=%1 IH=%2 IAC=%3 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="285"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation>IRE=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="289"/>
        <source>UAI=%1 </source>
        <translation>IANC=%1 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="291"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="360"/>
        <source>Reporting from %1 to %2</source>
        <translation>Reportando desde %1 hasta %2</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="432"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation>Forma de onda del día completo</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="438"/>
        <source>Current Selection</source>
        <translation>Selección actual</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="448"/>
        <source>Entire Day</source>
        <translation>Día completo</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="564"/>
        <source>%1 %2 %3</source>
        <translation type="unfinished">%1 %2 %3</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="573"/>
        <source>Page %1 of %2</source>
        <translation>Página %1 de %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Jan</source>
        <translation>Ene</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Feb</source>
        <translation>Feb</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Mar</source>
        <translation>Mar</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Apr</source>
        <translation>Abr</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>May</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Jun</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jul</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Aug</source>
        <translation>Ago</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Sep</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Oct</source>
        <translation>Oct</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Nov</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Dec</source>
        <translation>Dic</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="369"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="367"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="370"/>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="384"/>
        <source>(% %1 in events)</source>
        <translation>(%-%1 en eventos)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="123"/>
        <source>Therapy Pressure</source>
        <translation>Presión Terapéutica</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="124"/>
        <source>Inspiratory Pressure</source>
        <translation>Presión Inspiratoria</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="125"/>
        <source>Lower Inspiratory Pressure</source>
        <translation>Presión inspiratoria inferior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="126"/>
        <source>Higher Inspiratory Pressure</source>
        <translation>Presión inspiratoria superior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="127"/>
        <source>Expiratory Pressure</source>
        <translation>Presión Espiratoria</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="128"/>
        <source>Lower Expiratory Pressure</source>
        <translation>Presión espiratoria inferior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="129"/>
        <source>Higher Expiratory Pressure</source>
        <translation>Presión espiratoria superior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="130"/>
        <source>Pressure Support</source>
        <translation>Soporte de presión</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>PS Min</source>
        <translation>SP Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Pressure Support Minimum</source>
        <translation>Soporte de presión mínimo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>PS Max</source>
        <translation>SP Máx</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Pressure Support Maximum</source>
        <translation>Soporte de presión máximo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Min Pressure</source>
        <translation>Presión Mín</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Minimum Therapy Pressure</source>
        <translation>Presión de terapia mínima</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Max Pressure</source>
        <translation>Presión Máxima</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Maximum Therapy Pressure</source>
        <translation>Máxima terapia de presión</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Time</source>
        <translation>Tiempo de rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Delay Period</source>
        <translation>Periodo de retraso de rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Ramp Pressure</source>
        <translation>Presión de rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Starting Ramp Pressure</source>
        <translation>Presión inicial de rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Ramp Event</source>
        <translation>Evento de Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="180"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Ramp</source>
        <translation>Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>Vibratory Snore (VS2) </source>
        <translation>Ronquido vibratorio (VS2) </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>Mask On Time</source>
        <translation>Mascarilla a tiempo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>Time started according to str.edf</source>
        <translation>Hora de inicio según str.edf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>Summary Only</source>
        <translation>Sólo resumen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="657"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>An apnea where the airway is open</source>
        <translation>Una apnea mientras la vía aérea se encuentra abierta</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>An apnea caused by airway obstruction</source>
        <translation>Una apnea provocada por obstrucción de la vía aérea</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Hypopnea</source>
        <translation>Hipoapnea</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>A partially obstructed airway</source>
        <translation>Una vía aérea parcialmente obstruida</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>Unclassified Apnea</source>
        <translation>Apnea no clasificada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>UA</source>
        <translation>ANC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>Vibratory Snore</source>
        <translation>Ronquido vibratorio</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>A vibratory snore</source>
        <translation>Un ronquido vibratorio</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>A vibratory snore as detcted by a System One machine</source>
        <translation>Un ronquido vibratorio detectado por la máquina System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8947"/>
        <source>Pressure Pulse</source>
        <translation>Pulso de presión</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8948"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation>Un Pulso de Presión &apos;lanzado&apos; para detectar una vía aérea cerrada.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="781"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>Large Leak</source>
        <translation>Fuga Grande</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>A large mask leak affecting machine performance.</source>
        <translation>Una gran fuga en la mascarilla que afecta al rendimiento de la máquina.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="782"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>LL</source>
        <translation>LL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>Non Responding Event</source>
        <translation>Evento de no respuesta</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation>Un tipo de evento respiratorio que no responde a los incrementos de presión.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Expiratory Puff</source>
        <translation>Soplo espiratorio</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation>Evento de intellipap en el cual se espira por la boca.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation>Característica de SensAwake con la cual se reduce la presión cuando se detecta el despertar.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>User Flag #1</source>
        <translation>Indicador de usuario #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>User Flag #2</source>
        <translation>Indicador de usuario #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>User Flag #3</source>
        <translation>Indicador de usuario #3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Heart rate in beats per minute</source>
        <translation>Frecuencia cardiaca en latidos por minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation>Porcentaje de saturación de oxígeno en sangre</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>SpO2 %</source>
        <translation>% SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>Plethysomogram</source>
        <translation>Pletismograma</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation>Un foto-pletismograma óptico mostrando el ritmo cardiaco</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>Pulse Change</source>
        <translation>Cambio de Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation>Un repentino (definible por el usuario) cambio en el ritmo cardiaco</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>SpO2 Drop</source>
        <translation>Caída de SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation>Una repentina (definible por el usuario) caída en la saturación de oxígeno en sangre</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>SD</source>
        <translatorcomment>???????????????????</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="212"/>
        <source>Breathing flow rate waveform</source>
        <translation>Forma de onda de flujo respiratorio</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="660"/>
        <source>L/min</source>
        <translation>L/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="215"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>Mask Pressure</source>
        <translation>Presión de mascarilla</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>Amount of air displaced per breath</source>
        <translation>Volumen de aire desplazado por inspiración</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Graph displaying snore volume</source>
        <translation>Gráfico mastrando el volumen de los ronquidos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Minute Ventilation</source>
        <translation>Ventilaciónes Minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Amount of air displaced per minute</source>
        <translation>Cantidad de aire desplazado por minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Respiratory Rate</source>
        <translation>Frecuencia respiratoria</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Rate of breaths per minute</source>
        <translation>Tasa de inspiraciones por minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Patient Triggered Breaths</source>
        <translation>Inpiraciones iniciadas por el paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation>Porcentaje de inpiraciones iniciadas por el paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Pat. Trig. Breaths</source>
        <translation>Insp. inic. x paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Leak Rate</source>
        <translation>Tasa de fuga</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Rate of detected mask leakage</source>
        <translation>Tasa de fugas de mascarilla detectadas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="240"/>
        <source>I:E Ratio</source>
        <translation>Tasa I:E</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="240"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation>Relación entre el tiempo de inspiración y espiración</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="665"/>
        <source>ratio</source>
        <translation>tasa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Pressure Min</source>
        <translation>Presión Mínima</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Pressure Max</source>
        <translation>Presión Máxima</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>Cheyne Stokes Respiration</source>
        <translation>Respiración Cheyne Stokes</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>Un período anormal de respiración Cheyne Stokes</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="760"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>CSR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Periodic Breathing</source>
        <translation>Respiración Periódica</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation>Un período anormal de Respiración Periódica</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Clear Airway</source>
        <translatorcomment>Vía respiratoria despejada</translatorcomment>
        <translation>Vía Resp. Libre</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>Obstructive</source>
        <translation>Obstructiva</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>Respiratory Effort Related Arousal: An restriction in breathing that causes an either an awakening or sleep disturbance.</source>
        <translation>Despertar relacionado con el esfuerzo respiratorio: Una restricción en la respiración que causa ya sea un despertar o una alteración del sueño.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>Leak Flag</source>
        <translation>Indicador de fuga</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>LF</source>
        <translation>BF</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation>Un suceso definible por el usuario detectado por el procesador de forma de onda de flujo de OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>Perfusion Index</source>
        <translation>Índice de perfusión</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation>Una evaluación relativa de la fuerza del pulso en el lugar de monitorización</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>Perf. Index %</source>
        <translation>Índice perf. %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Expiratory Time</source>
        <translation>Tiempo espiración</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Time taken to breathe out</source>
        <translation>El tiempo que tarda espirar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Inspiratory Time</source>
        <translation>Tiempo inspiratorio</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Time taken to breathe in</source>
        <translation>El tiempo que toma inspirar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Respiratory Event</source>
        <translation>Evento respiratorio</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Graph showing severity of flow limitations</source>
        <translation>Gráfico que muestra la severidad de las limitaciones de flujo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Flow Limit.</source>
        <translation>Límit. de Flujo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Target Minute Ventilation</source>
        <translation>Ventilación minuto objetivo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation>Una apnea que no se pudo determinar como Central u Obstructiva.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="111"/>
        <source>Couldn&apos;t parse Channels.xml, OSCAR cannot continue and is exiting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Pressure Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Pressure Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>IPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>IPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>EPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>EPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation>Una restricción en la respiración desde lo normal, causando un aplanamiento de la forma de onda del flujo.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>Mask Pressure (High frequency)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>A ResMed data item: Trigger Cycle Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Maximum Leak</source>
        <translation>Fuga máxima</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>The maximum rate of mask leakage</source>
        <translation>La tasa máxima de fuga de la mascarilla</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Max Leaks</source>
        <translation>Fugas máximas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Apnea Hypopnea Index</source>
        <translation>Índice Apnea-Hipoapnea</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation>Gráfico que muestra la IAH corriente de la última hora</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Total Leak Rate</source>
        <translation>Tasa de fuga total</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation>Fuga de mascarilla detectada, incluyendo fugas naturales de la mascarilla</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median Leak Rate</source>
        <translation>Mediana de tasa de fuga</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median rate of detected mask leakage</source>
        <translation>Mediana de la tasa de fugas detectadas de mascarilla</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median Leaks</source>
        <translation>Fugas Medianas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Respiratory Disturbance Index</source>
        <translation>Índice de perturbación respiratoria</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation>Gráfico que muestra el IPR corriente de la última hora</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Movement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Movement detector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>CPAP Session contains summary data only</source>
        <translation>La sesión de CPAP sólo contiene un resumen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8954"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="290"/>
        <source>PAP Mode</source>
        <translation>Modo PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="290"/>
        <source>PAP Device Mode</source>
        <translation>Modo del dispositivo PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="294"/>
        <source>APAP (Variable)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="298"/>
        <source>ASV (Fixed EPAP)</source>
        <translation>ASV (EPAP fijo)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="299"/>
        <source>ASV (Variable EPAP)</source>
        <translation>ASV (EPAP variable)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Height</source>
        <translation>Altura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Physical Height</source>
        <translation>Altura Física</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Bookmark Notes</source>
        <translation>Notas Marcador</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>Body Mass Index</source>
        <translation>Índice de Masa Corporal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation>Cómo te sientes (0 = like basura, 10 = imparable)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>Bookmark Start</source>
        <translation>Marcador Inicio</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="313"/>
        <source>Bookmark End</source>
        <translation>Marcador final</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="314"/>
        <source>Last Updated</source>
        <translation>Últiima Actualización</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>Journal Notes</source>
        <translation>Notas de diario</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>Journal</source>
        <translation>Diario</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation>1=Despierto 2=REM 3=Sueño Ligero 4=Sueño Profundo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Brain Wave</source>
        <translation>Onda Cerebral</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>BrainWave</source>
        <translation>OndaCerebral</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Awakenings</source>
        <translation>Despertares</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Number of Awakenings</source>
        <translation>Número de Despertares</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Morning Feel</source>
        <translation>Sensación Matutina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>How you felt in the morning</source>
        <translation>Cómo te sentiste por la mañana</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Time Awake</source>
        <translation>Tiempo Despierto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Time spent awake</source>
        <translation>Tiempo de permanencia despierto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time In REM Sleep</source>
        <translation>Tiempo en el sueño REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time spent in REM Sleep</source>
        <translation>Tiempo de permanencia en el sueño REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time in REM Sleep</source>
        <translation>Tiempo en el sueño REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Time In Light Sleep</source>
        <translation>Tiempo en sueño Ligero</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Time spent in light sleep</source>
        <translation>Tiempo de permanencia en un sueño ligero</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Time in Light Sleep</source>
        <translation>Tiempo en sueño Ligero</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="330"/>
        <source>Time In Deep Sleep</source>
        <translation>Tiempo en sueño profundo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="330"/>
        <source>Time spent in deep sleep</source>
        <translation>Tiempo de permanencia en sueño profundo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="330"/>
        <source>Time in Deep Sleep</source>
        <translation>Tiempo en sueño profundo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="331"/>
        <source>Time to Sleep</source>
        <translation>Tiempo para Dormir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="331"/>
        <source>Time taken to get to sleep</source>
        <translation>Tiempo necesario para dormir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="332"/>
        <source>Zeo ZQ</source>
        <translation>Zeo ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="332"/>
        <source>Zeo sleep quality measurement</source>
        <translation>Medición de la calidad del sueño Zeo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="332"/>
        <source>ZEO ZQ</source>
        <translation>ZEO ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Debugging channel #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Top secret internal stuff you&apos;re not supposed to see ;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Test #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Debugging channel #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Test #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="748"/>
        <source>Zero</source>
        <translation>Cero</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="751"/>
        <source>Upper Threshold</source>
        <translation>Umbral Superior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="754"/>
        <source>Lower Threshold</source>
        <translation>Umbral inferior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="806"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>Orientation</source>
        <translation>Orientación</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>Sleep position in degrees</source>
        <translation>Posición de dormir en grados</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="805"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>Inclination</source>
        <translation>Inclinación</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>Upright angle in degrees</source>
        <translatorcomment>??????????</translatorcomment>
        <translation>Ángulo vertical en grados</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="997"/>
        <source>Days: %1</source>
        <translation>Días: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1001"/>
        <source>Low Usage Days: %1</source>
        <translation>Días de uso bajo: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1002"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation>(%1% cumplido, definido como &gt; %2 horas)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1118"/>
        <source>(Sess: %1)</source>
        <translation>(Ses: %1)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1126"/>
        <source>Bedtime: %1</source>
        <translation>Hora de dormir: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1128"/>
        <source>Waketime: %1</source>
        <translation>Hora de despertar: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1168"/>
        <source>90%</source>
        <translation>90%</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1240"/>
        <source>(Summary Only)</source>
        <translation>(Sólo resumen)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="459"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation>Ya hay un archivo de cierre (lockfile) presente para el perfil &apos;%1&apos;, reclamado en &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="175"/>
        <source>Peak</source>
        <translation>Pico</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="179"/>
        <source>%1% %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="295"/>
        <source>Fixed Bi-Level</source>
        <translation>Bi-nivel fijo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="296"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation>Bi-nivel automático (PS fijo)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="297"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation>Bi-nivel automático (PS variable)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1496"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1518"/>
        <source>n/a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1535"/>
        <source>Fixed %1 (%2)</source>
        <translation>%1 Fijo (%2)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1537"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation>Mín %1 Máx %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1539"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1541"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translatorcomment>ps??</translatorcomment>
        <translation>PS %1 sobre %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1543"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1552"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translatorcomment>ps??</translatorcomment>
        <translation>EPAP mín %1 IPAP máx %2 PS %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1548"/>
        <source>EPAP %1 PS %2-%3 (%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1559"/>
        <source>EPAP %1 IPAP %2-%3 (%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="166"/>
        <location filename="../oscar/SleepLib/day.cpp" line="168"/>
        <location filename="../oscar/SleepLib/day.cpp" line="170"/>
        <location filename="../oscar/SleepLib/day.cpp" line="175"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="340"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation>Datos de oximetría más recientes: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="341"/>
        <source>(last night)</source>
        <translation>(anoche)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="342"/>
        <source>(1 day ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="343"/>
        <source>(%2 days ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="348"/>
        <source>No oximetry data has been imported yet.</source>
        <translation>Aún no se han importado datos de oximetría.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>Contec</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <source>CMS50</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>Fisher &amp; Paykel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>ICON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>DeVilbiss</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>Intellipap</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="85"/>
        <source>SmartFlex Settings</source>
        <translation>Configuración de SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>ChoiceMMed</source>
        <translation>ElijaMMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>MD300</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>Respironics</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>M-Series</source>
        <translation>Serie-M</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="419"/>
        <source>Philips Respironics</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="419"/>
        <source>System One</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="92"/>
        <source>ResMed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="93"/>
        <source>S9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="126"/>
        <source>EPR: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose Software</source>
        <translation>Software Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="39"/>
        <source>Zeo</source>
        <translation>Zeo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="39"/>
        <source>Personal Sleep Coach</source>
        <translation>Consejero de sueño personal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="405"/>
        <source>Locating STR.edf File(s)...</source>
        <translation>Localización archivos STR.edf....</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="557"/>
        <source>Cataloguing EDF Files...</source>
        <translation>Catalogación de archivos EDF....</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="576"/>
        <source>Queueing Import Tasks...</source>
        <translation>Cola de tareas de importación....</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="603"/>
        <source>Finishing Up...</source>
        <translation>Terminando...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="85"/>
        <source>CPAP Mode</source>
        <translation>Modo de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="91"/>
        <source>VPAP-T</source>
        <translation>VPAP-T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="92"/>
        <source>VPAP-S</source>
        <translation>VPAP-S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="93"/>
        <source>VPAP-S/T</source>
        <translation>VPAP-S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="95"/>
        <source>VPAPauto</source>
        <translation>Auto VPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="97"/>
        <source>ASVAuto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="100"/>
        <source>Auto for Her</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="103"/>
        <source>EPR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="103"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation>Alivio de presión de exhalación de ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="108"/>
        <source>Patient???</source>
        <translation>¿paciente?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="111"/>
        <source>EPR Level</source>
        <translation>Nivel EPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="111"/>
        <source>Exhale Pressure Relief Level</source>
        <translation>Alivio de presión de exhalación</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="94"/>
        <source>?5?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="98"/>
        <source>?9?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="99"/>
        <source>?10?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>SmartStart</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>Machine auto starts by breathing</source>
        <translation>La máquina arranca automáticamente al respirar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>Smart Start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>Humid. Status</source>
        <translation>Estado Humid.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>Humidifier Enabled Status</source>
        <translation>Estado Humidificador Activado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="130"/>
        <source>Humid. Level</source>
        <translation>Nivel Humid.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="130"/>
        <source>Humidity Level</source>
        <translation>Nivel de Humedad</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="143"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="143"/>
        <source>ClimateLine Temperature</source>
        <translation>Temperatura ClimateLine</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Temp. Enable</source>
        <translation>Temp. Habilitada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>ClimateLine Temperature Enable</source>
        <translation>Habilitación de temperatura ClimateLine</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Temperature Enable</source>
        <translation>Temeperatura Habilitada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="155"/>
        <source>AB Filter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="155"/>
        <source>Antibacterial Filter</source>
        <translation>Filtro Antibacterias</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="161"/>
        <source>Pt. Access</source>
        <translation>Pt. Acceso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="161"/>
        <source>Patient Access</source>
        <translation>Acceso para pacientes</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="167"/>
        <source>Climate Control</source>
        <translation>Control de Climatización</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="170"/>
        <source>Manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="989"/>
        <source>Parsing STR.edf records...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="9142"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="169"/>
        <source>Auto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="173"/>
        <source>Mask</source>
        <translation>Máscarilla</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="173"/>
        <source>ResMed Mask Setting</source>
        <translation>Ajuste Mascarilla ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="175"/>
        <source>Pillows</source>
        <translation>Almohadas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="176"/>
        <source>Full Face</source>
        <translation>Cara completa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="177"/>
        <source>Nasal</source>
        <translation>Nasal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="180"/>
        <source>Ramp Enable</source>
        <translation>Rampa Habilitada</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="196"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation>Base de datos obsoleta
Por favor recompile datos del CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="373"/>
        <source> (%2 min, %3 sec)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="375"/>
        <source> (%3 sec)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="350"/>
        <source>Snapshot %1</source>
        <translation>Instantánea %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="388"/>
        <source>Pop out Graph</source>
        <translation>Gráfico Desplegable</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1420"/>
        <source>I&apos;m very sorry your machine doesn&apos;t record useful data to graph in Daily View :(</source>
        <translation>Lamento mucho que su máquina no registre datos útiles para graficar en Daily View :(</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1423"/>
        <source>There is no data to graph</source>
        <translation>No hay datos para graficar</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1608"/>
        <source>d MMM yyyy [ %1 - %2 ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2166"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2209"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2280"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2297"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2261"/>
        <source>Hide All Events</source>
        <translation>Ocultar todos los eventos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2262"/>
        <source>Show All Events</source>
        <translation>Mostrar todos los eventos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2603"/>
        <source>Unpin %1 Graph</source>
        <translatorcomment>pin?</translatorcomment>
        <translation>Soltar gráfico %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2605"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2680"/>
        <source>Popout %1 Graph</source>
        <translation>Gráfico %1 Desplegable</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2682"/>
        <source>Pin %1 Graph</source>
        <translation>Fijar gráfico %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1042"/>
        <source>Plots Disabled</source>
        <translation>Gráficos deshabilitados</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1116"/>
        <source>Duration %1:%2:%3</source>
        <translation>Duración %1:%2:%3</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1117"/>
        <source>AHI %1</source>
        <translation>IAH %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="108"/>
        <source>%1: %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="151"/>
        <source>Relief: %1</source>
        <translation>Alivio: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="157"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation>Horas: %1h, %2m, %3s</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="260"/>
        <source>Machine Information</source>
        <translation>Información de la máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="30"/>
        <source>Journal Data</source>
        <translation>Datos de diario</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="48"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation>OSCAR encontró una vieja carpeta de Diario, pero parece que ha sido renombrada:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="50"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation>Esta carpeta no será tocada por OSCAR y se creará una nueva.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="51"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation>Por favor, tenga cuidado al jugar en las carpetas de perfil de OSCAR :-P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="58"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation>Por alguna razón, OSCAR no pudo encontrar un registro de objeto de diario en su perfil, pero sí encontró varias carpetas de datos de diario.

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="59"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation>OSCAR escogió sólo el primero de ellos, y lo utilizará en el futuro.

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="61"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation>Si
Sí faltan los datos antiguos, copie manualmente el contenido de todas las demás carpetas del Diario_XXXXXXXXX a éste.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F3.7</source>
        <translation>CMS50F3.7</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F</source>
        <translation>CMS50F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1543"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1545"/>
        <source>SmartFlex Mode</source>
        <translation>Modo SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1544"/>
        <source>Intellipap pressure relief mode.</source>
        <translation>Modo de alivio de presión Intellipap.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1550"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="106"/>
        <source>Ramp Only</source>
        <translation>Sólo rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1551"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="107"/>
        <source>Full Time</source>
        <translation>Tiempo completo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1554"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1556"/>
        <source>SmartFlex Level</source>
        <translation>Nivel SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1555"/>
        <source>Intellipap pressure relief level.</source>
        <translation>Nivel de alivio de presión Intellipap.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>Weinmann</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>SOMNOsoft2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50D+</source>
        <translation>CMS50D+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50E/F</source>
        <translation>CMS50E/F</translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="223"/>
        <source>%1
Line %2, column %3</source>
        <translation>%1
Linea %2, columna %3</translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="236"/>
        <source>Could not parse Updates.xml file.</source>
        <translation>No se pudo analizar el archivo Updates.xml.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="682"/>
        <source>Loading %1 data for %2...</source>
        <translation>Cargando %1 datos para %2...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="692"/>
        <source>Scanning Files</source>
        <translation>Escaneo de archivos....</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="726"/>
        <source>Migrating Summary File Location</source>
        <translation>Ubicación del archivo de resumen de la migración</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="889"/>
        <source>Loading Summaries.xml.gz</source>
        <translation>Cargando Resúmenes.xml.gz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1020"/>
        <source>Loading Summary Data</source>
        <translation>Cargar datos totalizados</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="14"/>
        <source>Please Wait...</source>
        <translation>Por favor Espere...</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="312"/>
        <source>Peak %1</source>
        <translation>Pico %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/dreem_loader.h" line="36"/>
        <source>Dreem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="85"/>
        <source>Your Viatom device generated data that OSCAR has never seen before.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="86"/>
        <source>The imported data may not be entirely accurate, so the developers would like a copy of your Viatom files to make sure OSCAR is handling the data correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="39"/>
        <source>Viatom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="39"/>
        <source>Viatom Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="514"/>
        <source>Loading summaries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="166"/>
        <source>Updating Statistics cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="708"/>
        <source>Usage Statistics</source>
        <translation type="unfinished">Estadísticas de uso</translation>
    </message>
</context>
<context>
    <name>Report</name>
    <message>
        <location filename="../oscar/reports.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../oscar/reports.ui" line="27"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="246"/>
        <source>%1h %2m</source>
        <translation>%1h %2m</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="289"/>
        <source>No Sessions Present</source>
        <translation>No hay sesiones presentes</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="1199"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1150"/>
        <source>Most Recent</source>
        <translation>Más reciente</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="541"/>
        <source>Therapy Efficacy</source>
        <translation>Eficacia de la terapia</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1152"/>
        <source>Last 30 Days</source>
        <translation>Últimos 30 días</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1154"/>
        <source>Last Year</source>
        <translation>Último Año</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="602"/>
        <location filename="../oscar/statistics.cpp" line="603"/>
        <source>Average %1</source>
        <translation>Promedio %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="534"/>
        <source>CPAP Statistics</source>
        <translation>Estadísticas de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="537"/>
        <location filename="../oscar/statistics.cpp" line="1358"/>
        <source>CPAP Usage</source>
        <translation>Uso de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="538"/>
        <source>Average Hours per Night</source>
        <translation>Promedio de horas por noche</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="539"/>
        <source>Compliance (%1 hrs/day)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="551"/>
        <source>Leak Statistics</source>
        <translation>Estadísticas de fuga</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="556"/>
        <source>Pressure Statistics</source>
        <translation>Estadísticas de presión</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="581"/>
        <source>Oximeter Statistics</source>
        <translation>Estadísticas del oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="585"/>
        <source>Blood Oxygen Saturation</source>
        <translation>Saturación de oxígeno en sangre</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="590"/>
        <source>Pulse Rate</source>
        <translation>Frecuencia de Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="601"/>
        <source>%1 Median</source>
        <translation>%1 Mediana</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="605"/>
        <source>Min %1</source>
        <translation>Mín %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="606"/>
        <source>Max %1</source>
        <translation>Max %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="607"/>
        <source>%1 Index</source>
        <translation>Índice %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="608"/>
        <source>% of time in %1</source>
        <translation>% del tiempo en %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="609"/>
        <source>% of time above %1 threshold</source>
        <translation>% del tiempo por encima del umbral de %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="610"/>
        <source>% of time below %1 threshold</source>
        <translation>% del tiempo por debajo del umbral de %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="633"/>
        <source>Name: %1, %2</source>
        <translation>Nombre: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="635"/>
        <source>DOB: %1</source>
        <translation>Fecha de Nacimiento: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="638"/>
        <source>Phone: %1</source>
        <translation>Teléfono: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="641"/>
        <source>Email: %1</source>
        <translation>Correo Electrónico: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="644"/>
        <source>Address:</source>
        <translation>Domicilio:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1051"/>
        <source>Oscar has no data to report :(</source>
        <translation>Oscar no tiene datos que reportar :(</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1359"/>
        <source>Days Used: %1</source>
        <translation>Días de uso. %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1360"/>
        <source>Low Use Days: %1</source>
        <translation>Días de uso reducido: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1361"/>
        <source>Compliance: %1%</source>
        <translation>Cumplimiento: %1%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1385"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation>Días de 5 o más IAH %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1392"/>
        <source>Best AHI</source>
        <translation>Mejor IAH</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1396"/>
        <location filename="../oscar/statistics.cpp" line="1408"/>
        <source>Date: %1 AHI: %2</source>
        <translation>Fecha: %1 IAH: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1402"/>
        <source>Worst AHI</source>
        <translation>Peor IAH</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1439"/>
        <source>Best Flow Limitation</source>
        <translation>Mejor Limitación de Flujo</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1443"/>
        <location filename="../oscar/statistics.cpp" line="1456"/>
        <source>Date: %1 FL: %2</source>
        <translation>Fecha:%1 FL: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1449"/>
        <source>Worst Flow Limtation</source>
        <translation>Peor lLmitación de Flujo</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1461"/>
        <source>No Flow Limitation on record</source>
        <translation>No hay registro de Limitación de Flujo</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1482"/>
        <source>Worst Large Leaks</source>
        <translation>Las peores Fugas Grandes</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1490"/>
        <source>Date: %1 Leak: %2%</source>
        <translation>Fecha: %1 Fuga: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1496"/>
        <source>No Large Leaks on record</source>
        <translation>No hay grandes fugas en los registros</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1519"/>
        <source>Worst CSR</source>
        <translation>Peor CSR</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1527"/>
        <source>Date: %1 CSR: %2%</source>
        <translation>Fecha: %1 CSR: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1532"/>
        <source>No CSR on record</source>
        <translation>No hay registro de CSR</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1549"/>
        <source>Worst PB</source>
        <translatorcomment>Peor RP Respiración Periodica</translatorcomment>
        <translation>Peor PB</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1557"/>
        <source>Date: %1 PB: %2%</source>
        <translatorcomment>RP, Respiración Periodica</translatorcomment>
        <translation>Fecha: %1 PB: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1562"/>
        <source>No PB on record</source>
        <translatorcomment>RP Respiracion Periodica</translatorcomment>
        <translation>No hay registro de PB</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1570"/>
        <source>Want more information?</source>
        <translation>¿Desea más información?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1571"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation>OSCAR necesita todos los datos de resumen cargados para calcular los mejores/peores datos para días individuales.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1572"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation>Por favor, active la casilla de verificación Resúmenes previos a la carga en las preferencias para asegurarse de que estos datos están disponibles.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1592"/>
        <source>Best RX Setting</source>
        <translation>Mejor Prescripción</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1595"/>
        <location filename="../oscar/statistics.cpp" line="1607"/>
        <source>Date: %1 - %2</source>
        <translation>Fecha: %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1598"/>
        <location filename="../oscar/statistics.cpp" line="1610"/>
        <source>AHI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1599"/>
        <location filename="../oscar/statistics.cpp" line="1611"/>
        <source>Total Hours: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1604"/>
        <source>Worst RX Setting</source>
        <translation>Peor Prescripción</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1151"/>
        <source>Last Week</source>
        <translation>Última Semana</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="727"/>
        <source>This report was prepared on %1 by OSCAR %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="963"/>
        <source>Changes to Machine Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1049"/>
        <source>No data found?!?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1153"/>
        <source>Last 6 Months</source>
        <translation>Últimos 6 meses</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1158"/>
        <source>Last Session</source>
        <translation>Última sesión</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1213"/>
        <source>No %1 data available.</source>
        <translation>No hay datos %1 disponibles.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1216"/>
        <source>%1 day of %2 Data on %3</source>
        <translation>%1 día(s) de datos %2 en %3</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1222"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation>%1 día(s) de datos %2 entre %3 y %4</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="730"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation>OSCAR es un software gratuito de código abierto para informes de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="971"/>
        <source>Days</source>
        <translation>Días</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="975"/>
        <source>Pressure Relief</source>
        <translation>Alivio de Presión</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="977"/>
        <source>Pressure Settings</source>
        <translation>Ajustes de presión</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="904"/>
        <source>Machine Information</source>
        <translation>Información de la máquina</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="910"/>
        <source>First Use</source>
        <translation>Primer Uso</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="911"/>
        <source>Last Use</source>
        <translation>Último Uso</translation>
    </message>
</context>
<context>
    <name>UpdaterWindow</name>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="60"/>
        <source>A new version of $APP is available</source>
        <translation>Una nueva versión de $APP está disponible</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="106"/>
        <source>Version Information</source>
        <translation>Información de la versión</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="117"/>
        <source>Release Notes</source>
        <translation>Notas de la Versión</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="14"/>
        <source>OSCAR Updater</source>
        <translation>Actualización OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="142"/>
        <source>Build Notes</source>
        <translation>Notas de Compilación</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="195"/>
        <source>Maybe &amp;Later</source>
        <translation>Quizás &amp;Después</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="215"/>
        <source>&amp;Upgrade Now</source>
        <translation>&amp;Actualizar Ahora</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="250"/>
        <source>Please wait while updates are downloaded and installed...</source>
        <translation>Por favor espere mientras se descargan e instalan las actualizaciones...</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="264"/>
        <source>Updates</source>
        <translation>Actualizaciones</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="304"/>
        <source>Component</source>
        <translation>Componente</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="309"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="314"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="319"/>
        <source>Progress</source>
        <translation>Progreso</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="328"/>
        <source>Log</source>
        <translation>Bitácora</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="374"/>
        <source>Downloading &amp; Installing Updates</source>
        <translation>Descargando e instalando actualizaciones</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="394"/>
        <source>&amp;Finished</source>
        <translation>&amp;Finalizado</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="146"/>
        <source>Requesting </source>
        <translation>Solicitando </translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="363"/>
        <source>No updates were found for your platform.</source>
        <translation>No se encontraron actualizaciones para su plataforma.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="171"/>
        <source>%1 bytes received</source>
        <translation>%1 bytes recibidos</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="93"/>
        <source>Updates are not yet implemented</source>
        <translation>Las actualizaciones aún no se han implementado</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="127"/>
        <source>Checking for OSCAR Updates</source>
        <translation>Comprobación actualizaciones de OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="363"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="369"/>
        <source>OSCAR Updates</source>
        <translation>Actualización OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <source>OSCAR Updates are currently unavailable for this platform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="318"/>
        <source>Version %1 of OSCAR is available, opening link to download site.</source>
        <translation>Versión %1 de OSCAR está disponible, abriendo el enlace a la página de descargas.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="321"/>
        <source>You are already running the latest version.</source>
        <translation>Ya está ejecutando la última versión.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="370"/>
        <source>New OSCAR Updates are avilable:</source>
        <translation>Nuevas actualizaciones de OSCAR están disponibles:</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="371"/>
        <source>Would you like to download and install them now?</source>
        <translation>¿Desea descargarlos e instalarlos ahora?</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="128"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translatorcomment>Traducido al Español por Perchas</translatorcomment>
        <translation>Bienvenido al Reporter de Análisis CPAP de Código Abierto
Perchas Tradujo al Español</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="143"/>
        <source>What would you like to do?</source>
        <translation>¿Qué puedo hacer por usted?</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="186"/>
        <source>CPAP Importer</source>
        <translation>Importar CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="224"/>
        <source>Oximetry Wizard</source>
        <translation>Asist. Oximetría</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="262"/>
        <source>Daily View</source>
        <translation>Vista Diaria</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="300"/>
        <source>Overview</source>
        <translation>Vista General</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="338"/>
        <source>Statistics</source>
        <translation>Estadísticas</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="581"/>
        <source>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards need to be locked &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;before inserting into your computer.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Some operating systems write index files to the card without asking, which can render your card unreadable by your cpap machine.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation>Sería una buena idea revisar Archivo-&gt;Preferencias primero,</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>as there are some options that affect import.</source>
        <translation>ya que hay algunas opciones que afectan a la importación.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="162"/>
        <source>Note that some preferences are forced when a ResMed machine is detected</source>
        <translation>Tenga en cuenta que algunas preferencias son obligatorias cuando se detecta un equipo de ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>First import can take a few minutes.</source>
        <translation>La primera importación puede tardar unos minutos.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="176"/>
        <source>The last time you used your %1...</source>
        <translation>La última vez que usó su %1...</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="180"/>
        <source>last night</source>
        <translation>anoche</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="182"/>
        <source>%2 days ago</source>
        <translation>hace %2 días</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="184"/>
        <source>was %1 (on %2)</source>
        <translation>fue %1 (el %2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="192"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1 horas, %2 minutos, %3 segundos</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="195"/>
        <source>Your machine was on for %1.</source>
        <translation>Su máquina estuvo funcionando durante %1.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="196"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color = red&gt;Sólo tuvo la mascarilla puesta por %1.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="208"/>
        <source>under</source>
        <translation>debajo</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="209"/>
        <source>over</source>
        <translation>encima</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="210"/>
        <source>reasonably close to</source>
        <translation>razonablemente cerca de</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="211"/>
        <source>equal to</source>
        <translation>igual a</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="225"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>Tuvo un IAH de %1,que está por %2 de su promedio de %3 dias de %4.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="256"/>
        <source>Your CPAP machine used a constant %1 %2 of air</source>
        <translation>Su máquina CPAP usó una constante %1 %2 de aire</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="259"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>Su presión estuvo debajo de %1 %2 por el %3% del tiempo.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="263"/>
        <source>Your machine used a constant %1-%2 %3 of air.</source>
        <translation>Su máquina usó una constante %1-%2 %3 de aire.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="271"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>Su presión de EPAP se fijó en %1 %2.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="272"/>
        <location filename="../oscar/welcome.cpp" line="278"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Su presión de IPAP estuvo debajo de %1 %2 por %3% del tiempo.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="277"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Su presión de EPAP estuvo debajo de %1 %2 por %3% del tiempo.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="267"/>
        <source>Your machine was under %1-%2 %3 for %4% of the time.</source>
        <translation>Su máquina estuvo debajo de %1-%2 %3 por %4% del tiempo.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="181"/>
        <source>1 day ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="298"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>Las fugas promedio fueron %1 %2, que está por %3 a su promedio de %4  días de %5.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="304"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>No se han importado datos de CPAP todavía.</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="793"/>
        <source>%1 days</source>
        <translation>%1 días</translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="393"/>
        <source>100% zoom level</source>
        <translation>100% nivel de zoom</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="395"/>
        <source>Restore X-axis zoom to 100% to view entire selected period.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="397"/>
        <source>Restore X-axis zoom to 100% to view entire day&apos;s data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="399"/>
        <source>Reset Graph Layout</source>
        <translation>Reinicio de disposición del gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="400"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>Restablece todos los gráficos a una altura uniforme y a un orden predeterminado.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="403"/>
        <source>Y-Axis</source>
        <translation>Eje-Y</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="404"/>
        <source>Plots</source>
        <translation>Gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="409"/>
        <source>CPAP Overlays</source>
        <translation>Superposición CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="412"/>
        <source>Oximeter Overlays</source>
        <translation>Superposición Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="415"/>
        <source>Dotted Lines</source>
        <translation>Líneas de puntos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1801"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1854"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation>Doble clic en el título para pin / sin pin
Haga clic y arrastre para reordenar las gráficas</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2099"/>
        <source>Remove Clone</source>
        <translation>Eliminar Clon</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2103"/>
        <source>Clone %1 Graph</source>
        <translation>Clone %1 Gráfico</translation>
    </message>
</context>
</TS>
